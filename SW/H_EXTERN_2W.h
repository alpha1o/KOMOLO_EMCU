/// Info.
typedef struct
{
    UINT8	ProgramVer[2];	// [0] F/W Main 버전, [1]F/W Sub 버전 0x10 : V1.0
	UINT8	MakerID;	//	0x23:동원시스템(주)	0x24:(주)에이스테크놀로지	0x25:(주)GT&T	0x26:(주)에프알텍	0x28:(주)위다스
						//	0x29:단암전자통신(주)	0x2A:영우통신(주)	0x2B:(주)네오텔레콤	0x2C:(주)넥스트링크
						//	0x2D:(주)기산텔레콤	0x2E:(주)쏠리테크	0x2F:(주)엠티아이	0x30:(주)케이엠더블유
						//	0x31:(주)파인디지털	0x32:니코스테크놀로지(주)	0x33:하이웨이브(주)	0x34:텔레콤랜드
						//	0x35:(주)BcNE Global	0x36:(주)하이게인텔레콤	0x37:삼지전자(주)	0x38: (주)GS텔레텍
	
    UINT8   CpuMaker;	//	0x23:(주)에스에이티	0x24:(주)아트웨어	0x25:(주)케이에스티	0x26:(주)코라텍
						//	0x28:(주)리보텍	0x29:동원시스템즈(주)	0x2A:(주)파인디지털	0x2B:(주)기산텔레콤
						//	0x2C:(주)엠티아이	0x2D:(주)이노알에스	0x2E:M2Tech	0x2F:(주)텔콤코리아
						//	0x30:(주)BcNE Global	0x34:(주)아이스톰


	UINT8   RepeaterID[10];	// GUI Reserved, 내부 사용,

}INFODATA; 

//-----------------------------------------------------------------------//
typedef struct{
    UINT8   UMHU_UMU_STS;	//BIT7, 6 ; UMHU, BIT 5, 4 : UMU
	UINT8	UMU_PORT1_ERU_STS[8][2]; 
	UINT8	EHU_TOTAL_STS[8][17];
	
} TREE_INFO; 



typedef struct{

	INFODATA	m_InfoData;
	
	TREE_INFO	m_InstallInfo;
	
} INSTALL_INFO; 

/*********************************************************/
/*  		UMHU Status & Control Structue  		 */
/*********************************************************/
typedef struct
{
    //-----------------------------------------------------------------------//
    // 공통
        
    //------------------------------------------//
    // 구성정보
 	INFODATA	m_InfoData;

	UINT8	Reserved_0;
    //------------------------------------------//
    // Alarm
    UINT8	Alarm[4];		//	1 : Alarm, 0 : Normal

	UINT8	Reserved_1;
    //-----------------------------------------------------------------------//
    // CTRL
	UINT8	Tx_Attn;		//	UINT8, 1dBm
	UINT8	Rx_Attn;		//	UINT8, 1dBm
	UINT8	DL_Attn_H;		//	UINT8, 0.5dB, *2
	UINT8	UL_Attn_H;		//	UINT8, 0.5dB, *2

	INT8	Rx_ALC_High;	//	INT8, 1dBm
	INT8	Rx_ALC_Offset;	//	INT8, 1dBm
	INT8	Rx_SD;			//	INT8, 1dBm

	INT8	Temp_high_Limit;	//	INT8, 1'C
	INT8	Temp_Low_Limit;		//	INT8, 1'C

	INT8	Rx_Pwr_High_Limit;	//	INT8, 0.5dBm
	INT8	Rx_Pwr_Low_Limit;	//	INT8, 0.5dBm

	INT8	LD_Low_Limit;		//	INT8, 0.5dBm, *2
	INT8	PD_Low_Limit;		//	INT8, 0.5dBm, *2

	UINT8	Reserved_2_2;		//DTU_Path;			// 	0x00 : None	0x01 : Path1	0x02 : Path2
	UINT8	Reserved_2;

	UINT8	Ctrl_OnOff;		//	Bit 1:ON,  0:OFF
	UINT8	SAW_OnOff;			//	Bit 1:ON,  0:OFF
	
    //------------------------------------------//
    // Power Offset
   	INT8	Tx_Out_Power_Offset;	// INT8, 0.1dB
   	INT8	Rx_Out_Power_Offset;	// INT8, 0.1dB
	INT8	LD_Power_Offset;		//	INT8, 0.5dB
	INT8	PD_Power_Offset;		//	INT8, 0.5dB

	
	//------------------------------------------//
    // 함체온도
    INT8	Temperature;	//	INT8, 1'C
	//------------------------------------------//
    // POWER
    INT8	Tx_Out_Power[2];		//	INT16, 0.1dBm
	INT8	Rx_Out_Power[2];		//	INT16, 0.1dBm
	//------------------------------------------//
    // VOLT
	UINT8	Tx_Out_Volt_H[2];		//	UINT16L * 5 / 1023
	UINT8	Rx_Out_Volt_H[2];		//	UINT16L * 5 / 1023
	//------------------------------------------//
    // MDTU
	INT8	LD_Power;	//	INT8, 0.5dBm
	INT8	PD_Power;	//	INT8, 0.5dBm

	UINT8	BIP_H;
	UINT8	DTU_Version_H;		//	0x10 : V1.0 
    UINT8	Debug[4];
	
	//------------------------------------------//
    // 년/월/일/시/분(HIDDEN)
    UINT8	Time_H[5];	//	0x0A : 2010년	0x06 : 06월	0x13 : 19일	0x14 : 20시	0x0F : 15분
	/////////////
	TREE_INFO	m_InstallInfo;

}UMHU_STATUS;



// ALARMFLAG			
////////////////////////////////////////////////////////////
//Alarm[0]
#define DEF_ARM_UMHU_AC				BITSET(0)	
#define DEF_ARM_UMHU_DC				BITSET(1)	
#define DEF_ARM_UMHU_DOOR			BITSET(2)	
#define DEF_ARM_UMHU_TMEPHIGH		BITSET(3)	
#define DEF_ARM_UMHU_TEMPLOW		BITSET(4)

//Alarm[1]
#define DEF_ARM_UMHU_RXSD			BITSET(0)	
#define DEF_ARM_UMHU_RXPWRHIGH		BITSET(1)	
#define DEF_ARM_UMHU_RXPWRLOW		BITSET(2)	
#define DEF_ARM_UMHU_TXPLL			BITSET(3)	
#define DEF_ARM_UMHU_RXPLL			BITSET(4)

//Alarm[2]
#define DEF_ARM_UMHU_LD				BITSET(0)	
#define DEF_ARM_UMHU_PD				BITSET(1)	
#define DEF_ARM_UMHU_LDPWRLOW		BITSET(2)	
#define DEF_ARM_UMHU_PDPWRLOW		BITSET(3)	
#define DEF_ARM_UMHU_OPTIC			BITSET(4)
#define DEF_ARM_UMHU_DTUSTS			BITSET(5)

//Alarm[3]
#define DEF_ARM_UMHU_LinkFail		BITSET(0)	
#define DEF_ARM_UMHU_SUM			BITSET(1)	
						

////////////////////////////////////////////////////////////

typedef struct
{
     //-----------------------------------------------------------------------//
    // ControlFlag
    UINT8   ControlFlag[8];                                                       // 090827
    // [0]Attn Flag [1]Config Flag [2]MDTU Flag [3]Path Flag [4]Ctrl On/Off Flag
    // [5]SAW SW On/Off Flag [6]Power Offset
	UINT8	Reserved_0;
    //-----------------------------------------------------------------------//
    // 공통
    
    //-----------------------------------------------------------------------//
    // CTRL
    UINT8	Tx_Attn;		//	UINT8, 1dBm
	UINT8	Rx_Attn;		//	UINT8, 1dBm
	UINT8	DL_Attn_H;		//	UINT8, 0.5dB, *2
	UINT8	UL_Attn_H;		//	UINT8, 0.5dB, *2

	
	INT8	Rx_ALC_High;	//	INT8, 1dBm
	INT8	Rx_ALC_Offset;	//	INT8, 1dBm
	INT8	Rx_SD;			//	INT8, 1dBm

	INT8	Temp_high_Limit;	//	INT8, 1'C
	INT8	Temp_Low_Limit;		//	INT8, 1'C
	
	INT8	Rx_Pwr_High_Limit;	//	INT8, 0.5dBm
	INT8	Rx_Pwr_Low_Limit;	//	INT8, 0.5dBm

	INT8	LD_Low_Limit;		//	INT8, 0.5dBm, *2
	INT8	PD_Low_Limit;		//	INT8, 0.5dBm, *2

	UINT8	Reserved_2_2;		//DTU_Path;			// 	0x00 : None	0x01 : Path1	0x02 : Path2
	UINT8	Reserved_2;

	UINT8	Ctrl_OnOff;		//	Bit 1:ON,  0:OFF
	UINT8	SAW_OnOff;			//	Bit 1:ON,  0:OFF
	
    //------------------------------------------//
    // Power Offset
   	INT8	Tx_Out_Power_Offset;	// INT8, 0.1dB
   	INT8	Rx_Out_Power_Offset;	// INT8, 0.1dB
	INT8	LD_Power_Offset;		//	INT8, 0.5dB
	INT8	PD_Power_Offset;		//	INT8, 0.5dB
	
}UMHU_CONTROL;

// CTRLFLAG			
////////////////////////////////////////////////////////////
//CtrlFalg[0]
#define DEF_CTRL_UMHU_TXATT				BITSET(0)	
#define DEF_CTRL_UMHU_RXATT				BITSET(1)	
#define DEF_CTRL_UMHU_DLATT				BITSET(2)	
#define DEF_CTRL_UMHU_ULATT				BITSET(3)	
//CtrlFalg[1]
#define DEF_CTRL_UMHU_RXALC				BITSET(0)	
#define DEF_CTRL_UMHU_RXSD				BITSET(2)	
//CtrlFalg[2]
#define DEF_CTRL_UMHU_TEMPHIGH			BITSET(0)	
#define DEF_CTRL_UMHU_TEMPLOW			BITSET(1)	
#define DEF_CTRL_UMHU_RXPWRHIGH			BITSET(2)	
#define DEF_CTRL_UMHU_RXPWRLOW			BITSET(3)	
//CtrlFalg[3]
#define DEF_CTRL_UMHU_LDLOW				BITSET(0)	
#define DEF_CTRL_UMHU_PDLOW				BITSET(1)	
//CtrlFalg[4] 
//CtrlFalg[5]
#define DEF_CTRL_UMHU_RXHPA				BITSET(0)	
#define DEF_CTRL_UMHU_RXALCONOFF		BITSET(1)
#define DEF_CTRL_UMHU_RXSDONOFF			BITSET(3)
#define DEF_CTRL_UMHU_TEMPONOFF			BITSET(4)	
#define DEF_CTRL_UMHU_TXFILTERONOFF		BITSET(5)	
#define DEF_CTRL_UMHU_RXFILTERONOFF		BITSET(6)	
//CtrlFalg[6]
#define DEF_CTRL_UMHU_TXSAWSW1			BITSET(0)
#define DEF_CTRL_UMHU_TXSAWSW2			BITSET(1)	
#define DEF_CTRL_UMHU_TXSAWSW3			BITSET(2)	
#define DEF_CTRL_UMHU_TXSAWSW4			BITSET(3)	
#define DEF_CTRL_UMHU_RXSAWSW1			BITSET(4)	
#define DEF_CTRL_UMHU_RXSAWSW2			BITSET(5)	
#define DEF_CTRL_UMHU_RXSAWSW3			BITSET(6)	
#define DEF_CTRL_UMHU_RXSAWSW4			BITSET(7)	
//CtrlFalg[7]
#define DEF_CTRL_UMHU_TXPWR				BITSET(0)
#define DEF_CTRL_UMHU_RXPWR				BITSET(1)	
#define DEF_CTRL_UMHU_LDOFS				BITSET(2)	
#define DEF_CTRL_UMHU_PDOFS				BITSET(3)	

/*********************************************************/
/*  		UMU, EHU Status & Control Structue  		 */
/*********************************************************/
typedef struct{
    
    //-----------------------------------------------------------------------//
    // 공통
        
    //------------------------------------------//
    // 구성정보
	INFODATA	m_InfoData;

	UINT8	Reserved_0;
    //------------------------------------------//	15
    // Alarm
    UINT8	Alarm[6];		//	1 : Alarm, 0 : Normal
    
	UINT8	Reserved_1;
    //-----------------------------------------------------------------------//	22
    // CTRL	
    INT8	FAN_Oper_Temp;		//	INT8, 1'C
	INT8	Temp_high_Limit;	//	INT8, 1'C
	INT8	Temp_Low_Limit;		//	INT8, 1'C

	INT8	TR_LD_Low_Limit;		//	INT8, 0.5dBm, *2
	INT8	TR_PD_Low_Limit;		//	INT8, 0.5dBm, *2
	INT8	IR_LD_Low_Limit;		//	INT8, 0.5dBm, *2
	INT8	IR_PD_Low_Limit;		//	INT8, 0.5dBm, *2

	INT8	Tx_Delay;			//	INT8, 0 ~ 80us
	INT8	Rx_Delay;			//	INT8, 0 ~ 80us

	UINT8	ReMS_OnOff;			//ReMS ON/OFF(내부통신용)	1:ON,  0:OFF

	//------------------------------------------//	32
    // ON/OFF
	UINT8	OPTIC_SUM_OnOff;	//	Bit 1:ON,  0:OFF

	UINT8	Ctrl_OnOff;		//	Bit 1:ON,  0:OFF
	UINT8	LAN_SW_OnOff;		//	Bit 1:ON,  0:OFF
	UINT8	RX_OnOff;			//	Bit 1:ON,  0:OFF
	UINT8	PoE_OnOff;			//	Bit 1:ON,  0:OFF

	//------------------------------------------//
    // INSTALL
    
	UINT8	UMHU_Install;		//	1:Install, 0:None
	UINT8	EHU_Install;		//	1:Install, 0:None
	UINT8	UTP_Port_Install;	//	1:Install, 0:None
	
    //------------------------------------------//
    // Power Offset
	INT8	TR_LD_Offset_H;		//	INT8, 0.5dB
	INT8	TR_PD_Offset_H;		//	INT8, 0.5dB
	INT8	IR_LD_Offset_H;		//	INT8, 0.5dB
	INT8	IR_PD_Offset_H;		//	INT8, 0.5dB
	
	//------------------------------------------//
    // 함체온도
    INT8	Temperature;	//	INT8, 1'C
	//------------------------------------------//
    // POWER
    INT8	TR_LD_Power;		//	INT8, 0.1dBm
    INT8	TR_PD_Power;		//	INT8, 0.1dBm
    INT8	IR_LD_Power;		//	INT8, 0.1dBm
    INT8	IR_PD_Power;		//	INT8, 0.1dBm
	
	UINT8	PoE_Power[8][2];		//	INT16L, 0.1W
	
	//------------------------------------------//
    // BIP
	INT8	TR_BIP_H;	//	INT8, 0.5dBm
	INT8	IR_BIP_H;	//	INT8, 0.5dBm

	UINT8	RX_UTP_LINK;		//	1 : 연결, 0 : 미 연결
	UINT8	DTU_Version_H;		//	0x10 : V1.0 
    UINT8	Debug[3];
	
	//------------------------------------------//
    // 년/월/일/시/분(HIDDEN)
    UINT8	Time_H[5];	//	0x0A : 2010년	0x06 : 06월	0x13 : 19일	0x14 : 20시	0x0F : 15분

	UINT8	ERU_Insert[8];
 	/////////////
	TREE_INFO	m_InstallInfo;
   
} UMU_EHU_STATUS;

// Alarm[0]
#define DEF_ARM_UMU_DC				BITSET(0)
#define DEF_ARM_UMU_DOOR			BITSET(1)
#define DEF_ARM_UMU_FAN				BITSET(2)
#define DEF_ARM_UMU_TEMPHIGH		BITSET(3)
#define DEF_ARM_UMU_TEMPLOW			BITSET(4)
											
// Alarm[1]
#define DEF_ARM_UMU_TRLD			BITSET(0)
#define DEF_ARM_UMU_TRPD			BITSET(1)
#define DEF_ARM_UMU_IRLD			BITSET(2)
#define DEF_ARM_UMU_IRPD			BITSET(3)
											
// Alarm[2]
#define DEF_ARM_UMU_TRLDLOW			BITSET(0)
#define DEF_ARM_UMU_TRPDLOW			BITSET(1)
#define DEF_ARM_UMU_IRLDLOW			BITSET(2)
#define DEF_ARM_UMU_IRPDLOW			BITSET(3)

// Alarm[3]
#define DEF_ARM_UMU_POE1			BITSET(0)    
#define DEF_ARM_UMU_POE2			BITSET(1)    
#define DEF_ARM_UMU_POE3			BITSET(2)    
#define DEF_ARM_UMU_POE4			BITSET(3)    
#define DEF_ARM_UMU_POE5			BITSET(4)    
#define DEF_ARM_UMU_POE6			BITSET(5)    
#define DEF_ARM_UMU_POE7			BITSET(6)    
#define DEF_ARM_UMU_POE8			BITSET(7)       

// Alarm[4]
#define DEF_ARM_UMU_TROPTIC			BITSET(0)    
#define DEF_ARM_UMU_IROPTIC			BITSET(1)    
#define DEF_ARM_UMU_DTUSTS			BITSET(2)    
#define DEF_ARM_UMU_TROPTINS		BITSET(3)    
#define DEF_ARM_UMU_IROPTINS		BITSET(4)       

// Alarm[5]
#define DEF_ARM_UMU_LinkFail		BITSET(0)	
#define DEF_ARM_UMU_SUM				BITSET(1)	



//-----------------------------------------------------------------------//
// UMU_EHU Control
//-----------------------------------------------------------------------//

typedef struct{

   //-----------------------------------------------------------------------//
    // ControlFlag
    UINT8   ControlFlag[10];                                                       // 090827
    // [0]Config Flag [1]MDTU Flag [2]Delay Flag [3]Path Flag [4]Ctrl On/Off Flag
    // [5]LAN SW On/Off Flag [6]RX On/Off Flag [7]PoE On/Off Flag  [8]Install Flag  [9]Power Offset Flag 
	
	UINT8	Reserved_0;
    //-----------------------------------------------------------------------//
    // 공통
    //-----------------------------------------------------------------------//
    // CTRL	
    INT8	FAN_Oper_Temp;		//	INT8, 1'C
	INT8	Temp_high_Limit;	//	INT8, 1'C
	INT8	Temp_Low_Limit;		//	INT8, 1'C

	INT8	TR_LD_Low_Limit;		//	INT8, 0.5dBm, *2
	INT8	TR_PD_Low_Limit;		//	INT8, 0.5dBm, *2
	INT8	IR_LD_Low_Limit;		//	INT8, 0.5dBm, *2
	INT8	IR_PD_Low_Limit;		//	INT8, 0.5dBm, *2

	INT8	Tx_Delay;			//	INT8, 0 ~ 80us
	INT8	Rx_Delay;			//	INT8, 0 ~ 80us

	UINT8	DTU_Path;			// 	0x00 : None	0x01 : Path1	0x02 : Path2

	//------------------------------------------//
    // ON/OFF
	UINT8	OPTIC_SUM_OnOff;	//	Bit 1:ON,  0:OFF

	UINT8	Ctrl_OnOff;		//	Bit 1:ON,  0:OFF
	UINT8	LAN_SW_OnOff;		//	Bit 1:ON,  0:OFF
	UINT8	RX_OnOff;			//	Bit 1:ON,  0:OFF
	UINT8	PoE_OnOff;			//	Bit 1:ON,  0:OFF

	//------------------------------------------//
    // INSTALL
    
	UINT8	UMHU_Install;		//	1:Install, 0:None
	UINT8	EHU_Install;		//	1:Install, 0:None
	UINT8	UTP_Port_Install;	//	1:Install, 0:None
	
    //------------------------------------------//
    // Power Offset
	INT8	TR_LD_Offset_H;		//	INT8, 0.5dB
	INT8	TR_PD_Offset_H;		//	INT8, 0.5dB
	INT8	IR_LD_Offset_H;		//	INT8, 0.5dB
	INT8	IR_PD_Offset_H;		//	INT8, 0.5dB
	
} UMU_EHU_CONTROL;

//-----------------------------------------------------------------------//
// ControlFlag      -> 0: Not Change, 1: Cange

// ControlFalg[0] : Config Flag
#define DEF_CTRL_UMU_FANOPERTEMP		BITSET(0)
#define DEF_CTRL_UMU_TEMPHIGH			BITSET(1)
#define DEF_CTRL_UMU_TEMPLOW			BITSET(2)

// ControlFalg[1] : MDTU Flag
#define DEF_CTRL_UMU_TRLDLOW			BITSET(0)
#define DEF_CTRL_UMU_TRPDLOW			BITSET(1)
#define DEF_CTRL_UMU_IRLDLOW			BITSET(2)
#define DEF_CTRL_UMU_IRPDLOW			BITSET(3)

// ControlFlag[2] :Delay
#define DEF_CTRL_UMU_TXDLEAY			BITSET(0)
#define DEF_CTRL_UMU_RXDELAY			BITSET(1)

// ControlFlag[3] :Path Flag
#define DEF_CTRL_UMU_DTUPATH			BITSET(0)
#define DEF_CTRL_UMU_OPTICSUM			BITSET(1)

// ControlFlag[4] :Ctrl On/Off Flag
#define DEF_CTRL_UMU_FANAUTO			BITSET(0)
#define DEF_CTRL_UMU_FANMANUAL			BITSET(1)
#define DEF_CTRL_UMU_OPTICONOFF			BITSET(2)
#define DEF_CTRL_UMU_LANONOFF			BITSET(3)

// ControlFlag[5] :LAN SW On/Off Flag
#define DEF_CTRL_UMU_LANSW1				BITSET(0)
#define DEF_CTRL_UMU_LANSW2				BITSET(1)
#define DEF_CTRL_UMU_LANSW3				BITSET(2)
#define DEF_CTRL_UMU_LANSW4				BITSET(3)
#define DEF_CTRL_UMU_LANSW5				BITSET(4)
#define DEF_CTRL_UMU_LANSW6				BITSET(5)
#define DEF_CTRL_UMU_LANSW7				BITSET(6)
#define DEF_CTRL_UMU_LANSW8				BITSET(7)

// ControlFlag[6] :RX On/Off
#define DEF_CTRL_UMU_RX1				BITSET(0)
#define DEF_CTRL_UMU_RX2				BITSET(1)
#define DEF_CTRL_UMU_RX3				BITSET(2)
#define DEF_CTRL_UMU_RX4				BITSET(3)
#define DEF_CTRL_UMU_RX5				BITSET(4)
#define DEF_CTRL_UMU_RX6				BITSET(5)
#define DEF_CTRL_UMU_RX7				BITSET(6)
#define DEF_CTRL_UMU_RX8				BITSET(7)

// ControlFlag[7] :PoE On/Off Flag
#define DEF_CTRL_UMU_POE1				BITSET(0)
#define DEF_CTRL_UMU_POE2				BITSET(1)
#define DEF_CTRL_UMU_POE3				BITSET(2)
#define DEF_CTRL_UMU_POE4				BITSET(3)
#define DEF_CTRL_UMU_POE5				BITSET(4)
#define DEF_CTRL_UMU_POE6				BITSET(5)
#define DEF_CTRL_UMU_POE7				BITSET(6)
#define DEF_CTRL_UMU_POE8				BITSET(7)

// ControlFlag[8] :Install Flag
#define DEF_CTRL_UMU_UMHUINS			BITSET(0)
#define DEF_CTRL_UMU_EHUINS				BITSET(1)
#define DEF_CTRL_UMU_UTPPORTINS			BITSET(2)


// ControlFlag[9] :Power Offset
#define DEF_CTRL_UMU_TRLDOFS			BITSET(0)
#define DEF_CTRL_UMU_TRPDOFS			BITSET(1)
#define DEF_CTRL_UMU_IRLDOFS			BITSET(2)
#define DEF_CTRL_UMU_IRPDOFS			BITSET(3)

/*********************************************************/
/*  		ERU Status & Control Structue  		 */
/*********************************************************/
typedef struct{
    
    //-----------------------------------------------------------------------//
    // 공통
        
    //------------------------------------------//
    // 구성정보
	INFODATA	m_InfoData;

	UINT8	Reserved_0;
	
    //------------------------------------------//
    // Type
    UINT8	ERU_Type;	//	0x00: ERU10S	0x01: ERU10D	0x02: ERU10R	0x03: ERU20S	0x04: ERU20D	0x05: ERU40S

    //------------------------------------------//
    // Alarm
    UINT8	Alarm[4];		//	1 : Alarm, 0 : Normal
    
    //-----------------------------------------------------------------------//
    // CTRL	
	UINT8	Tx_Attn;		//	UINT8, 1dBm
	UINT8	Rx_Attn;		//	UINT8, 1dBm
	UINT8	DL_Attn_H;		//	UINT8, 0.5dB, *2
	UINT8	UL_Attn_H;		//	UINT8, 0.5dB, *2

	INT8	Tx_ALC_High;	//	INT8, 1dBm
	INT8	Tx_ALC_Offset;	//	INT8, 1dBm
	INT8	Tx_SD;			//	INT8, 1dBm
	
	INT8	Temp_high_Limit;	//	INT8, 1'C
	INT8	Temp_Low_Limit;		//	INT8, 1'C

	INT8	Tx_Out_High_Limit;	//	INT8, 0.5dBm
	INT8	Tx_Out_Low_Limit;	//	INT8, 0.5dBm
	
	//------------------------------------------//
    // ON/OFF
	UINT8	RFCU_OnOff;	//	Bit 1:ON,  0:OFF

    //------------------------------------------//
    // Power Offset
	INT8	Tx_Out_Power_Offset_H;		//	INT8, 0.1dB
	INT8	Rx_Out_Power_Offset_H;		//	INT8, 0.1dB

	//------------------------------------------//
    // 함체온도
    INT8	Temperature;		//	INT8, 1'C
    INT8	HPA_Temperature;	//	INT8, 1'C
	//------------------------------------------//
    // POWER
    INT8	Tx_Power[2];			//	INT16L, 0.1dBm
    INT8	Rx_Power[2];			//	INT16L, 0.1dBm
    UINT8	Tx_Power_Volt[2];		//	UINT16L * 5 / 1023
    UINT8	Rx_Power_Volt[2];		//	UINT16L * 5 / 1023
	
	//------------------------------------------//
    // Rems
	UINT8	ReMS_OnOff;		//	'1':On, '0':Off

	UINT8	DTU_Version_H;		//	0x10 : V1.0 

	UINT8	Debug[3];
	//------------------------------------------//
    // 년/월/일/시/분(HIDDEN)
    UINT8	Time_H[5];	//	0x0A : 2010년	0x06 : 06월	0x13 : 19일	0x14 : 20시	0x0F : 15분

	UINT8	IR_ERU_Insert;		// 	ERU40S용	그 외 Type은 0
 	/////////////
	TREE_INFO	m_InstallInfo;
   
} ERU_STATUS;

// Alarm[0]
#define DEF_ARM_ERU_DOOR			BITSET(0)
#define DEF_ARM_ERU_AC				BITSET(1)
#define DEF_ARM_ERU_DC				BITSET(2)
#define DEF_ARM_ERU_TEMPHIGH		BITSET(3)
#define DEF_ARM_ERU_TEMPLOW			BITSET(4)
#define DEF_ARM_ERU_IRRXLink		BITSET(5)
											
// Alarm[1]
#define DEF_ARM_ERU_TXSD			BITSET(0)
#define DEF_ARM_ERU_TXPWRHIGH		BITSET(1)
#define DEF_ARM_ERU_TXPWRLOW		BITSET(2)
#define DEF_ARM_ERU_TXPLL			BITSET(3)
#define DEF_ARM_ERU_RXPLL			BITSET(4)
#define DEF_ARM_ERU_DTUSTS			BITSET(5)
			
// Alarm[2]
#define DEF_ARM_ERU_OVERPWR			BITSET(0)
#define DEF_ARM_ERU_OVERTEMP		BITSET(1)
#define DEF_ARM_ERU_VSWR			BITSET(2)

//Alarm[3]
#define DEF_ARM_ERU_LinkFail		BITSET(0)	
#define DEF_ARM_ERU_SUM			BITSET(1)



typedef struct{

   //-----------------------------------------------------------------------//
    // ControlFlag
    UINT8   ControlFlag[5];                                                       // 090827
    // [0]Attn Flag [1]ALC Flag [2]Config Flag [3]RFCU On/Off Flag [4]Power Offset Flag
    
    //-----------------------------------------------------------------------//
    // 공통
    //-----------------------------------------------------------------------//
    // CTRL	
    UINT8	Tx_Attn;		//	UINT8, 1dBm
	UINT8	Rx_Attn;		//	UINT8, 1dBm
	UINT8	DL_Attn_H;		//	UINT8, 0.5dB, *2
	UINT8	UL_Attn_H;		//	UINT8, 0.5dB, *2

	INT8	Tx_ALC_High;	//	INT8, 1dBm
	INT8	Tx_ALC_Offset;	//	INT8, 1dBm
	INT8	Tx_SD;			//	INT8, 1dBm
	
	INT8	Temp_high_Limit;	//	INT8, 1'C
	INT8	Temp_Low_Limit;		//	INT8, 1'C

	INT8	Tx_Out_High_Limit;	//	INT8, 0.5dBm
	INT8	Tx_Out_Low_Limit;	//	INT8, 0.5dBm
	
	//------------------------------------------//
    // ON/OFF
	UINT8	RFCU_OnOff;	//	Bit 1:ON,  0:OFF

    //------------------------------------------//
    // Power Offset
	INT8	Tx_Out_Power_Offset_H;		//	INT8, 0.1dB
	INT8	Rx_Out_Power_Offset_H;		//	INT8, 0.1dB

	//------------------------------------------//
    // Type
    UINT8	ERU_Type;	//	0x00: ERU10S	0x01: ERU10D	0x02: ERU10R	0x03: ERU20S	0x04: ERU20D	0x05: ERU40S

	UINT8	DTU_Config;
	
} ERU_CONTROL;

// ControlFlag      -> 0: Not Change, 1: Cange

// ControlFalg[0] : Config Flag
#define DEF_CTRL_ERU_TXATT			BITSET(0)
#define DEF_CTRL_ERU_RXATT			BITSET(1)
#define DEF_CTRL_ERU_DLATT			BITSET(2)
#define DEF_CTRL_ERU_ULATT			BITSET(3)

// ControlFalg[1] : ALC Flag
#define DEF_CTRL_ERU_TXALCHIGH		BITSET(0)
#define DEF_CTRL_ERU_TXALCOFS		BITSET(1)
#define DEF_CTRL_ERU_TXSD			BITSET(2)

// ControlFalg[2] : MDTU Flag
#define DEF_CTRL_ERU_TEMPHIGH		BITSET(0)
#define DEF_CTRL_ERU_TEMPLOW		BITSET(1)
#define DEF_CTRL_ERU_TXPWRHIGH		BITSET(2)
#define DEF_CTRL_ERU_TXPWRLOW		BITSET(3)
#define DEF_CTRL_ERU_REMSRESET		BITSET(4)
#define DEF_CTRL_ERU_TYPE			BITSET(5)
#define DEF_CTRL_ERU_CFRONOFF		BITSET(6)


// ControlFalg[3] : RFCU_OnOff Flag
#define DEF_CTRL_ERU_TXHPAONOFF		BITSET(0)
#define DEF_CTRL_ERU_TXALCHIGHONOFF	BITSET(1)
#define DEF_CTRL_ERU_TXALCOFSONOFF	BITSET(2)
#define DEF_CTRL_ERU_TXSDONOFF		BITSET(3)
#define DEF_CTRL_ERU_TXRXONOFF		BITSET(4)
#define DEF_CTRL_ERU_IRRXONOFF		BITSET(5)
#define DEF_CTRL_ERU_LANONOFF		BITSET(6)
#define DEF_CTRL_ERU_TEMOCOMP		BITSET(7)



// ControlFalg[4] : RFCU_OnOff Flag
#define DEF_CTRL_ERU_TXPWROFS		BITSET(0)
#define DEF_CTRL_ERU_RXPWROFS		BITSET(1)

///////////////////////////////////////
/**********************************************************/
/*			 USER DEFINED VARIABLES						  */
/**********************************************************/

// Pannel Window Handle
////////////////////////////////////////////////////////////
// Pannel Window Handle
////////////////////////////////////////////////////////////
int m_hWndUmu;
int m_hWndUmhu;
int m_hWndEhu; 
int m_hWndEru;

int m_hWndUmuSub;
int m_hWndUmhuSub;
int m_hWndEhuSub; 
int m_hWndEruSub;

TREE_INFO		* m_pTempTreeInfo;     
INSTALL_INFO	* m_pTreeInfo;
UMHU_STATUS		* m_pUmhuSts;
UMHU_CONTROL	* m_pUmhuSet;
UMU_EHU_STATUS	* m_pUmuSts;
UMU_EHU_CONTROL	* m_pUmuSet;
UMU_EHU_STATUS	* m_pEhuSts;
UMU_EHU_CONTROL	* m_pEhuSet;
ERU_STATUS	* m_pEruSts;
ERU_CONTROL	* m_pEruSet;



// Set.c
void UmuSetData(void);
void UmhuSetData(void);  
void EhuSetData(void); 
void EruSetData(void);  

void UmuSubSetData(void);
void UmhuSubSetData(void);
void EhuSubSetData(void);
void EruSubSetData(void);

//display.c
void MainTreeDraw (void);
void UmuStatusDataDisplay(void);
void UmhuStatusDataDisplay(void);
void EhuStatusDataDisplay(void);
void EruStatusDataDisplay(void);

