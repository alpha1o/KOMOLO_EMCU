#ifndef RS232SATLIB
#define RS232SATLIB

#include "StdUtil.h"

//General Define
#define EVENT_SEND		0x01
#define EVENT_RECV		0x02
#define EVENT_PTDROP	0x03

//Callbakc Func Type Define
typedef int (__cdecl *PRSLENCALLBACK)(int iIdx, BYTE *pLenBuf);
typedef int (__cdecl *PRSEVENTCALLBACK)(int iIdx, BYTE dwEventFlag, BYTE *pData, int nLength);
typedef int (__cdecl *DEBUGCALLBACK)(int iIdx, BYTE *pData, int nLength);
typedef int (__cdecl *PRSFRAMECHECKCALLBACK)(int iIdx, BYTE *pData, int nLength);


typedef struct _SENDITEM
{
	int 	nDataCount;
	BYTE   *pData;
	
}SENDITEM;

#define MAX_RCV_BUF			1024 * 32	//BYTE
#define MAX_SEND_PACKET		1024 * 32	//COUNT OF SENDITEM STRUCT

typedef struct
{
	 int L_nRcvPacketMax;
	 UINT8 L_nLenStartByte;
	 UINT8 L_nLenFieldSize;
	 PRSLENCALLBACK L_LengthFunc;
	 PRSEVENTCALLBACK L_EventFunc;
	 DEBUGCALLBACK L_DebugFunc;
	 BOOL L_bUseExtraCheck;
	 int L_ExtraCheckStartByte;
	 int L_ExtraCheckLength;
	 PRSFRAMECHECKCALLBACK L_ExtraCheckFunc;
	 BOOL L_bOpenPort;
	 
	 COMATTR L_ComAttr;
	 
	 BOOL L_bDebugMode;


	 HANDLE L_hSendEvent;
	 SENDITEM *L_SendItemList[MAX_SEND_PACKET];
	 int L_nSendItemCount;
	 BYTE L_RcvBuff[MAX_RCV_BUF];
	 int L_nRcvDataSize;
	 int L_nCntPacketSize;

	 BOOL L_bThreadRunRCV;
	 BOOL L_ThreadEndFlagRCV;

	 BOOL L_bThreadRunSND;
	 BOOL L_ThreadEndFlagSND;

	 BOOL L_bCriticalEnter;

	 BOOL L_bUseSyncCode;
	 BYTE L_SyncBuf[32];
	 int  L_SyncLen;

	 BOOL L_bThreadHold;

	/////////////////////////////
	// Add 0507
	 BYTE L_CopyBuff[MAX_RCV_BUF];
	 int L_CopyDataLen;
	 int L_hMainPnl;  
} COMMDATA_SER;

COMMDATA_SER 	* m_pCOMM_SetInfo;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : RS232Init
// Purpose  : Serial Port Open & 초기화
// Prameter : 
//				hMainPnl	  - Main 패널의 핸들 (LoadPanel() 에서 얻어진 int 값)
//				*ComAttr      - COM port 를 open 하기 위해 필요한 환경 변수를 포함하는 COMATTR 구조체 포인터 
//				nLenStartByte - 패킷 구조 중 데이터 길이를 나타내는 필드의 시작 바이트
//				nLenFieldSize - 데이터 길이를 나타내는 필드의 크기
//				pLengthFunc   - 데이터 길이를 계산하기 위한 callback 함수 포인터. //								
//				pEventFunc	  - COM port Event (Send, Rcv) 를 감지하기 위한 callbakc 함수 포인터.
// Return   : 
//				TRUE  [1] - Success
//				FALSE [0] - Fail
// Remart   : 
//				pLenthFunc - 잘라지거나, 혹은 여러개의 패킷이 동시에 밀려들어오는 경우를 대비하여 본 Lib 를 사용한다.
//							 그러기 위해서 패킷구조상 데이터의 길이를 나타내는 필드를 실제 유효한 길이값으로 변환해야 하는데
//							 이 작업을 App 에서 대신한다. 이렇게 함으로써 본 Lib 는 공용성을 유지할 수 있다.
//							 pLenthFunc 함수는 App 에서 정의 해야 하며 다음과 같은 기능을 제공해야 한다.
//								
//								Input  : BYTE *pLenBuf - 길이 필드 포인터 (길이 필드의 byte 수는 APP 에서 이미 알고 있다.)
//								Output : int 		   - pLenBuf 를 사용하여 패킷 전체의 길이를 계산, return 한다.
//
//				pEventFunc - 본 RS232 Lib 는 데이터를 패킷탄위로 Send, Rcv 하므로 한 패킷이 보내졌을때, 그리고 한 패킷이 수신되었을때 
//							 이 callback 함수가 불리어진다.
//							 pEventFunc 함수는 App 에서 정의 해야 하며 전달되는 파라미터는 다음과 같다.
//
//								Input  : BYTE dwEventFlag - EVENT_SEND (0x01) : Send 명령에 의해 데이터가 실제로 전송된 경우 0x01 값으로 셋팅된다.
//									     				  - EVENT_RECV (0x02) : 한 패킷이 완전히 수신된 경우 0x02 값으로 셋팅된다.
//										 BYTE *pData	  - 데이터 포인터 
//										 int  nLength	  - 데이터 길이
//								Output : Ignore
//
//							 주의) !!! 본 함수에서 사용되는 두개의 callback 함수들을 반드시 정의해야 Serial 통신이 제대로 동작할 수 있다. !!!
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
BOOL 	RS232Init(int iIdx, int hMainPnl, COMATTR *ComAttr, UINT8 nLenStartByte, UINT8 nLenFieldSize, PRSLENCALLBACK pLengthFunc, PRSEVENTCALLBACK pEventFunc);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : RS232ExtraCheck
// Purpose  : RS232Init 으로 설정한 Frame Format 외에 별도로 Frame 의 유효성을 Check 할 필요가 있을경우 본 함수 사용.
// Prameter : 
//				nCheckStartByte		- 별도로 Check 할 시작 Byte
//				nCheckFieldSize		- 별도로 Check 할 Byte 수	
//				pExtraCheckFunc		- Check 할 데이터가 모두 받아진 후 불리어질 Callback 함수			
// Return   : 
//				TRUE  [1] - Success
//				FALSE [0] - Fail
// Remart   : pExtraCheckFunc 에서 TRUE [1] 을 Return 하면 해당 Frame 이 유효하다는 의미, 
//			  FALSE [0] 을 Return 하면 해당 Frame 이 유효하지 않다는 의미이므로 해당 프레임(현제까지 받은 데이터) 무시 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
BOOL 	RS232ExtraCheck(int iIdx, UINT8 nCheckStartByte, UINT8 nCheckFieldSize, PRSFRAMECHECKCALLBACK pExtraCheckFunc);



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : RS232End
// Purpose  : COM PORT 사용 종료 (Close)
// Prameter : 
// Return   : 
// Remart   : RS232Init 호출 후 RS232End() 를 호출하지 않으면 데이터 수신 Thread 가 종료되지 않는다. 
//			  따라서 프로그램을 끝내기 전 반드시 호출해야 한다.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void	RS232End(int iIdx);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : RS232Send
// Purpose  : Serial Send
// Prameter : 
//				pData - 전송할 데이터
//				nSize - 전송할 데이터 사이즈
// Return   : 
//				Success - 실제로 전송된 데이터 크기 
//				Fail	- 0
// Remart   : 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int		RS232Send(int iIdx, BYTE *pData, int nSize);



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : RS232IsOpen
// Purpose  : 현재 사용하고 있는 포트가 열려있는지 여부 확인
// Prameter : 
// Return   : 
//				TRUE 	- 포트 열린 상태
//				FALSE	- 포트 닫힌 상태
// Remart   : 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
BOOL	RS232IsOpen(int iIdx);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : UDPSetSync
// Purpose  : 패킷 Sync 코드 사용 유무 결정 (ex 0x16161616 Sync Code 사용)
// Prameter : 
//				pSyncData - Sync Code 로 사용할 코드 버퍼 (MAX 32 Byte) 
//				nSize - Sync Code 길이
// Return   :   
// Remart   :   본 함수를 호출하지 않으면 Sync Code 를 사용하지 않는 것으로 간주, Packet Header 를 사용하지 않는다.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void	RS232SetSync(int iIdx, BYTE *pSyncData, int nSize);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : RS232SetMode
// Purpose  : Debug Mode <--> Release Mode 전환 
// Prameter : 
//				pNotiFunc		   : 디버그 모드에서 수신한 데이터를 전달할 Callback Function.
//				bDebugMode - TRUE  : Set Debug Mode
//							 FALSE : Set Release Mode 
// Return   :   
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void	RS232SetMode(int iIdx, DEBUGCALLBACK pNotiFunc, BOOL bDebugMode);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : RS232IsDebugMode
// Purpose  : Debug Mode 인지 Release Mode 인지 확인 
// Prameter : 
// Return   : TRUE - Debug Mode, FALSE - Release Mode  
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
BOOL	RS232IsDebugMode(int iIdx);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : RS232SetRcvMaxSize
// Purpose  : 수신될 Packet 의 Length 필드가 깨진경우 심각한 오류가 발생될 수 있다.
//			  이를 예방하기 위해서 App 에서 예상되는 Packet Size 의 최대값을 설정하고 수신된 Packet 이 
//			  최대치를 초과할 경우 해당 패킷을 무시한다. 
// Prameter : 
//			  nMaxSize				: 수신 Packet 의 최대 크기 ( 단, 32 Kbyte 를 넘을 수 없다. )
// Return   : 
// Remark   : 본 함수를 호출하기 전에는 Max 값이 32k 로 설정되어 있다.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void	RS232SetRcvMaxSize(int iIdx, int nMaxSize);

#endif
