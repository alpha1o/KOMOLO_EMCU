#include "G_MAIN.h"
#include <winsock.h>
#include <userint.h>
#include <ansi_c.h>
#include <utility.h>
#include "H_EXTERN.H"

//////////////////
//Common Function
//////////////////

int CalculLength_UDP(int iIdx, BYTE *pLenBuf)
{
	return MAKEWORD(pLenBuf[1], pLenBuf[0]);
}


void PhasePacket_UDP(int iIdx, int nDataSize)
{
	UINT16	Get_Command;
	
	Get_Command =	MAKEWORD(m_pRecvIO->Command[1], m_pRecvIO->Command[0]); 

	switch(Get_Command)
	{   
		case STATUS_RPY:
			
			memcpy(m_pDebugSts, m_pRecvIO->DataBuf, sizeof(DEBUG_STATUS));
			
			MainDataDisplay();
			
			if (  STATUS_RPY_Flag != 1)
			{
				SYSStatusDataDisplay();
				STATUS_RPY_Flag = 1;
			}
			
			
			break;
		
		case CONTROL_RPY:
			
			STATUS_RPY_Flag = 0; 
			
			break;
		

		case HPA_CONTROL_RPY:
			
				if( m_pRecvIO->DataBuf[0] == 0x00)
						MessagePopup("Success!", " Data Setup is Completed.");	
				else	MessagePopup("False!", " Data Setup is False.");
			
			break;
		
		case HPA_CON_STS_RPY:
			
				memcpy(m_pHPASet, m_pRecvIO->DataBuf, sizeof(HPA_CONTROL));
				
				HPASetDataDisplay();
				
				SendRequest(0, HPA_STATUS_REQ, NULL, 0);
				
			break;
		
		case HPA_STATUS_RPY:

				memcpy(m_pHPASts, m_pRecvIO->DataBuf, sizeof(HPA_STATUS));
	
				HPAStatusDataDisplay(); 
				
				if ( m_pCOMM[0].iPanelHandle_Table > 0)
				{
					TableCntVoltDisplay(0);
				}
				
			break;
		
		case HPA_PWR_CON_RPY:
				if( m_pRecvIO->DataBuf[3] = 0x01)
						MessagePopup("Success!", " Data Setup is Completed.");	
				else	MessagePopup("False!", " Data Setup is False.");
				
			break;
		
		case HPA_PWR_STS_RPY:
				TableDataDisplay(0);  
			break;
		
		case HPA_TEMP_CON_RPY:
				if( m_pRecvIO->DataBuf[3] == 0x01)
						MessagePopup("Success!", " Data Setup is Completed.");	
				else	MessagePopup("False!", " Data Setup is False.");
			break;
		
		case HPA_TEMP_STS_RPY:
				TableDataDisplay(0); 
			break;
		
		case AMP_CONTROL_RPY:
				if( m_pRecvIO->DataBuf[0] == 0x00)
						MessagePopup("Success!", " Data Setup is Completed.");	
				else	MessagePopup("False!", " Data Setup is False.");		
			break;
		
		case AMP_CON_STS_RPY:
	
				memcpy(m_pAMPSet, m_pRecvIO->DataBuf, sizeof(AMP_CONTROL));
	
				AMPSetDataDisplay();
				
				SendRequest(0, AMP_STATUS_REQ, NULL, 0); 

			break;
		
		case AMP_STATUS_RPY:
			
				memcpy(m_pAMPSts, m_pRecvIO->DataBuf, sizeof(AMP_STATUS));
				
				AMPStatusDataDisplay();
				
			break;
		

		
		case LNA_CONTROL_RPY:
				if( m_pRecvIO->DataBuf[0] == 0x00)
						MessagePopup("Success!", " Data Setup is Completed.");	
				else	MessagePopup("False!", " Data Setup is False.");
			break;
		
		case LNA_CON_STS_RPY:

				memcpy(m_pLNASet, m_pRecvIO->DataBuf, sizeof(LNA_CONTROL));
				
				LNASetDataDisplay();
				
				SendRequest(0, LNA_STATUS_REQ, NULL, 0);  

			break;
		
		case LNA_STATUS_RPY:

				memcpy(m_pLNASts, m_pRecvIO->DataBuf, sizeof(LNA_STATUS));
				
				LNAStatusDataDisplay();
				
				if ( m_pCOMM[0].iPanelHandle_Table > 0)
				{
					TableCntVoltDisplay(0);
				}
				
			break;
		
		case CHANNEL_CON_RPY:
			//	memcpy(m_pCHASet, m_pRecvIO->DataBuf, sizeof(CHASET));
				
				if( m_pRecvIO->DataBuf[0] == 0x00)
						MessagePopup("Success!", " Data Setup is Completed.");	
				else	MessagePopup("False!", " Data Setup is False.");
				
				
			//	CHAStatusDataDisplay();	
				
			break;
		
		case CHANNEL_CON_STS_RPY:
				memcpy(m_pCHASts, m_pRecvIO->DataBuf, sizeof(CHASET));
				
				CHADisplay_Init();  
				
				CHAStatusDataDisplay();
				
				
				
			break;
			
		case IP_SET_RPY:
			
			if( m_pRecvIO->DataBuf[0] == 0x00)
					MessagePopup("Success!", " IP Setup is Completed.");	
			else	MessagePopup("False!", " IP Setup is False.");
			
			break;
			
		case IP_STS_RPY:
			
			memcpy(m_pIPSet, m_pRecvIO->DataBuf, sizeof(IP_SET));
			if (m_hWndIP)
			{
				SetCtrlVal(m_hWndIP, IPPNL_NMR_SERVER1, m_pIPSet->EMCU_IP[0]);
				SetCtrlVal(m_hWndIP, IPPNL_NMR_SERVER2, m_pIPSet->EMCU_IP[1]);
				SetCtrlVal(m_hWndIP, IPPNL_NMR_SERVER3, m_pIPSet->EMCU_IP[2]);
				SetCtrlVal(m_hWndIP, IPPNL_NMR_SERVER4, m_pIPSet->EMCU_IP[3]);
			
				m_pIPSet->EMCU_PORT = 11121;
	
				SetCtrlVal(m_hWndIP, IPPNL_NMR_PORT, m_pIPSet->EMCU_PORT);
	
			
	
				SetCtrlVal(m_hWndIP, IPPNL_NMR_GW1, m_pIPSet->EMCU_GATEWAY[0]);
				SetCtrlVal(m_hWndIP, IPPNL_NMR_GW2, m_pIPSet->EMCU_GATEWAY[1]);
				SetCtrlVal(m_hWndIP, IPPNL_NMR_GW3, m_pIPSet->EMCU_GATEWAY[2]);
				SetCtrlVal(m_hWndIP, IPPNL_NMR_GW4, m_pIPSet->EMCU_GATEWAY[3]);
	
				SetCtrlVal(m_hWndIP, IPPNL_NMR_SM1, m_pIPSet->EMCU_SUBNET[0]);
				SetCtrlVal(m_hWndIP, IPPNL_NMR_SM2, m_pIPSet->EMCU_SUBNET[1]);
				SetCtrlVal(m_hWndIP, IPPNL_NMR_SM3, m_pIPSet->EMCU_SUBNET[2]);
				SetCtrlVal(m_hWndIP, IPPNL_NMR_SM4, m_pIPSet->EMCU_SUBNET[3]);
	
				SetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_U_1, m_pIPSet->EBU_U_IP[0]);
				SetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_U_2, m_pIPSet->EBU_U_IP[1]);
				SetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_U_3, m_pIPSet->EBU_U_IP[2]);
				SetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_U_4, m_pIPSet->EBU_U_IP[3]);
	
				SetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_L_1, m_pIPSet->EBU_L_IP[0]);
				SetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_L_2, m_pIPSet->EBU_L_IP[1]);
				SetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_L_3, m_pIPSet->EBU_L_IP[2]);
				SetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_L_4, m_pIPSet->EBU_L_IP[3]);
			}
								  
			break;
			

		//----------------------------------------------------------------------------------------------//
	}
	
}


int OnPacket_UDP(int iIdx, BYTE dwEventFlag, BYTE *pData, int  nLength)
{

	// RS232 Connect Drop Event       <<-    System 오류로 인해 Port 가 저절로 닫힌 경우의 Event
	if(dwEventFlag == EVENT_PTDROP)
	{
		EnableTimer(m_hWndMain, m_pCOMM[iIdx].iPortDropTimer, TRUE);
		return 0;
	}
	
	if(!pData)
		return 0;

	if(nLength <= 0)
		return 0;	

	// On One Pakcet Send Successfully
	if(dwEventFlag == EVENT_SEND)
	{
		SetCtrlVal(m_hWndMain, MAINPNL_TX_LED, 1);
		SetCtrlVal(m_hWndMain, MAINPNL_RX_LED, 0);
	
		DebugStatusDisplay(iIdx, pData, nLength, TRUE);
	}		
	// On One Pakcet Receive Successfully
	else if(dwEventFlag == EVENT_RECV)
	{
		SetCtrlVal(m_hWndMain, MAINPNL_TX_LED, 0);
		SetCtrlVal(m_hWndMain, MAINPNL_RX_LED, 1);

		if(!RS232IsDebugMode(iIdx))
			DebugStatusDisplay(iIdx, pData, nLength, FALSE);
			
		// Config Packet
		memcpy(m_pRecvIO, pData, nLength);
		if(!IsValidPacket())
		{
		//	MessagePopupEx("Error", "CRC Error On Read Packet\nSize %d", nLength);
			return 0;
		}
		
		m_pRecvIO->DataLength = nLength;
	
		// Phase Packet
		PhasePacket_UDP(iIdx, nLength);			
	}	

	return 0;
}


//////////////////
//Serial Function
//////////////////


///////////////////
//UDP Function
////////////////// 
void UDPClose(int iIdx)
{
	int i;
	
	while(m_pCOMM_UDP[iIdx].L_UDP_bThreadRunRCV || m_pCOMM_UDP[iIdx].L_UDP_bThreadRunSND)
	{
		m_pCOMM_UDP[iIdx].bUdpThreadEndFlagRCV = TRUE;
		m_pCOMM_UDP[iIdx].bUdpThreadEndFlagSND = TRUE;
		
		if(m_pCOMM_UDP[iIdx].L_UDP_hSendEvent)
			SetEvent(m_pCOMM_UDP[iIdx].L_UDP_hSendEvent);
			
		Sleep(10);
	}
	
	for(i=0; i<m_pCOMM_UDP[iIdx].L_UDP_nSendItemCount; i++)
	{
		free(m_pCOMM_UDP[iIdx].L_UDP_SendItemList[i]->pData);
		free(m_pCOMM_UDP[iIdx].L_UDP_SendItemList[i]);
	}		   
	
	if(m_pCOMM_UDP[iIdx].L_UDP_hSendEvent)
	{
		CloseHandle(m_pCOMM_UDP[iIdx].L_UDP_hSendEvent);
		m_pCOMM_UDP[iIdx].L_UDP_hSendEvent = NULL;
	}	   
	
	m_pCOMM_UDP[iIdx].L_UDP_nSendItemCount = 0;
	m_pCOMM_UDP[iIdx].L_UDP_nRcvDataSize = 0;
	m_pCOMM_UDP[iIdx].L_UDP_nCntPacketSize = UDP_MAX_RCV_BUF;
	
	
	m_pCOMM_UDP[iIdx].bConnect = FALSE;
	
	if(m_pCOMM_UDP[iIdx].hSocket >= 0)	 ////////////????
		closesocket(m_pCOMM_UDP[iIdx].hSocket);
	
	if (m_pCOMM[iIdx].iPortDropTimer > 0)
	{
		EnableTimer(m_hWndMain, m_pCOMM[iIdx].iPortDropTimer, FALSE);
		DiscardCtrl (m_hWndMain, m_pCOMM[iIdx].iPortDropTimer);	
		m_pCOMM[iIdx].iPortDropTimer  = 0;
	}
	if (m_pCOMM[iIdx].iPollingTimer > 0)
	{
		EnableTimer(m_hWndMain, m_pCOMM[iIdx].iPollingTimer, FALSE);
		DiscardCtrl (m_hWndMain, m_pCOMM[iIdx].iPollingTimer);	
		m_pCOMM[iIdx].iPollingTimer  = 0;
	}
	if (m_pCOMM[iIdx].iWaitTimer > 0)
	{
		EnableTimer(m_hWndMain, m_pCOMM[iIdx].iWaitTimer, FALSE);
		DiscardCtrl (m_hWndMain, m_pCOMM[iIdx].iWaitTimer);	
		m_pCOMM[iIdx].iWaitTimer  = 0;
	}
	if (m_pCOMM[iIdx].iUDPEventTimer > 0)
	{
		EnableTimer(m_hWndMain, m_pCOMM[iIdx].iUDPEventTimer, FALSE);
		DiscardCtrl (m_hWndMain, m_pCOMM[iIdx].iUDPEventTimer);	
		m_pCOMM[iIdx].iUDPEventTimer  = 0;
	}
	
	if (m_pCOMM_UDP[iIdx].hThreadRecvFuncId > 0)
	{
		CmtDiscardThreadPool (m_pCOMM_UDP[iIdx].hThreadRecvFuncId);   
		m_pCOMM_UDP[iIdx].hThreadRecvFuncId = 0;
	}
	if (m_pCOMM_UDP[iIdx].hThreadRecvFuncId > 0)
	{
		CmtDiscardThreadPool (m_pCOMM_UDP[iIdx].hThreadSendFuncId);   
		m_pCOMM_UDP[iIdx].hThreadSendFuncId = 0;
	}
}

int	UDPSendData(int iIdx, BYTE *pData, int nSize, char *stIP, UINT16 nPort)
{   
	HANDLE hEvent; 
	UDP_SENDITEM *pItem;
	
	if(!pData)
		return 0;
		
	if(nSize <= 0)
		return 0;
	
	if(!stIP)
		return 0;
	
	if(nPort <= 0)
		return 0;
		
	if(m_pCOMM_UDP[iIdx].hSocket < 0)
		return 0;
	
	if(m_pCOMM_UDP[iIdx].L_UDP_nSendItemCount >= UDP_MAX_SEND_PACKET)
		return 0;		//overflow
		
	pItem = malloc(sizeof(UDP_SENDITEM));
	if(!pItem)
		return 0;
		
	pItem->pData = malloc(nSize);
	if(!pItem->pData)
	{
		free(pItem);
		return 0;
	}
	
	strcpy(pItem->stIP, stIP);
	pItem->nPort = nPort;
	pItem->nDataCount = nSize;
	memcpy(pItem->pData, pData, nSize);
	
	while(m_pCOMM_UDP[iIdx].L_UDP_bCriticalEnter)
		Sleep(10);
		
	m_pCOMM_UDP[iIdx].L_UDP_bCriticalEnter = TRUE;
	m_pCOMM_UDP[iIdx].L_UDP_SendItemList[m_pCOMM_UDP[iIdx].L_UDP_nSendItemCount++] = pItem;
	m_pCOMM_UDP[iIdx].L_UDP_bCriticalEnter = FALSE;
	
	if(m_pCOMM_UDP[iIdx].L_UDP_hSendEvent)
		SetEvent(m_pCOMM_UDP[iIdx].L_UDP_hSendEvent);
	
	return nSize;
}	  

void UDPSetSync_Temp( BYTE *pSyncData, int nSize)
{						  
	if(!pSyncData)
		return;
		
	if(nSize <= 0)
		return;
	
	if(nSize > 32)
		return;

	memcpy(L_UDP_SyncBuf, pSyncData, nSize);
	L_UDP_SyncLen = nSize;
	L_UDP_bUseSyncCode = TRUE;	   
}

void UDP_FindPacketHeader_Temp(int iIdx)
{
	int i, j;
	int iHeaderIndex = -1;
	BOOL bSyncMatch;
	
	for( i=0; i<m_pCOMM_UDP[iIdx].L_UDP_nRcvDataSize-L_UDP_SyncLen; i++)
	{
		iHeaderIndex = i;

		bSyncMatch = TRUE;
		for(j=0; j<L_UDP_SyncLen; j++)
		{
			if(m_pCOMM_UDP[iIdx].L_UDP_RcvBuff[i+j] != L_UDP_SyncBuf[j])
			{
				bSyncMatch = FALSE;
				break;
			}
		}
		
		if(bSyncMatch)
			break;					
	}

	if(iHeaderIndex >= m_pCOMM_UDP[iIdx].L_UDP_nRcvDataSize - (L_UDP_SyncLen + 1))
	{
		m_pCOMM_UDP[iIdx].L_UDP_nRcvDataSize = 0;
		return;
	}
	else if(iHeaderIndex >= 0)
	{
		SMemcopy(&m_pCOMM_UDP[iIdx].L_UDP_RcvBuff[0], &m_pCOMM_UDP[iIdx].L_UDP_RcvBuff[iHeaderIndex], m_pCOMM_UDP[iIdx].L_UDP_nRcvDataSize - iHeaderIndex);
		m_pCOMM_UDP[iIdx].L_UDP_nRcvDataSize -= iHeaderIndex;
	}
}

void UDPSetMode_Temp(int iIdx, PUDPDEBUGCALLBACK pNotiFunc, BOOL bDebugMode)
{				   
	m_pCOMM_UDP[iIdx].L_UDP_DebugFunc = *pNotiFunc;
	m_pCOMM_UDP[iIdx].L_UDP_bDebugMode = bDebugMode;
}

BOOL	UDPIsDebugMode_Temp(int iIdx)
{
	return m_pCOMM_UDP[iIdx].L_UDP_bDebugMode;
}			 

static int CVICALLBACK UDP_DataSendFunc(void *pData)
{   
	int iIdx = 0;
	
	int Y, M, D, h, m, s;
	char strEvent[32];
	GetSystemTime(&h, &m, &s);
	GetSystemDate(&M, &D, &Y);
	memset (strEvent, '\0', sizeof(strEvent));
	sprintf(strEvent, "%04d%02d%02d%02d%02d%02d%d\n",Y, M, D, h, m, s, iIdx);

 
										 
	if(m_pCOMM_UDP[iIdx].L_UDP_hSendEvent)
	{
		CloseHandle(m_pCOMM_UDP[iIdx].L_UDP_hSendEvent);
		m_pCOMM_UDP[iIdx].L_UDP_hSendEvent = NULL;
	}	
	
	//m_pCOMM_UDP[iIdx].L_UDP_hSendEvent = CreateEvent(NULL, FALSE, FALSE, ("UDPSENDEVENT"));
	m_pCOMM_UDP[iIdx].L_UDP_hSendEvent = CreateEvent(NULL, FALSE, FALSE, strEvent);

	
	if(!m_pCOMM_UDP[iIdx].L_UDP_hSendEvent)
		return 0;
	
	m_pCOMM_UDP[iIdx].L_UDP_bThreadRunSND = TRUE;
	
	while(!m_pCOMM_UDP[iIdx].bUdpThreadEndFlagSND)
	{
		if(WaitForSingleObject(m_pCOMM_UDP[iIdx].L_UDP_hSendEvent, INFINITE) == WAIT_OBJECT_0)
		{   
			while(m_pCOMM_UDP[iIdx].L_UDP_bCriticalEnter)
				Sleep(10);
				
			m_pCOMM_UDP[iIdx].L_UDP_bCriticalEnter = TRUE;
			
			while(m_pCOMM_UDP[iIdx].L_UDP_nSendItemCount > 0)
			{
				int i;
				UDP_SENDITEM *pItem =  m_pCOMM_UDP[iIdx].L_UDP_SendItemList[0];
				
				if(pItem)
				{   
					int err;
					SOCKADDR_IN sin;        
					int nSendCount = 0;
					
					sin.sin_family = AF_INET; 
					sin.sin_addr.s_addr = inet_addr(pItem->stIP);
					sin.sin_port = htons(pItem->nPort);
					
					while(nSendCount < pItem->nDataCount)
					{   
						err = sendto(m_pCOMM_UDP[iIdx].hSocket, pItem->pData+nSendCount, pItem->nDataCount - nSendCount, 0, (LPSOCKADDR)&sin, sizeof(SOCKADDR)); 
						if(err < 0)
							break;
						else
							nSendCount += err;
					}
					
					(*m_pCOMM_UDP[iIdx].L_UDP_EventFunc)(iIdx, UDP_EVENT_SEND, pItem->pData, pItem->nDataCount); 
					
					free(pItem->pData);
					free(pItem);
							
					m_pCOMM_UDP[iIdx].L_UDP_nSendItemCount--;
					for(i=0; i<m_pCOMM_UDP[iIdx].L_UDP_nSendItemCount; i++)
						m_pCOMM_UDP[iIdx].L_UDP_SendItemList[i] = m_pCOMM_UDP[iIdx].L_UDP_SendItemList[i+1];
					
				}
				else
				{   
					m_pCOMM_UDP[iIdx].L_UDP_nSendItemCount--;
					for(i=0; i<m_pCOMM_UDP[iIdx].L_UDP_nSendItemCount; i++)
						m_pCOMM_UDP[iIdx].L_UDP_SendItemList[i] = m_pCOMM_UDP[iIdx].L_UDP_SendItemList[i+1];
				}
				
				Sleep(20);
			} 
		}
		
		m_pCOMM_UDP[iIdx].L_UDP_bCriticalEnter = FALSE;
	}
	
	if(m_pCOMM_UDP[iIdx].L_UDP_hSendEvent)
	{
		CloseHandle(m_pCOMM_UDP[iIdx].L_UDP_hSendEvent);
		m_pCOMM_UDP[iIdx].L_UDP_hSendEvent = NULL;
	}
		
	m_pCOMM_UDP[iIdx].L_UDP_bThreadRunSND = FALSE;
	
	return 0;	
}

static int CVICALLBACK UDP_DataRecvFunc(void *pData)
{ 
	int iIdx = 0;

	struct  timeval  timeout;
	fd_set readfds;
	int fromSize;
	int nRcvSize;
	SOCKADDR_IN ssin;
	int i;
	BOOL bSyncMatch;

	m_pCOMM_UDP[iIdx].L_UDP_bThreadRunRCV = TRUE;
	
	while(!m_pCOMM_UDP[iIdx].bUdpThreadEndFlagRCV)
	{
		timeout.tv_sec = 0;
		timeout.tv_usec = 10;
	
		FD_ZERO(&readfds);
		FD_SET(m_pCOMM_UDP[iIdx].hSocket, &readfds);
		nRcvSize = select(m_pCOMM_UDP[iIdx].hSocket, &readfds, NULL, NULL, &timeout);

		if(nRcvSize > 0)
		{   
			fromSize = sizeof(SOCKADDR_IN);		
			nRcvSize = recvfrom(m_pCOMM_UDP[iIdx].hSocket, &m_pCOMM_UDP[iIdx].L_UDP_RcvBuff[m_pCOMM_UDP[iIdx].L_UDP_nRcvDataSize], m_pCOMM_UDP[iIdx].L_UDP_nCntPacketSize - m_pCOMM_UDP[iIdx].L_UDP_nRcvDataSize, 0, (LPSOCKADDR)&ssin, &fromSize);
			
			if(nRcvSize > 0)
			{
				if(m_pCOMM_UDP[iIdx].L_UDP_bDebugMode)
					(*m_pCOMM_UDP[iIdx].L_UDP_DebugFunc)(iIdx, &m_pCOMM_UDP[iIdx].L_UDP_RcvBuff[m_pCOMM_UDP[iIdx].L_UDP_nRcvDataSize], nRcvSize);

				m_pCOMM_UDP[iIdx].L_UDP_nRcvDataSize += nRcvSize;
		
				if(L_UDP_bUseSyncCode)
				{   
					if(m_pCOMM_UDP[iIdx].L_UDP_nRcvDataSize <= L_UDP_SyncLen)
						continue;
					
					bSyncMatch = TRUE;
					for(i=0; i<L_UDP_SyncLen; i++)
					{
						if(m_pCOMM_UDP[iIdx].L_UDP_RcvBuff[i] != L_UDP_SyncBuf[i])
						{
							bSyncMatch = FALSE;
							break;
						}
					}
					
					if(!bSyncMatch)
						UDP_FindPacketHeader_Temp(iIdx);		
				}
				
				while(m_pCOMM_UDP[iIdx].L_UDP_nRcvDataSize >= (m_pCOMM_UDP[iIdx].nLenStartByte + m_pCOMM_UDP[iIdx].nLenFieldSize))
				{
				//	m_pCOMM_UDP[iIdx].L_UDP_nCntPacketSize = (*L_UDP_LengthFunc)(iIdx, &m_pCOMM_UDP[iIdx].L_UDP_RcvBuff[m_pCOMM_UDP[iIdx].nLenStartByte]);
					m_pCOMM_UDP[iIdx].L_UDP_nCntPacketSize = (*m_pCOMM_UDP[iIdx].L_UDP_LengthFunc)(iIdx, &m_pCOMM_UDP[iIdx].L_UDP_RcvBuff[m_pCOMM_UDP[iIdx].nLenStartByte]);
					
					if(m_pCOMM_UDP[iIdx].L_UDP_nRcvDataSize >= m_pCOMM_UDP[iIdx].L_UDP_nCntPacketSize)
					{
					//	(*L_EventFunc)(EVENT_RECV, L_RcvBuff, L_nCntPacketSize);

	/*					SMemcopy(m_pCOMM_UDP[iIdx].L_UDP_RcvBuff, &m_pCOMM_UDP[iIdx].L_UDP_RcvBuff[m_pCOMM_UDP[iIdx].L_UDP_nCntPacketSize], UDP_MAX_RCV_BUF - m_pCOMM_UDP[iIdx].L_UDP_nCntPacketSize);
						m_pCOMM_UDP[iIdx].L_UDP_nRcvDataSize -= m_pCOMM_UDP[iIdx].L_UDP_nCntPacketSize;
						m_pCOMM_UDP[iIdx].L_UDP_nCntPacketSize = UDP_MAX_RCV_BUF;
	*/					/////////////////////////////
						
						memcpy(m_pCOMM_UDP[iIdx].L_UDP_CopyBuff, m_pCOMM_UDP[iIdx].L_UDP_RcvBuff, m_pCOMM_UDP[iIdx].L_UDP_nCntPacketSize);
						m_pCOMM_UDP[iIdx].L_UDP_CopyDataLen = m_pCOMM_UDP[iIdx].L_UDP_nCntPacketSize;
				
						ResetTimer(m_hWndMain, m_pCOMM[iIdx].iUDPEventTimer);
						SetCtrlAttribute (m_hWndMain, m_pCOMM[iIdx].iUDPEventTimer, ATTR_ENABLED, 1);
						
						Sleep(50);

						SMemcopy(m_pCOMM_UDP[iIdx].L_UDP_RcvBuff, &m_pCOMM_UDP[iIdx].L_UDP_RcvBuff[m_pCOMM_UDP[iIdx].L_UDP_nCntPacketSize], UDP_MAX_RCV_BUF - m_pCOMM_UDP[iIdx].L_UDP_nCntPacketSize);
						m_pCOMM_UDP[iIdx].L_UDP_nRcvDataSize -= m_pCOMM_UDP[iIdx].L_UDP_nCntPacketSize;
						m_pCOMM_UDP[iIdx].L_UDP_nCntPacketSize = UDP_MAX_RCV_BUF;
						
					}
					
				}
			}
			
		}

	}
	
	m_pCOMM_UDP[iIdx].L_UDP_bThreadRunRCV = FALSE;
	
	return 0;
}

int CVICALLBACK UDPRcvEventFuncTmr(int panel, int control, int event, void *callbackData, int eventData1, int eventData2)
{	
	int i;
	
	if(event != EVENT_TIMER_TICK)
		return 0;
	/*
	for (i=0; i<DEF_COMM_CNT;	i++)
	{
		if(control == m_pCOMM[i].iUDPEventTimer)
		{
			ResetTimer(panel, control);
			SetCtrlAttribute (panel, control, ATTR_ENABLED, 0);
	
			(*m_pCOMM_UDP[i].L_UDP_EventFunc)(i,UDP_EVENT_RECV, m_pCOMM_UDP[i].L_UDP_CopyBuff, m_pCOMM_UDP[i].L_UDP_CopyDataLen);
	
			return 0;
		}
	}
	*/
	

		if(control == m_pCOMM[0].iUDPEventTimer)
		{
			ResetTimer(panel, m_pCOMM[0].iUDPEventTimer);
			SetCtrlAttribute (panel, m_pCOMM[0].iUDPEventTimer, ATTR_ENABLED, 0);
	
			(*m_pCOMM_UDP[0].L_UDP_EventFunc)(0,UDP_EVENT_RECV, m_pCOMM_UDP[0].L_UDP_CopyBuff, m_pCOMM_UDP[0].L_UDP_CopyDataLen);
			
			return 0;
		}
	
	return 0;	  
}				  


BOOL UDPInitTemp(int iIndex, int hMainPnl, UINT16 nPort, UINT8 nLenStartByte, UINT8 nLenFieldSize, PLENCALLBACK pLengthFunc, PUDPEVENTCALLBACK pEventFunc)
{   
	SOCKADDR_IN sin;
	WORD wVersionRequested; 
	WSADATA wsaData; 
	int err, t = 1; 
	unsigned long u = 0;

	////////////////
	if(nPort <= 0)
		return FALSE;
		
	if(nLenStartByte < 0)
		return FALSE;
		
	if(nLenFieldSize < 0)
		return FALSE;

	if(hMainPnl <= 0)
		return FALSE;

	if(!pLengthFunc)
		return FALSE;
		
	if(!pEventFunc)
		return FALSE;		  	
	////////////////		
	m_pCOMM_UDP[iIndex].L_UDP_nCntPacketSize = UDP_MAX_RCV_BUF;

	m_pCOMM_UDP[iIndex].nLenStartByte = nLenStartByte;
	m_pCOMM_UDP[iIndex].nLenFieldSize = nLenFieldSize;
	
	//Close Socket if Prev use.
	if(m_pCOMM_UDP[iIndex].hSocket >= 0)
		closesocket(m_pCOMM_UDP[iIndex].hSocket);
	
	//End Thread if Prev Recv Thread are Running.
	UDPClose(iIndex);
		
	//Set Callback Pointer
	m_pCOMM_UDP[iIndex].L_UDP_LengthFunc = pLengthFunc;
	m_pCOMM_UDP[iIndex].L_UDP_EventFunc = pEventFunc;

	
	//Init Socket
	
	wVersionRequested = MAKEWORD(1, 1); 
	if((err = WSAStartup(wVersionRequested, &wsaData)) != 0)
	{
		WSACleanup();
		return FALSE;
	}
	
	
	// Open a socket
	m_pCOMM_UDP[iIndex].hSocket = socket(AF_INET, SOCK_DGRAM, 0);
	
	if(m_pCOMM_UDP[iIndex].hSocket == INVALID_SOCKET)
	{   
		closesocket(m_pCOMM_UDP[iIndex].hSocket);
		WSACleanup();
		return FALSE;
	}
	
	
	// bind the socket to an Address and Port
	sin.sin_family = AF_INET; 
	sin.sin_addr.s_addr = INADDR_ANY;
	sin.sin_port = htons(nPort);
	
	if((err = bind(m_pCOMM_UDP[iIndex].hSocket, (LPSOCKADDR)&sin, sizeof(sin))) != 0)
	{   
		closesocket(m_pCOMM_UDP[iIndex].hSocket);
		WSACleanup();
		return FALSE;
	}
	
	// Set the socket to Blocking I/O
	ioctlsocket(m_pCOMM_UDP[iIndex].hSocket, FIONBIO, &u);
	// Set the socket to Broadcast
	setsockopt(m_pCOMM_UDP[iIndex].hSocket, SOL_SOCKET, SO_BROADCAST, (char*)&t, sizeof(int));
/*	
	iUDPEventTimer = NewCtrl(hMainPnl, CTRL_TIMER, "UDP Lib Event Timer", 0, 0);
	SetCtrlAttribute(hMainPnl, iUDPEventTimer,  ATTR_INTERVAL, 0.01);
	SetCtrlAttribute(hMainPnl, iUDPEventTimer,  ATTR_ENABLED, 	0);
	SetCtrlAttribute(hMainPnl, iUDPEventTimer,	ATTR_CALLBACK_FUNCTION_POINTER, UDPRcvEventFunc);    
*/
	//Create Receive Lookup Thread
/*	m_pCOMM[iIndex].bUdpThreadEndFlagRCV = FALSE;
	CmtScheduleThreadPoolFunction(DEFAULT_THREAD_POOL_HANDLE, UDP_DataRecvFunc, NULL, NULL);
	
	m_pCOMM[iIndex].bUdpThreadEndFlagSND = FALSE;
	CmtScheduleThreadPoolFunction(DEFAULT_THREAD_POOL_HANDLE, UDP_DataSendFunc, NULL, NULL);
*/	
	m_pCOMM[iIndex].iPollingTimer = NewCtrl (m_hWndMain, CTRL_TIMER, "Timer", 	10, 10);
	SetCtrlAttribute (m_hWndMain, 	m_pCOMM[iIndex].iPollingTimer, ATTR_INTERVAL, 	1.0);
	SetCtrlAttribute (m_hWndMain,	m_pCOMM[iIndex].iPollingTimer, ATTR_CALLBACK_FUNCTION_POINTER, PollingTimer);    
	SetCtrlAttribute (m_hWndMain,	m_pCOMM[iIndex].iPollingTimer, ATTR_ENABLED, 	FALSE);
	
	m_pCOMM[iIndex].iPortDropTimer = NewCtrl (m_hWndMain, CTRL_TIMER, "Timer", 	10, 10);
	SetCtrlAttribute (m_hWndMain, 	m_pCOMM[iIndex].iPortDropTimer, ATTR_INTERVAL, 	1.0);
	SetCtrlAttribute (m_hWndMain,	m_pCOMM[iIndex].iPortDropTimer, ATTR_CALLBACK_FUNCTION_POINTER, PortDropTimer);    
	SetCtrlAttribute (m_hWndMain,	m_pCOMM[iIndex].iPortDropTimer, ATTR_ENABLED, 	FALSE);
	
	m_pCOMM[iIndex].iWaitTimer = NewCtrl (m_hWndMain, CTRL_TIMER, "Timer", 	10, 10);
	SetCtrlAttribute (m_hWndMain, 	m_pCOMM[iIndex].iWaitTimer, ATTR_INTERVAL, 	10.0);
	SetCtrlAttribute (m_hWndMain,	m_pCOMM[iIndex].iWaitTimer, ATTR_CALLBACK_FUNCTION_POINTER, ResponseTimer);    
	SetCtrlAttribute (m_hWndMain,	m_pCOMM[iIndex].iWaitTimer, ATTR_ENABLED, 	FALSE);

	m_pCOMM[iIndex].iUDPEventTimer = NewCtrl (m_hWndMain, CTRL_TIMER, "Timer", 	10, 10);
	SetCtrlAttribute (m_hWndMain, 	m_pCOMM[iIndex].iUDPEventTimer, ATTR_INTERVAL, 	0.01);
	SetCtrlAttribute (m_hWndMain,	m_pCOMM[iIndex].iUDPEventTimer, ATTR_CALLBACK_FUNCTION_POINTER, UDPRcvEventFuncTmr);    
	SetCtrlAttribute (m_hWndMain,	m_pCOMM[iIndex].iUDPEventTimer, ATTR_ENABLED, 	FALSE);

	m_pCOMM_UDP[iIndex].bUdpThreadEndFlagRCV = FALSE;
	CmtNewThreadPool(1, &m_pCOMM_UDP[iIndex].hThreadRecvFuncId);
	CmtScheduleThreadPoolFunction (m_pCOMM_UDP[iIndex].hThreadRecvFuncId, UDP_DataRecvFunc, (void *)iIndex, NULL);
	
	m_pCOMM_UDP[iIndex].bUdpThreadEndFlagSND = FALSE;
	CmtNewThreadPool(1, &m_pCOMM_UDP[iIndex].hThreadSendFuncId);
	CmtScheduleThreadPoolFunction (m_pCOMM_UDP[iIndex].hThreadSendFuncId, UDP_DataSendFunc, (void *)iIndex, NULL);

	return TRUE;
}


/////////////////
//static int UdpThreadInit (int iData)
int UdpThreadInit (int iData)
{
	int i = 0;
	BYTE cSync[4] = {0x7E};
	
	UDPSetSync_Temp(cSync, 1);   

	if(!UDPInitTemp(i, m_hWndMain, m_pCOMM_UDP[i].m_iUdpPort, 1, 2, CalculLength_UDP, OnPacket_UDP))
	{
		if (m_bConnectMsg)
			MessagePopup("AutoConnect", "UDP 모듈을 초기화 할 수 없습니다.");
		return 0;
	}	
	else
	{
		m_pCOMM_UDP[i].bConnect = TRUE;
		
		if (m_bConnectMsg)
			MessagePopup("AutoConnect", "UDP Init Success");
		EnableTimer(m_hWndMain, m_pCOMM[i].iPollingTimer, TRUE);  
		return 1;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
int CVICALLBACK OnCmdConnect (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	if(event != EVENT_COMMIT)
		return 0;   

	return 0;
}
