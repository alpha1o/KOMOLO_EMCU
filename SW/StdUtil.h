#ifndef STDUTIL_HEADER
#define STDUTIL_HEADER

/*********************************************************************************************
*   FILE			:	StdUtil.h									
*	PURPOSE			:	Define Constants, Macros and Functions [!!!! Project independent !!!!]
*   USER NAME		:   JIN-WON, KANG
*   CREATE DATE		:   2003/11/19
*	LAST UPDATE		:   2003/11/26
**********************************************************************************************/

#ifndef BOOL
#define BOOL int
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef NULL
#define NULL 0
#endif


/*********************************************************/									     
/*   		Simple Type define 			  	   			 */		 		  					  
/*********************************************************/
typedef signed char				INT8;
typedef unsigned char			UINT8, BYTE;
typedef signed short int		INT16;
typedef unsigned short int		UINT16, WORD;
typedef signed int				INT32;
typedef unsigned int			UINT32;


/*********************************************************/									     
/*   		Public Struct Type define	  	   			 */		 		  					  
/*********************************************************/
typedef struct
{
	int		iComport;			// Com Port Number
	char	*pStrDeviceName;	// Com Port Name ( Pointer Only, Not Array .. )
	int		iBaudrate;			// BaudRate
	int		iParity;			// Parity
	int		iDatabits;			// DataBits
	int		iStopbits;			// StopBits
	int		nInputq;			// Input Queue Size
	int		nOutputq;			// Output Queue Size
	int		iCtsmode;			// CTS Mode
	int		iXmode;				// X Mode
	double	nTimeout;			// TimeOut - Seconds

}COMATTR;

/*********************************************************/									     
/*   		Temporary Variable define	  	   			 */		 		  					  
/*********************************************************/
char Util_stBuff[64];


/*********************************************************/									     
/*   		MACRO define	 			  	   			 */		 		  					  
/*********************************************************/
#ifndef HIWORD
#define HIWORD(l)			((WORD) (((DWORD) (l) >> 16) & 0xFFFF)) 
#endif

#ifndef LOWORD
#define LOWORD(l)			((WORD) (l))
#endif

#ifndef HIBYTE
#define HIBYTE(w)			((BYTE) (((WORD) (w) >> 8) & 0xFF)) 
#endif

#ifndef LOBYTE
#define LOBYTE(w)			((BYTE) (w)) 
#endif

#ifndef MAKELONG
#define MAKELONG(l, h)      ((LONG)(((WORD)(l)) | ((DWORD)((WORD)(h))) << 16))
#endif

#ifndef MAKEWORD
#define MAKEWORD(l, h)      ((INT16)(((BYTE)(l)) | ((WORD)((BYTE)(h))) << 8))
#endif

#ifndef HINIBBLE
#define HINIBBLE(c)			((BYTE) (((c) >> 4) & 0x0F))
#endif

#ifndef LONIBBLE
#define LONIBBLE(c)			((BYTE) ((c) & 0x0F))
#endif

#ifndef MAKEBYTE
#define MAKEBYTE(h, l)		(((h & 0x0F) << 4) | (l & 0x0F))
#endif

/* Swap bytes in 32 bit value.  */
#ifndef bSWAP_32
#define bSWAP_32(x) \
     ((((x) & 0xff000000) >> 24) | (((x) & 0x00ff0000) >>  8) |           \
      (((x) & 0x0000ff00) <<  8) | (((x) & 0x000000ff) << 24))
#endif
      
/* Swap bytes in 16 bit value.  */
#ifndef bSWAP_16
#define bSWAP_16(x) \
     ((((x) & 0xff00) >>  8) | (((x) & 0x00ff) << 8))
#endif
      

/*********************************************************/									     
/*   		Public Utilty Functions define 	   			 */		 		  					  
/*********************************************************/



////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : MessagePopupEx
// Prameter : 
//				lpszTitle  - Title of MessgeBox
//				lpszFormat - Output Text Format
//				...	       - Format Info
// Return   : 
// Remart   : This function is like to printf(). So, You can use this Function As printf().
//
// Useage	: MessagePopupEx("Title", "Error");
//		    : MessagePopupEx("Title", "ErrorCode : %d", 3);
//			: MessagePopupEx("Title", "%d - %s - %f - %c%c\n", iValue, "str", 3.0, 'c', 'd');
////////////////////////////////////////////////////////////////////////////////////////////
void MessagePopupEx(char *lpszTitle, char *lpszFormat, ...);




////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : SMemcopy
// Prameter : 
//				pDest  - Dest Memory Pointer
//				pSrc   - Source Memory Pointer
//				nSize  - Size to Copy ( byte )
// Return   : 
// Remart   : Safe Memory copy for overlab memory area
//			: Ex) 
//					Dest 0x00, Src 0x01, size 16  --> memory shift left
//												  --> memcpy is not suport overlab area copy
//
// Useage	: see memcpy usage
////////////////////////////////////////////////////////////////////////////////////////////
void SMemcopy(void *pDest, void *pSrc, int nSize);




////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : CVIShowWindow
// Prameter : 
//				iPanelID   - Control's Panel ID
//				iControlID - Control ID
//				bShow	   - Show or Hide Flag
// Return   : 
// Remart   : Show / Hide Panel or Contorl
// Useage	: 
//			  ShowWindow(PanelID, ControlID, TRUE)		: Show Control
//			  ShowWindow(PanelID, ControlID, FALSE)		: Hide Control
//			  ShowWindow(PanelID, -1, TRUE)				: Show Panel
//			  ShowWindow(PanelID, -1, FALSE)			: Hide Panel
////////////////////////////////////////////////////////////////////////////////////////////
void CVIShowWindow(int iPanelID, int iControlID, BOOL bShow);




////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : CVIEnableWindow
// Prameter : 
//				iPanelID   - Control's Panel ID
//				iControlID - Control ID
//				bEnable	   - Enable or Disable Flag
// Return   : 
// Remart   : Enable / Disable Panel or Contorl
// Useage	: 
//			  EnableWindow(PanelID, ControlID, TRUE)		: Enable Control
//			  EnableWindow(PanelID, ControlID, FALSE)		: Disable Control
//			  EnableWindow(PanelID, -1, TRUE)				: Enable Panel
//			  EnableWindow(PanelID, -1, FALSE)				: Disable Panel
////////////////////////////////////////////////////////////////////////////////////////////
void CVIEnableWindow(int iPanelID, int iControlID, BOOL bEnable);




////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : ConfirmValidData
// Prameter : 
//				iPanelID   - Control's Panel ID
//				iControlID - Control ID
//				bRepair	   - whether repair this control's value to safe range value 
//
// Return   : TRUE - Value is Safe, FALSE - Value is out of range
// Remart   : Confirm Control's data is Valid
// Useage	: 
//			  ConfirmValidData(PanelID, ControlID, TRUE)				: Fix wrong value
//			  if(ConfirmValidData(PanelID, ControlID, FALSE)) ...		: Confirm data but, NO Fixed
////////////////////////////////////////////////////////////////////////////////////////////
BOOL ConfirmValidData(int iPannelID, int iControlID, BOOL bRepair);



////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : ModalMsgBox
// Prameter : 
//				iPanelID   - Main Pannel ID
//				iTimerID   - MsgBox Timer ID
//				lpszTitle  - Title of MessageBox
//				lpszFormat - String Format & Value
//
// Return   : 
// Remark   : Show Domal MessageBox in Thread
// Useage	: Look MessagePopupEx !!!
////////////////////////////////////////////////////////////////////////////////////////////
void ModalMsgBox(int iPannelID, int iTimerID, char *lpszTitle, char *lpszFormat, ...);

////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : BCDtoDec
// Prameter : 
//				cBcdByte   - BDC Format Value
//
// Return   : Decimal Value of Inputed BCD Value
// Remark   : 
// Useage	: 
////////////////////////////////////////////////////////////////////////////////////////////
BYTE BCDtoDec(BYTE cBcdByte);

////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : DECtoBcd
// Prameter : 
//				cDecByte   - Decimal Value
//
// Return   : BCD Format Value of Inputed Decimal Value
// Remark   : 
// Useage	: 
////////////////////////////////////////////////////////////////////////////////////////////
BYTE DECtoBcd(BYTE cDecByte);					   


////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : EnableTimer
// Prameter : 
//				hPanel   	- Panel Handle of Control
//				ControlID	- Control ID of Timer 
//				bEnable		- TRUE : Enable Timer
//							- FALSE : Disable Timer
// Return   : 
// Remark   : 
// Useage	: 
////////////////////////////////////////////////////////////////////////////////////////////
void EnableTimer(int hPanel, int ControlID, BOOL bEnable);



////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : BitFix
// Prameter : 
//				pByte   	- DestByte Pointer
//				BitMask		- Bit Mask Byte
//				bSet		- TRUE : Set Bit by 1
//							- FALSE : Set Bit by 0
// Return   : 
// Remark   : 
// Useage	: 
////////////////////////////////////////////////////////////////////////////////////////////
void BitFix(UINT8 *pByte, UINT8 BitMask, BOOL bSet);


////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : IsCheckBoxSet
// Prameter : 
//				hPanel   	- Panel Handle of Control
//				CheckBoxID	- CheckBox ID
//
// Return   : TRUE : CheckBox Setted, FALSE : CheckBox Not Setted
// Remark   : 
// Useage	: 
////////////////////////////////////////////////////////////////////////////////////////////
BOOL IsCheckBoxSet(int hPanel, int CheckBoxID);


////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : GetVersionString
// Prameter : 
//				cHexVer   	- Version Info ( ex : 0x34 -> 3.4 )
//
// Return   : Version String ( "3.4" )
// Remark   : 
// Useage	: 
////////////////////////////////////////////////////////////////////////////////////////////
char * GetVersionString(BYTE cHexVer);


#endif

