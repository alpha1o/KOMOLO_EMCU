#ifndef EXTERN_DEFINE_HEADER
#define EXTERN_DEFINE_HEADER

#include "SATRS232.h"



#define Test_COMM		0

// Table Ring Set
#define D_RINGDEF		0 
#define D_RINGSET		1

// Mode Define
//////////////////////////////////////////////////////////////

/*********************************************************/									     
/*   		Set Ctrl Item Type
/*********************************************************/
#define	iCT_BUTTON		0
#define	iCT_NUMERIC		1
#define	iCT_CHECKBOX	2
#define	iCT_RING		3
#define	iCT_STRING		4
#define	iCT_TEXT		5

#define iCT_NONE		0
#define iCT_DBM			1
#define iCT_DB			2
#define iCT_C			3
#define iCT_US			4
#define	ICT_CHAR		5
#define	iCT_INT			6

////////////////////////////////////////
#define DEF_INDEX_CTRLNAME		0
#define DEF_INDEX_CTRLFLAG		1
#define DEF_INDEX_CTRLTYPE		2
#define DEF_INDEX_RANGE			3
#define DEF_INDEX_INSTALL		4
#define DEF_INDEX_CTRLMIN		5
#define DEF_INDEX_CTRLMAX		6
#define DEF_INDEX_CTRLUNIT		7

/*********************************************************/									     
/*   		Tree Icon
/*********************************************************/
#define DEF_TREE_NOTINSERT		0
#define DEF_TREE_NORMAL			1
#define DEF_TREE_ALARM			2
#define DEF_TREE_LINKFAIL		3


/*===============================================================================*/
#define	C_INSTALL	MakeColor(192, 192, 192)   //GRAY
#define	C_LINK		MakeColor(250, 190, 0)	   //ORANGE
#define	C_ALARM		MakeColor(204, 51, 51)	   //RED
#define	C_NORMAL	MakeColor(0, 153, 0)	   //GREEN
/*===============================================================================*/


/*=================================================================================*
 *          PROTOCOL FRAME FILED DEFINE            								   *
 *=================================================================================*
 *  SYNC  | SRC_ID |  DEST_ID  |  COMMAND  |   LEN   | SUB_DATA |  CRC   |		   *
 *---------------------------------------------------------------------------------*
 * 4 BYTE | 1 BYTE |  1 BYTE   |  1 BYTE   |  2 BYTE | LEN BYTE | 2 BYTE |		   *
 *=================================================================================*/ 

/*********************************************************/									     
/*   		S/W Information define  	  	   			 */		 		  					  
/*********************************************************/
#define SW_VERSION				"0.0.1"
#define SW_TITLE				"EMCU 1.0.0  " 	// 

int		m_SWType;    
#define	SWTYPE_KTF				1
#define	SWTYPE_COMPANY			2
/*********************************************************/									     
/*   		Macro define    			  	   			 */		 		  					  
/*********************************************************/
#define BITSET(X)				(0x01 << X)





/*********************************************************/
/*     		COMMAND DEFINE     			  				 */
/*********************************************************/

#define IP_SET_REQ				0x0001 
#define IP_SET_RPY				0x8001  
#define IP_STS_REQ				0x0002  
#define IP_STS_RPY				0x8002 

// Status & Setting
///////////////////////////////////////////////////////////
#define STATUS_REQ				0x0006
#define STATUS_RPY				0x8006

#define	CONTROL_REQ				0x0003	
#define	CONTROL_RPY				0x8003
									  
										   
// HPA
///////////////////////////////////////////////////////////
#define	HPA_CONTROL_REQ				0x0007	
#define	HPA_CONTROL_RPY				0x8007

#define	HPA_CON_STS_REQ				0x0008	
#define	HPA_CON_STS_RPY				0x8008

#define	HPA_STATUS_REQ				0x0009	
#define	HPA_STATUS_RPY				0x8009

#define	HPA_PWR_CON_REQ				0x000A
#define	HPA_PWR_CON_RPY				0x800A

#define	HPA_PWR_STS_REQ				0x000B
#define	HPA_PWR_STS_RPY				0x800B

#define	HPA_TEMP_CON_REQ			0x000C	
#define	HPA_TEMP_CON_RPY			0x800C

#define	HPA_TEMP_STS_REQ			0x000D	
#define	HPA_TEMP_STS_RPY			0x800D


// AMP
///////////////////////////////////////////////////////////
#define	AMP_CONTROL_REQ				0x000E	
#define	AMP_CONTROL_RPY				0x800E

#define	AMP_CON_STS_REQ				0x000F	
#define	AMP_CON_STS_RPY				0x800F

#define	AMP_STATUS_REQ				0x0010	
#define	AMP_STATUS_RPY				0x8010

#define	AMP_PWR_CON_REQ				0x0011
#define	AMP_PWR_CON_RPY				0x8011

#define	AMP_PWR_STS_REQ				0x0012
#define	AMP_PWR_STS_RPY				0x8012

#define	AMP_TEMP_CON_REQ			0x0013	
#define	AMP_TEMP_CON_RPY			0x8013

#define	AMP_TEMP_STS_REQ			0x0014	
#define	AMP_TEMP_STS_RPY			0x8014


// LNA
///////////////////////////////////////////////////////////
#define	LNA_CONTROL_REQ				0x0015	
#define	LNA_CONTROL_RPY				0x8015

#define	LNA_CON_STS_REQ				0x0016	
#define	LNA_CON_STS_RPY				0x8016

#define	LNA_STATUS_REQ				0x0017	
#define	LNA_STATUS_RPY				0x8017


// CHANNEL
///////////////////////////////////////////////////////////
#define	CHANNEL_CON_REQ				0x0018	
#define	CHANNEL_CON_RPY				0x8018

#define	CHANNEL_CON_STS_REQ			0x0019
#define	CHANNEL_CON_STS_RPY			0x8019


// Download
//////////////////////////////////////////////////////////
#define	DNSTART_CMD				0x0B	
#define	DNSTART_RSP				0x0B
#define	DNDATACONF_CMD			0x0C
#define	DNDATACONF_RSP			0x0C	
#define	DNDATA_CMD				0x0D
#define	DNDATA_RSP				0x0D	
#define	DNEND_CMD				0x0E
#define	DNEND_CRSP				0x0E

#define DNENDWAIT_CMD			0x0A




// IP Set

//------------------------------------------//

#define IpSetRQST               0x40	 
#define IpSetRSPS               0xC0	 
#define IpSTSRQST               0x41	 
#define IpSTSRSPS               0xC1



/*********************************************************/									     
/*   		Protocol define    			 				 */		 		  					  
/*********************************************************/
#define UIR_FILE				"G_MAIN.uir"	


#define	SYNC					0x7E
#define	DATABUFMAX				1024
#define	FRAMECNT				0x00
#define	TAIL					0xF5   

#define DN_LEN					512
#define DN_ACK					0x01
#define DN_NACK					0x00
#define DN_WAITTIME				30		// sec

#define	OLU_NONE				0x00
#define	OLU_C					0x01
#define	OLU_I					0x02
/*********************************************************/
/*     		Base SIO Protocol Struture  				 */
/*********************************************************/
typedef struct {
    UINT8   STX;
	UINT8	DataLen[2];	
	UINT8	Command[2];
	UINT8	DataBuf[DATABUFMAX];
	
	// ETC
    //////////////////////////////////
	UINT16 BodyLength;
	UINT16 DataLength;
}COMMIO;

 //=====================================================================//
// DEBUG Status / Control
//=====================================================================//
//-----------------------------------------------------------------------//
// DEBUG Status
//-----------------------------------------------------------------------//

typedef struct{

    //------------------------------------------//
    // 备己沥焊
 	//---------------------------------
	UINT8	TEMP_A;
	INT8	LNA;
	INT8	HPA;
	UINT8	DOOR;
	UINT8	FIRE;
	UINT8	FAN_D;
	UINT8	FAN_I;
	UINT8	POWER;
	
	UINT8	TEMP[2];
	UINT8	PSU_P[2];
	UINT8	PSU_N[2];
	UINT8	AC_CURRENT[2];
	UINT8	AC_VOLTAGE[2];
	
	UINT8	TUNNEL_TYPE[2];
	UINT8	TUNNEL_ID;
	UINT8	TUNNEL_NAME[32];
	
	UINT8	TEMP_OFFSET;
	INT8	OVER_TEMP_Limit;
	INT8	UNDER_TEMP_Limit;
	INT8	FAN_ON_Limit;
	INT8	FAN_OFF_Limit;
	UINT8	PSU_P_Limit[2];
	UINT8	PSU_N_Limit[2];
	UINT8	AC_CURRENT_Limit[2];
	UINT8	AC_VOLTAGE_Limit[2];

	UINT8	VENDER[2];
	UINT8	Version[4];
    UINT8	EBU_1[4];
	UINT8	FPGA_1[4];
	UINT8	EBU_2[4];
	UINT8	FPGA_2[4];
 	
	UINT8	AM_L_1[4];
	UINT8	AM_FPGA_L1[4];
	UINT8	FM_L_1[4];
	UINT8	FM_FPGA_L1[4];
	UINT8	DMB_L_1[4];
	UINT8	DMB_FPGA_L1[4];
	UINT8	TRS_L_1[4];
	UINT8	TRS_FPGA_L1[4];
	
	UINT8	TRS_PWR[2];
	UINT8	AM_PWR[2];
	UINT8	SM_PWR[2];
	UINT8	DMB_PWR[2];
	
	
} DEBUG_STATUS;




//-----------------------------------------------------------------------//
// DEBUG Control
//-----------------------------------------------------------------------//

typedef struct{			   
	
	UINT8	TUNNEL_TYPE[2];
	UINT8	TUNNEL_ID;
	UINT8	TUNNEL_NAME[32];
	
	UINT8	TESMP_OFFSET;
	INT8	OVER_TEMP_Limit;
	INT8	UNDER_TEMP_Limit;
	UINT8	FAN_ON_Limit;
	UINT8	FAN_OFF_Limit;
	UINT8	PSU_P_Limit[2];
	UINT8	PSU_N_Limit[2];
	UINT8	AC_CURRENT_Limit[2];
	UINT8	AC_VOLTAGE_Limit[2];	

} DEBUG_CONTROL;
 


typedef struct{
    //------------------------------------------//
    // 备己沥焊
    UINT8	AM1_AMP;
    UINT8	AM1_ALC;
	UINT8	AM1_ALC_LEVEL[2];
    UINT8	AM1_OUT_ATTN[2];
	UINT8	AM1_ASD_LEVEL[2];
	UINT8	AM1_ASD_TEMP;
	
	UINT8	AM2_AMP;
    UINT8	AM2_ALC;
	UINT8	AM2_ALC_LEVEL[2];
    UINT8	AM2_OUT_ATTN[2];
	UINT8	AM2_ASD_LEVEL[2];
	UINT8	AM2_ASD_TEMP;
	
	UINT8	FM1_AMP;
    UINT8	FM1_ALC;
	UINT8	FM1_ALC_LEVEL[2];
    UINT8	FM1_OUT_ATTN[2];
	UINT8	FM1_ASD_LEVEL[2];
	UINT8	FM1_ASD_TEMP;
	
	UINT8	FM2_AMP;
    UINT8	FM2_ALC;
	UINT8	FM2_ALC_LEVEL[2];
    UINT8	FM2_OUT_ATTN[2];
	UINT8	FM2_ASD_LEVEL[2];
	UINT8	FM2_ASD_TEMP;
	
	UINT8	DMB_AMP;
    UINT8	DMB_ALC;
	UINT8	DMB_ALC_LEVEL[2];
    UINT8	DMB_OUT_ATTN[2];
	UINT8	DMB_ASD_LEVEL[2];
	UINT8	DMB_ASD_TEMP;
	
	UINT8	TRS_DL_AMP;
    UINT8	TRS_DL_ALC;
	UINT8	TRS_DL_ALC_LEVEL[2];
    UINT8	TRS_DL_OUT_ATTN[2];
	UINT8	TRS_DL_ASD_LEVEL[2];
	UINT8	TRS_DL_ASD_TEMP;
	
   	UINT8	TRS_UL_AMP;
    UINT8	TRS_UL_ALC;
	UINT8	TRS_UL_ALC_LEVEL[2];
    UINT8	TRS_UL_OUT_ATTN[2];
	UINT8	TRS_UL_ASD_LEVEL[2];
	UINT8	TRS_UL_ASD_TEMP;
	
} HPA_CONTROL;







typedef struct{

    //-----------------------------------------------------------------------//
    // ControlFlag
    UINT8	AM1_ASD;
    INT8 	AM1_Temp;
    UINT8	AM1_oPower[2];
    UINT8	AM1_oAttn[2];
    UINT8	AM1_OverPwr;
    UINT8	AM1_OverTemp;
	UINT8	AM1_RSSI[2];
	
	UINT8	AM2_ASD;
    INT8 	AM2_Temp;
    UINT8	AM2_oPower[2];
    UINT8	AM2_oAttn[2];
    UINT8	AM2_OverPwr;
    UINT8	AM2_OverTemp;
	UINT8	AM2_RSSI[2];
	
	UINT8	FM1_ASD;
    INT8 	FM1_Temp;
    UINT8	FM1_oPower[2];
    UINT8	FM1_oAttn[2];
    UINT8	FM1_OverPwr;
    UINT8	FM1_OverTemp;
	UINT8	FM1_RSSI[2];
	
	UINT8	FM2_ASD;
    INT8 	FM2_Temp;
    UINT8	FM2_oPower[2];
    UINT8	FM2_oAttn[2];
    UINT8	FM2_OverPwr;
    UINT8	FM2_OverTemp;
	UINT8	FM2_RSSI[2];
	
	UINT8	DMB_ASD;
    INT8 	DMB_Temp;
    UINT8	DMB_oPower[2];
    UINT8	DMB_oAttn[2];
    UINT8	DMB_OverPwr;
    UINT8	DMB_OverTemp;
	UINT8	DMB_RSSI[2];
	
	UINT8	TRS_DL_ASD;
    INT8 	TRS_DL_Temp;
    UINT8	TRS_DL_oPower[2];
    UINT8	TRS_DL_oAttn[2];
    UINT8	TRS_DL_OverPwr;
    UINT8	TRS_DL_OverTemp;
	UINT8	TRS_DL_RSSI[2];
	
	UINT8	TRS_UL_ASD;
    INT8 	TRS_UL_Temp;
    UINT8	TRS_UL_oPower[2];
    UINT8	TRS_UL_oAttn[2];
    UINT8	TRS_UL_OverPwr;
    UINT8	TRS_UL_OverTemp;
	UINT8	TRS_UL_RSSI[2];
	
} HPA_STATUS;


typedef struct {
    UINT8	ID;
	UINT8	DIR;
	UINT8	TYPE;
	UINT8	AMP;
	UINT8	ALC;
	UINT8	ALC_LEVEL[2];
	UINT8	Out_Attn[2];
} AMPSET;

typedef struct{
    //------------------------------------------//
    // 备己沥焊
    UINT8	TOTAL_CNT;
	AMPSET	AMP_CTRL[20]; 
	
} AMP_CONTROL;


typedef struct {
    UINT8	ID;
    UINT8	DIR;
    UINT8	TYPE;
    UINT8 	ALARM;
	UINT8 	ASD;
    INT8	Temp;
    UINT8	Out_pwr[2];
	UINT8	Out_Attn[2];
} AMPSTS;

typedef struct{
    //------------------------------------------//
    // 备己沥焊
    UINT8	TOTAL_CNT;	
    AMPSTS	AMP_STS[20];

} AMP_STATUS;


typedef struct{
    //------------------------------------------//
    // 备己沥焊
	
	UINT8	TRS_AMP;	
    UINT8	TRS_ALC;
	UINT8	TRS_ALC_LEVEL[2];
    UINT8	TRS_ATTN[2];
	
    UINT8	AM_AMP;	
    UINT8	AM_ALC;
	UINT8	AM_ALC_LEVEL[2];
    UINT8	AM_ATTN[2];

	UINT8	FM_AMP;	
    UINT8	FM_ALC;
	UINT8	FM_ALC_LEVEL[2];
    UINT8	FM_ATTN[2];
	
	UINT8	DMB_AMP;	
    UINT8	DMB_ALC;
	UINT8	DMB_ALC_LEVEL[2];
    UINT8	DMB_ATTN[2];
	
	UINT8	TRS_UL_AMP;	
    UINT8	TRS_UL_ALC;
	UINT8	TRS_UL_ALC_LEVEL[2];
    UINT8	TRS_UL_ATTN[2];
	
	
} LNA_CONTROL;


typedef struct{
    //------------------------------------------//
    // 备己沥焊

	
    UINT8	TRS_InputPOWER[2];	
	UINT8	TRS_ATTN[2];
	UINT8	TRS_RSSI[2];
	
	UINT8	AM_InputPOWER[2];	
	UINT8	AM_ATTN[2];
	UINT8	AM_RSSI[2];
	
	UINT8	FM_InputPOWER[2];	
	UINT8	FM_ATTN[2];
	UINT8	FM_RSSI[2];
	
	UINT8	DMB_InputPOWER[2];	
	UINT8	DMB_ATTN[2];
	UINT8	DMB_RSSI[2];	
	
	UINT8	TRS_UL_InputPOWER[2];	
	UINT8	TRS_UL_ATTN[2];
	UINT8	TRS_UL_RSSI[2];
	
} LNA_STATUS;



typedef struct {
    UINT8 	NUM;
    UINT8	Enable;
    UINT8	Gain_x2[2];
} SUB_CH;

typedef struct {
    UINT8	TYPE;
    UINT8	CFR_en;
    UINT8	CFR_thresh[2];
    UINT8	AGC;
	UINT8	DSP_Output_power[2];
    UINT8	Ch_cnt;
    SUB_CH	CH[40];
} CHASET;


typedef struct {
    UINT8	EMCU_IP[4];
    UINT8	EMCU_SUBNET[4];
	UINT8	EMCU_GATEWAY[4];
	
	UINT8	EBU_U_IP[4];
	UINT8	EBU_U_SUBNET[4];
	UINT8	EBU_U_GATEWAY[4];
	
	UINT8	EBU_L_IP[4];
	UINT8	EBU_L_SUBNET[4];
	UINT8	EBU_L_GATEWAY[4];
	
	UINT16	EMCU_PORT;
	
} IP_SET;





/**********************************************************/
/*			 USER DEFINED VARIABLES						  */
/**********************************************************/
// Pannel Window Handle
////////////////////////////////////////////////////////////
#define DEF_WIN_PART1	1
#define DEF_WIN_PART2	2

int m_hWndMain;

int m_hWndTable;

int m_hWndDownload;
int m_hWndProgress;
int m_hWndDnWait;										 


int m_hWndHPA;
int m_hWndLNA;
int m_hWndAMP;
int m_hWndCHA; 
int m_hWndBROAD;

 	     

int m_hWndEMERGENCY;
int m_hWndEMECH; 
int m_hWndSPEAKER;
int m_hWndIP;
int m_hWndEMCU;

int m_hWndENV; 




//////////////////////////////////////////////////////////

#define PRF_OFFSET  23

// Data Struct Instance
////////////////////////////////////////////////////////////
COMMIO		* m_pSendIO;	
COMMIO		* m_pRecvIO;

DEBUG_STATUS	* m_pDebugSts;
DEBUG_CONTROL	* m_pDebugSet;


HPA_CONTROL		* m_pHPASet;
HPA_STATUS		* m_pHPASts;	

AMP_CONTROL		* m_pAMPSet;
AMP_STATUS		* m_pAMPSts; 


LNA_CONTROL		* m_pLNASet;
LNA_STATUS		* m_pLNASts;


CHASET			* m_pCHASet;
CHASET			* m_pCHASts;

IP_SET			* m_pIPSet; 		

	
UINT8	STATUS_RPY_Flag;
////////////////////////////////////////////////////////////

// Commport Attribute Set
////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////
int m_DnWaitTime;

unsigned int m_iIndexBgn;
unsigned int m_iIndexEnd;


UINT8 m_iCtrlmodule; 	   //	0: MASTER, 1:SLAVE
#define Ctrl_Master		0
#define Ctrl_Slave		1


int	LOGIN_Value;

UINT8	Get_Channel_Select;

/**********************************************************/
/*			 USER DEFINED FUNCTIONS						  */						
/**********************************************************/

int AutoConnect (int iData);


void DisplayRS232Error(int error);
void SendRequest(int iIdx, int iCmd, void *pData, int iSize);



void SetupDataSave(void); 
void SetupDataInit(void);

int m_iSetupInfo[2][10];


void SerialMode(BYTE iMode);

						  
void IPInfoDataDisplay(int hWnd); 

void AllPanelCloseFunc(int iType);

void DebugStatusDisplay(int iIdx, BYTE *pData, int nLength, BOOL bTx);
BOOL OpenNewPanel(int *hWnd, int iPanel, BOOL bAlwaysTop, char *strTitle, char *strUirFile, BOOL bPopup);
BOOL OpenNewHidPanel(int *hWnd, int iPanel, BOOL bAlwaysTop, char *strTitle, char *strUirFile);
BOOL OpenSubPanel(int *hWnd, int iParentPanel, int iSubPanel, int iPanelTop, int iPanelLeft, char *strUirFile, int iSeld);
void ClosePanel(int *hWnd);
void PreOperation(int hWnd, int iSeld);
void CVIReturnDouble(double *dVal, int Power);




void GetOnOffVal(int hWnd, int iCtrl, BYTE *iSetByte);
void GetBitVal(int hWnd, int iCtrl, BYTE *pByte, BYTE iBit);

void CtrlsAttribute(int hWnd, int iFlagCtrl, BOOL bSetMode, int iValCtrl, int iCtrlType);
void GetInsertVal(int hWnd, int iCtrl, BYTE *pByte, BYTE iBit);
void GetInDoorVal(int hWnd, int iCtrl, BYTE *iSetByte);
void GetBitFlagVal(int hWnd, int iCtrl, BYTE *pByte, BYTE iBit);


// Display.c
void MainDataDisplay(void); 
void SYSSet(void);
void SYSStatusDataDisplay(void);

void HPASetData(void);
void HPASetDataDisplay(void);
void HPAStatusDataDisplay(void);

void AMPSetData(void);
void AMPSetDataDisplay(void);
void AMPStatusDataDisplay(void);
void AMPStatusChange(int control);

void LNASetData(void);
void LNASetDataDisplay(void);
void LNAStatusDataDisplay(void);


void CHAStatusDataDisplay(void);
void CHA_NUM_Change(int control);
void CHADisplay_Change(void);
void CHA_DIMMED (int control);
void CHADisplay_Init(void);
void CHASetData(void);

#define		CHA_TRS		0
#define		CHA_AM		1
#define		CHA_FM		2
#define		CHA_DMB		3

/**********************************************************/
/*			TABLE 			  							  */				
/**********************************************************/
// User Define Functions
////////////////////////////////////////////////////////////
// User Define Functions
////////////////////////////////////////////////////////////
void TableOpen(int iIdx);
void TableDataDisplay(int iTarget);
void TableDataSetup(int iTarget);
void TableCntVoltDisplay(int iTarget);
void SetTableCtrlColor(int iTarget);
void InitTableRing(int iTarget);
void SetOffsetApply(void);
void SetDataShift(BOOL bRightShift);
void DisplayCtrlsRetry(int iTarget);
void TableSetResponse (int iTarget);

void UserSetTblAttr(void);
void SetTblAttr(int iIndex, int nDataLen, int iGroup, BOOL bFixLen, char *pTblName, 
				char R_cUnit, double R_fStep, double R_fInit, char R_cOrder, char R_pDecPlace,
				char V_cUnit, char V_cBitNum, double V_fStep, double V_fForm, double V_fMaxVal, double V_fMinVal, char V_cDecPlace,
				BOOL F_bUse, double F_fStep, double F_fMaxVal, double F_fMinVal, double F_cDecPlace,
				BOOL E_bCaUse, BOOL E_bException, char *E_pTextVo, char *E_pTextAd);



void InitMapTableValue(int iTarget);
void MapTableDataSetup(int iTarget);
void MAP_TableDataDisplay(int iTarget);
void MAP_TableSetResponse (int iTarget);

// Table Backup 包访.. 
BOOL bTableBKMode;
#define	dBackupMake			0
#define	dBackupSet			1
int m_hTblBackUp;
int m_hTblBackCnt;

int	OPTIC_type_ID;

int	Config_LCMS_RCMS;

/**********************************************************/
/*			DOWNLOAD		  							  */				
/**********************************************************/						

// User Define Functions
///////////////////////////////////////////////////////////
void DownOpen(void);
void BinDnCommCheck(UINT8 Command);
void HiddenProgressDlg(void);
void ShowProgressDlg(void);

void DownloadWaitOpen(void);
void DownloadWaitClose(void);
void DownLoadIdDisplay (BYTE *pData);

						
/**********************************************************/
/*			CRC TABLE			  						  */
/**********************************************************/
unsigned short sum_CRC( UINT8 *buf_ptr, int len);

static unsigned short Crc_tbl[256] =
{ 
		0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
		0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
		0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
		0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
		0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
		0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
		0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
		0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
		0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
		0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
		0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
		0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
		0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
		0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
		0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
		0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
		0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
		0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
		0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
		0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
		0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
		0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
		0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
		0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
		0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
		0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
		0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
		0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
		0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
		0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
		0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
		0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0			
};
	



#define	 COMM_SERIAL		1
#define	 COMM_UDP			2


///////////////////////////////////
typedef int (__cdecl *PLENCALLBACK)(int iIdx, BYTE *pLenBuf);
typedef int (__cdecl *PUDPEVENTCALLBACK)(int iIdx, BYTE dwEventFlag, BYTE *pData, int nLength);
typedef int (__cdecl *PUDPDEBUGCALLBACK)(int iIdx, BYTE *pData, int nLength);


typedef struct _UDP_SENDITEM
{
	char 	stIP[16];	//Include NULL
	UINT16  nPort;
	int 	nDataCount;
	BYTE   *pData;
	
}UDP_SENDITEM;

static PLENCALLBACK L_UDP_LengthFunc = NULL;
static PUDPEVENTCALLBACK L_UDP_EventFunc = NULL;
static PUDPDEBUGCALLBACK L_UDP_DebugFunc = NULL;

#define UDP_EVENT_SEND	0x01
#define UDP_EVENT_RECV	0x02	  

#define UDP_MAX_RCV_BUF			1024 * 32	//BYTE
#define UDP_MAX_SEND_PACKET		1024 * 32	//COUNT OF SENDITEM STRUCT

static BOOL L_UDP_bUseSyncCode = FALSE;
static BYTE L_UDP_SyncBuf[32];
static int  L_UDP_SyncLen = 0;

typedef struct
{
	int iHandle;

	int m_iUdpPort;
	char m_cUdpIp[32];
	
	int bConnect;
	
	int hThreadRecvFuncId;
	int hThreadSendFuncId;
	
	UINT8 nLenStartByte;
	UINT8 nLenFieldSize;
	
	int hSocket;
	
	BOOL	bUdpThreadEndFlagRCV;
	BOOL 	L_UDP_bThreadRunRCV;


	HANDLE 	L_UDP_hSendEvent;
	BOOL 	L_UDP_bThreadRunSND;
	BOOL	bUdpThreadEndFlagSND;
	
	int 	L_UDP_nSendItemCount;

	BOOL L_UDP_bCriticalEnter;
	

	PLENCALLBACK L_UDP_LengthFunc;
	PUDPEVENTCALLBACK L_UDP_EventFunc;
	PUDPDEBUGCALLBACK L_UDP_DebugFunc;	
	
	UDP_SENDITEM *L_UDP_SendItemList[UDP_MAX_SEND_PACKET];
	

	BYTE L_UDP_RcvBuff[UDP_MAX_RCV_BUF];
	int L_UDP_nRcvDataSize;
	int L_UDP_nCntPacketSize;
	
	BOOL L_UDP_bDebugMode;  	
	
 	BYTE L_UDP_CopyBuff[UDP_MAX_RCV_BUF];
	int L_UDP_CopyDataLen;
	
} COMMINFO_UDP;

COMMINFO_UDP 	* m_pCOMM_UDP;





typedef struct
{
	int iHandle;

	COMATTR m_ComAttr;	   
	
	
	int bConnect;
	
	int hThreadRecvFuncId;
	int hThreadSendFuncId;
	
} COMMINFO_SER;

COMMINFO_SER 	* m_pCOMM_SER;

typedef struct
{
	int iCommMethod;
	int iSetupInfo[10];

	int iPollingTimer;
	int iWaitTimer;
	int iPortDropTimer;
	
	int iUDPEventTimer;
	int iEventTimer;
	
	int iPanelHandle_DNR;
	int iPanelHandle_Debug;
	int iPanelHandle_Table;
	int iPanelHandle_TableBK;
	int iPanelHandle_TableMap;  	
	int iPanelHandle_SPM;
	int iPanelHandle_DnrSub;
	int iPanelHandle_SPMSub;
	
	int iPanelHandle_IPInfo; 
	
	int iPanelHandle_AlarmMask; //2016.04.23        
	
	/////////////
	int ihWndSSPA;
	int ihWndSPSM;
	int ihWndUCM;
	int ihWndSCM;
	int ihWndSPM;
	int ihWndFAN;
	int ihWndACU;
	int ihWndAntenna;
	
	int ihWndMPSM;
	int ihWndDCM;
	int ihWndFSM;
	
	//////////
	int ihWndAlarmRpt;
	int ihWndBitCheck;
	
	BOOL bArmHisOpenSts;
	int m_iIndexBgn;
	int m_iIndexEnd;
	
	
} COMMINFO;

COMMINFO 	* m_pCOMM;




#define DEF_COMM_CNT	 1


//int CalculLength(BYTE *pLenBuf);
//int OnPacket(BYTE dwEventFlag, BYTE *pData, int  nLength);
BOOL IsValidPacket(void);

int	UDPSendData(int iIdx, BYTE *pData, int nSize, char *stIP, UINT16 nPort); 
void UDPClose(int iIdx);

int CalculLength_UDP(int iIdx, BYTE *pLenBuf);
void PhasePacket_UDP(int iIdx, int nDataSize);
int OnPacket_UDP(int iIdx, BYTE dwEventFlag, BYTE *pData, int  nLength);
void UDPSetMode_Temp(int iIdx, PUDPDEBUGCALLBACK pNotiFunc, BOOL bDebugMode);
int UdpThreadInit (int iData);

void ArmHisOpen(int iTarget);
void ArmHisDataDisplay(int iTarget, unsigned int nCnt);
void AddOneLine(int iTarget,  int iType, unsigned int iIndex, char *pTime, int iTemp, BOOL bArm, char *pArmName);
void ResetList(int iTarget);
void ArmHisPacket(int iTarget, int iCmd);

void BitDataDisplay(int iTarget); 

void BiteLogAddOneLine(int iTarget,  char *pArmName, int bAlarm);
void BiteLogAddOneLineStr(int iTarget, char *pArmName, double bAlarm);

void BITECHECKDisplay(int iIdx);


///
BOOL m_bConnectMsg;


////////

#define DEF_ACK		0x30
#define DEF_NACK	0x31

BOOL m_bLogin[2];
BOOL m_bLoginRst[2];


BOOL m_bPass[2];
BOOL m_bPassRst[2];


UINT8	Polling_Cnt; 



//////////////////
//2015.12.01
void MonitorDataDisplay_OPER(int iWinPart); 
void MonitorDataDisplay_TRANS(int iWinPart); 
void MonitorDataDisplay_ANT(int iWinPart);

void ControlDataDisplay_Tran(int iWinPart); 
void ControlDataDisplay_Oper(int iWinPart); 
void ControlDataDisplay_ACU(int iWinPart); 

void CONTranSetData(int iWinPart) ;
void CONOperSetData(int iWinPart) ;
void CONANTSetData(int iWinPart) ;

void SetCtrlMode_CON (int iIdx, int Ctrl) ;
void SetCtrlMode_SET (int iIdx, int Ctrl);


void SETLimitSetData(int iIdx);
void SETOffsetSetData(int iIdx);
void SETPRISetData(int iIdx);
void SETMAK1SetData(int iIdx);
void SETMAK2SetData(int iIdx);

void SetDataDisplay_Limit(int iIdx);
void SetDataDisplay_Offset(int iIdx);
void SetDataDisplay_PRIMARY(int iIdx);
void SetDataDisplay_MAK1(int iIdx);
void SetDataDisplay_MAK2(int iIdx); 


BOOL OpenNewPanel_Main(int *hWnd, int iPanel, BOOL bAlwaysTop, char *strTitle, char *strUirFile, BOOL bPopup);

void PasswordOpen(void);
void PasswordRspData(void) ;


//////////
void NewEventPacket(int iTarget);
void NewEventAddOneLine(int iTarget, int iType, char *pTime, int iTemp, BOOL bArm, char *pArmName); 
void NewEventDataDisplay(int iTarget);

void BuzzerOn(void);
BOOL m_bBuzzer;
char m_SysPath[1024];


void SaveStatusDataExcel(int iIdx, BOOL bPrint);
void SaveStatusDataCSV(int iIdx, BOOL bPrint);
int m_hWndMsg;

//////////////////
// 2015.12.13
void Freq_Conrol (int control);





///
void AlarmLogFileDel(int iIdx);
int m_hWndEventWait;

void LoginNACKData(int iIdx, int iVal);
void LoginRspData_CHB(void);

#define DEF_LOGINSTEP_ALOGINSEND			0x00
#define DEF_LOGINSTEP_ALOGINRSP				0x01
#define DEF_LOGINSTEP_ALOGINSEND_A			0x02
#define DEF_LOGINSTEP_ALOGINRSP_A			0x03

#define DEF_LOGINSTEP_BLOGINSEND			0x04
#define DEF_LOGINSTEP_BLOGINRSP				0x05
#define DEF_LOGINSTEP_BLOGINSEND_A			0x06
#define DEF_LOGINSTEP_BLOGINRSP_A			0x07

int m_LoginStepData;

#define DEF_LOGINNACK_PASS	0x30	
#define DEF_LOGINNACK_EQUAL	0x31
#define DEF_LOGINNACK_UP	0x32


#define DEF_LED_Black	"\\image\\led\\Black.png"    
#define DEF_LED_ALARM	"\\image\\led\\red.png"
#define DEF_LED_NORMAL	"\\image\\led\\green.png"	
#define DEF_LED_ON		"\\image\\led\\ON.png"
#define DEF_LED_OFF		"\\image\\led\\OFF.png"	

#define	DEF_COLOR_ON 		0x00FF6600L 
#define	DEF_COLOR_OFF   	0x00E0E0E0L
#define	DEF_COLOR_GREEN   	0x0000FF00L  
#define	DEF_COLOR_RED   	0x00E73502L 
#define	DEF_COLOR_ORANGE  	0x00FF9933L   //0x00FF6600L   
#define	DEF_COLOR_BLACK  	0x00000000L
#define	DEF_COLOR_WHITE  	0x00FFFFFFL  
#define	DEF_COLOR_GRAY  	0x00B0B0B0L 



void LastEnvInfoLoad(void);
void LastEnvInfoSave(void);


void StatusAutoSave(void);
void AutoSave(int iTarget);


//////////
int m_hWndEventAutoSaveWait;
void EventAutoDataSave(int iTarget, unsigned int nCnt);
void EventAutoDataSaveAddOneLine(int iTarget, int iType, unsigned int iIndex, char *pTime, int iTemp, BOOL bArm, char *pArmName);
void EventHisPacket(int iTarget, int iCmd) ;

#endif
