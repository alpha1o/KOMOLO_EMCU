#include <utility.h>
#include <userint.h>     
#include <ansi_c.h>      
#include <formatio.h>    
#include "G_MAIN.h"  
#include "H_EXTERN.H"    


void MainDataDisplay(void)
{
	char 	strTemp[64];  
	
	if(m_pDebugSts->TUNNEL_TYPE[0] == 'P') 
	{
			if(m_pDebugSts->TUNNEL_TYPE[1] == '1') SetCtrlVal (m_hWndMain, MAINPNL_RING_TYPE, 		0);
			else	SetCtrlVal (m_hWndMain, MAINPNL_RING_TYPE, 		1); 	
	}
	else	SetCtrlVal (m_hWndMain, MAINPNL_RING_TYPE, 		2); 

	SetCtrlVal (m_hWndMain, MAINPNL_NMR_ID, 		m_pDebugSts->TUNNEL_ID);
		
	memset (strTemp,'\0', sizeof(strTemp));   
	memcpy (strTemp, m_pDebugSts->TUNNEL_NAME, sizeof(m_pDebugSts->TUNNEL_NAME)); //중계기 ID  
	SetCtrlVal(m_hWndMain, MAINPNL_STR_NAME, strTemp);
																				
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_TEMP_HIGH, 			m_pDebugSts->TEMP_A & BITSET(0));	
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_TEMP_LOW, 			m_pDebugSts->TEMP_A & BITSET(1));
	
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_LNA_AM, 			m_pDebugSts->LNA & BITSET(0));	
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_LNA_FM, 			m_pDebugSts->LNA & BITSET(1));
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_LNA_DMB, 			m_pDebugSts->LNA & BITSET(2));
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_LNA_TRS_D, 			m_pDebugSts->LNA & BITSET(3));
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_LNA_TRS_U, 			m_pDebugSts->LNA & BITSET(4));
	
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_HPA_AM1, 			m_pDebugSts->HPA & BITSET(0));	
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_HPA_AM2, 			m_pDebugSts->HPA & BITSET(1));
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_HPA_FM1, 			m_pDebugSts->HPA & BITSET(2));
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_HPA_FM2, 			m_pDebugSts->HPA & BITSET(3));
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_HPA_DMB, 			m_pDebugSts->HPA & BITSET(4));
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_HPA_TRS_D, 			m_pDebugSts->HPA & BITSET(5));	
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_HPA_TRS_U, 			m_pDebugSts->HPA & BITSET(6));

	SetCtrlVal(m_hWndMain,	MAINPNL_LED_DOOR_F, 		m_pDebugSts->DOOR & BITSET(0));	
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_DOOR_R, 		m_pDebugSts->DOOR & BITSET(1));
	
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_FIRE_1, 			m_pDebugSts->FIRE & BITSET(0));	
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_FIRE_2, 			m_pDebugSts->FIRE & BITSET(1));
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_FIRE_3, 			m_pDebugSts->FIRE & BITSET(2));
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_FIRE_4, 			m_pDebugSts->FIRE & BITSET(3));
	
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_FAN_D1, 			m_pDebugSts->FAN_D & BITSET(0));	
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_FAN_D2, 			m_pDebugSts->FAN_D & BITSET(1));
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_FAN_D3, 			m_pDebugSts->FAN_D & BITSET(2));
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_FAN_D4, 			m_pDebugSts->FAN_D & BITSET(3));
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_FAN_D5, 			m_pDebugSts->FAN_D & BITSET(4));
	
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_FAN_I1, 			m_pDebugSts->FAN_I & BITSET(0));	
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_FAN_I2, 			m_pDebugSts->FAN_I & BITSET(1));
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_FAN_I3, 			m_pDebugSts->FAN_I & BITSET(2));
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_FAN_I4, 			m_pDebugSts->FAN_I & BITSET(3));
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_FAN_I5, 			m_pDebugSts->FAN_I & BITSET(4));
	
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_PWR_AC_A, 			m_pDebugSts->POWER & BITSET(0));	
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_PWR_AC_V, 			m_pDebugSts->POWER & BITSET(1));
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_PWR_PSU_P, 			m_pDebugSts->POWER & BITSET(2));
	SetCtrlVal(m_hWndMain,	MAINPNL_LED_PWR_PSU_N, 			m_pDebugSts->POWER & BITSET(3));

	SetCtrlVal (m_hWndMain, MAINPNL_NMR_TEMP, 		(INT16)(double)MAKEWORD(m_pDebugSts->TEMP[0], m_pDebugSts->TEMP[1]));
	
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_PSU_P, 		(double)MAKEWORD(m_pDebugSts->PSU_P[0], m_pDebugSts->PSU_P[1]));
	
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_PSU_N, 		(double)MAKEWORD(m_pDebugSts->PSU_N[0], m_pDebugSts->PSU_N[1]));
	
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_AC_A, 		(double)MAKEWORD(m_pDebugSts->AC_CURRENT[0], m_pDebugSts->AC_CURRENT[1])/1000.);
	
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_AC_V, 		(double)MAKEWORD(m_pDebugSts->AC_VOLTAGE[0], m_pDebugSts->AC_VOLTAGE[1])/1000.);

	
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_TRS_D_PWR, 		(double)MAKEWORD(m_pDebugSts->TRS_PWR[0], m_pDebugSts->TRS_PWR[1])/2.); 
	
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_TRS_U_PWR, 		(double)MAKEWORD(m_pDebugSts->TRS_PWR[0], m_pDebugSts->TRS_PWR[1])/2.); 
	
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_AM_PWR, 		(double)MAKEWORD(m_pDebugSts->AM_PWR[0], m_pDebugSts->AM_PWR[1])/2.); 
	
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_FM_PWR, 		(double)MAKEWORD(m_pDebugSts->SM_PWR[0], m_pDebugSts->SM_PWR[1])/2.); 
	
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_DMB_PWR, 		(double)MAKEWORD(m_pDebugSts->DMB_PWR[0], m_pDebugSts->DMB_PWR[1])/2.); 
	
}




void SYSSet(void)
{
	UINT8		cVal;
	int		iVal = 0; 
	double 	dVal; 
	char 	strTemp[64];
	
	if (!m_hWndENV) return; 

	GetCtrlVal (m_hWndENV,	ENVPNL_RING_TYPE, 		&cVal);
	
	if( iVal == 0)		
	{
		m_pDebugSet->TUNNEL_TYPE[0] = 'P';
		m_pDebugSet->TUNNEL_TYPE[1] = '1';
	}
	else if( iVal == 1)
	{
		m_pDebugSet->TUNNEL_TYPE[0] = 'P';
		m_pDebugSet->TUNNEL_TYPE[1] = '2';
	}
	else
	{
		m_pDebugSet->TUNNEL_TYPE[0] = 'B';
		m_pDebugSet->TUNNEL_TYPE[1] = ' ';
	}
		

	GetCtrlVal (m_hWndENV,	ENVPNL_NMR_ID, 		&cVal);  
	m_pDebugSet->TUNNEL_ID = cVal;
		
	memset (strTemp,'\0', sizeof(strTemp));   
	GetCtrlVal(m_hWndENV, ENVPNL_STR_NAME, strTemp);
	memcpy (m_pDebugSet->TUNNEL_NAME, strTemp, sizeof(m_pDebugSet->TUNNEL_NAME));
	

	GetCtrlVal (m_hWndENV,	ENVPNL_NMR_TEMP, 		&cVal);  
	m_pDebugSet->TESMP_OFFSET = cVal;
	
	GetCtrlVal (m_hWndENV,	ENVPNL_NMR_TEMP_HIGH, 		&cVal);  
	m_pDebugSet->OVER_TEMP_Limit = cVal;
	
	GetCtrlVal (m_hWndENV,	ENVPNL_NMR_TEMP_LOW, 		&cVal);  
	m_pDebugSet->UNDER_TEMP_Limit = cVal;
	
	GetCtrlVal (m_hWndENV,	ENVPNL_NMR_FAN_ON, 		&cVal);  
	m_pDebugSet->FAN_ON_Limit = cVal;
	
	GetCtrlVal (m_hWndENV,	ENVPNL_NMR_FAN_OFF, 		&cVal);  
	m_pDebugSet->FAN_OFF_Limit = cVal;
	

	GetCtrlVal(m_hWndENV, ENVPNL_NMR_PSU_P,	&dVal);
	m_pDebugSet->PSU_P_Limit[0] = LOBYTE((INT16)dVal);
	m_pDebugSet->PSU_P_Limit[1] = HIBYTE((INT16)dVal);

	GetCtrlVal(m_hWndENV, ENVPNL_NMR_PSU_N,	&dVal);
	m_pDebugSet->PSU_N_Limit[0] = LOBYTE((INT16)dVal);
	m_pDebugSet->PSU_N_Limit[1] = HIBYTE((INT16)dVal);
	
	GetCtrlVal(m_hWndENV, ENVPNL_NMR_AC_A,	&dVal);
	m_pDebugSet->AC_CURRENT_Limit[0] = LOBYTE((INT16)dVal);
	m_pDebugSet->AC_CURRENT_Limit[1] = HIBYTE((INT16)dVal);
	
	GetCtrlVal(m_hWndENV, ENVPNL_NMR_AC_V,	&dVal);
	m_pDebugSet->AC_VOLTAGE_Limit[0] = LOBYTE((INT16)dVal);
	m_pDebugSet->AC_VOLTAGE_Limit[1] = HIBYTE((INT16)dVal);
	
					
	SendRequest(0, CONTROL_REQ, m_pDebugSet, sizeof(DEBUG_CONTROL));  
}


void SYSStatusDataDisplay(void)
{
	char 	strTemp[64];
	
	if (!m_hWndENV) return; 
		
	strcpy(strTemp, "");
	Fmt(strTemp, "%d / %d / %d", MAKEWORD(m_pDebugSts->Version[2], m_pDebugSts->Version[3]), m_pDebugSts->Version[1], m_pDebugSts->Version[0]);
	SetCtrlVal(m_hWndENV, ENVPNL_STR_CPU,  strTemp);
	
	strcpy(strTemp, "");
	Fmt(strTemp, "%d / %d / %d", MAKEWORD(m_pDebugSts->EBU_1[2], m_pDebugSts->EBU_1[3]), m_pDebugSts->EBU_1[1], m_pDebugSts->EBU_1[0]);
	SetCtrlVal(m_hWndENV, ENVPNL_STR_EBU1_CPU,  strTemp);
	
	strcpy(strTemp, "");
	Fmt(strTemp, "%d / %d / %d", MAKEWORD(m_pDebugSts->FPGA_1[2], m_pDebugSts->FPGA_1[3]), m_pDebugSts->FPGA_1[1], m_pDebugSts->FPGA_1[0]);
	SetCtrlVal(m_hWndENV, ENVPNL_STR_EBU1_FPGA,  strTemp);
	
	strcpy(strTemp, "");
	Fmt(strTemp, "%d / %d / %d", MAKEWORD(m_pDebugSts->EBU_2[2], m_pDebugSts->EBU_2[3]), m_pDebugSts->EBU_2[1], m_pDebugSts->EBU_2[0]);
	SetCtrlVal(m_hWndENV, ENVPNL_STR_EBU2_CPU,  strTemp);
	
	strcpy(strTemp, "");
	Fmt(strTemp, "%d / %d / %d", MAKEWORD(m_pDebugSts->FPGA_2[2], m_pDebugSts->FPGA_2[3]), m_pDebugSts->FPGA_2[1], m_pDebugSts->FPGA_2[0]);
	SetCtrlVal(m_hWndENV, ENVPNL_STR_EBU2_FPGA,  strTemp);
	
	strcpy(strTemp, "");
	Fmt(strTemp, "%d / %d / %d", MAKEWORD(m_pDebugSts->AM_L_1[2], m_pDebugSts->AM_L_1[3]), m_pDebugSts->AM_L_1[1], m_pDebugSts->AM_L_1[0]);
	SetCtrlVal(m_hWndENV, ENVPNL_STR_AM_CPU,  strTemp);
	
	strcpy(strTemp, "");
	Fmt(strTemp, "%d / %d / %d", MAKEWORD(m_pDebugSts->AM_FPGA_L1[2], m_pDebugSts->AM_FPGA_L1[3]), m_pDebugSts->AM_FPGA_L1[1], m_pDebugSts->AM_FPGA_L1[0]);
	SetCtrlVal(m_hWndENV, ENVPNL_STR_AM_FPGA,  strTemp);
	
	
	strcpy(strTemp, "");
	Fmt(strTemp, "%d / %d / %d", MAKEWORD(m_pDebugSts->FM_L_1[2], m_pDebugSts->FM_L_1[3]), m_pDebugSts->FM_L_1[1], m_pDebugSts->FM_L_1[0]);
	SetCtrlVal(m_hWndENV, ENVPNL_STR_FM_CPU,  strTemp);
	
	strcpy(strTemp, "");
	Fmt(strTemp, "%d / %d / %d", MAKEWORD(m_pDebugSts->FM_FPGA_L1[2], m_pDebugSts->FM_FPGA_L1[3]), m_pDebugSts->FM_FPGA_L1[1], m_pDebugSts->FM_FPGA_L1[0]);
	SetCtrlVal(m_hWndENV, ENVPNL_STR_FM_FPGA,  strTemp);
	
	strcpy(strTemp, "");
	Fmt(strTemp, "%d / %d / %d", MAKEWORD(m_pDebugSts->DMB_L_1[2], m_pDebugSts->DMB_L_1[3]), m_pDebugSts->DMB_L_1[1], m_pDebugSts->DMB_L_1[0]);
	SetCtrlVal(m_hWndENV, ENVPNL_STR_DMB_CPU,  strTemp);
	
	strcpy(strTemp, "");
	Fmt(strTemp, "%d / %d / %d", MAKEWORD(m_pDebugSts->DMB_FPGA_L1[2], m_pDebugSts->DMB_FPGA_L1[3]), m_pDebugSts->DMB_FPGA_L1[1], m_pDebugSts->DMB_FPGA_L1[0]);
	SetCtrlVal(m_hWndENV, ENVPNL_STR_DMB_FPGA,  strTemp);
	
	strcpy(strTemp, "");
	Fmt(strTemp, "%d / %d / %d", MAKEWORD(m_pDebugSts->TRS_L_1[2], m_pDebugSts->TRS_L_1[3]), m_pDebugSts->TRS_L_1[1], m_pDebugSts->TRS_L_1[0]);
	SetCtrlVal(m_hWndENV, ENVPNL_STR_TRS_CPU,  strTemp);
	
	strcpy(strTemp, "");
	Fmt(strTemp, "%d / %d / %d", MAKEWORD(m_pDebugSts->TRS_FPGA_L1[2], m_pDebugSts->TRS_FPGA_L1[3]), m_pDebugSts->TRS_FPGA_L1[1], m_pDebugSts->TRS_FPGA_L1[0]);
	SetCtrlVal(m_hWndENV, ENVPNL_STR_TRS_FPGA,  strTemp);
	
	if(m_pDebugSts->TUNNEL_TYPE[0] == 'P') 
	{
			if(m_pDebugSts->TUNNEL_TYPE[1] == '1') SetCtrlVal (m_hWndENV, ENVPNL_RING_TYPE, 		0);
			else	SetCtrlVal (m_hWndENV, ENVPNL_RING_TYPE, 		1); 	
	}
	else	SetCtrlVal (m_hWndENV, ENVPNL_RING_TYPE, 		2); 

	SetCtrlVal (m_hWndENV, ENVPNL_NMR_ID, 		m_pDebugSts->TUNNEL_ID);
		
	memset (strTemp,'\0', sizeof(strTemp));   
	memcpy (strTemp, m_pDebugSts->TUNNEL_NAME, sizeof(m_pDebugSts->TUNNEL_NAME)); //중계기 ID  
	SetCtrlVal(m_hWndENV, ENVPNL_STR_NAME, strTemp);

	
	SetCtrlVal (m_hWndENV, ENVPNL_NMR_TEMP, 		m_pDebugSts->TEMP_OFFSET);
	
	SetCtrlVal (m_hWndENV, ENVPNL_NMR_TEMP_HIGH, 	m_pDebugSts->OVER_TEMP_Limit); 
	
	SetCtrlVal (m_hWndENV, ENVPNL_NMR_TEMP_LOW, 	m_pDebugSts->UNDER_TEMP_Limit); 
																					
	SetCtrlVal (m_hWndENV, ENVPNL_NMR_FAN_ON, 		m_pDebugSts->FAN_ON_Limit); 
	
	SetCtrlVal (m_hWndENV, ENVPNL_NMR_FAN_OFF, 		m_pDebugSts->FAN_OFF_Limit); 
	
	SetCtrlVal (m_hWndENV, ENVPNL_NMR_PSU_P, 		(double)MAKEWORD(m_pDebugSts->PSU_P_Limit[0], m_pDebugSts->PSU_P_Limit[1]));
	
	SetCtrlVal (m_hWndENV, ENVPNL_NMR_PSU_N, 		(double)MAKEWORD(m_pDebugSts->PSU_N_Limit[0], m_pDebugSts->PSU_N_Limit[1]));
	
	SetCtrlVal (m_hWndENV, ENVPNL_NMR_AC_A, 		(double)MAKEWORD(m_pDebugSts->AC_CURRENT_Limit[1], m_pDebugSts->AC_CURRENT_Limit[0])/1000.);
	
	SetCtrlVal (m_hWndENV, ENVPNL_NMR_AC_V, 		(double)MAKEWORD(m_pDebugSts->AC_VOLTAGE_Limit[0], m_pDebugSts->AC_VOLTAGE_Limit[1])/1000.);
	


}

//-------------------------------------------------------------------------------//
// PORT Status..!
//-------------------------------------------------------------------------------//

void HPASetData(void) ////////////////////////////////////////
{
	int		iVal = 0;
	double 	dVal; 
	
	
	if (!m_hWndHPA) return;
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_TG_AMP_AM1, 		&iVal);
	m_pHPASet->AM1_AMP = iVal;
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_TG_ALC_AM1, 		&iVal);
	m_pHPASet->AM1_ALC = iVal;
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_ALC_AM1,	&dVal);
	m_pHPASet->AM1_ALC_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->AM1_ALC_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_SetAttn_AM1,	&dVal);
	m_pHPASet->AM1_OUT_ATTN[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->AM1_OUT_ATTN[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_ASD_AM1,	&dVal);
	m_pHPASet->AM1_ASD_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->AM1_ASD_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_TEMP_AM1, 		&dVal);
	m_pHPASet->AM1_ASD_TEMP = dVal;
	
	//------------------------------------------------------------------
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_TG_AMP_AM2, 		&iVal);
	m_pHPASet->AM2_AMP = iVal;
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_TG_ALC_AM2, 		&iVal);
	m_pHPASet->AM2_ALC = iVal;
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_ALC_AM2,	&dVal);
	m_pHPASet->AM2_ALC_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->AM2_ALC_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_SetAttn_AM2,	&dVal);
	m_pHPASet->AM2_OUT_ATTN[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->AM2_OUT_ATTN[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_ASD_AM2,	&dVal);
	m_pHPASet->AM2_ASD_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->AM2_ASD_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_TEMP_AM2, 		&dVal);
	m_pHPASet->AM2_ASD_TEMP = dVal;
	
	//------------------------------------------------------------------
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_TG_AMP_FM1, 		&iVal);
	m_pHPASet->FM1_AMP = iVal;
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_TG_ALC_FM1, 		&iVal);
	m_pHPASet->FM1_ALC = iVal;
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_ALC_FM1,	&dVal);
	m_pHPASet->FM1_ALC_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->FM1_ALC_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_SetAttn_FM1,	&dVal);
	m_pHPASet->FM1_OUT_ATTN[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->FM1_OUT_ATTN[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_ASD_FM1,	&dVal);
	m_pHPASet->FM1_ASD_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->FM1_ASD_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_TEMP_FM1, 		&dVal);
	m_pHPASet->FM1_ASD_TEMP = dVal;
	
	//------------------------------------------------------------------
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_TG_AMP_FM2, 		&iVal);
	m_pHPASet->FM2_AMP = iVal;
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_TG_ALC_FM2, 		&iVal);
	m_pHPASet->FM2_ALC = iVal;
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_ALC_FM2,	&dVal);
	m_pHPASet->FM2_ALC_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->FM2_ALC_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_SetAttn_FM2,	&dVal);
	m_pHPASet->FM2_OUT_ATTN[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->FM2_OUT_ATTN[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_ASD_FM2,	&dVal);
	m_pHPASet->FM2_ASD_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->FM2_ASD_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_TEMP_FM2, 		&dVal);
	m_pHPASet->FM2_ASD_TEMP = dVal;
	
	//------------------------------------------------------------------
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_TG_AMP_DMB, 		&iVal);
	m_pHPASet->DMB_AMP = iVal;
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_TG_ALC_DMB, 		&iVal);
	m_pHPASet->DMB_ALC = iVal;
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_ALC_DMB,	&dVal);
	m_pHPASet->DMB_ALC_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->DMB_ALC_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_SetAttn_DMB,	&dVal);
	m_pHPASet->DMB_OUT_ATTN[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->DMB_OUT_ATTN[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_ASD_DMB,	&dVal);
	m_pHPASet->DMB_ASD_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->DMB_ASD_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_TEMP_DMB, 		&dVal);
	m_pHPASet->DMB_ASD_TEMP = dVal;
	
	//------------------------------------------------------------------
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_TG_AMP_TRS_DL, 		&iVal);
	m_pHPASet->TRS_DL_AMP = iVal;
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_TG_ALC_TRS_DL, 		&iVal);
	m_pHPASet->TRS_DL_ALC = iVal;
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_ALC_TRS_DL,	&dVal);
	m_pHPASet->TRS_DL_ALC_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->TRS_DL_ALC_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_SetAttn_TRS_DL,	&dVal);
	m_pHPASet->TRS_DL_OUT_ATTN[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->TRS_DL_OUT_ATTN[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_ASD_TRS_DL,	&dVal);
	m_pHPASet->TRS_DL_ASD_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->TRS_DL_ASD_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_TEMP_TRS_DL, 		&dVal);
	m_pHPASet->TRS_DL_ASD_TEMP = dVal;
	
	//------------------------------------------------------------------
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_TG_AMP_TRS_UL, 		&iVal);
	m_pHPASet->TRS_UL_AMP = iVal;
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_TG_ALC_TRS_UL, 		&iVal);
	m_pHPASet->TRS_UL_ALC = iVal;
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_ALC_TRS_UL,	&dVal);
	m_pHPASet->TRS_UL_ALC_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->TRS_UL_ALC_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_SetAttn_TRS_UL,	&dVal);
	m_pHPASet->TRS_UL_OUT_ATTN[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->TRS_UL_OUT_ATTN[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal(m_hWndHPA, HPAPNL_NMR_ASD_TRS_UL,	&dVal);
	m_pHPASet->TRS_UL_ASD_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pHPASet->TRS_UL_ASD_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_TEMP_TRS_UL, 		&dVal);
	m_pHPASet->TRS_UL_ASD_TEMP = dVal;
	

			
	SendRequest(0, HPA_CONTROL_REQ, m_pHPASet, sizeof(HPA_CONTROL));
	
		
}

void HPASetDataDisplay(void)
{

	if( m_pHPASet->AM1_AMP == 0)	SetCtrlVal(m_hWndMain, MAINPNL_STR_AM1,  "OFF");
	else							SetCtrlVal(m_hWndMain, MAINPNL_STR_AM1,  "ON");
	
	if( m_pHPASet->AM2_AMP == 0)	SetCtrlVal(m_hWndMain, MAINPNL_STR_AM2,  "OFF");
	else							SetCtrlVal(m_hWndMain, MAINPNL_STR_AM2,  "ON");
	
	if( m_pHPASet->FM1_AMP == 0)	SetCtrlVal(m_hWndMain, MAINPNL_STR_FM1,  "OFF");
	else							SetCtrlVal(m_hWndMain, MAINPNL_STR_FM1,  "ON");
	
	if( m_pHPASet->FM2_AMP == 0)	SetCtrlVal(m_hWndMain, MAINPNL_STR_FM2,  "OFF");
	else							SetCtrlVal(m_hWndMain, MAINPNL_STR_FM2,  "ON");
	
	if( m_pHPASet->DMB_AMP == 0)	SetCtrlVal(m_hWndMain, MAINPNL_STR_DMB,  "OFF");
	else							SetCtrlVal(m_hWndMain, MAINPNL_STR_DMB,  "ON");
	
	if( m_pHPASet->TRS_DL_AMP == 0)	SetCtrlVal(m_hWndMain, MAINPNL_STR_TRS_DL,  "OFF");
	else							SetCtrlVal(m_hWndMain, MAINPNL_STR_TRS_DL,  "ON");
	
	if (!m_hWndHPA) return;
	
	SetCtrlVal (m_hWndHPA,	HPAPNL_TG_AMP_AM1, 		m_pHPASet->AM1_AMP);
	SetCtrlVal (m_hWndHPA,	HPAPNL_TG_ALC_AM1, 		m_pHPASet->AM1_ALC);
	
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ALC_AM1, 		(double)MAKEWORD(m_pHPASet->AM1_ALC_LEVEL[0], m_pHPASet->AM1_ALC_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_SetAttn_AM1, 	(double)MAKEWORD(m_pHPASet->AM1_OUT_ATTN[0], m_pHPASet->AM1_OUT_ATTN[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_AM1, 		(double)MAKEWORD(m_pHPASet->AM1_ASD_LEVEL[0], m_pHPASet->AM1_ASD_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_TEMP_AM1, 	(double)(m_pHPASet->AM1_ASD_TEMP));
	

	SetCtrlVal (m_hWndHPA,	HPAPNL_TG_AMP_AM2, 		m_pHPASet->AM2_AMP);
	SetCtrlVal (m_hWndHPA,	HPAPNL_TG_ALC_AM2, 		m_pHPASet->AM2_ALC);
	
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ALC_AM2, 		(double)MAKEWORD(m_pHPASet->AM2_ALC_LEVEL[0], m_pHPASet->AM2_ALC_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_SetAttn_AM2, 	(double)MAKEWORD(m_pHPASet->AM2_OUT_ATTN[0], m_pHPASet->AM2_OUT_ATTN[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_AM2, 		(double)MAKEWORD(m_pHPASet->AM2_ASD_LEVEL[0], m_pHPASet->AM2_ASD_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_TEMP_AM2, 	(double)(m_pHPASet->AM2_ASD_TEMP));
	

	SetCtrlVal (m_hWndHPA,	HPAPNL_TG_AMP_FM1, 		m_pHPASet->FM1_AMP);
	SetCtrlVal (m_hWndHPA,	HPAPNL_TG_ALC_FM1, 		m_pHPASet->FM1_ALC);
	
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ALC_FM1, 		(double)MAKEWORD(m_pHPASet->FM1_ALC_LEVEL[0], m_pHPASet->FM1_ALC_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_SetAttn_FM1, 	(double)MAKEWORD(m_pHPASet->FM1_OUT_ATTN[0], m_pHPASet->FM1_OUT_ATTN[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_FM1, 		(double)MAKEWORD(m_pHPASet->FM1_ASD_LEVEL[0], m_pHPASet->FM1_ASD_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_TEMP_FM1, 	(double)(m_pHPASet->FM1_ASD_TEMP));
	
	SetCtrlVal (m_hWndHPA,	HPAPNL_TG_AMP_FM2, 		m_pHPASet->FM2_AMP);
	SetCtrlVal (m_hWndHPA,	HPAPNL_TG_ALC_FM2, 		m_pHPASet->FM2_ALC);
	
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ALC_FM2, 		(double)MAKEWORD(m_pHPASet->FM2_ALC_LEVEL[0], m_pHPASet->FM2_ALC_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_SetAttn_FM2, 	(double)MAKEWORD(m_pHPASet->FM2_OUT_ATTN[0], m_pHPASet->FM2_OUT_ATTN[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_FM2, 		(double)MAKEWORD(m_pHPASet->FM2_ASD_LEVEL[0], m_pHPASet->FM2_ASD_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_TEMP_FM2, 	(double)(m_pHPASet->FM2_ASD_TEMP));
	

	SetCtrlVal (m_hWndHPA,	HPAPNL_TG_AMP_DMB, 		m_pHPASet->DMB_AMP);
	SetCtrlVal (m_hWndHPA,	HPAPNL_TG_ALC_DMB, 		m_pHPASet->DMB_ALC);
	
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ALC_DMB, 		(double)MAKEWORD(m_pHPASet->DMB_ALC_LEVEL[0], m_pHPASet->DMB_ALC_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_SetAttn_DMB, 	(double)MAKEWORD(m_pHPASet->DMB_OUT_ATTN[0], m_pHPASet->DMB_OUT_ATTN[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_DMB, 		(double)MAKEWORD(m_pHPASet->DMB_ASD_LEVEL[0], m_pHPASet->DMB_ASD_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_TEMP_DMB, 	(double)(m_pHPASet->DMB_ASD_TEMP));
	

	SetCtrlVal (m_hWndHPA,	HPAPNL_TG_AMP_TRS_DL, 		m_pHPASet->TRS_DL_AMP);
	SetCtrlVal (m_hWndHPA,	HPAPNL_TG_ALC_TRS_DL, 		m_pHPASet->TRS_DL_ALC);
	
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ALC_TRS_DL, 		(double)MAKEWORD(m_pHPASet->TRS_DL_ALC_LEVEL[0], m_pHPASet->TRS_DL_ALC_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_SetAttn_TRS_DL, 	(double)MAKEWORD(m_pHPASet->TRS_DL_OUT_ATTN[0], m_pHPASet->TRS_DL_OUT_ATTN[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_TRS_DL, 		(double)MAKEWORD(m_pHPASet->TRS_DL_ASD_LEVEL[0], m_pHPASet->TRS_DL_ASD_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_TEMP_TRS_DL, 	(double)(m_pHPASet->TRS_DL_ASD_TEMP));
	
		   
	SetCtrlVal (m_hWndHPA,	HPAPNL_TG_AMP_TRS_UL, 		m_pHPASet->TRS_UL_AMP);
	SetCtrlVal (m_hWndHPA,	HPAPNL_TG_ALC_TRS_UL, 		m_pHPASet->TRS_UL_ALC);
	
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ALC_TRS_UL, 		(double)MAKEWORD(m_pHPASet->TRS_UL_ALC_LEVEL[0], m_pHPASet->TRS_UL_ALC_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_SetAttn_TRS_UL, 	(double)MAKEWORD(m_pHPASet->TRS_UL_OUT_ATTN[0], m_pHPASet->TRS_UL_OUT_ATTN[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_TRS_UL, 		(double)MAKEWORD(m_pHPASet->TRS_UL_ASD_LEVEL[0], m_pHPASet->TRS_UL_ASD_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ASD_TEMP_TRS_UL, 	(double)(m_pHPASet->TRS_UL_ASD_TEMP));
	
	
}


void HPAStatusDataDisplay(void)
{
	
	
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_HPA_AM1, 		(double)MAKEWORD(m_pHPASts->AM1_oPower[0], m_pHPASts->AM1_oPower[1]) /2. );  
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_HPA_AM2, 		(double)MAKEWORD(m_pHPASts->AM2_oPower[0], m_pHPASts->AM2_oPower[1]) /2. );  
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_HPA_FM1, 		(double)MAKEWORD(m_pHPASts->FM1_oPower[0], m_pHPASts->FM1_oPower[1]) /2. );  
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_HPA_FM2, 		(double)MAKEWORD(m_pHPASts->FM2_oPower[0], m_pHPASts->FM2_oPower[1]) /2. );  
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_HPA_DMB, 		(double)MAKEWORD(m_pHPASts->DMB_oPower[0], m_pHPASts->DMB_oPower[1]) /2. );  
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_HPA_TRS_D, 		(double)MAKEWORD(m_pHPASts->TRS_DL_oPower[0], m_pHPASts->TRS_DL_oPower[1]) /2. );  
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_HPA_TRS_U, 		(double)MAKEWORD(m_pHPASts->TRS_UL_oPower[0], m_pHPASts->TRS_UL_oPower[1]) /2. );  
	
	
	if (!m_hWndHPA) return;  
							  
	
	SetCtrlVal (m_hWndHPA,	HPAPNL_LED_ASD_AM1, 			m_pHPASts->AM1_ASD );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_Temp_AM1, 			(INT8)m_pHPASts->AM1_Temp ); 
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_PWR_AM1, 			(double)MAKEWORD(m_pHPASts->AM1_oPower[0], m_pHPASts->AM1_oPower[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ATTN_AM1, 			(double)MAKEWORD(m_pHPASts->AM1_oAttn[0], m_pHPASts->AM1_oAttn[1]) /2. ); 
	SetCtrlVal (m_hWndHPA,	HPAPNL_LED_PWR_AM1, 			m_pHPASts->AM1_OverPwr ); 
	SetCtrlVal (m_hWndHPA,	HPAPNL_LED_TEMP_AM1, 			m_pHPASts->AM1_OverTemp );
	
	SetCtrlVal (m_hWndHPA,	HPAPNL_LED_ASD_AM2, 				m_pHPASts->AM2_ASD );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_Temp_AM2, 			(INT8)m_pHPASts->AM2_Temp ); 
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_PWR_AM2, 			(double)MAKEWORD(m_pHPASts->AM2_oPower[0], m_pHPASts->AM2_oPower[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ATTN_AM2, 			(double)MAKEWORD(m_pHPASts->AM2_oAttn[0], m_pHPASts->AM2_oAttn[1]) /2. ); 
	SetCtrlVal (m_hWndHPA,	HPAPNL_LED_PWR_AM2, 			m_pHPASts->AM2_OverPwr ); 
	SetCtrlVal (m_hWndHPA,	HPAPNL_LED_TEMP_AM2, 			m_pHPASts->AM2_OverTemp );
	
	SetCtrlVal (m_hWndHPA,	HPAPNL_LED_ASD_FM1, 			m_pHPASts->FM1_ASD );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_Temp_FM1, 			(INT8)m_pHPASts->AM1_Temp ); 
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_PWR_FM1, 			(double)MAKEWORD(m_pHPASts->FM1_oPower[0], m_pHPASts->FM1_oPower[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ATTN_FM1, 			(double)MAKEWORD(m_pHPASts->FM1_oAttn[0], m_pHPASts->FM1_oAttn[1]) /2. ); 
	SetCtrlVal (m_hWndHPA,	HPAPNL_LED_PWR_FM1, 			m_pHPASts->FM1_OverPwr ); 
	SetCtrlVal (m_hWndHPA,	HPAPNL_LED_TEMP_FM1, 			m_pHPASts->FM1_OverTemp );
	
	SetCtrlVal (m_hWndHPA,	HPAPNL_LED_ASD_FM2, 			m_pHPASts->FM2_ASD );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_Temp_FM2, 			(INT8)m_pHPASts->FM2_Temp ); 
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_PWR_FM2, 			(double)MAKEWORD(m_pHPASts->FM2_oPower[0], m_pHPASts->FM2_oPower[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ATTN_FM2, 			(double)MAKEWORD(m_pHPASts->FM2_oAttn[0], m_pHPASts->FM2_oAttn[1]) /2. ); 
	SetCtrlVal (m_hWndHPA,	HPAPNL_LED_PWR_FM2, 			m_pHPASts->FM2_OverPwr ); 
	SetCtrlVal (m_hWndHPA,	HPAPNL_LED_TEMP_FM2, 			m_pHPASts->FM2_OverTemp );
	
	SetCtrlVal (m_hWndHPA,	HPAPNL_LED_ASD_TRS_DL, 			m_pHPASts->TRS_DL_ASD );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_Temp_TRS_DL, 		(INT8)m_pHPASts->TRS_DL_Temp ); 
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_PWR_TRS_DL, 			(double)MAKEWORD(m_pHPASts->TRS_DL_oPower[0], m_pHPASts->TRS_DL_oPower[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ATTN_TRS_DL, 		(double)MAKEWORD(m_pHPASts->TRS_DL_oAttn[0], m_pHPASts->TRS_DL_oAttn[1]) /2. ); 
	SetCtrlVal (m_hWndHPA,	HPAPNL_LED_PWR_TRS_DL, 			m_pHPASts->TRS_DL_OverPwr ); 
	SetCtrlVal (m_hWndHPA,	HPAPNL_LED_TEMP_TRS_DL, 		m_pHPASts->TRS_DL_OverTemp );
	
						
	SetCtrlVal (m_hWndHPA,	HPAPNL_LED_ASD_TRS_UL, 			m_pHPASts->TRS_UL_ASD );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_Temp_TRS_UL, 		(INT8)m_pHPASts->TRS_UL_Temp ); 
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_PWR_TRS_UL, 			(double)MAKEWORD(m_pHPASts->TRS_UL_oPower[0], m_pHPASts->TRS_UL_oPower[1]) /2. );
	SetCtrlVal (m_hWndHPA,	HPAPNL_NMR_ATTN_TRS_UL, 		(double)MAKEWORD(m_pHPASts->TRS_UL_oAttn[0], m_pHPASts->TRS_UL_oAttn[1]) /2. ); 
	SetCtrlVal (m_hWndHPA,	HPAPNL_LED_PWR_TRS_UL, 			m_pHPASts->TRS_UL_OverPwr ); 
	SetCtrlVal (m_hWndHPA,	HPAPNL_LED_TEMP_TRS_UL, 		m_pHPASts->TRS_UL_OverTemp );

}


//-------------------------------------------------------------------------------//
// AMP Status..!
//-------------------------------------------------------------------------------//

int AMP_ID[20] 	= {

		AMPPNL_RING_ID_0,	AMPPNL_RING_ID_1,	AMPPNL_RING_ID_2,	AMPPNL_RING_ID_3,	AMPPNL_RING_ID_4,
		AMPPNL_RING_ID_5,	AMPPNL_RING_ID_6,	AMPPNL_RING_ID_7,	AMPPNL_RING_ID_8,	AMPPNL_RING_ID_9,
		AMPPNL_RING_ID_10,	AMPPNL_RING_ID_11,	AMPPNL_RING_ID_12,	AMPPNL_RING_ID_13,	AMPPNL_RING_ID_14,
		AMPPNL_RING_ID_15,	AMPPNL_RING_ID_16,	AMPPNL_RING_ID_17,	AMPPNL_RING_ID_18,	AMPPNL_RING_ID_19,
};

int AMP_DIR[20] 	= {

		AMPPNL_TG_DIR_0,	AMPPNL_TG_DIR_1,	AMPPNL_TG_DIR_2,	AMPPNL_TG_DIR_3,	AMPPNL_TG_DIR_4,
		AMPPNL_TG_DIR_5,	AMPPNL_TG_DIR_16,	AMPPNL_TG_DIR_7,	AMPPNL_TG_DIR_8,	AMPPNL_TG_DIR_9,
		AMPPNL_TG_DIR_10,	AMPPNL_TG_DIR_11,	AMPPNL_TG_DIR_12,	AMPPNL_TG_DIR_13,	AMPPNL_TG_DIR_14,
		AMPPNL_TG_DIR_15,	AMPPNL_TG_DIR_16,	AMPPNL_TG_DIR_17,	AMPPNL_TG_DIR_18,	AMPPNL_TG_DIR_19,
};

int AMP_TYPE[20] 	= {

		AMPPNL_RING_TYPE_0,		AMPPNL_RING_TYPE_1,		AMPPNL_RING_TYPE_2,		AMPPNL_RING_TYPE_3,		AMPPNL_RING_TYPE_4,
		AMPPNL_RING_TYPE_5,		AMPPNL_RING_TYPE_6,		AMPPNL_RING_TYPE_7,		AMPPNL_RING_TYPE_8,		AMPPNL_RING_TYPE_9,
		AMPPNL_RING_TYPE_10,	AMPPNL_RING_TYPE_11,	AMPPNL_RING_TYPE_12,	AMPPNL_RING_TYPE_13,	AMPPNL_RING_TYPE_14,
		AMPPNL_RING_TYPE_15,	AMPPNL_RING_TYPE_16,	AMPPNL_RING_TYPE_17,	AMPPNL_RING_TYPE_18,	AMPPNL_RING_TYPE_19,
};


int AMP_ON[20] 	= {

		AMPPNL_TG_AMP_0,	AMPPNL_TG_AMP_1,	AMPPNL_TG_AMP_2,	AMPPNL_TG_AMP_3,	AMPPNL_TG_AMP_4,
		AMPPNL_TG_AMP_5,	AMPPNL_TG_AMP_6,	AMPPNL_TG_AMP_7,	AMPPNL_TG_AMP_8,	AMPPNL_TG_AMP_9,
		AMPPNL_TG_AMP_10,	AMPPNL_TG_AMP_11,	AMPPNL_TG_AMP_12,	AMPPNL_TG_AMP_13,	AMPPNL_TG_AMP_14,
		AMPPNL_TG_AMP_15,	AMPPNL_TG_AMP_16,	AMPPNL_TG_AMP_17,	AMPPNL_TG_AMP_18,	AMPPNL_TG_AMP_19,
};


int AMP_ALC[20] 	= {

		AMPPNL_TG_ALC_0,	AMPPNL_TG_ALC_1,	AMPPNL_TG_ALC_2,	AMPPNL_TG_ALC_3,	AMPPNL_TG_ALC_4,	
		AMPPNL_TG_ALC_5,	AMPPNL_TG_ALC_6,	AMPPNL_TG_ALC_7,	AMPPNL_TG_ALC_8,	AMPPNL_TG_ALC_9,
		AMPPNL_TG_ALC_10,	AMPPNL_TG_ALC_11,	AMPPNL_TG_ALC_12,	AMPPNL_TG_ALC_13,	AMPPNL_TG_ALC_14,
		AMPPNL_TG_ALC_15,	AMPPNL_TG_ALC_16,	AMPPNL_TG_ALC_17,	AMPPNL_TG_ALC_18,	AMPPNL_TG_ALC_19,
};

int AMP_ALC_LEVEL[20] 	= {

		AMPPNL_NMR_ALC_0,	AMPPNL_NMR_ALC_1,	AMPPNL_NMR_ALC_2,	AMPPNL_NMR_ALC_3,	AMPPNL_NMR_ALC_4,
		AMPPNL_NMR_ALC_5,	AMPPNL_NMR_ALC_6,	AMPPNL_NMR_ALC_7,	AMPPNL_NMR_ALC_8,	AMPPNL_NMR_ALC_9,
		AMPPNL_NMR_ALC_10,	AMPPNL_NMR_ALC_11,	AMPPNL_NMR_ALC_12,	AMPPNL_NMR_ALC_13,	AMPPNL_NMR_ALC_14,
		AMPPNL_NMR_ALC_15,	AMPPNL_NMR_ALC_16,	AMPPNL_NMR_ALC_17,	AMPPNL_NMR_ALC_18,	AMPPNL_NMR_ALC_19,
};

int AMP_OutAtt[20] 	= {

		AMPPNL_NMR_ATTN_0,	AMPPNL_NMR_ATTN_1,	AMPPNL_NMR_ATTN_2,	AMPPNL_NMR_ATTN_3,	AMPPNL_NMR_ATTN_4,
		AMPPNL_NMR_ATTN_5,	AMPPNL_NMR_ATTN_6,	AMPPNL_NMR_ATTN_7,	AMPPNL_NMR_ATTN_8,	AMPPNL_NMR_ATTN_9,
		AMPPNL_NMR_ATTN_10,	AMPPNL_NMR_ATTN_11,	AMPPNL_NMR_ATTN_12,	AMPPNL_NMR_ATTN_13,	AMPPNL_NMR_ATTN_14,
		AMPPNL_NMR_ATTN_15,	AMPPNL_NMR_ATTN_16,	AMPPNL_NMR_ATTN_17,	AMPPNL_NMR_ATTN_18,	AMPPNL_NMR_ATTN_19,
};

int AMP_LED_ALARM[20] 	= {

		AMPPNL_LED_ALM_0,	AMPPNL_LED_ALM_1,	AMPPNL_LED_ALM_2,	AMPPNL_LED_ALM_3,	AMPPNL_LED_ALM_4,
		AMPPNL_LED_ALM_5,	AMPPNL_LED_ALM_6,	AMPPNL_LED_ALM_7,	AMPPNL_LED_ALM_8,	AMPPNL_LED_ALM_9,
		AMPPNL_LED_ALM_10,	AMPPNL_LED_ALM_11,	AMPPNL_LED_ALM_12,	AMPPNL_LED_ALM_13,	AMPPNL_LED_ALM_14,
		AMPPNL_LED_ALM_15,	AMPPNL_LED_ALM_16,	AMPPNL_LED_ALM_17,	AMPPNL_LED_ALM_18,	AMPPNL_LED_ALM_19,
};

int AMP_LED_ASD[20] 	= {

		AMPPNL_LED_ASD_0,	AMPPNL_LED_ASD_1,	AMPPNL_LED_ASD_2,	AMPPNL_LED_ASD_3,	AMPPNL_LED_ASD_4,
		AMPPNL_LED_ASD_5,	AMPPNL_LED_ASD_6,	AMPPNL_LED_ASD_7,	AMPPNL_LED_ASD_8,	AMPPNL_LED_ASD_9,
		AMPPNL_LED_ASD_10,	AMPPNL_LED_ASD_11,	AMPPNL_LED_ASD_12,	AMPPNL_LED_ASD_13,	AMPPNL_LED_ASD_14,
		AMPPNL_LED_ASD_15,	AMPPNL_LED_ASD_16,	AMPPNL_LED_ASD_17,	AMPPNL_LED_ASD_18,	AMPPNL_LED_ASD_19,
};


int AMP_TEMP[20] 	= {

		AMPPNL_NMR_TEMP_0,	AMPPNL_NMR_TEMP_1,	AMPPNL_NMR_TEMP_2,	AMPPNL_NMR_TEMP_3,	AMPPNL_NMR_TEMP_4,
		AMPPNL_NMR_TEMP_5,	AMPPNL_NMR_TEMP_6,	AMPPNL_NMR_TEMP_7,	AMPPNL_NMR_TEMP_8,	AMPPNL_NMR_TEMP_9,
		AMPPNL_NMR_TEMP_10,	AMPPNL_NMR_TEMP_11,	AMPPNL_NMR_TEMP_12,	AMPPNL_NMR_TEMP_13,	AMPPNL_NMR_TEMP_14,
		AMPPNL_NMR_TEMP_15,	AMPPNL_NMR_TEMP_16,	AMPPNL_NMR_TEMP_17,	AMPPNL_NMR_TEMP_18,	AMPPNL_NMR_TEMP_19,
};


int AMP_POWER[20] 	= {

		AMPPNL_NMR_PWR_0,	AMPPNL_NMR_PWR_1,	AMPPNL_NMR_PWR_2,	AMPPNL_NMR_PWR_3,	AMPPNL_NMR_PWR_4, 	
		AMPPNL_NMR_PWR_5,	AMPPNL_NMR_PWR_6,	AMPPNL_NMR_PWR_7,	AMPPNL_NMR_PWR_8,	AMPPNL_NMR_PWR_9, 	
		AMPPNL_NMR_PWR_10,	AMPPNL_NMR_PWR_11,	AMPPNL_NMR_PWR_12,	AMPPNL_NMR_PWR_13,	AMPPNL_NMR_PWR_14,	
		AMPPNL_NMR_PWR_15,	AMPPNL_NMR_PWR_16,	AMPPNL_NMR_PWR_17,	AMPPNL_NMR_PWR_18,	AMPPNL_NMR_PWR_19,
};

int AMP_ATTEN[20] 	= {

		AMPPNL_NMR_OutATTN_0,	AMPPNL_NMR_OutATTN_1,	AMPPNL_NMR_OutATTN_2,	AMPPNL_NMR_OutATTN_3,	AMPPNL_NMR_OutATTN_4,
		AMPPNL_NMR_OutATTN_5,	AMPPNL_NMR_OutATTN_6,	AMPPNL_NMR_OutATTN_7,	AMPPNL_NMR_OutATTN_8,	AMPPNL_NMR_OutATTN_9,
		AMPPNL_NMR_OutATTN_10,	AMPPNL_NMR_OutATTN_11,	AMPPNL_NMR_OutATTN_12,	AMPPNL_NMR_OutATTN_13,	AMPPNL_NMR_OutATTN_14,
		AMPPNL_NMR_OutATTN_15,	AMPPNL_NMR_OutATTN_16,	AMPPNL_NMR_OutATTN_17,	AMPPNL_NMR_OutATTN_18,	AMPPNL_NMR_OutATTN_19,
};




int MAIN_BTN_AMP[40] 	= {

		MAINPNL_BTN_AMP_0,	MAINPNL_BTN_AMP_1,	MAINPNL_BTN_AMP_2,	MAINPNL_BTN_AMP_3,	MAINPNL_BTN_AMP_4,
		MAINPNL_BTN_AMP_5,	MAINPNL_BTN_AMP_6,	MAINPNL_BTN_AMP_7,	MAINPNL_BTN_AMP_8,	MAINPNL_BTN_AMP_9,
		MAINPNL_BTN_AMP_10,	MAINPNL_BTN_AMP_11,	MAINPNL_BTN_AMP_12,	MAINPNL_BTN_AMP_13,	MAINPNL_BTN_AMP_14,
		MAINPNL_BTN_AMP_15,	MAINPNL_BTN_AMP_16,	MAINPNL_BTN_AMP_17,	MAINPNL_BTN_AMP_18,	MAINPNL_BTN_AMP_19,
		MAINPNL_BTN_AMP_20,	MAINPNL_BTN_AMP_21,	MAINPNL_BTN_AMP_22,	MAINPNL_BTN_AMP_23,	MAINPNL_BTN_AMP_24,
		MAINPNL_BTN_AMP_25,	MAINPNL_BTN_AMP_26,	MAINPNL_BTN_AMP_27,	MAINPNL_BTN_AMP_28,	MAINPNL_BTN_AMP_29,
		MAINPNL_BTN_AMP_30,	MAINPNL_BTN_AMP_31,	MAINPNL_BTN_AMP_32,	MAINPNL_BTN_AMP_33,	MAINPNL_BTN_AMP_34,
		MAINPNL_BTN_AMP_35,	MAINPNL_BTN_AMP_36,	MAINPNL_BTN_AMP_37,	MAINPNL_BTN_AMP_38,	MAINPNL_BTN_AMP_39,
};

int MAIN_LED_AMP[40] 	= {

		MAINPNL_LED_AMP_0,	MAINPNL_LED_AMP_1,	MAINPNL_LED_AMP_2,	MAINPNL_LED_AMP_3,	MAINPNL_LED_AMP_4,
		MAINPNL_LED_AMP_5,	MAINPNL_LED_AMP_6,	MAINPNL_LED_AMP_7,	MAINPNL_LED_AMP_8,	MAINPNL_LED_AMP_9,
		MAINPNL_LED_AMP_10,	MAINPNL_LED_AMP_11,	MAINPNL_LED_AMP_12,	MAINPNL_LED_AMP_13,	MAINPNL_LED_AMP_14,
		MAINPNL_LED_AMP_15,	MAINPNL_LED_AMP_16,	MAINPNL_LED_AMP_17,	MAINPNL_LED_AMP_18,	MAINPNL_LED_AMP_19,
		MAINPNL_LED_AMP_20,	MAINPNL_LED_AMP_21,	MAINPNL_LED_AMP_22,	MAINPNL_LED_AMP_23,	MAINPNL_LED_AMP_24,
		MAINPNL_LED_AMP_25,	MAINPNL_LED_AMP_26,	MAINPNL_LED_AMP_27,	MAINPNL_LED_AMP_28,	MAINPNL_LED_AMP_29,
		MAINPNL_LED_AMP_30,	MAINPNL_LED_AMP_31,	MAINPNL_LED_AMP_32,	MAINPNL_LED_AMP_33,	MAINPNL_LED_AMP_34,
		MAINPNL_LED_AMP_35,	MAINPNL_LED_AMP_36,	MAINPNL_LED_AMP_37,	MAINPNL_LED_AMP_38,	MAINPNL_LED_AMP_39,
};

void AMP_PANEL_Init(UINT8 CNT)
{
	UINT8	i, DIMMED_Data;
	
	for(i=0; i < 20 ; i++)
	{
		if( i < CNT)
		{
			DIMMED_Data = 0;	
		}
		else
		{
			DIMMED_Data = 1;	
		}
		
		SetCtrlAttribute(m_hWndAMP, AMP_ID[i], ATTR_DIMMED, 0); 
		SetCtrlAttribute(m_hWndAMP, AMP_DIR[i], ATTR_DIMMED, DIMMED_Data);
		SetCtrlAttribute(m_hWndAMP, AMP_TYPE[i], ATTR_DIMMED, DIMMED_Data);
		SetCtrlAttribute(m_hWndAMP, AMP_ON[i], ATTR_DIMMED, DIMMED_Data);
		SetCtrlAttribute(m_hWndAMP, AMP_ALC[i], ATTR_DIMMED, DIMMED_Data);
		SetCtrlAttribute(m_hWndAMP, AMP_ALC_LEVEL[i], ATTR_DIMMED, DIMMED_Data);
		SetCtrlAttribute(m_hWndAMP, AMP_OutAtt[i], ATTR_DIMMED, DIMMED_Data);
		
		SetCtrlAttribute(m_hWndAMP, AMP_LED_ALARM[i], ATTR_DIMMED, DIMMED_Data);
		SetCtrlAttribute(m_hWndAMP, AMP_LED_ASD[i], ATTR_DIMMED, DIMMED_Data);
		SetCtrlAttribute(m_hWndAMP, AMP_TEMP[i], ATTR_DIMMED, DIMMED_Data);
		SetCtrlAttribute(m_hWndAMP, AMP_POWER[i], ATTR_DIMMED, DIMMED_Data);
		SetCtrlAttribute(m_hWndAMP, AMP_ATTEN[i], ATTR_DIMMED, DIMMED_Data);
	}		
}
	

void AMPSetData(void) ////////////////////////////////////////
{
	char	cVal, Sum_Cnt;
	int		iVal = 0, i;
	double 	dVal;
	
	if (!m_hWndAMP) return;   

//	GetCtrlVal (m_hWndAMP,	AMPPNL_RING_AMP, 		&cVal);
	
	Sum_Cnt = 0;
	for (i = 0; i < 20; i++)
	{
		GetCtrlVal (m_hWndAMP,	AMP_ID[i], 			&cVal);
		
		if(cVal != 0)
		{
			GetCtrlVal (m_hWndAMP,	AMP_ID[i], 			&cVal); 
			m_pAMPSet->AMP_CTRL[Sum_Cnt].ID = cVal; 
		
			GetCtrlVal (m_hWndAMP,	AMP_DIR[i], 		&iVal); 
			m_pAMPSet->AMP_CTRL[Sum_Cnt].DIR = iVal;
		
			GetCtrlVal (m_hWndAMP,	AMP_TYPE[i], 		&cVal); 
			m_pAMPSet->AMP_CTRL[Sum_Cnt].TYPE = cVal;
		
			GetCtrlVal (m_hWndAMP,	AMP_ON[i], 			&iVal); 
			m_pAMPSet->AMP_CTRL[Sum_Cnt].AMP = iVal;
		
			GetCtrlVal (m_hWndAMP,	AMP_ALC[i], 		&iVal); 
			m_pAMPSet->AMP_CTRL[Sum_Cnt].ALC = iVal;	
		
			GetCtrlVal (m_hWndAMP,	AMP_ALC_LEVEL[i],	&dVal);
			m_pAMPSet->AMP_CTRL[Sum_Cnt].ALC_LEVEL[0] = LOBYTE((INT16)dVal*2);
			m_pAMPSet->AMP_CTRL[Sum_Cnt].ALC_LEVEL[1] = HIBYTE((INT16)dVal*2);
		
			GetCtrlVal (m_hWndAMP,	AMP_OutAtt[i], 		&dVal);
			m_pAMPSet->AMP_CTRL[Sum_Cnt].Out_Attn[0] = LOBYTE((INT16)dVal*2);
			m_pAMPSet->AMP_CTRL[Sum_Cnt].Out_Attn[1] = HIBYTE((INT16)dVal*2);
			
			Sum_Cnt++;
		}
	}
	
	m_pAMPSet->TOTAL_CNT = Sum_Cnt;  
	
	SendRequest(0, AMP_CONTROL_REQ, m_pAMPSet, sizeof(AMP_CONTROL));

}

void AMPSetDataDisplay(void)
{
	UINT8	i, j;
	
	if (!m_hWndAMP) return;   

//	SetCtrlVal (m_hWndAMP,	AMPPNL_RING_AMP, 		m_pAMPSet->TOTAL_CNT);
	
	AMP_PANEL_Init(m_pAMPSet->TOTAL_CNT);
	
	for (i = 0; i < 20; i++)
	{
		if( i <  m_pAMPSet->TOTAL_CNT)
		{
			SetCtrlVal (m_hWndAMP,	AMP_ID[i], 			m_pAMPSet->AMP_CTRL[i].ID);	
			SetCtrlVal (m_hWndAMP,	AMP_DIR[i], 		m_pAMPSet->AMP_CTRL[i].DIR);
			SetCtrlVal (m_hWndAMP,	AMP_TYPE[i], 		m_pAMPSet->AMP_CTRL[i].TYPE);
			SetCtrlVal (m_hWndAMP,	AMP_ON[i], 			m_pAMPSet->AMP_CTRL[i].AMP);
			SetCtrlVal (m_hWndAMP,	AMP_ALC[i], 		m_pAMPSet->AMP_CTRL[i].ALC);
			SetCtrlVal (m_hWndAMP,	AMP_ALC_LEVEL[i],	(double)MAKEWORD(m_pAMPSet->AMP_CTRL[i].ALC_LEVEL[0], m_pAMPSet->AMP_CTRL[i].ALC_LEVEL[1]) /2. );
			SetCtrlVal (m_hWndAMP,	AMP_OutAtt[i], 		(double)MAKEWORD(m_pAMPSet->AMP_CTRL[i].Out_Attn[0], m_pAMPSet->AMP_CTRL[i].Out_Attn[1]) /2. );
			
			SetCtrlAttribute (m_hWndAMP,	AMP_ID[i],			ATTR_DIMMED, 0);
			SetCtrlAttribute (m_hWndAMP,	AMP_DIR[i],			ATTR_DIMMED, 0);
			SetCtrlAttribute (m_hWndAMP,	AMP_TYPE[i],		ATTR_DIMMED, 0);
			SetCtrlAttribute (m_hWndAMP,	AMP_ON[i],			ATTR_DIMMED, 0);
			SetCtrlAttribute (m_hWndAMP,	AMP_ALC[i],			ATTR_DIMMED, 0);
			SetCtrlAttribute (m_hWndAMP,	AMP_ALC_LEVEL[i],	ATTR_DIMMED, 0);
			SetCtrlAttribute (m_hWndAMP,	AMP_OutAtt[i],		ATTR_DIMMED, 0);
	
		}
		else
		{
			SetCtrlVal (m_hWndAMP,	AMP_ID[i], 			0);	
			

			SetCtrlAttribute (m_hWndAMP,	AMP_DIR[i],			ATTR_DIMMED, 1);
			SetCtrlAttribute (m_hWndAMP,	AMP_TYPE[i],		ATTR_DIMMED, 1);
			SetCtrlAttribute (m_hWndAMP,	AMP_ON[i],			ATTR_DIMMED, 1);
			SetCtrlAttribute (m_hWndAMP,	AMP_ALC[i],			ATTR_DIMMED, 1);
			SetCtrlAttribute (m_hWndAMP,	AMP_ALC_LEVEL[i],	ATTR_DIMMED, 1);
			SetCtrlAttribute (m_hWndAMP,	AMP_OutAtt[i],		ATTR_DIMMED, 1);
	
		}	
	}
	

}


void AMPStatusDataDisplay(void)
{
  	UINT8	i, Up_Cnt, Down_Cnt ;
	char 	strTemp[64];
	

	Up_Cnt = 0;
	Down_Cnt = 0;
	
	for (i = 0; i < 20; i++)
	{
		if( i <  m_pAMPSts->TOTAL_CNT)
		{
			if( m_pAMPSts->AMP_STS[i].ID != 0)
			{
				if(m_pAMPSts->AMP_STS[i].DIR == 0)
				{
					strcpy(strTemp, "");
					
					if(m_pAMPSts->AMP_STS[i].TYPE == 1)			Fmt(strTemp, "[%d]\n%s", (m_pAMPSts->AMP_STS[i].ID),"AM");	
					else if(m_pAMPSts->AMP_STS[i].TYPE == 3)	Fmt(strTemp, "[%d]\n%s", (m_pAMPSts->AMP_STS[i].ID),"FM");
					else if(m_pAMPSts->AMP_STS[i].TYPE == 5)	Fmt(strTemp, "[%d]\n%s", (m_pAMPSts->AMP_STS[i].ID),"DMB");
					else if(m_pAMPSts->AMP_STS[i].TYPE == 6)	Fmt(strTemp, "[%d]\n%s", (m_pAMPSts->AMP_STS[i].ID),"TRS-DL");
					else 										Fmt(strTemp, "[%d]\n%s", (m_pAMPSts->AMP_STS[i].ID),"TRS-UL");
					
					SetCtrlAttribute(m_hWndMain, MAIN_BTN_AMP[Up_Cnt],  ATTR_LABEL_TEXT, strTemp);
					SetCtrlVal (m_hWndMain,	MAIN_LED_AMP[Up_Cnt], 	m_pAMPSts->AMP_STS[i].ALARM);
					
					SetCtrlAttribute(m_hWndMain, MAIN_BTN_AMP[Up_Cnt],  ATTR_DIMMED, 0);
					SetCtrlAttribute(m_hWndMain, MAIN_LED_AMP[Up_Cnt],  ATTR_DIMMED, 0);
			
						
					Up_Cnt++;
				}
				else
				{
					strcpy(strTemp, "");
					
					if(m_pAMPSts->AMP_STS[i].TYPE == 1)			Fmt(strTemp, "[%d]\n%s", (m_pAMPSts->AMP_STS[i].ID),"AM");	
					else if(m_pAMPSts->AMP_STS[i].TYPE == 3)	Fmt(strTemp, "[%d]\n%s", (m_pAMPSts->AMP_STS[i].ID),"FM");
					else if(m_pAMPSts->AMP_STS[i].TYPE == 5)	Fmt(strTemp, "[%d]\n%s", (m_pAMPSts->AMP_STS[i].ID),"DMB");
					else if(m_pAMPSts->AMP_STS[i].TYPE == 6)	Fmt(strTemp, "[%d]\n%s", (m_pAMPSts->AMP_STS[i].ID),"TRS-DL");
					else 										Fmt(strTemp, "[%d]\n%s", (m_pAMPSts->AMP_STS[i].ID),"TRS-UL");
					
					SetCtrlAttribute(m_hWndMain, MAIN_BTN_AMP[20+Down_Cnt],  ATTR_LABEL_TEXT, strTemp);
					SetCtrlVal (m_hWndMain,	MAIN_LED_AMP[20+Down_Cnt], 	m_pAMPSts->AMP_STS[i].ALARM);
					
					SetCtrlAttribute(m_hWndMain, MAIN_BTN_AMP[20+Down_Cnt],  ATTR_DIMMED, 0);
					SetCtrlAttribute(m_hWndMain, MAIN_LED_AMP[20+Down_Cnt],  ATTR_DIMMED, 0);
				
					Down_Cnt++;
				}
			}
				
		}
	}
	
	for (i = Up_Cnt; i < 20; i++)
	{
			SetCtrlAttribute(m_hWndMain, MAIN_BTN_AMP[i],  ATTR_LABEL_TEXT, "[NONE]");	  
			SetCtrlVal (m_hWndMain,	MAIN_LED_AMP[i], 	0);
			
			SetCtrlAttribute(m_hWndMain, MAIN_BTN_AMP[i],  ATTR_DIMMED, 1);
			SetCtrlAttribute(m_hWndMain, MAIN_LED_AMP[i],  ATTR_DIMMED, 1);
	}
	
	for (i = Down_Cnt; i < 20; i++)
	{
			SetCtrlAttribute(m_hWndMain, MAIN_BTN_AMP[20+i],  ATTR_LABEL_TEXT, "[NONE]");	  
			SetCtrlVal (m_hWndMain,	MAIN_LED_AMP[20+i], 	0);
			
			SetCtrlAttribute(m_hWndMain, MAIN_BTN_AMP[20+i],  ATTR_DIMMED, 1);
			SetCtrlAttribute(m_hWndMain, MAIN_LED_AMP[20+i],  ATTR_DIMMED, 1);
	}
	
	
	if (!m_hWndAMP) return;   

//	SetCtrlVal (m_hWndAMP,	AMPPNL_RING_AMP, 		m_pAMPSts->TOTAL_CNT);
	
//	AMP_PANEL_Init(m_pAMPSts->TOTAL_CNT);
	
	for (i = 0; i < 20; i++)
	{
		if( i <  m_pAMPSts->TOTAL_CNT)
		{
	//		SetCtrlVal (m_hWndAMP,	AMP_ID[i], 			m_pAMPSts->AMP_STS[i].ID);	
	//		SetCtrlVal (m_hWndAMP,	AMP_DIR[i], 		m_pAMPSts->AMP_STS[i].DIR);
	//		SetCtrlVal (m_hWndAMP,	AMP_TYPE[i], 		m_pAMPSts->AMP_STS[i].TYPE);
			SetCtrlVal (m_hWndAMP,	AMP_LED_ALARM[i], 	m_pAMPSts->AMP_STS[i].ALARM);
			SetCtrlVal (m_hWndAMP,	AMP_LED_ASD[i], 	m_pAMPSts->AMP_STS[i].ASD); 
			SetCtrlVal (m_hWndAMP,	AMP_TEMP[i], 		m_pAMPSts->AMP_STS[i].Temp);
			SetCtrlVal (m_hWndAMP,	AMP_POWER[i],		(double)MAKEWORD(m_pAMPSts->AMP_STS[i].Out_pwr[0], m_pAMPSts->AMP_STS[i].Out_pwr[1]) /2. ); 
			SetCtrlVal (m_hWndAMP,	AMP_ATTEN[i], 		(double)MAKEWORD(m_pAMPSts->AMP_STS[i].Out_Attn[0], m_pAMPSts->AMP_STS[i].Out_Attn[1]) /2. );
			
/*			SetCtrlAttribute (m_hWndAMP,	AMP_ID[i],			ATTR_DIMMED, 0);
			SetCtrlAttribute (m_hWndAMP,	AMP_DIR[i],			ATTR_DIMMED, 0);
			SetCtrlAttribute (m_hWndAMP,	AMP_TYPE[i],		ATTR_DIMMED, 0);
			SetCtrlAttribute (m_hWndAMP,	AMP_ON[i],			ATTR_DIMMED, 0);
			SetCtrlAttribute (m_hWndAMP,	AMP_ALC[i],			ATTR_DIMMED, 0);
			SetCtrlAttribute (m_hWndAMP,	AMP_ALC_LEVEL[i],	ATTR_DIMMED, 0);
			SetCtrlAttribute (m_hWndAMP,	AMP_OutAtt[i],		ATTR_DIMMED, 0);
*/			
			SetCtrlAttribute (m_hWndAMP,	AMP_LED_ALARM[i],	ATTR_DIMMED, 0);
			SetCtrlAttribute (m_hWndAMP,	AMP_LED_ASD[i],		ATTR_DIMMED, 0);
			SetCtrlAttribute (m_hWndAMP,	AMP_TEMP[i],		ATTR_DIMMED, 0);
			SetCtrlAttribute (m_hWndAMP,	AMP_POWER[i],		ATTR_DIMMED, 0);
			SetCtrlAttribute (m_hWndAMP,	AMP_ATTEN[i],		ATTR_DIMMED, 0);
			
		
			
	
		}
		else
		{
/*			SetCtrlVal (m_hWndAMP,	AMP_ID[i], 			0);	

			SetCtrlAttribute (m_hWndAMP,	AMP_DIR[i],			ATTR_DIMMED, 1);
			SetCtrlAttribute (m_hWndAMP,	AMP_TYPE[i],		ATTR_DIMMED, 1);
			SetCtrlAttribute (m_hWndAMP,	AMP_ON[i],			ATTR_DIMMED, 1);
			SetCtrlAttribute (m_hWndAMP,	AMP_ALC[i],			ATTR_DIMMED, 1);
			SetCtrlAttribute (m_hWndAMP,	AMP_ALC_LEVEL[i],	ATTR_DIMMED, 1);
			SetCtrlAttribute (m_hWndAMP,	AMP_OutAtt[i],		ATTR_DIMMED, 1);
*/			
			SetCtrlAttribute (m_hWndAMP,	AMP_LED_ALARM[i],	ATTR_DIMMED, 1);
			SetCtrlAttribute (m_hWndAMP,	AMP_LED_ASD[i],		ATTR_DIMMED, 1);
			SetCtrlAttribute (m_hWndAMP,	AMP_TEMP[i],		ATTR_DIMMED, 1);
			SetCtrlAttribute (m_hWndAMP,	AMP_POWER[i],		ATTR_DIMMED, 1);
			SetCtrlAttribute (m_hWndAMP,	AMP_ATTEN[i],		ATTR_DIMMED, 1);
	
		}	
	}
	

}

void AMPStatusChange(int control)
{
	UINT8		i, IDVal, NUMVal, Dimmed_Data;


	IDVal = 0;
	
	for( i=0; i<20; i++)
	{
		if(AMP_ID[i] == control)
		{
			  IDVal = i;
			  break;
		}
	}
	
						 
		
		GetCtrlVal(m_hWndAMP, AMP_ID[IDVal], &NUMVal);
		
		if( NUMVal == 0) // NONE Check
		{
			Dimmed_Data = 1;
		}
		else
		{
			Dimmed_Data	= 0;
		}
		
		SetCtrlAttribute (m_hWndAMP,	AMP_DIR[i],			ATTR_DIMMED, Dimmed_Data);
		SetCtrlAttribute (m_hWndAMP,	AMP_TYPE[i],		ATTR_DIMMED, Dimmed_Data);
		SetCtrlAttribute (m_hWndAMP,	AMP_ON[i],			ATTR_DIMMED, Dimmed_Data);
		SetCtrlAttribute (m_hWndAMP,	AMP_ALC[i],			ATTR_DIMMED, Dimmed_Data);
		SetCtrlAttribute (m_hWndAMP,	AMP_ALC_LEVEL[i],	ATTR_DIMMED, Dimmed_Data);
		SetCtrlAttribute (m_hWndAMP,	AMP_OutAtt[i],		ATTR_DIMMED, Dimmed_Data);
		
		SetCtrlAttribute (m_hWndAMP,	AMP_LED_ALARM[i],	ATTR_DIMMED, Dimmed_Data);
		SetCtrlAttribute (m_hWndAMP,	AMP_LED_ASD[i],		ATTR_DIMMED, Dimmed_Data);
		SetCtrlAttribute (m_hWndAMP,	AMP_TEMP[i],		ATTR_DIMMED, Dimmed_Data);
		SetCtrlAttribute (m_hWndAMP,	AMP_POWER[i],		ATTR_DIMMED, Dimmed_Data);
		SetCtrlAttribute (m_hWndAMP,	AMP_ATTEN[i],		ATTR_DIMMED, Dimmed_Data);
	
}

//-------------------------------------------------------------------------------//
// AMP Status..!
//-------------------------------------------------------------------------------//

void LNASetData(void) ////////////////////////////////////////
{
	int		iVal = 0;
	double 	dVal;

	if (!m_hWndLNA) return;
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_TG_AMP_AM, 		&iVal); 
	m_pLNASet->AM_AMP = iVal;
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_TG_ALC_AM, 		&iVal); 
	m_pLNASet->AM_ALC = iVal;
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_NMR_ALC_AM, 		&dVal);
	m_pLNASet->AM_ALC_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pLNASet->AM_ALC_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_NMR_SetAttn_AM, 	&dVal);
	m_pLNASet->AM_ATTN[0] = LOBYTE((INT16)dVal*2);
	m_pLNASet->AM_ATTN[1] = HIBYTE((INT16)dVal*2);
	
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_TG_AMP_FM, 		&iVal); 
	m_pLNASet->FM_AMP = iVal;
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_TG_ALC_FM, 		&iVal); 
	m_pLNASet->FM_ALC = iVal;
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_NMR_ALC_FM, 		&dVal);
	m_pLNASet->FM_ALC_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pLNASet->FM_ALC_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_NMR_SetAttn_FM, 	&dVal);
	m_pLNASet->FM_ATTN[0] = LOBYTE((INT16)dVal*2);
	m_pLNASet->FM_ATTN[1] = HIBYTE((INT16)dVal*2);
	
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_TG_AMP_DMB, 		&iVal); 
	m_pLNASet->DMB_AMP = iVal;
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_TG_ALC_DMB, 		&iVal); 
	m_pLNASet->DMB_ALC = iVal;
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_NMR_ALC_DMB, 		&dVal);
	m_pLNASet->DMB_ALC_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pLNASet->DMB_ALC_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_NMR_SetAttn_DMB, 	&dVal);
	m_pLNASet->DMB_ATTN[0] = LOBYTE((INT16)dVal*2);
	m_pLNASet->DMB_ATTN[1] = HIBYTE((INT16)dVal*2);
	
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_TG_AMP_TRS, 		&iVal); 
	m_pLNASet->TRS_AMP = iVal;
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_TG_ALC_TRS, 		&iVal); 
	m_pLNASet->TRS_ALC = iVal;
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_NMR_ALC_TRS, 		&dVal);
	m_pLNASet->TRS_ALC_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pLNASet->TRS_ALC_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_NMR_SetAttn_TRS, 	&dVal);
	m_pLNASet->TRS_ATTN[0] = LOBYTE((INT16)dVal*2);
	m_pLNASet->TRS_ATTN[1] = HIBYTE((INT16)dVal*2);
	
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_TG_AMP_TRS_UL, 		&iVal); 
	m_pLNASet->TRS_UL_AMP = iVal;
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_TG_ALC_TRS_UL, 		&iVal); 
	m_pLNASet->TRS_UL_ALC = iVal;
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_NMR_ALC_TRS_UL, 		&dVal);
	m_pLNASet->TRS_UL_ALC_LEVEL[0] = LOBYTE((INT16)dVal*2);
	m_pLNASet->TRS_UL_ALC_LEVEL[1] = HIBYTE((INT16)dVal*2);
	
	GetCtrlVal (m_hWndLNA,	LNAPNL_NMR_SetAttn_TRS_UL, 	&dVal);
	m_pLNASet->TRS_UL_ATTN[0] = LOBYTE((INT16)dVal*2);
	m_pLNASet->TRS_UL_ATTN[1] = HIBYTE((INT16)dVal*2);


			
	SendRequest(0, LNA_CONTROL_REQ, m_pLNASet, sizeof(LNA_CONTROL));
}

void LNASetDataDisplay(void)
{
	if (!m_hWndLNA) return;
	
	SetCtrlVal (m_hWndLNA,	LNAPNL_TG_AMP_AM, 		m_pLNASet->AM_AMP);
	SetCtrlVal (m_hWndLNA,	LNAPNL_TG_ALC_AM, 		m_pLNASet->AM_ALC);
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_ALC_AM, 		(double)MAKEWORD(m_pLNASet->AM_ALC_LEVEL[0], m_pLNASet->AM_ALC_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_SetAttn_AM, 	(double)MAKEWORD(m_pLNASet->AM_ATTN[0], m_pLNASet->AM_ATTN[1]) /2. );

	SetCtrlVal (m_hWndLNA,	LNAPNL_TG_AMP_FM, 		m_pLNASet->FM_AMP);
	SetCtrlVal (m_hWndLNA,	LNAPNL_TG_ALC_FM, 		m_pLNASet->FM_ALC);
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_ALC_FM, 		(double)MAKEWORD(m_pLNASet->FM_ALC_LEVEL[0], m_pLNASet->FM_ALC_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_SetAttn_FM, 	(double)MAKEWORD(m_pLNASet->FM_ATTN[0], m_pLNASet->FM_ATTN[1]) /2. );
	
	SetCtrlVal (m_hWndLNA,	LNAPNL_TG_AMP_DMB, 		m_pLNASet->DMB_AMP);
	SetCtrlVal (m_hWndLNA,	LNAPNL_TG_ALC_DMB, 		m_pLNASet->DMB_ALC);
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_ALC_DMB, 	(double)MAKEWORD(m_pLNASet->DMB_ALC_LEVEL[0], m_pLNASet->DMB_ALC_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_SetAttn_DMB, (double)MAKEWORD(m_pLNASet->DMB_ATTN[0], m_pLNASet->DMB_ATTN[1]) /2. );
	
	SetCtrlVal (m_hWndLNA,	LNAPNL_TG_AMP_TRS, 		m_pLNASet->TRS_AMP);
	SetCtrlVal (m_hWndLNA,	LNAPNL_TG_ALC_TRS, 		m_pLNASet->TRS_ALC);
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_ALC_TRS, 	(double)MAKEWORD(m_pLNASet->TRS_ALC_LEVEL[0], m_pLNASet->TRS_ALC_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_SetAttn_TRS, (double)MAKEWORD(m_pLNASet->TRS_ATTN[0], m_pLNASet->TRS_ATTN[1]) /2. );
	
	SetCtrlVal (m_hWndLNA,	LNAPNL_TG_AMP_TRS_UL, 		m_pLNASet->TRS_UL_AMP);
	SetCtrlVal (m_hWndLNA,	LNAPNL_TG_ALC_TRS_UL, 		m_pLNASet->TRS_UL_ALC);
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_ALC_TRS_UL, 		(double)MAKEWORD(m_pLNASet->TRS_UL_ALC_LEVEL[0], m_pLNASet->TRS_UL_ALC_LEVEL[1]) /2. );
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_SetAttn_TRS_UL, (double)MAKEWORD(m_pLNASet->TRS_UL_ATTN[0], m_pLNASet->TRS_UL_ATTN[1]) /2. );
	

}


void LNAStatusDataDisplay(void)
{
	
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_LNA_AM, 		(double)MAKEWORD(m_pLNASts->AM_InputPOWER[0], m_pLNASts->AM_InputPOWER[1]) /2. );  
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_LNA_FM, 		(double)MAKEWORD(m_pLNASts->FM_InputPOWER[0], m_pLNASts->FM_InputPOWER[1]) /2. );  
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_LNA_DMB, 		(double)MAKEWORD(m_pLNASts->DMB_InputPOWER[0], m_pLNASts->DMB_InputPOWER[1]) /2. );  
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_LNA_TRS_D, 		(double)MAKEWORD(m_pLNASts->TRS_InputPOWER[0], m_pLNASts->TRS_InputPOWER[1]) /2. );  
	SetCtrlVal (m_hWndMain, MAINPNL_NMR_LNA_TRS_U, 		(double)MAKEWORD(m_pLNASts->TRS_UL_InputPOWER[0], m_pLNASts->TRS_UL_InputPOWER[1]) /2. );
	
	if (!m_hWndLNA) return;  
							  
	
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_PWR_AM, 		(double)MAKEWORD(m_pLNASts->AM_InputPOWER[0], m_pLNASts->AM_InputPOWER[1]) /2. );  
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_ATTN_AM, 	(double)MAKEWORD(m_pLNASts->AM_ATTN[0], m_pLNASts->AM_ATTN[1]) /2. );
	
    SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_PWR_FM, 		(double)MAKEWORD(m_pLNASts->FM_InputPOWER[0], m_pLNASts->FM_InputPOWER[1]) /2. );  
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_ATTN_FM, 	(double)MAKEWORD(m_pLNASts->FM_ATTN[0], m_pLNASts->FM_ATTN[1]) /2. );
	
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_PWR_DMB, 	(double)MAKEWORD(m_pLNASts->DMB_InputPOWER[0], m_pLNASts->DMB_InputPOWER[1]) /2. );  
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_ATTN_DMB, 	(double)MAKEWORD(m_pLNASts->DMB_ATTN[0], m_pLNASts->DMB_ATTN[1]) /2. );
	
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_PWR_TRS, 	(double)MAKEWORD(m_pLNASts->TRS_InputPOWER[0], m_pLNASts->TRS_InputPOWER[1]) /2. );  
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_ATTN_TRS, 	(double)MAKEWORD(m_pLNASts->TRS_ATTN[0], m_pLNASts->TRS_ATTN[1]) /2. );
	
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_PWR_TRS_UL, 	(double)MAKEWORD(m_pLNASts->TRS_UL_InputPOWER[0], m_pLNASts->TRS_UL_InputPOWER[1]) /2. );  
	SetCtrlVal (m_hWndLNA,	LNAPNL_NMR_ATTN_TRS_UL, (double)MAKEWORD(m_pLNASts->TRS_UL_ATTN[0], m_pLNASts->TRS_UL_ATTN[1]) /2. );
		
}



//-------------------------------------------------------------------------------//
// CHA CHANNEL Status..!
//-------------------------------------------------------------------------------//

#define CHA_AM_Channel_MAX	121
#define CHA_FM_Channel_MAX	101
#define CHA_DMB_Channel_MAX	22
#define CHA_TRS_Channel_MAX	26

#define CHA_AM_NUM_MAX			40
#define CHA_FM_NUM_MAX			40
#define CHA_DMB_NUM_MAX			6
#define CHA_TRS_NUM_MAX			5

#define	CHA_AM_NONE			120
#define	CHA_FM_NONE			100
#define	CHA_DMB_NONE		21
#define	CHA_TRS_NONE		25

// AM : 120개(526.5~1606.5KHz, 9KHz간격 최대 40개 선택)
char CHA_AM_Channel[CHA_AM_Channel_MAX][12] =    
{
	" 531kHz",	" 540kHz",	" 549kHz",	" 558kHz",	" 567kHz",	" 576kHz", 	" 585kHz", 	" 594kHz", 	" 603kHz",	" 612kHz",
	" 621kHz",	" 630kHz",	" 639kHz",	" 648kHz",	" 657kHz",	" 666kHz", 	" 675kHz", 	" 684kHz", 	" 693kHz", 	" 702kHz",
	" 711kHz",	" 720kHz",	" 729kHz",	" 738kHz",	" 747kHz",	" 756kHz", 	" 765kHz", 	" 774kHz", 	" 783kHz", 	" 792kHz",
	" 801kHz",	" 810kHz",	" 819kHz",	" 828kHz",	" 837kHz",	" 846kHz", 	" 855kHz", 	" 864kHz", 	" 873kHz", 	" 882kHz",
	" 891kHz",	" 900kHz",	" 909kHz",	" 918kHz",	" 927kHz",	" 936kHz", 	" 945kHz", 	" 954kHz", 	" 963kHz", 	" 972kHz",
	" 981kHz",	" 990kHz",	" 999kHz",	"1008kHz",	"1017kHz",	"1026kHz", 	"1035kHz", 	"1044kHz", 	"1053kHz", 	"1062kHz",
	"1071kHz",	"1080kHz",	"1089kHz",	"1098kHz",	"1107kHz",	"1116kHz", 	"1125kHz", 	"1134kHz", 	"1143kHz", 	"1152kHz",
	"1161kHz",	"1170kHz",	"1179kHz",	"1188kHz",	"1197kHz",	"1206kHz", 	"1215kHz", 	"1224kHz", 	"1233kHz", 	"1242kHz",
	"1251kHz",	"1260kHz",	"1269kHz",	"1278kHz",	"1287kHz",	"1296kHz", 	"1305kHz", 	"1314kHz", 	"1323kHz", 	"1332kHz",
	"1341kHz",	"1350kHz",	"1359kHz",	"1368kHz",	"1377kHz",	"1386kHz", 	"1395kHz", 	"1404kHz", 	"1413kHz", 	"1422kHz",
	"1431kHz",	"1440kHz",	"1449kHz",	"1458kHz",	"1467kHz",	"1476kHz", 	"1485kHz", 	"1494kHz", 	"1503kHz", 	"1512kHz",
	"1521kHz",	"1530kHz",	"1539kHz",	"1548kHz",	"1557kHz",	"1566kHz", 	"1575kHz", 	"1584kHz", 	"1593kHz", 	"1602kHz",
	"    NONE",
			 
};

//FM : 100개(88.1MHz ~ 107.9MHz, 0.2MHz 간격, 최대 40개 선택)  
char CHA_FM_Channel[CHA_FM_Channel_MAX][12] =
{
	" 88.1MHz",		" 88.3MHz",		" 88.5MHz",		" 88.7MHz",		" 88.9MHz",		" 89.1MHz", 	" 89.3MHz", 	" 89.5MHz", 	" 89.7MHz", 	" 89.9MHz",
	" 90.1MHz",		" 90.3MHz",		" 90.5MHz",		" 90.7MHz",		" 90.9MHz",		" 91.1MHz", 	" 91.3MHz", 	" 91.5MHz", 	" 91.7MHz", 	" 91.9MHz",
	" 92.1MHz",		" 92.3MHz",		" 92.5MHz",		" 92.7MHz",		" 92.9MHz",		" 93.1MHz", 	" 93.3MHz", 	" 93.5MHz", 	" 93.7MHz", 	" 93.9MHz",
	" 94.1MHz",		" 94.3MHz",		" 94.5MHz",		" 94.7MHz",		" 94.9MHz",		" 95.1MHz", 	" 95.3MHz", 	" 95.5MHz", 	" 95.7MHz", 	" 95.9MHz",
	" 96.1MHz",		" 96.3MHz",		" 96.5MHz",		" 96.7MHz",		" 96.9MHz",		" 97.1MHz", 	" 97.3MHz", 	" 97.5MHz", 	" 97.7MHz", 	" 97.9MHz",
	" 98.1MHz",		" 98.3MHz",		" 98.5MHz",		" 98.7MHz",		" 98.9MHz",		" 99.1MHz", 	" 99.3MHz", 	" 99.5MHz", 	" 99.7MHz", 	" 99.9MHz",
	"100.1MHz",		"100.3MHz",		"100.5MHz",		"100.7MHz",		"100.9MHz",		"101.1MHz",		"101.3MHz",		"101.5MHz",		"101.7MHz",		"101.9MHz",	
	"102.1MHz",		"102.3MHz",		"102.5MHz",		"102.7MHz",		"102.9MHz",		"103.1MHz",		"103.3MHz",		"103.5MHz",		"103.7MHz",		"103.9MHz",	
	"104.1MHz",		"104.3MHz",		"104.5MHz",		"104.7MHz",		"104.9MHz",		"105.1MHz",		"105.3MHz",		"105.5MHz",		"105.7MHz",		"105.9MHz",	
	"106.1MHz",		"106.3MHz",		"106.5MHz",		"106.7MHz",		"106.9MHz",		"107.1MHz",		"107.3MHz",		"107.5MHz",		"107.7MHz",		"107.9MHz",	
	"    NONE",
			 
};

//DMB : 21개(7A ~ 13C, 174~216MHz, 6MHz간격, 최대 6개 선택) 
char CHA_DMB_Channel[CHA_DMB_Channel_MAX][18] =
{
	"  7A(175.280MHz)",		"  7B(177.008MHz)",		"  7C(178.736MHz)",	
	"  8A(181.280MHz)",		"  8B(183.008MHz)",		"  8C(184.736MHz)",
	"  9A(187.280MHz)",		"  9B(189.008MHz)",		"  9C(190.736MHz)",
	" 10A(193.280MHz)",		" 10B(195.008MHz)",		" 10C(196.736MHz)",
	" 11A(199.280MHz)",		" 11B(201.008MHz)",		" 11C(202.736MHz)",
	" 12A(205.280MHz)",		" 12B(207.008MHz)",		" 12C(208.736MHz)",
	" 13A(211.280MHz)",		" 13B(213.008MHz)",		" 13C(214.736MHz)",
	"NONE",
			 
};

//TRS : 25개(851.1125 ~ 855.9125MHz, 0.2MHz 간격 최대 5개 선택)
char CHA_TRS_Channel[CHA_TRS_Channel_MAX][12] =
{
	"851.1125MHz",	"851.3125MHz",	"851.5125MHz",	"851.7125MHz",	"851.9125MHz",	
	"852.1125MHz",	"852.3125MHz",	"852.5125MHz",	"852.7125MHz",	"852.9125MHz",	
	"853.1125MHz",	"853.3125MHz",	"853.5125MHz",	"853.7125MHz",	"853.9125MHz",	
	"854.1125MHz",	"854.3125MHz",	"854.5125MHz",	"854.7125MHz",	"854.9125MHz",	
	"855.1125MHz",	"855.3125MHz",	"855.5125MHz",	"855.7125MHz",	"855.9125MHz",	"    NONE",
			 
};



int CHA_NUM[40] 	= {
		CHAPNL_RING_NUM_0,	CHAPNL_RING_NUM_1,	CHAPNL_RING_NUM_2,	CHAPNL_RING_NUM_3,	CHAPNL_RING_NUM_4,
		CHAPNL_RING_NUM_5,	CHAPNL_RING_NUM_6,	CHAPNL_RING_NUM_7,	CHAPNL_RING_NUM_8,	CHAPNL_RING_NUM_9,
		CHAPNL_RING_NUM_10,	CHAPNL_RING_NUM_11,	CHAPNL_RING_NUM_12,	CHAPNL_RING_NUM_13,	CHAPNL_RING_NUM_14,
		CHAPNL_RING_NUM_15,	CHAPNL_RING_NUM_16,	CHAPNL_RING_NUM_17,	CHAPNL_RING_NUM_18,	CHAPNL_RING_NUM_19,
		CHAPNL_RING_NUM_20,	CHAPNL_RING_NUM_21,	CHAPNL_RING_NUM_22,	CHAPNL_RING_NUM_23,	CHAPNL_RING_NUM_24,
		CHAPNL_RING_NUM_25,	CHAPNL_RING_NUM_26,	CHAPNL_RING_NUM_27,	CHAPNL_RING_NUM_28,	CHAPNL_RING_NUM_29,
		CHAPNL_RING_NUM_30,	CHAPNL_RING_NUM_31,	CHAPNL_RING_NUM_32,	CHAPNL_RING_NUM_33,	CHAPNL_RING_NUM_34,
		CHAPNL_RING_NUM_35,	CHAPNL_RING_NUM_36,	CHAPNL_RING_NUM_37,	CHAPNL_RING_NUM_38,	CHAPNL_RING_NUM_39,
};

int CHA_GAIN[40] 	= {
		CHAPNL_NMR_GAIN_0,	CHAPNL_NMR_GAIN_1,	CHAPNL_NMR_GAIN_2,	CHAPNL_NMR_GAIN_3,	CHAPNL_NMR_GAIN_4,
		CHAPNL_NMR_GAIN_5,	CHAPNL_NMR_GAIN_6,	CHAPNL_NMR_GAIN_7,	CHAPNL_NMR_GAIN_8,	CHAPNL_NMR_GAIN_9,
		CHAPNL_NMR_GAIN_10,	CHAPNL_NMR_GAIN_11,	CHAPNL_NMR_GAIN_12,	CHAPNL_NMR_GAIN_13,	CHAPNL_NMR_GAIN_14,
		CHAPNL_NMR_GAIN_15,	CHAPNL_NMR_GAIN_16,	CHAPNL_NMR_GAIN_17,	CHAPNL_NMR_GAIN_18,	CHAPNL_NMR_GAIN_19,
		CHAPNL_NMR_GAIN_20,	CHAPNL_NMR_GAIN_21,	CHAPNL_NMR_GAIN_22,	CHAPNL_NMR_GAIN_23,	CHAPNL_NMR_GAIN_24,
		CHAPNL_NMR_GAIN_25,	CHAPNL_NMR_GAIN_26,	CHAPNL_NMR_GAIN_27,	CHAPNL_NMR_GAIN_28,	CHAPNL_NMR_GAIN_29,
		CHAPNL_NMR_GAIN_30,	CHAPNL_NMR_GAIN_31,	CHAPNL_NMR_GAIN_32,	CHAPNL_NMR_GAIN_33,	CHAPNL_NMR_GAIN_34,
		CHAPNL_NMR_GAIN_35,	CHAPNL_NMR_GAIN_36,	CHAPNL_NMR_GAIN_37,	CHAPNL_NMR_GAIN_38,	CHAPNL_NMR_GAIN_39,
};

int CHA_CH_EN[40] 	= {
		CHAPNL_TG_CH_0,		CHAPNL_TG_CH_1,		CHAPNL_TG_CH_2,		CHAPNL_TG_CH_3,		CHAPNL_TG_CH_4,
		CHAPNL_TG_CH_5,		CHAPNL_TG_CH_6,		CHAPNL_TG_CH_7,		CHAPNL_TG_CH_8,		CHAPNL_TG_CH_9,
		CHAPNL_TG_CH_10,		CHAPNL_TG_CH_11,		CHAPNL_TG_CH_12,		CHAPNL_TG_CH_13,		CHAPNL_TG_CH_14,
		CHAPNL_TG_CH_15,		CHAPNL_TG_CH_16,		CHAPNL_TG_CH_17,		CHAPNL_TG_CH_18,		CHAPNL_TG_CH_19,
		CHAPNL_TG_CH_20,		CHAPNL_TG_CH_21,		CHAPNL_TG_CH_22,		CHAPNL_TG_CH_23,		CHAPNL_TG_CH_24,
		CHAPNL_TG_CH_25,		CHAPNL_TG_CH_26,		CHAPNL_TG_CH_27,		CHAPNL_TG_CH_28,		CHAPNL_TG_CH_29,
		CHAPNL_TG_CH_30,		CHAPNL_TG_CH_31,		CHAPNL_TG_CH_32,		CHAPNL_TG_CH_33,		CHAPNL_TG_CH_34,
		CHAPNL_TG_CH_35,		CHAPNL_TG_CH_36,		CHAPNL_TG_CH_37,		CHAPNL_TG_CH_38,		CHAPNL_TG_CH_39,
};

void CHASetData(void)	/////////////////////////////////////
{
	int		iVal = 0;
	UINT16	iVal2 = 0;
	UINT8	cVal = 0;
	double 	dVal; 
	UINT8	i = 0, ID_Cnt = 0;
	UINT8	AMP_SET_ID;
	
	UINT8	CHA_Channel_MAX, CHA_NUM_MAX, CHA_NONE, Get_Channel; 

	GetCtrlVal (m_hWndCHA,	CHAPNL_RING_TYPE, 		&Get_Channel);

	if(Get_Channel ==  CHA_TRS)		{CHA_Channel_MAX = CHA_TRS_Channel_MAX; CHA_NUM_MAX = CHA_TRS_NUM_MAX; CHA_NONE = CHA_TRS_NONE;}
	else if(Get_Channel ==  CHA_AM)	{CHA_Channel_MAX = CHA_AM_Channel_MAX; CHA_NUM_MAX = CHA_AM_NUM_MAX; CHA_NONE = CHA_AM_NONE;}
	else if(Get_Channel ==  CHA_FM)	{CHA_Channel_MAX = CHA_FM_Channel_MAX; CHA_NUM_MAX = CHA_FM_NUM_MAX; CHA_NONE = CHA_FM_NONE;}
	else if(Get_Channel ==  CHA_DMB)	{CHA_Channel_MAX = CHA_DMB_Channel_MAX; CHA_NUM_MAX = CHA_DMB_NUM_MAX; CHA_NONE = CHA_DMB_NONE;}
	
	if (!m_hWndCHA) return;
	
	GetCtrlVal (m_hWndCHA,	CHAPNL_RING_TYPE, 		&cVal);
	m_pCHASet->TYPE = cVal;
	
	GetCtrlVal (m_hWndCHA,	CHAPNL_TG_CFR, 		&iVal);
	m_pCHASet->CFR_en = iVal;
	
	GetCtrlVal (m_hWndCHA,	CHAPNL_NMR_CFR_THE, 		&iVal2);
	m_pCHASet->CFR_thresh[0] = LOBYTE((INT16)iVal2);
	m_pCHASet->CFR_thresh[1] = HIBYTE((INT16)iVal2);
						
	GetCtrlVal (m_hWndCHA,	CHAPNL_TG_AGC, 		&iVal);
	m_pCHASet->AGC = iVal;
	
	GetCtrlVal(m_hWndCHA, CHAPNL_NMR_OUT_PWR,	&dVal);
	m_pCHASet->DSP_Output_power[0] = LOBYTE((INT16)dVal*2);
	m_pCHASet->DSP_Output_power[1] = HIBYTE((INT16)dVal*2);

	m_pCHASet->Ch_cnt = 0;

	for(i = 0; i < CHA_NUM_MAX; i++)
	{
		GetCtrlVal (m_hWndCHA,	CHA_NUM[i], 		&cVal);
		m_pCHASet->CH[m_pCHASet->Ch_cnt].NUM = cVal;
		
		
	
		GetCtrlVal (m_hWndCHA,	CHA_GAIN[i], 		&dVal);
		m_pCHASet->CH[m_pCHASet->Ch_cnt].Gain_x2[0] = LOBYTE((INT16)dVal*2.);
		m_pCHASet->CH[m_pCHASet->Ch_cnt].Gain_x2[1] = HIBYTE((INT16)dVal*2.);
	
	
		GetCtrlVal (m_hWndCHA,	CHA_CH_EN[i], 		&iVal);
		m_pCHASet->CH[m_pCHASet->Ch_cnt].Enable = iVal;
		
		if( cVal != CHA_NONE ) m_pCHASet->Ch_cnt++;
	}
	

	SendRequest(0, CHANNEL_CON_REQ, m_pCHASet, (m_pCHASet->Ch_cnt*4)+8);
}
	
void CHAStatusDataDisplay(void)
{
	UINT8	i = 0;
	UINT8	AMP_SET_ID = 0;

	UINT8	CHA_Channel_MAX, CHA_NUM_MAX, CHA_NONE, Get_Channel; 

	GetCtrlVal (m_hWndCHA,	CHAPNL_RING_TYPE, 		&Get_Channel);

	if(Get_Channel ==  CHA_TRS)		{CHA_Channel_MAX = CHA_TRS_Channel_MAX; CHA_NUM_MAX = CHA_TRS_NUM_MAX; CHA_NONE = CHA_TRS_NONE;}
	else if(Get_Channel ==  CHA_AM)	{CHA_Channel_MAX = CHA_AM_Channel_MAX; CHA_NUM_MAX = CHA_AM_NUM_MAX; CHA_NONE = CHA_AM_NONE;}
	else if(Get_Channel ==  CHA_FM)	{CHA_Channel_MAX = CHA_FM_Channel_MAX; CHA_NUM_MAX = CHA_FM_NUM_MAX; CHA_NONE = CHA_FM_NONE;}
	else if(Get_Channel ==  CHA_DMB)	{CHA_Channel_MAX = CHA_DMB_Channel_MAX; CHA_NUM_MAX = CHA_DMB_NUM_MAX; CHA_NONE = CHA_DMB_NONE;}

	
	if (!m_hWndCHA) return;
	
	SetCtrlAttribute(m_hWndCHA, CHAPNL_BTN_SET, ATTR_DIMMED, 0);
	
	SetCtrlVal (m_hWndCHA,	CHAPNL_RING_TYPE, 		m_pCHASts->TYPE);
	  	
	SetCtrlVal (m_hWndCHA,	CHAPNL_TG_CFR, 			m_pCHASts->CFR_en);
	
	SetCtrlVal(m_hWndCHA, CHAPNL_NMR_OUT_PWR,  (double)MAKEWORD(m_pCHASts->DSP_Output_power[0], m_pCHASts->DSP_Output_power[1])/2); 	

	if(m_pCHASts->CFR_en ==1) // ON
	{
		SetCtrlAttribute(m_hWndCHA, CHAPNL_NMR_CFR_THE, ATTR_DIMMED, 0);
	}
	else
	{
		SetCtrlAttribute(m_hWndCHA, CHAPNL_NMR_CFR_THE, ATTR_DIMMED, 1);
	}
	
	SetCtrlVal (m_hWndCHA,	CHAPNL_NMR_CFR_THE, 		MAKEWORD(m_pCHASts->CFR_thresh[0], m_pCHASts->CFR_thresh[1]));
	
	SetCtrlVal (m_hWndCHA,	CHAPNL_TG_AGC, 			m_pCHASts->AGC);
	
	if( m_pCHASts->AGC == 1)
	{
		SetCtrlAttribute(m_hWndCHA, CHAPNL_NMR_OUT_PWR, ATTR_DIMMED, 0); 		
	}
	else
	{
		SetCtrlAttribute(m_hWndCHA, CHAPNL_NMR_OUT_PWR, ATTR_DIMMED, 1); 	
	}
	
	
	for(i = 0; i < CHA_NUM_MAX; i++)
	{
		if( i < m_pCHASts->Ch_cnt)
		{

			SetCtrlVal (m_hWndCHA,	CHA_NUM[i], 		m_pCHASts->CH[i].NUM);
			SetCtrlVal (m_hWndCHA,	CHA_GAIN[i], 		(double)MAKEWORD(m_pCHASts->CH[i].Gain_x2[0], m_pCHASts->CH[i].Gain_x2[1]) / 2.);
			SetCtrlVal (m_hWndCHA,	CHA_CH_EN[i], 			m_pCHASts->CH[i].Enable);
		}
		else
		{
			SetCtrlVal (m_hWndCHA,	CHA_NUM[i], 		CHA_NONE); 
			
		}
		
		CHA_DIMMED(CHA_NUM[i]);
	}
	
}

int CVICALLBACK CtrlCHACheck (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	/*
	int i, j;
	int hWnd = m_hWndCONOPER;
	
	if(event!=EVENT_COMMIT && event!=EVENT_VAL_CHANGED && event!=EVENT_KEYPRESS && event != EVENT_LEFT_CLICK )
		return 0;
	
	
	for(i=0; i<2; i++)
	{								 
		for(j=0; j<DEF_CTRLOPER_CNT; j++)
		{		   
			if(iCtrlOperCtrl[i][j] == control)
			{
				int iCtrlStyle;
				
				EnableTimer(m_hWndMain, m_pCOMM[i].iPollingTimer, FALSE);
				EnableTimer(m_hWndMain, m_pCOMM[i].iWaitTimer, TRUE);
				
				GetCtrlAttribute (panel, control, ATTR_CTRL_STYLE, &iCtrlStyle);       
				if(iCtrlStyle != CTRL_CHECK_BOX)
				{
					int iCtrlMode;
					GetCtrlAttribute (panel, iCtrlOperCtrl[i][j], ATTR_CTRL_MODE, &iCtrlMode);
					if (iCtrlMode == VAL_INDICATOR)
						break;
			
					SetCtrlAttribute (hWnd, iCtrlOperCtrl[i][j], ATTR_TEXT_COLOR, VAL_BLUE);
					SetCtrlAttribute (hWnd, iCtrlOperCtrl[i][j], ATTR_TEXT_BOLD, TRUE);
				}
				SetCtrlVal(hWnd, iCtrlOperCtrlFlag[i][j], TRUE);
				
				if(event == EVENT_VAL_CHANGED)Freq_Conrol(control);
				
			
				return 0;
			}
		}
	}  
	*/

	if(event == EVENT_LEFT_CLICK)
	{
		CHA_NUM_Change(control);	
	}
		
	if(event == EVENT_VAL_CHANGED)
	{
		CHA_DIMMED(control);
	}
	
	return 0;
}



void CHA_NUM_Change(int control)   
{
	int 	i, InsertList;
	UINT8	NUMVal, Get_Con_Num;
	char	Data_Buffer[CHA_AM_Channel_MAX];
	
	UINT8	CHA_Channel_MAX, CHA_NUM_MAX, CHA_NONE, Get_Channel; 

	GetCtrlVal (m_hWndCHA,	CHAPNL_RING_TYPE, 		&Get_Channel);

	if(Get_Channel ==  CHA_TRS)		{CHA_Channel_MAX = CHA_TRS_Channel_MAX; CHA_NUM_MAX = CHA_TRS_NUM_MAX; CHA_NONE = CHA_TRS_NONE;}
	else if(Get_Channel ==  CHA_AM)	{CHA_Channel_MAX = CHA_AM_Channel_MAX; CHA_NUM_MAX = CHA_AM_NUM_MAX; CHA_NONE = CHA_AM_NONE;}
	else if(Get_Channel ==  CHA_FM)	{CHA_Channel_MAX = CHA_FM_Channel_MAX; CHA_NUM_MAX = CHA_FM_NUM_MAX; CHA_NONE = CHA_FM_NONE;}
	else if(Get_Channel ==  CHA_DMB)	{CHA_Channel_MAX = CHA_DMB_Channel_MAX; CHA_NUM_MAX = CHA_DMB_NUM_MAX; CHA_NONE = CHA_DMB_NONE;}

	GetCtrlVal(m_hWndCHA, control, &Get_Con_Num);    
	
	for( i=0; i<CHA_Channel_MAX; i++) Data_Buffer[i] = 0;
	for( i=0; i<CHA_NUM_MAX; i++)
	{
		GetCtrlVal(m_hWndCHA, CHA_NUM[i], &NUMVal);
		
		if(( NUMVal < CHA_NONE) && ( NUMVal != Get_Con_Num ))
		{
			Data_Buffer[NUMVal]	= 1;
		}
	}
		
	  	  SetCtrlAttribute (m_hWndCHA, CHAPNL_RING_NUM_0, ATTR_TEXT_COLOR, VAL_BLUE);

	
	DeleteListItem (m_hWndCHA, control, 0, -1);
		
	for( i=0; i<CHA_Channel_MAX; i++)
	{
		if( Data_Buffer[i] == 0)
		{
			if(Get_Channel ==  CHA_TRS)			InsertListItem(m_hWndCHA, control, -1, CHA_TRS_Channel[i], i);
			else if(Get_Channel ==  CHA_AM)		InsertListItem(m_hWndCHA, control, -1, CHA_AM_Channel[i], i);
			else if(Get_Channel ==  CHA_FM)		InsertListItem(m_hWndCHA, control, -1, CHA_FM_Channel[i], i);
			else if(Get_Channel ==  CHA_DMB)	InsertListItem(m_hWndCHA, control, -1, CHA_DMB_Channel[i], i);
		}
			
	}
	
	SetCtrlVal(m_hWndCHA, control, Get_Con_Num);
}


void CHADisplay_Change(void)
{
	int 	i, iVal;
	
	UINT8	CHA_Channel_MAX, CHA_NUM_MAX, CHA_NONE, Get_Channel; 

	GetCtrlVal (m_hWndCHA,	CHAPNL_RING_TYPE, 		&Get_Channel);

	if(Get_Channel ==  CHA_TRS)		{CHA_Channel_MAX = CHA_TRS_Channel_MAX; CHA_NUM_MAX = CHA_TRS_NUM_MAX; CHA_NONE = CHA_TRS_NONE;}
	else if(Get_Channel ==  CHA_AM)	{CHA_Channel_MAX = CHA_AM_Channel_MAX; CHA_NUM_MAX = CHA_AM_NUM_MAX; CHA_NONE = CHA_AM_NONE;}
	else if(Get_Channel ==  CHA_FM)	{CHA_Channel_MAX = CHA_FM_Channel_MAX; CHA_NUM_MAX = CHA_FM_NUM_MAX; CHA_NONE = CHA_FM_NONE;}
	else if(Get_Channel ==  CHA_DMB)	{CHA_Channel_MAX = CHA_DMB_Channel_MAX; CHA_NUM_MAX = CHA_DMB_NUM_MAX; CHA_NONE = CHA_DMB_NONE;}
	
	
	GetCtrlVal (m_hWndCHA,	CHAPNL_TG_AGC, 		&iVal);
		
	if( iVal == 1)
	{
		SetCtrlAttribute(m_hWndCHA, CHAPNL_NMR_OUT_PWR, ATTR_DIMMED, 0); 		
	}
	else
	{
		SetCtrlAttribute(m_hWndCHA, CHAPNL_NMR_OUT_PWR, ATTR_DIMMED, 1); 	
	}
	
	for( i=0; i<CHA_NUM_MAX; i++)
	{
		CHA_DIMMED(CHA_NUM[i]);
	}
	
}

void CHADisplay_Init(void)
{
	int 	i, Set_DIMMED;

	UINT8	CHA_Channel_MAX, CHA_NUM_MAX, CHA_NONE, Get_Channel; 

	GetCtrlVal (m_hWndCHA,	CHAPNL_RING_TYPE, 		&Get_Channel);

	if(Get_Channel ==  CHA_TRS)		{CHA_Channel_MAX = CHA_TRS_Channel_MAX; CHA_NUM_MAX = CHA_TRS_NUM_MAX; CHA_NONE = CHA_TRS_NONE;}
	else if(Get_Channel ==  CHA_AM)	{CHA_Channel_MAX = CHA_AM_Channel_MAX; CHA_NUM_MAX = CHA_AM_NUM_MAX; CHA_NONE = CHA_AM_NONE;}
	else if(Get_Channel ==  CHA_FM)	{CHA_Channel_MAX = CHA_FM_Channel_MAX; CHA_NUM_MAX = CHA_FM_NUM_MAX; CHA_NONE = CHA_FM_NONE;}
	else if(Get_Channel ==  CHA_DMB)	{CHA_Channel_MAX = CHA_DMB_Channel_MAX; CHA_NUM_MAX = CHA_DMB_NUM_MAX; CHA_NONE = CHA_DMB_NONE;}
	

		
	for( i = 0; i <  40 ; i++)	
	{
		if( i < CHA_NUM_MAX)	Set_DIMMED = 1;
		else					Set_DIMMED = 0;

		SetCtrlAttribute(m_hWndCHA, CHA_NUM[i], ATTR_VISIBLE, Set_DIMMED);
		SetCtrlAttribute(m_hWndCHA, CHA_GAIN[i], ATTR_VISIBLE, Set_DIMMED);
		SetCtrlAttribute(m_hWndCHA, CHA_CH_EN[i], ATTR_VISIBLE, Set_DIMMED);
		
		CHA_NUM_Change(CHA_NUM[i]);
		
		SetCtrlVal(m_hWndCHA, CHA_NUM[i], CHA_NONE); 
	}
	
}



void CHA_DIMMED (int control)
{
	int		i, IDVal, iVal;
	UINT8	NUMVal;

	UINT8	CHA_Channel_MAX, CHA_NUM_MAX, CHA_NONE, Get_Channel; 

	GetCtrlVal (m_hWndCHA,	CHAPNL_RING_TYPE, 		&Get_Channel);

	if(Get_Channel ==  CHA_TRS)		{CHA_Channel_MAX = CHA_TRS_Channel_MAX; CHA_NUM_MAX = CHA_TRS_NUM_MAX; CHA_NONE = CHA_TRS_NONE;}
	else if(Get_Channel ==  CHA_AM)	{CHA_Channel_MAX = CHA_AM_Channel_MAX; CHA_NUM_MAX = CHA_AM_NUM_MAX; CHA_NONE = CHA_AM_NONE;}
	else if(Get_Channel ==  CHA_FM)	{CHA_Channel_MAX = CHA_FM_Channel_MAX; CHA_NUM_MAX = CHA_FM_NUM_MAX; CHA_NONE = CHA_FM_NONE;}
	else if(Get_Channel ==  CHA_DMB)	{CHA_Channel_MAX = CHA_DMB_Channel_MAX; CHA_NUM_MAX = CHA_DMB_NUM_MAX; CHA_NONE = CHA_DMB_NONE;}
	

	IDVal = 0;
	
	for( i=0; i<CHA_NUM_MAX; i++)
	{
		if(CHA_NUM[i] == control)
		{
			  IDVal = i;
			  break;
		}
	}
	

		
		GetCtrlVal(m_hWndCHA, CHA_NUM[IDVal], &NUMVal);
		
		if( NUMVal < CHA_NONE) // NONE Check
		{
			GetCtrlVal (m_hWndCHA,	CHAPNL_TG_AGC, 		&iVal);
		
			if( iVal == 1)
			{
				SetCtrlAttribute(m_hWndCHA, CHA_GAIN[i], ATTR_DIMMED, 1); 		
			}
			else
			{
				SetCtrlAttribute(m_hWndCHA, CHA_GAIN[i], ATTR_DIMMED, 0); 	
			}
		
			SetCtrlAttribute(m_hWndCHA, CHA_CH_EN[i], ATTR_DIMMED, 0); 
		
		}
		else
		{
			SetCtrlAttribute(m_hWndCHA, CHA_GAIN[IDVal], ATTR_DIMMED, 1);
			SetCtrlAttribute(m_hWndCHA, CHA_CH_EN[IDVal], ATTR_DIMMED, 1);	
		}
			
}


void CHASaveData(void)	/////////////////////////////////////
{

	int 	i,hFile, ID_Cnt;
	double 	fval;
	double 	dVal; 
	int		iVal;
	UINT8 	cVal;
	char 	strPathName[260]={0,};
	
	UINT8	CHA_Channel_MAX, CHA_NUM_MAX, CHA_NONE, Get_Channel; 

	GetCtrlVal (m_hWndCHA,	CHAPNL_RING_TYPE, 		&Get_Channel);

	if(Get_Channel ==  CHA_TRS)		{CHA_Channel_MAX = CHA_TRS_Channel_MAX; CHA_NUM_MAX = CHA_TRS_NUM_MAX; CHA_NONE = CHA_TRS_NONE;}
	else if(Get_Channel ==  CHA_AM)	{CHA_Channel_MAX = CHA_AM_Channel_MAX; CHA_NUM_MAX = CHA_AM_NUM_MAX; CHA_NONE = CHA_AM_NONE;}
	else if(Get_Channel ==  CHA_FM)	{CHA_Channel_MAX = CHA_FM_Channel_MAX; CHA_NUM_MAX = CHA_FM_NUM_MAX; CHA_NONE = CHA_FM_NONE;}
	else if(Get_Channel ==  CHA_DMB)	{CHA_Channel_MAX = CHA_DMB_Channel_MAX; CHA_NUM_MAX = CHA_DMB_NUM_MAX; CHA_NONE = CHA_DMB_NONE;}
	

		
	FileSelectPopup("", "*.txt", "*.txt", "DSP Data Save",
					 VAL_SAVE_BUTTON, 0, 0, 1, 1, strPathName);

	if(strPathName[0] == 0) return;
	
	hFile = OpenFile(strPathName, 2, 0, 1); 
				

	if (!m_hWndCHA) return;
	
	GetCtrlVal (m_hWndCHA,	CHAPNL_RING_TYPE, 		&cVal);
	FmtFile(hFile, "%s<%d", cVal);
	WriteFile(hFile, "\n", 1);
	
	GetCtrlVal (m_hWndCHA,	CHAPNL_TG_CFR, 		&iVal);
	FmtFile(hFile, "%s<%d", iVal);
	WriteFile(hFile, "\n", 1);
	
	GetCtrlVal (m_hWndCHA,	CHAPNL_NMR_CFR_THE, 		&iVal);
	FmtFile(hFile, "%s<%d", iVal);
	WriteFile(hFile, "\n", 1);
						
	GetCtrlVal (m_hWndCHA,	CHAPNL_TG_AGC, 		&iVal);
	FmtFile(hFile, "%s<%d", iVal);
	WriteFile(hFile, "\n", 1);

	ID_Cnt = 40;
	
	FmtFile(hFile, "%s<%d", ID_Cnt);
	WriteFile(hFile, "\n", 1);
	
	
	for(i = 0; i < CHA_NUM_MAX; i++)
	{
		GetCtrlVal (m_hWndCHA,	CHA_NUM[i], 		&cVal);
		FmtFile(hFile, "%s<%d", cVal);
		WriteFile(hFile, "\n", 1);

		GetCtrlVal (m_hWndCHA,	CHA_GAIN[i], 		&dVal);
		FmtFile(hFile, "%s<%f[x]", dVal);
		WriteFile(hFile, "\n", 1); 
	
		GetCtrlVal (m_hWndCHA,	CHA_CH_EN[i], 		&iVal);
		FmtFile(hFile, "%s<%d", iVal);
		WriteFile(hFile, "\n", 1);
	}
	
	CloseFile(hFile); 
}

void CHALoadDataDisplay(void)
{
	UINT8	AMP_SET_ID = 0;
	char 	strTemp[32];
	int i,hFile;
	char strPathName[260]={0,};
	int 	iVal;
	double 	dVal;

	UINT8	CHA_Channel_MAX, CHA_NUM_MAX, CHA_NONE, Get_Channel; 

	GetCtrlVal (m_hWndCHA,	CHAPNL_RING_TYPE, 		&Get_Channel);

	if(Get_Channel ==  CHA_TRS)		{CHA_Channel_MAX = CHA_TRS_Channel_MAX; CHA_NUM_MAX = CHA_TRS_NUM_MAX; CHA_NONE = CHA_TRS_NONE;}
	else if(Get_Channel ==  CHA_AM)	{CHA_Channel_MAX = CHA_AM_Channel_MAX; CHA_NUM_MAX = CHA_AM_NUM_MAX; CHA_NONE = CHA_AM_NONE;}
	else if(Get_Channel ==  CHA_FM)	{CHA_Channel_MAX = CHA_FM_Channel_MAX; CHA_NUM_MAX = CHA_FM_NUM_MAX; CHA_NONE = CHA_FM_NONE;}
	else if(Get_Channel ==  CHA_DMB)	{CHA_Channel_MAX = CHA_DMB_Channel_MAX; CHA_NUM_MAX = CHA_DMB_NUM_MAX; CHA_NONE = CHA_DMB_NONE;}
	
	
	FileSelectPopup("", "*.txt", "*.txt", "Table Data Load",
					 VAL_LOAD_BUTTON, 0, 0, 1, 0, strPathName);

	if(strPathName[0] == 0) return; 

	hFile = OpenFile(strPathName, VAL_READ_ONLY, VAL_OPEN_AS_IS,VAL_ASCII);			

	ScanFile(hFile, "%s>%d", &iVal);
	m_pCHASts->TYPE = iVal;
	ScanFile(hFile, "%s>%d", &iVal);
	m_pCHASts->CFR_en = iVal;
	ScanFile(hFile, "%s>%d", &iVal);
	m_pCHASts->CFR_thresh[0] = LOBYTE((INT16)iVal);
	m_pCHASts->CFR_thresh[1] = HIBYTE((INT16)iVal);
	ScanFile(hFile, "%s>%d", &iVal);
	m_pCHASts->AGC = iVal;
	ScanFile(hFile, "%s>%d", &iVal);
	m_pCHASts->Ch_cnt = iVal;
	
	for(i = 0; i < CHA_NUM_MAX; i++)
	{
		if( i < m_pCHASts->Ch_cnt)
		{
			ScanFile(hFile, "%s>%d", &iVal);
			m_pCHASts->CH[i].NUM = iVal;
			ScanFile(hFile, "%s>%f[x]", &dVal);
			m_pCHASts->CH[i].Gain_x2[0] = LOBYTE((INT16)dVal * 2);
			m_pCHASts->CH[i].Gain_x2[1] = HIBYTE((INT16)dVal * 2);
			
			
			ScanFile(hFile, "%s>%d", &iVal);
			m_pCHASts->CH[i].Enable = iVal;
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	
	
	SetCtrlVal (m_hWndCHA,	CHAPNL_RING_TYPE, 	m_pCHASts->TYPE);
	
	SetCtrlVal (m_hWndCHA,	CHAPNL_TG_CFR, 		m_pCHASts->CFR_en);

	if(m_pCHASts->CFR_en ==1) // ON
	{
		SetCtrlAttribute(m_hWndCHA, CHAPNL_NMR_CFR_THE, ATTR_DIMMED, 0);
	}
	else
	{
		SetCtrlAttribute(m_hWndCHA, CHAPNL_NMR_CFR_THE, ATTR_DIMMED, 1);
	}
	
	SetCtrlVal (m_hWndCHA,	CHAPNL_NMR_CFR_THE, 		(double)MAKEWORD(m_pCHASts->CFR_thresh[0], m_pCHASts->CFR_thresh[1]));
	
	SetCtrlVal (m_hWndCHA,	CHAPNL_TG_AGC, 			m_pCHASts->AGC);
	
	for(i = 0; i < CHA_NUM_MAX; i++)
	{
		if( i < m_pCHASts->Ch_cnt)
		{

			SetCtrlVal (m_hWndCHA,	CHA_NUM[i], 		m_pCHASts->CH[i].NUM);
			SetCtrlVal (m_hWndCHA,	CHA_GAIN[i], 		(double)MAKEWORD(m_pCHASts->CH[i].Gain_x2[0], m_pCHASts->CH[i].Gain_x2[1])/ 2.);
			SetCtrlVal (m_hWndCHA,	CHA_CH_EN[i], 			m_pCHASts->CH[i].Enable);
		}
		else
		{
			SetCtrlVal (m_hWndCHA,	CHA_NUM[i], 		CHA_NONE); 
			
		}
		
		CHA_DIMMED(CHA_NUM[i]);
	}
	
	CHASetData(); 
	
}	
