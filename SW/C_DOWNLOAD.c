#include <userint.h>
#include <formatio.h>
#include <ansi_c.h>
#include "G_MAIN.h"
#include "H_EXTERN.H"
#include "H_DOWNLOAD.h"

/****************************************************************************/
/* User Define Options														*/			
/****************************************************************************/

// Last Frame Size
//////////////////////////////////////////////////////////////////////////////

int DownLoad_REST = 0;		// 0: Length 만큼..
							// 1: 나머지 부분 0xFF 로 채워서 512 Byte 만큼..


// System Selection 
//////////////////////////////////////////////////////////////////////////////

int DOWN_TYPE = 0;			// 0: 단독 System으로 ID가 한가지 TYPE일 경우.
							// 1: MULTI System으로 Donor/Remote별로 ID가 다를경우.. 

							
// Size of One Packet Frame 
//////////////////////////////////////////////////////////////////////////////

int m_iPacketSize = 1024;	//	1024


unsigned int crccode;


// System ID Setup
//////////////////////////////////////////////////////////////////////////////

int CVICALLBACK OnChangeIDRingDn (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int iValue;
	int i;
	
	if(event != EVENT_COMMIT)
		return 0;


	GetCtrlVal(m_hWndDownload, DLPNL_RING_SYSTEM, &iValue);
	

	return 0;
}

void DownLoadIdDisplay (BYTE *pData)
{
	if(!m_hWndDownload)
		return;


}

/****************************************************************************/
/* User Define Options END													*/			
/****************************************************************************/



//--------------------------------------------------------------------------//	
// Download Window Open & Close
//--------------------------------------------------------------------------//	
void DownOpen(void)
{
	char strTit[32];
	int iIdx;

	AllPanelCloseFunc(-1);
	EnableTimer(m_hWndMain, MAINPNL_TIMER, FALSE);
	
	OpenNewPanel(&m_hWndDownload, DLPNL, TRUE, "Download", UIR_FILE, FALSE);

	iDnStep = DN_READY;	
//	SetCtrlVal(m_hWndDownload, DLPNL_RING_SYSTEM, m_iSysID1[0]);

	OnChangeIDRingDn(0, DLPNL_RING_SYSTEM, EVENT_COMMIT, 0, 0, 0);
} 

//--------------------------------------------------------------------------//	
// 	DownLoad Progress Time..!
//--------------------------------------------------------------------------//	
int CVICALLBACK DLTimer_Progress (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	char cBuf[64];

	if(event != EVENT_TIMER_TICK)
		return 0;

	if(m_hWndDnWait)
	{
		char stTemp[8];

		m_DnWaitTime--;

		Fmt(stTemp, "%d 초", m_DnWaitTime);
		SetCtrlVal(m_hWndDnWait, WAITPNL_TXT_TIME, stTemp);

		//DownLoad_Start(DNENDWAIT_CMD);
		SendRequest(0, STATUS_REQ, NULL, 0);

		if(m_DnWaitTime <= 0)
		{
			m_DnWaitTime = DN_WAITTIME;
			DownloadWaitClose();
 			MessagePopup("Warning!", "Download Fail!!");
		}
	}
	else
	{
		iDnTimeS++;
	
		if(iDnTimeS == 60)
		{
			iDnTimeM++;
			iDnTimeS = 0;
		}
		
		if (iDnTimeM != 0)   
		{
			Fmt(cBuf, " Progress Time : %i min %i sec", iDnTimeM, iDnTimeS);
			SetCtrlVal(m_hWndDownload,	DLPNL_PROGTIME,	cBuf);
		}
		else
		{
			Fmt(cBuf, " Progress Time : %i sec", iDnTimeS);
			SetCtrlVal(m_hWndDownload, DLPNL_PROGTIME, cBuf);
		}
	}
		
	return 0;
}


//-------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------//
//	DownLoad (BIN)                                    
//-------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------//
int CVICALLBACK DLTimer_Response (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
	if(event != EVENT_TIMER_TICK)
		return 0;
	
	if( nRetryCnt >= 3 )
	{
		nRetryCnt = 0;
		SetCtrlAttribute(m_hWndDownload, DLPNL_TIMER_DLRESP, ATTR_ENABLED, 0);
		HiddenProgressDlg();
		EnableTimer(m_hWndDownload, DLPNL_TIMER_DLPROG, FALSE);		
		
		if(iDnStep == DN_STARTCONFIRM) MessagePopup ("Warning!", "There is no response of the Download.");
		else if(iDnStep == DN_DATASEND) MessagePopup ("Warning!", "There is no response of the Download Progress.");
		else if(iDnStep == DN_ENDCONFIRM) MessagePopup ("Warning!", "There is no response of the Download Success.");
		
		iDnStep = DN_READY;			
	}
	else
	{
		nRetryCnt++;
		if(iDnStep == DN_STARTCONFIRM) 		DownLoad_Start(DNSTART_CMD);
		else if(iDnStep == DN_ENDCONFIRM) 	DownLoad_Start(DNEND_CMD); 
		else if(iDnStep == DN_DATASEND) 	DownLoad_Data_Send(iFrameIndex);	
	}
	
    return 0;
}

//-------------------------------------------------------------------------------//
void DownLoad_Start(UINT8 Command)
{	
	BYTE cBuf[1024*2];
	int nDataSize;
	unsigned int nFileSize;
	unsigned int nFrameCnt = 0;	
	int iID;

	if((!m_hWndProgress) && (!m_hWndDnWait))	 //if((!m_hWndProgress) && (!m_hWndDnWait)) 
		return;
		
	memset(cBuf, NULL, 1024*2);
	GetCtrlVal(m_hWndDownload, DLPNL_DLFRAME, &nFrameCnt);		
	
	if(iDnStep == DN_STARTCONFIRM)
	{
		cBuf[0] = 0x00;//버젼  
		cBuf[1] = 0x00;  
		cBuf[2] = 0x00;//중계기제조사
		cBuf[3] = 0x00;//컨트롤제조사
		cBuf[4] = LOBYTE(nFrameCnt); //프레임수
		cBuf[5] = HIBYTE(nFrameCnt);	
		cBuf[6] = 0x80;//reserved 2Byte
		cBuf[7] = 0x80;//reserved 2Byte
		
		nDataSize = 8;
	}
	else if (iDnStep == DN_ENDCONFIRM)
	{
		cBuf[0] = 0x00;//버젼  
		cBuf[1] = 0x00;  
		cBuf[2] = 0x00;//중계기제조사
		cBuf[3] = 0x00;//컨트롤제조사
		cBuf[4] = LOBYTE(nFrameCnt); //프레임수
		cBuf[5] = HIBYTE(nFrameCnt);	
		
		// 전체 데이터의 CRC 2Byte..
		//////////////////////////////////////////////
		GetFileInfo(strFileName, &nFileSize);  

		if(DownLoad_REST == 1)	
			nFileSize += (512 - nLastDataLength);

		crccode = sum_CRC(cDataBuf, nFileSize);
		
 		cBuf[6] = HIBYTE(crccode);		
		cBuf[7] = LOBYTE(crccode);   

		nDataSize = 7;
	}

	// Data Send
	//////////////////////////////////////////////////

	SendRequest(0, Command, cBuf, nDataSize); //데이터SEND요청	
	EnableTimer(m_hWndDownload, DLPNL_TIMER_DLRESP, TRUE);
}

//-------------------------------------------------------------------------------//
void DownLoad_Data_Send (unsigned int Data_Offset)
{
	BYTE cBuf[1024*2];
	int nDataSize;	
	int	i=0;	
	int iID;
	
	if(!m_hWndProgress)
		return;

	memset(cBuf, NULL, 1024*2);

	if(bLastFrame)
	{
		nDataSize = nLastDataLength;
		memset(cBuf, 0xff, 1024*2);
	}
	else
		nDataSize = m_iPacketSize;	
	
	GetCtrlVal(m_hWndDownload, DLPNL_DLFRAME, &nFrameCnt);		

	cBuf[0] = LOBYTE(nFrameCnt); //프레임수
	cBuf[1] = HIBYTE(nFrameCnt);	

	cBuf[2] = LOBYTE(Data_Offset);
	cBuf[3] = HIBYTE(Data_Offset);		

	for(i=0; i<nDataSize; i++)
		cBuf[4+i] = cDataBuf[Data_Offset*m_iPacketSize+i];		
	
	SetCtrlVal(m_hWndProgress, PROPNL_PROGRESS, (int)((100 * (Data_Offset+1))/nFrameCnt));
	
	// Data Send
	//////////////////////////////////////////////////
	SendRequest(0, DNDATA_CMD, cBuf, nDataSize + 4);
 	EnableTimer(m_hWndDownload, DLPNL_TIMER_DLRESP, TRUE);
}


void BinDnCommCheck (UINT8 Command)
{
	unsigned int nFrameIndex;

	switch(Command)
	{
		case DNSTART_RSP:
		{
			UINT8	ucVer[2];
			UINT8	ucMaker;
			UINT8	ucCtrlMaker;
			
			ucVer[0] = m_pRecvIO->DataBuf[0];
			ucVer[1] = m_pRecvIO->DataBuf[1];
			
			ucMaker = m_pRecvIO->DataBuf[2];
			ucCtrlMaker = m_pRecvIO->DataBuf[3];  
			
			nFrameIndex = MAKEWORD(m_pRecvIO->DataBuf[4], m_pRecvIO->DataBuf[5]);
			
			if((m_pRecvIO->DataBuf[6] == DN_ACK) && (nFrameIndex == nFrameCnt)) 
			{
				EnableTimer(m_hWndDownload, DLPNL_TIMER_DLRESP, FALSE);
							    
				nRetryCnt = 0;
				iFrameIndex = 0;
				iDnStep = DN_DATASEND;
				DownLoad_Data_Send (iFrameIndex);
			}
			else if((m_pRecvIO->DataBuf[6] == DN_NACK) || (nFrameIndex != nFrameCnt))
			{
				if(nRetryCnt >= 3) 
				{
					EnableTimer(m_hWndDownload, DLPNL_TIMER_DLRESP, FALSE);
					nRetryCnt = 0;
					iDnStep = DN_READY;
					EnableTimer(m_hWndDownload, DLPNL_TIMER_DLPROG, FALSE);
				
					HiddenProgressDlg();
					MessagePopup("Warning!", "Download Start Fail!!");
				}
				else
				{
					nRetryCnt++;
					iDnStep = DN_STARTCONFIRM;
					DownLoad_Start(DNSTART_CMD);
				}
			} 
			
			break;
		}
	
		//------------------------------------------------------------------------------------//

		case DNDATA_RSP:
			if (iDnStep == DN_DATASEND)
			{
				int nTotalFrame = MAKEWORD(m_pRecvIO->DataBuf[0], m_pRecvIO->DataBuf[1]);
				nFrameIndex = MAKEWORD(m_pRecvIO->DataBuf[2], m_pRecvIO->DataBuf[3]);				
				
				//if((m_pRecvIO->DataBuf[4] == DN_ACK) && (nFrameIndex == iFrameIndex))
				if(nFrameIndex == iFrameIndex)
				{
					EnableTimer(m_hWndDownload, DLPNL_TIMER_DLRESP, FALSE);					
					nRetryCnt = 0;
					
					if(iFrameIndex == nFrameCnt-2)
					{
						bLastFrame = 1;
						iFrameIndex = iFrameIndex+1;
						iDnStep = DN_DATASEND;
						DownLoad_Data_Send(iFrameIndex);
					}
					else if(iFrameIndex != nFrameCnt-1)
					{
						iFrameIndex = iFrameIndex+1;
						iDnStep = DN_DATASEND;
						DownLoad_Data_Send(iFrameIndex);
					}
					else
					{
						iDnStep = DN_ENDCONFIRM;
						DownLoad_Start(DNEND_CMD);
					}
				}
				else if(nFrameIndex != iFrameIndex)
				{
					if(nRetryCnt >= 3) 
					{
						EnableTimer(m_hWndDownload, DLPNL_TIMER_DLRESP, FALSE);						
						nRetryCnt = 0;
						iDnStep = DN_READY;
						EnableTimer(m_hWndDownload, DLPNL_TIMER_DLPROG, FALSE);
					
						MessagePopup ("Warning!", "Download Fail!!");
						HiddenProgressDlg();
					}
					else
					{
						nRetryCnt++;
						iDnStep = DN_DATASEND;
						DownLoad_Data_Send (iFrameIndex);
					}
				}
		/*		else if(m_pRecvIO->DataBuf[4] == DN_NACK)
				{
					EnableTimer(m_hWndDownload, DLPNL_TIMER_DLRESP, FALSE);					
					nRetryCnt = 0;
					
					iFrameIndex = MAKEWORD(m_pRecvIO->DataBuf[2], m_pRecvIO->DataBuf[3]);
					
					if(iFrameIndex == nFrameCnt-2)
					{
						bLastFrame = 1;
						iFrameIndex = iFrameIndex+1;
						iDnStep = DN_DATASEND;
						DownLoad_Data_Send(iFrameIndex);
					}
					else if(iFrameIndex != nFrameCnt-1)
					{
						iFrameIndex = iFrameIndex+1;
						iDnStep = DN_DATASEND;
						DownLoad_Data_Send(iFrameIndex);
					}
					else
					{
						iDnStep = DN_ENDCONFIRM;
						DownLoad_Start(DNEND_CMD);
					}
				}
			*/	
			}
			break;
		//------------------------------------------------------------------------------------//
		case DNEND_CRSP:
			if (iDnStep == DN_ENDCONFIRM)
			{
				UINT8	ucMaker;
				UINT8	ucCtrlMaker;
				unsigned int RecvCrcCode;

	
				ucMaker = m_pRecvIO->DataBuf[0];
				ucCtrlMaker = m_pRecvIO->DataBuf[1];  
				
			//	printf("%d\n", crccode);  
				RecvCrcCode = (UINT16)MAKEWORD(m_pRecvIO->DataBuf[3], m_pRecvIO->DataBuf[2]);	
			//	printf("%d\n", RecvCrcCode); 
				RecvCrcCode = (UINT16)MAKEWORD(m_pRecvIO->DataBuf[2], m_pRecvIO->DataBuf[3]);				
			//	printf("%d\n", RecvCrcCode);
				
				if((m_pRecvIO->DataBuf[4] == DN_ACK) && (crccode == RecvCrcCode))
				{
					EnableTimer(m_hWndDownload, DLPNL_TIMER_DLRESP, FALSE);
					nRetryCnt = 0;
					iDnStep = DN_ENDWAIT;
					HiddenProgressDlg();
					EnableTimer(m_hWndDownload, DLPNL_TIMER_DLPROG, FALSE);
					
					DownloadWaitOpen();					
				}
				else if((m_pRecvIO->DataBuf[4] == DN_NACK) || (crccode != RecvCrcCode))
				{
					if(nRetryCnt >= 3) 
					{
						EnableTimer(m_hWndDownload, DLPNL_TIMER_DLRESP, FALSE);
						nRetryCnt = 0;
						iDnStep = DN_READY;
						EnableTimer(m_hWndDownload, DLPNL_TIMER_DLPROG, FALSE);
					
						HiddenProgressDlg();
						MessagePopup ("Warning!", "Download Fail!!");
					}
					else
					{
						nRetryCnt++;
						iDnStep = DN_ENDCONFIRM;
						DownLoad_Start(DNEND_CMD);
					}
				}
			}
			break;
		//------------------------------------------------------------------------------------//
		case DNENDWAIT_CMD:
		{
			char cBuf[32];

			if(!m_hWndDnWait)
				return;
		
			DownloadWaitClose();

			Fmt(cBuf, "Download End - F/W Ver %x.%x", (m_pRecvIO->DataBuf[0] >> 4) & 0x0F, m_pRecvIO->DataBuf[0] & 0x0F);
			MessagePopup ("Success!", cBuf);
			
			break;
		}
		case STATUS_RPY:
		{
			if(!m_hWndDnWait)
				return;
		
			DownloadWaitClose();
			MessagePopup ("Success!", "Download End");
			break;
		}
		
		//------------------------------------------------------------------------------------//
	}    
}


int CVICALLBACK OnCmdDown (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int iVal, iVis;
	if(event != EVENT_COMMIT)
		return 0;
		
	// DownLoad File Open Button
	if (control == DLPNL_BTN_FILEOPEN)
	{
		int filetype;
		int Open_File;
		int FileSize;
		int DnFileHandle;
		double	Frame_Cnt_1;
		unsigned int Frame_Cnt_2;

		Open_File = FileSelectPopup ("", "*.*", "*.*", "Name of File to Read", 	VAL_OK_BUTTON, 0, 1, 1, 0, strFileName);		
		
		if (Open_File < 0)
		{
			MessagePopup ("Warning!", "File Select Error!");
			return 0;
		}
		//-------------------------------------------------------------------------------------------------------------------------//
		if (Open_File > 0)
		{
 			GetFileInfo (strFileName, &FileSize);
			SetCtrlVal (m_hWndDownload, DLPNL_DLSIZE, FileSize);
			SetCtrlVal (m_hWndDownload, DLPNL_DLPATH, strFileName);

			DnFileHandle = OpenFile (strFileName, VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
			FileToArray (strFileName, cDataBuf, VAL_UNSIGNED_CHAR, FileSize, 1, VAL_GROUPS_TOGETHER, VAL_GROUPS_AS_COLUMNS, VAL_BINARY);
			CloseFile(DnFileHandle);
			
			Frame_Cnt_1 = (double)FileSize/(double)m_iPacketSize;
			Frame_Cnt_2 = (int)Frame_Cnt_1;
			Frame_Cnt_1 = Frame_Cnt_1*10000.;

			if(((unsigned int)Frame_Cnt_1)%10000) 
			{
				Frame_Cnt_2 = Frame_Cnt_2+1;
				nLastDataLength = FileSize%m_iPacketSize;
			}
			else nLastDataLength = m_iPacketSize;
	
			nFrameCnt = Frame_Cnt_2;
		
			SetCtrlVal (m_hWndDownload, DLPNL_DLFRAME, nFrameCnt);
			iDnStep = DN_READY;
			//-------------------------------------------------------------------------------------------------------------------------//
			SetCtrlAttribute (m_hWndDownload, DLPNL_BTN_DLSTART, ATTR_DIMMED, 0);
			//-------------------------------------------------------------------------------------------------------------------------//
		}
	}


	// DownLoad Start Button
	else if (control == DLPNL_BTN_DLSTART)
	{
		int iSelFile;
		int iSysVal;
		int i, iVal;
		
		// ID채우기 //
//		GetCtrlVal (m_hWndDownload,	DLPNL_RING_SYSTEM1,		&iVal);
//		m_iSysID[0] = (BYTE)iVal;
//		GetCtrlVal (m_hWndDownload,	DLPNL_RING_SYSTEM2,		&iVal);
//		m_iSysID[1] = (BYTE)iVal;
//		GetCtrlVal (m_hWndDownload,	DLPNL_RING_SYSTEM3,		&iVal);
//		m_iSysID[2] = (BYTE)iVal;
//		GetCtrlVal (m_hWndDownload,	DLPNL_RING_SYSTEM4,		&iVal);
//		m_iSysID[3] = (BYTE)iVal;
			
		bLastFrame = 0;		
		//-------------------------------------------------------------------------------------------------------------------------//
		iDnTimeM = 0;
		iDnTimeS = 0;
		//-------------------------------------------------------------------------------------------------------------------------//
		ShowProgressDlg();
		nRetryCnt=0;
		//-------------------------------------------------------------------------------------------------------------------------//
		iDnStep = DN_STARTCONFIRM;
		DownLoad_Start(DNSTART_CMD);
		//-------------------------------------------------------------------------------------------------------------------------//
		SetCtrlAttribute (m_hWndDownload, DLPNL_PROGTIME, ATTR_VISIBLE,	1);
		EnableTimer(m_hWndDownload, DLPNL_TIMER_DLPROG, TRUE);		
		//-------------------------------------------------------------------------------------------------------------------------//
	}

	// DownLoad Window Close Button
	else if (control == DLPNL_BTN_EXIT)
	{
		int i;
		
		ClosePanel(&m_hWndDownload);
		

		EnableTimer (m_hWndMain,	MAINPNL_TIMER,	TRUE);
		
	}

	return 0;
}

/*===============================================================================*/

void ShowProgressDlg(void)
{
	OpenNewPanel(&m_hWndProgress, PROPNL, TRUE, "S/W DownLoad", UIR_FILE, FALSE);
	InstallPopup(m_hWndProgress);
}

void HiddenProgressDlg(void)
{
	EnableTimer(m_hWndDownload, DLPNL_TIMER_DLRESP, FALSE);
	
	ClosePanel(&m_hWndProgress);
	EnableTimer(m_hWndDownload, DLPNL_TIMER_DLPROG, FALSE);
}

int CVICALLBACK OnCmdProgress (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	if(event != EVENT_COMMIT)	return 0;
	HiddenProgressDlg();   
	return 0;
}

/*===============================================================================*/

void DownloadWaitOpen(void)
{
	OpenNewPanel(&m_hWndDnWait, WAITPNL, TRUE, "Download", UIR_FILE, FALSE);
	InstallPopup(m_hWndDnWait);
	
	EnableTimer(m_hWndDownload, DLPNL_TIMER_DLPROG, TRUE);
}

void DownloadWaitClose(void)
{
	if(m_hWndDnWait < 0)
		return;
		
	iDnStep = 0;
	m_DnWaitTime = DN_WAITTIME;
		
	EnableTimer(m_hWndDownload, DLPNL_TIMER_DLPROG, FALSE);
	
	ClosePanel(&m_hWndDnWait);

}

int CVICALLBACK DownloadWaitQuitButton (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	if(event != EVENT_COMMIT)
		return 0;

	DownloadWaitClose();
	return 0;
}

//////////////////////////////////////////////////////////////////////////////

	
//////////////


