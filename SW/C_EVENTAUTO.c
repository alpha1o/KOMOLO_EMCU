#include "toolbox.h"
#include <utility.h>
#include <userint.h>
#include <ansi_c.h>
#include <formatio.h>
#include "G_MAIN.h"
#include "H_EXTERN.H"

#define UIR_FILE	"G_MAIN.uir"


//====================================================================================//
// Alarm History Byte Define
//====================================================================================//

#define DATABYTE	21

//--------------------------------------------------------------------------//	
// Alarm History Structue Define
//--------------------------------------------------------------------------//	
typedef struct
{
	UINT8	iIndex[2];
	UINT8	iType;	   //0 : Aalrm	   1:Control
	UINT8	cTime[6];			//	ex) 2034년 01월 23일 12시 34분 56초
								//	=> 0x20, 0x34, 0x01, 0x23, 0x12, 0x34, 0x56  
	UINT8	cFlag[DATABYTE];
	UINT8	cData[DATABYTE];

} EVENTHIS;

EVENTHIS * m_pEventHis;

typedef struct
{
	UINT8	StartDate[3];		
	UINT8	StartIndex[2];
	
	UINT8	EndDate[3];		
	UINT8	EndIndex[2];
	
} EVENTINDEX;

EVENTINDEX * m_pEventIndex;


char *m_pEventName[DATABYTE*8][3] = 
{
//----------------------------------------------------------//
//----------------------------------------------------------//
//	* 	사용되지 않는 Bit 에 대해서는  
//		
//		반드시 "Reserved" 로 표기..!	*
//----------------------------------------------------------//
//----------------------------------------------------------//

 //----------------------------------------------------------//
// Byte,Bit		|	Alarm Name								//
//----------------------------------------------------------//
// Alarm[0] // SSPA_Alarm[0] 	
/* 0	0 */	{	"SSPA #1 Insert",					  	"not Inserted ",       	  "Inserted ",                 	},   
/* 0	1 */	{	"SSPA #2 Insert",					  	"not Inserted",        	  "Inserted",                   },   
/* 0	2 */	{	"SSPA #3 Insert",					  	"not Inserted",        	  "Inserted",                   },   
/* 0	3 */	{	"SSPA #4 Insert",					  	"not Inserted",        	  "Inserted",                   },    
/* 0	4 */	{	"SSPA #1 EEPROM",			 		 	"Invalid",             	  "Valid",                      },   
/* 0	5 */	{	"SSPA #2 EEPROM",					  	"Invalid",             	  "Valid",                      },   
/* 0	6 */	{	"SSPA #3 EEPROM",					  	"Invalid",             	  "Valid",                      },   
/* 0	7 */	{	"SSPA #4 EEPROM",					  	"Invalid",             	  "Valid",                      },   
														  	                       	                                   
// Alarm[1] 	// SSPA_Alarm[1] 						  	SPA_Alarm[1]           	  SPA_Alarm[1]                     
/* 1	0 */	{	"SSPA #1 High Power",				  	"Warning",             	  "Warning cleared",            },    
/* 1	1 */	{	"SSPA #2 High Power",				  	"Warning",             	  "Warning cleared",            },    
/* 1	2 */	{	"SSPA #3 High Power",      			  	"Warning",             	  "Warning cleared",            },    
/* 1	3 */	{	"SSPA #4 High Power",      		  		"Warning",             	  "Warning cleared",            },    
/* 1	4 */	{	"SSPA #1 Low Power",				  	"Warning",             	  "Warning cleared",            },    
/* 1	5 */	{	"SSPA #2 Low Power",			 	 	"Warning",             	  "Warning cleared",            },    
/* 1	6 */	{	"SSPA #3 Low Power",				  	"Warning",             	  "Warning clearedr",           },    
/* 1	7 */	{	"SSPA #4 Low Power",				  	"Warning",             	  "Warning cleared",            },    
														  	                       	                                   
// Alarm[2]  	// SSPA_Alarm[2]     					  	SPA_Alarm[2]           	  SPA_Alarm[2]                     
/* 2	0 */	{	"SSPA #1 Over Temp",				  	"Warning",             	  "Warning cleared",            },    
/* 2	1 */	{	"SSPA #2 Over Temp",  			 	  	"Warning",             	  "Warning cleared",            },    
/* 2	2 */	{	"SSPA #3 Over Temp",   				  	"Warning",             	  "Warning cleared",            },    
/* 2	3 */	{	"SSPA #4 Over Temp",  	 			  	"Warning",             	  "Warning cleared",            },    
/* 2	4 */	{	"-",								  	"-",                   	  "-",                          },    
/* 2	5 */	{	"-", 								  	"-",                   	  "-",                          },    
/* 2	6 */	{	"-", 								  	"-",                   	  "-",                          },    
/* 2	7 */	{	"-", 								  	"-",                   	  "-",                          },    
														  	                       	                                   
// Alarm[3]  	// SPSM_Alarm[0]   						  	PSM_Alarm[0]           	  PSM_Alarm[0]                     
/* 3	0 */	{	"SPSM #1 Insert",					  	"Not Inserted",        	  "Inserted",                   },    
/* 3	1 */	{	"SPSM #2 Insert",      				  	"Not Inserted",        	  "Inserted",                   },    
/* 3	2 */	{	"SPSM #3 Insert",      				  	"Not Inserted",        	  "Inserted",                   },    
/* 3	3 */	{	"SPSM #4 Insert",      				  	"Not Inserted",        	  "Inserted",                   },    
/* 3	4 */	{	"-", 								  	"-",                   	  "-",                          },    
/* 3	5 */	{	"-", 								  	"-",                   	  "-",                          },    
/* 3	6 */	{	"-", 								  	"-",                   	  "-",                          },    
/* 3	7 */	{	"-", 								  	"-",                   	  "-",                          },    
														  	                       	                                   
// Alarm[4]  	// SPSM_Alarm[1]   						  	PSM_Alarm[1]           	  PSM_Alarm[1]                     
/* 3	0 */	{	"SPSM #1 Voltage",					  	"Alarm",               	  "Alarm cleared",              },    
/* 3	1 */	{	"SPSM #2 Voltage",   				  	"Alarm",               	  "Alarm cleared",              },    
/* 3	2 */	{	"SPSM #3 Voltage",    				  	"Alarm",               	  "Alarm cleared",              },    
/* 3	3 */	{	"SPSM #4 Voltage",					  	"Alarm",               	  "Alarm cleared",              },    
/* 3	4 */	{	"SPSM #1 Current", 					  	"Alarm",               	  "Alarm cleared",              },    
/* 3	5 */	{	"SPSM #2 Current",           		  	"Alarm",               	  "Alarm cleared",              },    
/* 3	6 */	{	"SPSM #3 Current",          		  	"Alarm",               	  "Alarm cleared",              },    
/* 3	7 */	{	"SPSM #4 Current",     				  	"Alarm",               	  "Alarm cleared",              },    
														  	                       	                                   
// Alarm[5]  	// SPSM_Alarm[2]   						  	PSM_Alarm[2]           	  PSM_Alarm[1]                     
/* 3	0 */	{	"SPSM #1 40V High Voltage",  			"Alarm",               	  "Alarm cleared",              },    
/* 3	1 */	{	"SPSM #1 40V Low Voltage",   			"Alarm",               	  "Alarm cleared",              },    
/* 3	2 */	{	"SPSM #2 40V High Voltage", 		 	"Alarm",               	  "Alarm cleared",              },    
/* 3	3 */	{	"SPSM #2 40V Low Voltage",	  			"Alarm",               	  "Alarm cleared",              },    
/* 3	4 */	{	"SPSM #3 40V High Voltage", 		 	"Alarm",               	  "Alarm cleared",              },    
/* 3	5 */	{	"SPSM #3 40V Low Voltage",  		 	"Alarm",               	  "Alarm cleared",              },    
/* 3	6 */	{	"SPSM #4 40V High Voltage",			  	"Alarm",               	  "Alarm cleared",              },    
/* 3	7 */	{	"SPSM #4 40V Low Voltage",   			"Alarm",               	  "Alarm cleared",              },    
														  	                       	                                   
// Alarm[6]  	// SPSM_Alarm[3]   						  	PSM_Alarm[3]           	  PSM_Alarm[1]                     
/* 3	0 */	{	"SPSM #1 28V High Voltage", 		 	"Alarm",               	  "Alarm cleared",              },    
/* 3	1 */	{	"SPSM #1 28V Low Voltage",  		 	"Alarm",               	  "Alarm cleared",              },    
/* 3	2 */	{	"SPSM #2 28V High Voltage", 		 	"Alarm",               	  "Alarm cleared",              },    
/* 3	3 */	{	"SPSM #2 28V Low Voltage",			  	"Alarm",               	  "Alarm cleared",              },    
/* 3	4 */	{	"SPSM #3 28V High Voltage", 		 	"Alarm",               	  "Alarm cleared",              },    
/* 3	5 */	{	"SPSM #3 28V Low Voltage",  		 	"Alarm",               	  "Alarm cleared",              },    
/* 3	6 */	{	"SPSM #4 28V High Voltage", 		 	"Alarm",               	  "Alarm cleared",              },    
/* 3	7 */	{	"SPSM #4 28V Low Voltage",   			"Alarm",               	  "Alarm cleared",              },    
  														  	                       	                                   
// Alarm[7]  	// MPSM_Alarm   						  	PSM_Alarm              	  PSM_Alarm                        
/* 3	0 */	{	"MPSM Insert",						  	"Not Inserted ",       	  "Inserted ",                  },    
/* 3	1 */	{	"MPSM Voltage",      				  	"Alarm",               	  "Alarm cleared",              },    
/* 3	2 */	{	"MPSM Over Current",     		 	  	"Alarm",               	  "Alarm cleared",              },    
/* 3	3 */	{	"MPSM -6V High Voltage", 			  	"Alarm",               	  "Alarm cleared",              },    
/* 3	4 */	{	"MPSM -6V Low Voltage", 			  	"Alarm",               	  "Alarm cleared",              },    
/* 3	5 */	{	"-", 								  	"-",                   	  "-",                          },    
/* 3	6 */	{	"-", 								  	"-",                   	  "-",                          },    
/* 3	7 */	{	"-", 								  	"-",                   	  "-",                          },    
														  	                       	                                   
// Alarm[8]  	// MPSM_Alarm   						  	PSM_Alarm[1]           	  PSM_Alarm[1]                     
/* 3	0 */	{	"MPSM 40V High Voltage",			  	"Alarm",               	  "Alarm cleared",              },    
/* 3	1 */	{	"MPSM 40V Low Voltage",    			  	"Alarm",               	  "Alarm cleared",              },    
/* 3	2 */	{	"MPSM 28V High Voltage",   			  	"Alarm",               	  "Alarm cleared",              },    
/* 3	3 */	{	"MPSM 28V Low Voltage", 			  	"Alarm",               	  "Alarm cleared",              },    
/* 3	4 */	{	"MPSM 24V High Voltage", 			  	"Alarm",               	  "Alarm cleared",              },    
/* 3	5 */	{	"MPSM 24V Low Voltage", 			  	"Alarm",               	  "Alarm cleared",              },    
/* 3	6 */	{	"MPSM 6V High Voltage", 			  	"Alarm",               	  "Alarm cleared",              },    
/* 3	7 */	{	"MPSM 6V Low Voltage", 	  				"Alarm",               	  "Alarm cleared",              },    
   														  	                       	                                   
// Alarm[9]  	// UCM_Alarm							  	CM_Alarm               	  CM_Alarm                         
/* 3	0 */	{	"UCM Insert",						  	"not Inserted",        	  "Inserted",                   },    
/* 3	1 */	{	"UCM EEPROM ",      				  	"Invalid",             	  "Valid ",                     },    
/* 3	2 */	{	"UCM High Power",      				  	"Warning",             	  "Warning cleared",            },    
/* 3	3 */	{	"UCM Low Power",      				  	"Warning",             	  "Warning cleared",            },    
/* 3	4 */	{	"UCM Local 1", 						  	"Alarm",               	  "Alarm cleared",              },    
/* 3	5 */	{	"UCM Local 2", 						  	"Alarm",               	  "Alarm cleared",              },    
/* 3	6 */	{	"-", 								  	"-",                   	  "-",                          },    
/* 3	7 */	{	"-", 								  	"-",                   	  "-",                          },    
														  	                       	                                   
// Alarm[10]  	// DCM_Alarm   							  	CM_Alarm               	  CM_Alarm                         
/* 3	0 */	{	"DCM Insert",						  	"not Inserted",        	  "Inserted",                   },    
/* 3	1 */	{	"DCM EEPROM",      					  	"Invalid",             	  "Valid ",                     },    
/* 3	2 */	{	"DCM High Power",      				  	"Warning",             	  "Warning cleared",            },    
/* 3	3 */	{	"DCM Low Power",      				  	"Warning",             	  "Warning cleared",            },    
/* 3	4 */	{	"DCM Local 1 ", 					  	"Alarm",               	  "Alarm cleared",              },    
/* 3	5 */	{	"DCM Local 2 ", 					  	"Alarm",               	  "Alarm cleared",              },    
/* 3	6 */	{	"-", 								  	"-",                 	"-",                          },    
/* 3	7 */	{	"-", 								  	"-",                 	"-",                          },    
														  	                       	                                   
// Alarm[11]  	// FSM_Alarm   							  	SM_Alarm               	  SM_Alarm                         
/* 3	0 */	{	"FSM Insert",						  	"not Inserted",      	"Inserted",                   },    
/* 3	1 */	{	"FSM PLL 1 Lock ",      		 	 	"Denied",           	"Granted ",                   },    
/* 3	2 */	{	"FSM PLL 2 Lock ",      		  		"Denied",           	"Granted",                    },    
/* 3	3 */	{	"FSM PLL 3 Lock ",      		  		"Denied",           	"Granted ",                   },    
/* 3	4 */	{	"FSM PLL 4 Lock ", 		 		 		"Denied",            	"Granted",                    },    
/* 3	5 */	{	"FSM PLL 5 Lock ", 		 		 		"Denied",           	"Granted",                    },    
/* 3	6 */	{	"-", 								  	"-",                  	"-",                          },    
/* 3	7 */	{	"-", 								  	"-",                  	"-",                          },    
														  	                       	                                   
// Alarm[12]  	// SYS_Alarm   							  	YS_Alarm               	  YS_Alarm                         
/* 3	0 */	{	"Noise Figure",						  	"Alarm",             	"Alarm cleared",              },    
/* 3	1 */	{	"VSWR",      						  	"Alarm",             	"Alarm cleared",              },    
/* 3	2 */	{	"ACU Link",      					  	"Disconnected",       	"Connected",                  },    
/* 3	3 */	{	"SPM Link ",      					  	"Disconnected ",      	"Connected ",                 },    
/* 3	4 */	{	"ASDE FWD Power", 					  	"Alarm",              	"Alarm cleared",              },    
/* 3	5 */	{	"BitCheck Alarm", 					  	"Alarm",              	"Alarm cleared",              },    
/* 3	6 */	{	"Reset Alarm", 							"-",                  	"-",                          },    
/* 3	7 */	{	"-", 								  	"-",             		"-",                          },    
														  	                       	                                   
// Alarm[13]  	// ACU_Alarm[0]     					  	CU_Alarm[0]            	  CU_Alarm[0]                      
/* 3	0 */	{	"Motor Over Heat ", 				  	"Alarm",             	"Alarm cleared",              },    
/* 3	0 */	{	"Motor Over Load ", 				  	"Alarm",             	"Alarm cleared",              },    
/* 3	0 */	{	"Encoder #1 ", 						  	"Fault",             	"Fault cleared",              },    
/* 3	0 */	{	"Encoder #2 ", 						  	"Fault",             	"Fault cleared",              },    
/* 3	7 */	{	"Motor High Temp ", 				  	"-",                  	"-",                          },    
/* 3	7 */	{	"Motor Low Temp ",   				  	"-",                  	"-",                          },    
/* 3	0 */	{	"-",								  	"-",                  	"-",                          },    
/* 3	0 */	{	"-",								  	"-",                  	"-",                          },    
														  	                       	                                   
// Alarm[14]  	//	ACU_Alarm[1]						  	                       	                                   
/* 3	0 */	{	"Wave Guide 1 ",					  	"Alarm",            	"Alarm cleared ",             },    
/* 3	0 */	{	"Wave Guide 2 ",					  	"Alarm",            	"Alarm cleared ",             },    
/* 3	0 */	{	"Wave Guide 3 ",					  	"Alarm",             	"Alarm cleared ",             },    
/* 3	0 */	{	"-",   								  	"-",                  	"-",                          },    
/* 3	0 */	{	"Ant. Inverter P1",				 	 	"Alarm",            	"Alarm cleared",              },    
/* 3	0 */	{	"Ant. Inverter P2",					  	"Alarm",             	"Alarm cleared",              },    
/* 3	7 */	{	"-", 								  	"-",                  	"-",                          },    
/* 3	7 */	{	"-", 								  	"-",                  	"-",                          },    
				  										  	                       	                                   
// Alarm[15]  	//	ACU_Alarm[2]						  	                       	                                   
/* 3	0 */	{	"Ant. Rotation Interlock 1",		  	"Opened",            	"Closed",                 	},    
/* 3	0 */	{	"Ant. Rotation Interlock 2",  		  	"Opened",            	"Closed",                 	},    
/* 3	0 */	{	"Ant. Rotation Interlock 3", 		  	"Opened",            	"Closed",                 	},    
/* 3	0 */	{	"Ant. Safety LED",   				  	"Alarm",             	"Alarm cleared",          	},    
/* 3	0 */	{	"Ant Oil Level ",					  	"Low Fault",        	"Normal",                 	},    
/* 3	0 */	{	"Ant Oil Temp ",					  	"High ",             	"Normal",                	},    
/* 3	0 */	{	"Ant Oil Temp ",					  	"Low ",              	"Normal",                	},    
/* 3	7 */	{	"-", 								  	"-",                  	"-",                      	},    
														  	                       	                                   
// Alarm[16]  	//	ACU_Alarm[3]						  	                       	                                   
/* 3	0 */	{	"Low_Pressure",					  		"Alarm",             	"Alarm cleared",          	},      
/* 3	0 */	{	"High_Pressure",				  		"Alarm",             	"Alarm cleared",          	},      
/* 3	0 */	{	"High_Duty_Cycle_W",				  	"Alarm",             	"Alarm cleared",          	},      
/* 3	0 */	{	"High_Temp_W",						 	"Alarm",             	"Alarm cleared",          	},      
/* 3	0 */	{	"Low_Temp_W",						  	"Alarm",             	"Alarm cleared",          	},     
/* 3	7 */	{	"-", 								  	"-",                  	"-",                      	},    
/* 3	7 */	{	"-", 								  	"-",                  	"-",                      	},    
/* 3	7 */	{	"-", 								  	"-", 		          	"-", 		          		},  
														  	                       	                                   
// Alarm[17]  	//	ACU_Alarm[4]						  	                       	                                   
/* 3	0 */	{	"C_1_Heat",						  		"Alarm",             	"Alarm cleared",          	},      
/* 3	0 */	{	"C_1_Cool",						  		"Alarm",             	"Alarm cleared",          	},      
/* 3	0 */	{	"C_2_Heat",							  	"Alarm",             	"Alarm cleared",          	},      
/* 3	0 */	{	"C_2_Cool",							 	"Alarm",             	"Alarm cleared",          	},      
/* 3	0 */	{	"Unable_Pressurize",				  	"Alarm",             	"Alarm cleared",          	},      
/* 3	0 */	{	"Dew_Point_Alarm",						"Alarm",             	"Alarm cleared",       		},      
/* 3	7 */	{	"-", 								  	"-",                  	"-",                       	},      
/* 3	7 */	{	"-", 								  	"-", 		          	"-", 		              	},    
														  	                       	                                   
// Alarm[18]  	// Middle_FAN_Aalrm   					  	iddle_FAN_Aalrm        	  iddle_FAN_Aalrm                  
/* 3	0 */	{	"#1 FAN 1 Tray",					  	"Warning",            	"Warning cleared",        	},      
/* 3	1 */	{	"#1 FAN 2 Tray",      				  	"Warning",    	 	 	"Warning cleared",        	},      
/* 3	2 */	{	"#1 FAN 3 Tray",      				  	"Warning",      		"Warning cleared",          },      
/* 3	3 */	{	"-",      							  	"-",                  	"-",                        },      
/* 3	4 */	{	"-", 								  	"-",                  	"-",                        },      
/* 3	5 */	{	"-", 								  	"-",                  	"-",                        },      
/* 3	6 */	{	"-", 								  	"-",                 	"-",                        },      
/* 3	7 */	{	"-", 								  	"-",                  	"-",                        },      
														  	                       	                                   
// Alarm[19]  	// Front_FAN_Aalrm   					  	ront_FAN_Aalrm         	  ront_FAN_Aalrm                   
/* 3	0 */	{	"#2 FAN 1 Tray",					  	"Warning",            	"Warning cleared",          },      
/* 3	1 */	{	"#2 FAN 2 Tray",      				  	"Warning",            	"Warning cleared",          },      
/* 3	2 */	{	"#2 FAN 3 Tray",      				  	"Warning",            	"Warning cleared",          },      
/* 3	3 */	{	"-",       							  	"-",                  	"-",                        },      
/* 3	4 */	{	"-", 								  	"-",                  	"-",                        },      
/* 3	5 */	{	"-", 								  	"-",                  	"-",                        },      
/* 3	6 */	{	"-", 								  	"-",                 	"-",                        },      
/* 3	7 */	{	"-", 	   							  	"-", 	             	"-", 	                  	},   

// Alarm[20]  	// SYS_2_Alarm   					  		ront_FAN_Aalrm         	  ront_FAN_Aalrm                   
/* 3	0 */	{	"FWD_H_PWR_Alm_Bit",				  	"Warning",            	"Warning cleared",        	},      
/* 3	1 */	{	"FWD_L_PWR_Alm_Bit",      			  	"Warning",            	"Warning cleared",        	},      
/* 3	2 */	{	"TEMP_H_Alm_Bit",      				  	"Warning",            	"Warning cleared",        	},      
/* 3	3 */	{	"TEMP_L_Alm_Bit",       				"Warning",            	"Warning cleared",        	},      
/* 3	4 */	{	"-", 								  	"-",                   	"-",                      	},      
/* 3	5 */	{	"-", 								  	"-",                	"-",                      	},      
/* 3	6 */	{	"-", 								  	"-",                 	"-",                       	},      
/* 3	7 */	{	"-", 	   							  	"-", 	               	"-", 	                	},  

};


char *m_pEventCtrlName[DATABYTE*8][3] = 
{
//----------------------------------------------------------//
//----------------------------------------------------------//
//	* 	사용되지 않는 Bit 에 대해서는  
//		
//		반드시 "Reserved" 로 표기..!	*
//----------------------------------------------------------//
//----------------------------------------------------------//

 //----------------------------------------------------------//
// Byte,Bit		|	Alarm Name								//
//----------------------------------------------------------//
// Alarm[0] 	//	ControlFlag[0] 
/* 0	0 */	{	"Change_Over_CTRL ",					"Selected", 			"Selected",					},   
/* 0	1 */	{	"Change_Over_Mode",						"Auto", 				"Manual",					},    
/* 0	2 */	{	"PRF_Select",							"Auto", 				"Manual",      				},        
/* 0	3 */	{	"Antenna Motor Heater", 				"on", 					"off",  					},   
/* 0	4 */	{	"SSPA ALL Radiation",   				"on", 					"off",       				},   
/* 0	5 */	{	"SPSM ALL Radiation",   				"on", 					"off",       				},   
/* 0	6 */	{	"FAN ALL",	 							"on", 					"off",       				},   
/* 0	7 */	{	"VSWR_Limit",  							"-",					"-",						},   
																																			          
// Alarm[1] 	//	ControlFlag[1] 																											          
/* 1	0 */	{	"SSPA #1 Radiation",	 				"on", 					"off",       				},   
/* 1	1 */	{	"SSPA #2 Radiation",	 				"on", 					"off",       				},   
/* 1	2 */	{	"SSPA #3 Radiation",     				"on", 					"off",        				},   
/* 1	3 */	{	"SSPA #4 Radiation",     				"on", 					"off",        				},   
/* 1	4 */	{	"SPSM #1 On/Off",		 				"on", 					"off",  					},   
/* 1	5 */	{	"SPSM #2 On/Off",		 				"on", 					"off",  					},   
/* 1	6 */	{	"SPSM #3 On/Off",		 				"on", 					"off",  					},   
/* 1	7 */	{	"SPSM #4 On/Off",		 				"on", 					"off",  					},   
																																			          
// Alarm[2]  	//	ControlFlag[2]     																										          
/* 2	0 */	{	"FAN Tray #1",		  					"on", 					"off",      				},   
/* 2	1 */	{	"FAN Tray #2",   	  					"on", 					"off",      				},   
/* 2	2 */	{	"Stagger function",     				"on", 					"off",         				},   
/* 2	3 */	{	"Blank Map function",   				"on", 					"off",      				},   
/* 2	4 */	{	"Noise_Figure_limit", 					"Selected", 			"Selected", 				},   
/* 2	5 */	{	"Sector Blank function", 				"on", 					"off",      				},   
/* 2	6 */	{	"ENCODER_1_ARP_OUT_DELAY",  			"Selected", 			"Selected",          		},   
/* 2	7 */	{	"ENCODER_2_ARP_OUT_DELAY",  			"Selected", 			"Selected",          		},   
																																			          
// Alarm[3]  	//	ControlFlag[3]   																										          
/* 3	0 */	{	"Frequency operation Mode",  			"Selected", 			"Selected",					},   
/* 3	1 */	{	"STC Selection",     					"Selected", 			"Selected",					},   
/* 3	2 */	{	"Antenna rotation ",       				"Selected", 			"Selected",					},
/* 3	3 */	{	"-",  									"-",					"-",						},
/* 3	4 */	{	"Encoder Select",   					"#1 selected", 			"#2 selected",          	},   
/* 3	5 */	{	"Encoder mode", 	 					"Auto mode selected", 	"manual mode selected",     },   
/* 3	6 */	{	"ASDE CHA UCM Atten", 		 			"changed",		 		"changed",					},   
/* 3	7 */	{	"ASDE CHA DCM Atten", 		 			"changed", 				"changed",					},   

																																			          
// Alarm[4]  	//	ControlFlag[4]   																										          
/* 3	0 */	{	"Long Pulse Width",						"Selected", 			"Selected",       					},   
/* 3	1 */	{	"Long Pulse OnOff",   					"on", 					"off",      				},   
/* 3	2 */	{	"Short Pulse OnOff",    				"on", 					"off",        				},   
/* 3	3 */	{	"Loop Back",							"Selected", 			"",        					},   
/* 3	4 */	{	"CFAR_THRESHOLD_GAIN_CONTROL", 			"Selected", 			"",       					},   
/* 3	5 */	{	"CFAR_AVERAGE_LENGTH",   	 			"Selected", 			"",              			},   
/* 3	6 */	{	"Video Out OnOff",           			"on", 					"off",  					},   
/* 3	7 */	{	"STC_Mode",  							"changed", 				"changed", 					},   
																																			          
// Alarm[5]  	//	ControlFlag[5]   																										          
/* 3	0 */	{	"Buzzer_OnOff_Flag",				  	"on", 					"off",  					},   
/* 3	1 */	{	"-",  									"-",					"-",						},	//"SafetySD_OnOff",   	   				"on", 					"off",           			},   
/* 3	2 */	{	"Short Pulse Width",     				"Selected", 			"Selected",					},   
/* 3	3 */	{	"FWD_H_Lmt",  							"Selected", 			"Selected",  				},   
/* 3	4 */	{	"PLL1 S1 Select", 			   			"Selected", 			"Selected",					},   
/* 3	5 */	{	"PLL2 L1 Select",          	   			"Selected", 			"Selected", 				},   
/* 3	6 */	{	"PLL3 S2 Select",          	   			"Selected", 			"Selected",					},   
/* 3	7 */	{	"PLL4 L2 Select",     		   			"Selected", 			"Selected",					},   
																																			          
// Alarm[6]  	//	ControlFlag[6]   																										          
/* 3	0 */	{	"SSPA High Power Limit",	   			"changed",		 		"changed",					},   
/* 3	1 */	{	"-",  									"-",					"-",						},   
/* 3	2 */	{	"-",  									"-",					"-",						},   
/* 3	3 */	{	"-",  									"-",					"-",						},   
/* 3	4 */	{	"SSPA Low Power Limit", 	   			"changed",		 		"changed",      			},   
/* 3	5 */	{	"-",  									"-",					"-",						},   
/* 3	6 */	{	"-",  									"-",					"-",						},   
/* 3	7 */	{	"-",  									"-",					"-",						},   
																																			          
// Alarm[7]  	//	ControlFlag[7]   																											          
/* 3	0 */	{	"SSPA Over Temp Limit ",				"changed",		 		"changed",					},   
/* 3	1 */	{	"-",  									"-",					"-",						},
/* 3	2 */	{	"-",  									"-",					"-",						},
/* 3	3 */	{	"-",  									"-",					"-",						},   
/* 3	4 */	{	"SSPA #1 Power Offset", 		   		"changed",		 		"changed",   				},   
/* 3	5 */	{	"SSPA #2 Power Offset", 		   		"changed",		 		"changed", 					},   
/* 3	6 */	{	"SSPA #3 Power Offset", 		   		"changed",		 		"changed",					},   
/* 3	7 */	{	"SSPA #4 Power Offset", 		   		"changed",		 		"changed",					},   
																																			          
// Alarm[8]  	//	ControlFlag[8]   																											          
/* 3	0 */	{	"UCM High Power Offset",				"changed",		 		"changed",					},   
/* 3	1 */	{	"UCM Low Power Offset",        			"changed",		 		"changed",					},   
/* 3	2 */	{	"DCM High Power Offset",       			"changed",		 		"changed",					},   
/* 3	3 */	{	"DCM Low Power Offset",	   				"changed",		 		"changed",					},   
/* 3	4 */	{	"NMR_FWD_L_Lmt", 		   				"changed",		 		"changed",					},   
/* 3	5 */	{	"Output_Gain",  						"changed",		 		"changed",					},   
/* 3	6 */	{	"Output_Gain_Watt",  					"changed",		 		"changed",					},   
/* 3	7 */	{	"VSWR_Offset",  						"changed",		 		"changed",					},   
																																			          
// Alarm[9]  	//	ControlFlag[9]																												          
/* 3	0 */	{	"BYPASS_OnOff",	   						"on", 					"off",     					},   
/* 3	1 */	{	"FAN_MODE",     						"Auto", 				"Manual",		   			},   
/* 3	2 */	{	"FANTEMP_Limit",   					  	"changed",		 		"changed",		        	},   
/* 3	3 */	{	"-",  									"-",					"-",						},
/* 3	4 */	{	"-",  									"-",					"-",						},   
/* 3	5 */	{	"-",  									"-",					"-",						},   
/* 3	6 */	{	"SCM_TEMP_H_Limit",      				"changed",		 		"changed",			   		},   
/* 3	7 */	{	"-",  									"-",					"-",						},   
																																			          
// Alarm[10]  	//	ControlFlag[10]   																											          
/* 3	0 */	{	"-",  									"-",					"-",						},
/* 3	1 */	{	"-",  									"-",					"-",						},   
/* 3	2 */	{	"DCM Offset"			      	   		"changed",		 		"changed",     				},   
/* 3	3 */	{	"UCM Offset", 	     	   				"changed",		 		"changed", 					},   
/* 3	4 */	{	"FWD Offset", 			   				"changed",		 		"changed",					},   
/* 3	5 */	{	"RVS Offset", 							"changed",		 		"changed",					},   
/* 3	6 */	{	"Noise Offset	", 		   				"changed",		 		"changed",					},   
/* 3	7 */	{	"Temp OnOff", 			   				"changed",		 		"changed",					},   
																																			          
// Alarm[11]  	//	ControlFlag[11]   																											          
/* 3	0 */	{	"-",  									"-",					"-",						},   
/* 3	1 */	{	"MOTORTEMP_Limit",    					"changed",		 		"changed",			  		},   
/* 3	2 */	{	"OILTEMP_Limit",    					"changed",		 		"changed",			  		},
/* 3	3 */	{	"CURRENT_Limit",    					"changed",		 		"changed",			  		},
/* 3	4 */	{	"PRIMARY_ALM_1", 	 					"changed",		 		"changed",			  		},
/* 3	5 */	{	"PRIMARY_ALM_2", 						"changed",		 		"changed",			  		},
/* 3	6 */	{	"PRIMARY_ALM_3", 	   					"changed",		 		"changed",			  		},
/* 3	7 */	{	"PRIMARY_ALM_4", 	   					"changed",		 		"changed",			  		},
																																			          
// Alarm[12]  	//	ControlFlag[12]   																											          
/* 3	0 */	{	"-",  									"-",					"-",						},   
/* 3	1 */	{	"-",  									"-",					"-",						},     
/* 3	2 */	{	"WG_MODE", 								 "Auto", 				"Manual",		   			}, 
/* 3	3 */	{	"WG_1_CTRL",     						"#A", 					"#B",	        			},   
/* 3	4 */	{	"WG_2_CTRL",     						"#A", 					"#B",	        			},     
/* 3	5 */	{	"WG_3_CTRL",     						"#A", 					"#B",	        			},     
/* 3	6 */	{	"-",  									"-",					"-",						},        
/* 3	7 */	{	"-",  									"-",					"-",						},          
																																			          
// Alarm[13]  	//	ControlFlag[13]  																											          
/* 3	0 */	{	"-",  									"-",					"-",						},        
/* 3	1 */	{	"-",  									"-",					"-",						},
/* 3	2 */	{	"-",  									"-",					"-",						},
/* 3	3 */	{	"-",  									"-",					"-",						},
/* 3	4 */	{	"-",  									"-",					"-",						},
/* 3	5 */	{	"-",  									"-",					"-",						},
/* 3	6 */ 	{	"-",  									"-",					"-",						},
/* 3	7 */ 	{	"-",  									"-",					"-",						},
																																			          
// Alarm[14]  	//	ControlFlag[14]																										          
/* 3	0 */	{	"-",  									"-",					"-",						},        
/* 3	1 */	{	"-",  									"-",					"-",						},
/* 3	2 */	{	"-",  									"-",					"-",						},
/* 3	3 */	{	"-",  									"-",					"-",						},
/* 3	4 */	{	"-",  									"-",					"-",						},
/* 3	5 */	{	"-",  									"-",					"-",						},
/* 3	6 */ 	{	"-",  									"-",					"-",						},
/* 3	7 */ 	{	"-",  									"-",					"-",						},  
				  																															          
// Alarm[15]  	//	ControlFlag[15]																										          
/* 3	0 */	{	"-",  									"-",					"-",						},        
/* 3	1 */	{	"-",  									"-",					"-",						},
/* 3	2 */	{	"-",  									"-",					"-",						},
/* 3	3 */	{	"-",  									"-",					"-",						},
/* 3	4 */	{	"-",  									"-",					"-",						},
/* 3	5 */	{	"-",  									"-",					"-",						},
/* 3	6 */ 	{	"-",  									"-",					"-",						},
/* 3	7 */ 	{	"-",  									"-",					"-",						},   
																																			          
// Alarm[16]  	//	ControlFlag[16]																										          
/* 3	0 */	{	"-",  									"-",					"-",						},        
/* 3	1 */	{	"-",  									"-",					"-",						},
/* 3	2 */	{	"-",  									"-",					"-",						},
/* 3	3 */	{	"-",  									"-",					"-",						},
/* 3	4 */	{	"-",  									"-",					"-",						},
/* 3	5 */	{	"-",  									"-",					"-",						},
/* 3	6 */ 	{	"-",  									"-",					"-",						},
/* 3	7 */ 	{	"-",  									"-",					"-",						},
																																			          
// Alarm[17]  	//	ControlFlag[17]																										          
/* 3	0 */	{	"-",  									"-",					"-",						},        
/* 3	1 */	{	"-",  									"-",					"-",						},
/* 3	2 */	{	"-",  									"-",					"-",						},
/* 3	3 */	{	"-",  									"-",					"-",						},
/* 3	4 */	{	"-",  									"-",					"-",						},
/* 3	5 */	{	"-",  									"-",					"-",						},
/* 3	6 */ 	{	"-",  									"-",					"-",						},
/* 3	7 */ 	{	"-",  									"-",					"-",						},  
																																			          
// Alarm[18]  	//	ControlFlag[18]																									          
/* 3	0 */	{	"-",  									"-",					"-",						},        
/* 3	1 */	{	"-",  									"-",					"-",						},
/* 3	2 */	{	"-",  									"-",					"-",						},
/* 3	3 */	{	"-",  									"-",					"-",						},
/* 3	4 */	{	"-",  									"-",					"-",						},
/* 3	5 */	{	"-",  									"-",					"-",						},
/* 3	6 */ 	{	"-",  									"-",					"-",						},
/* 3	7 */ 	{	"-",  									"-",					"-",						},  
																																			          
// Alarm[19]  	//	ControlFlag[19] 																										          
/* 3	0 */	{	"-",  									"-",					"-",						},        
/* 3	1 */	{	"-",  									"-",					"-",						},
/* 3	2 */	{	"-",  									"-",					"-",						},
/* 3	3 */	{	"-",  									"-",					"-",						},
/* 3	4 */	{	"-",  									"-",					"-",						},
/* 3	5 */	{	"-",  									"-",					"-",						},
/* 3	6 */ 	{	"-",  									"-",					"-",						},
/* 3	7 */ 	{	"-",  									"-",					"-",						},   

// Alarm[20]  	//	ControlFlag[20]  																										          
/* 3	0 */	{	"Event Log Clear", 						"-",					"-",						},        
/* 3	1 */	{	"-",  									"-",					"-",						},
/* 3	2 */	{	"-",  									"-",					"-",						},
/* 3	3 */	{	"-",  									"-",					"-",						},
/* 3	4 */	{	"-",  									"-",					"-",						},
/* 3	5 */	{	"-",  									"-",					"-",						},
/* 3	6 */ 	{	"-",  									"-",					"-",						},
/* 3	7 */ 	{	"-",  									"-",					"-",						},

};



/****************************************************************************/
/* User Define Options END													*/			
/****************************************************************************/
#define DATETIME_FORMATSTRING "%Y-%m-%d"

int phEventSaveFile;

void EventAutoSave_step1(int iTarget) ;


void EventAutoSaveFileCreate(int iTarget) 
////////////////////////////Log File 생성	
{	 
	char CurrentDir[MAX_PATHNAME_LEN]="";
	char WantedDir[20];
	char TargetDir[MAX_PATHNAME_LEN]="";
	char strSaveFileName[50];
	int Y, M, D, h, m, s; 
	int	DataLength1, DataLength2, DataLength3;
	int	Result;		
	char cData[256];

	
	GetSystemTime(&h, &m, &s);
	GetSystemDate(&M, &D, &Y);	   

	sprintf(WantedDir, "%s", "\\AutoData\\");
	memset (strSaveFileName, '\0', sizeof(strSaveFileName));   
	sprintf(strSaveFileName, "ASDE#%d_%02d%02d%02d%02d%02d%02d.csv", iTarget+1, Y, M, D, h, m, s);

    GetProjectDir(CurrentDir);
	
	DataLength1 = StringLength(CurrentDir);
	DataLength2 = StringLength(WantedDir);
	DataLength3 = StringLength(strSaveFileName);
	
	strcpy(TargetDir,CurrentDir); 
	strcat(TargetDir,WantedDir); 
	DisableBreakOnLibraryErrors ();
	if( (Result = SetDir(TargetDir)) < 0)
	{
		MakeDir(TargetDir);
		SetDir(TargetDir);
	}
	EnableBreakOnLibraryErrors ();
	strcat(TargetDir,strSaveFileName); 		   

	phEventSaveFile = OpenFile(TargetDir, VAL_WRITE_ONLY, VAL_TRUNCATE, VAL_ASCII); 
	
	memset(cData, '\0', sizeof(cData));
	
	sprintf(cData, "Date : %04d / %02d / %02d - %02d:%02d:%02d", Y, M, D, h, m, s); 
	FmtFile(phEventSaveFile, "%s<%s\n", cData);   
	
	strcpy(cData, "");
	Fmt(cData, "Target : ASDE #%d", iTarget+1);
	FmtFile(phEventSaveFile, "%s<%s\n", cData);    
}


void onCloseEventAutoSaveWait(int iIdx)
{
	char stBuf[16];  
	if (iIdx == 0)
	{
		memset(stBuf, NULL, sizeof(stBuf));
		GetCtrlVal(m_hWndMain, MAINPNL_STR_CONNECT2,  stBuf);  		
		if (strcmp(stBuf, "DisConnect") != 0) 
		{
			EventAutoSave_step1(1);//Channel1
		}   
		else
			ClosePanel(&m_hWndEventAutoSaveWait);
	}
	else 
	{   
		ClosePanel(&m_hWndEventAutoSaveWait);
	}
	
	EnableTimer(m_hWndMain, m_pCOMM[iIdx].iPollingTimer, FALSE);     
}

void EventAutoSaveStart(void)
{
	char stBuf[16];

	m_pEventHis = NULL;
	m_pEventHis = (EVENTHIS *)malloc(sizeof(EVENTHIS));

	m_pEventIndex = NULL;
	m_pEventIndex = (EVENTINDEX *)malloc(sizeof(EVENTINDEX));
	
	/////////////
	m_hWndEventAutoSaveWait = LoadPanel (m_hWndMain, UIR_FILE_G, PNLEVENTAT); 
	
	SetPanelAttribute (m_hWndEventAutoSaveWait, ATTR_TITLEBAR_VISIBLE, 0);
	SetPanelPos(m_hWndEventAutoSaveWait, VAL_AUTO_CENTER, VAL_AUTO_CENTER);
	SetPanelAttribute (m_hWndEventAutoSaveWait, ATTR_FLOATING, VAL_FLOAT_APP_ACTIVE);
	
	DisplayPanel (m_hWndEventAutoSaveWait);
	/////////////
	
	memset(stBuf, NULL, sizeof(stBuf));
	GetCtrlVal(m_hWndMain, MAINPNL_STR_CONNECT1,  stBuf);     		
	if (strcmp(stBuf, "DisConnect") != 0) 
	{
		EventAutoSave_step1(0);//Channel1
	}
	else
	{
		memset(stBuf, NULL, sizeof(stBuf));
		GetCtrlVal(m_hWndMain, MAINPNL_STR_CONNECT2,  stBuf);     		
		if (strcmp(stBuf, "DisConnect") != 0) 
		{
			EventAutoSave_step1(1);//Channel1
		} 
		else
		{
			onCloseEventAutoSaveWait(1);
		}
	}
}

void EventAutoSave_step1(int iTarget)
{   
	char strTemp[64];

	
	EnableTimer(m_hWndMain, m_pCOMM[iTarget].iPollingTimer, FALSE);

	SetCtrlVal(m_hWndEventAutoSaveWait, PNLEVENTAT_NMR_SINDEX, 0);
	SetCtrlVal(m_hWndEventAutoSaveWait, PNLEVENTAT_NMR_EINDEX, 0);
	
	strcpy(strTemp, "");
	if (iTarget == 0)
		Fmt(strTemp, "Channel #A Event Log Auto Saving... ");
	else
		Fmt(strTemp, "Channel #B Event Log Auto Saving... ");
	
	
	SetCtrlVal(m_hWndEventAutoSaveWait, PNLEVENTAT_TEXTMSG, strTemp);//"Channel #A Event Log Auto Saving...
	SetCtrlVal(m_hWndEventAutoSaveWait, PNLEVENTAT_STR_CNT, "0 / 0");//"Channel #A Event Log Auto Saving...
	
	
	EventAutoSaveFileCreate(iTarget);
	
	SendRequest(iTarget, AHAUTOINDEX_REQ, NULL, 0);
	
}


void EventHisPacket(int iTarget, int iCmd)
{
	int  iIdx;
	if(!m_hWndEventAutoSaveWait)
		return;
	
	GetCtrlVal(m_hWndEventAutoSaveWait, PNLEVENTAT_NMR_HANDLE, &iIdx);
	
	if (iTarget != iIdx)
		return;
	
	switch(iCmd)
	{
		case AHAUTOINDEX_RPY:
		{ 
			char strCnt[64];
			unsigned int 	nCnt, iCnt;
 			char 			stSet[10];
 			int		iCheck;

			memcpy(m_pEventIndex, m_pRecvIO->DataBuf, sizeof(EVENTINDEX)); 			   
			
			SetCtrlVal(m_hWndEventAutoSaveWait, 	PNLEVENTAT_NMR_Start_Year,  	BCDtoDec(m_pEventIndex->StartDate[0])+2000);
			SetCtrlVal(m_hWndEventAutoSaveWait, 	PNLEVENTAT_NMR_Start_Month,  	BCDtoDec(m_pEventIndex->StartDate[1]));
			SetCtrlVal(m_hWndEventAutoSaveWait, 	PNLEVENTAT_NMR_Start_Day,  	BCDtoDec(m_pEventIndex->StartDate[2]));
			
			SetCtrlVal(m_hWndEventAutoSaveWait, PNLEVENTAT_NMR_SINDEX, MAKEWORD(m_pEventIndex->StartIndex[0], m_pEventIndex->StartIndex[1]));   
			SetCtrlVal(m_hWndEventAutoSaveWait, PNLEVENTAT_NMR_EINDEX, MAKEWORD(m_pEventIndex->EndIndex[0], m_pEventIndex->EndIndex[1]));
			
			SetCtrlAttribute(m_hWndEventAutoSaveWait, PNLEVENTAT_PRO_EVENT, ATTR_MAX_VALUE, MAKEWORD(m_pEventIndex->EndIndex[0], m_pEventIndex->EndIndex[1]));
			SetCtrlVal(m_hWndEventAutoSaveWait, PNLEVENTAT_PRO_EVENT, MAKEWORD(m_pEventIndex->StartIndex[0], m_pEventIndex->StartIndex[1]));
			
			nCnt = MAKEWORD(m_pEventIndex->StartIndex[0], m_pEventIndex->StartIndex[1]);
			iCnt = 0;
			
			stSet[iCnt++] = 0;
	
			GetCtrlVal(m_hWndEventAutoSaveWait, 	PNLEVENTAT_NMR_Start_Year,  	&iCheck);
			if( iCheck - 2000 < 0)
					iCheck = 0;	
			else	iCheck = iCheck - 2000;
			stSet[iCnt++] = DECtoBcd(iCheck);
				
			GetCtrlVal(m_hWndEventAutoSaveWait, 	PNLEVENTAT_NMR_Start_Month,  	&iCheck);
			stSet[iCnt++] = DECtoBcd(iCheck); 
			GetCtrlVal(m_hWndEventAutoSaveWait, 	PNLEVENTAT_NMR_Start_Day,  	&iCheck);
			stSet[iCnt++] = DECtoBcd(iCheck); 
			
			
			GetCtrlVal(m_hWndEventAutoSaveWait, 	PNLEVENTAT_NMR_Start_Year,  	&iCheck);
			if( iCheck - 2000 < 0)
					iCheck = 0;	
			else	iCheck = iCheck - 2000;
			stSet[iCnt++] = DECtoBcd(iCheck);
			GetCtrlVal(m_hWndEventAutoSaveWait, 	PNLEVENTAT_NMR_Start_Month,  &iCheck);
			stSet[iCnt++] = DECtoBcd(iCheck);
			GetCtrlVal(m_hWndEventAutoSaveWait, 	PNLEVENTAT_NMR_Start_Day,  	&iCheck);
			stSet[iCnt++] = DECtoBcd(iCheck);
			
			
			stSet[iCnt++] = LOBYTE(nCnt);
			stSet[iCnt++] = HIBYTE(nCnt);  
			
			if (m_hWndEventAutoSaveWait)		   
				SendRequest(iTarget, AHAUTODATA_REQ, stSet, 9);
			
			break;
		}
		
		case AHAUTODATA_RPY:
		{	
			int		iCheck;
			unsigned int 	nCnt, iCnt, iTotalCnt;
			char 			stSet[10];
			char strCnt[64];
			
			memcpy(m_pEventHis, m_pRecvIO->DataBuf, sizeof(EVENTHIS)); 
			nCnt = MAKEWORD(m_pEventHis->iIndex[0], m_pEventHis->iIndex[1]);
			
			EventAutoDataSave(iTarget, nCnt);
			
			nCnt++;
						  
			GetCtrlVal(m_hWndEventAutoSaveWait, PNLEVENTAT_NMR_EINDEX, &iTotalCnt);
			
			strcpy(strCnt, "");
			Fmt(strCnt, "%d : %d", nCnt, iTotalCnt);
			SetCtrlVal(m_hWndEventAutoSaveWait,  PNLEVENTAT_STR_CNT, strCnt);  
			SetCtrlVal(m_hWndEventAutoSaveWait, PNLEVENTAT_PRO_EVENT, nCnt);

			if(nCnt >  iTotalCnt)
			{
				CloseFile(phEventSaveFile);		 
				onCloseEventAutoSaveWait(iIdx);
				break;
			}
			
			iCnt = 0;
			
			stSet[iCnt++] = 0;
	
			GetCtrlVal(m_hWndEventAutoSaveWait, 	PNLEVENTAT_NMR_Start_Year,  	&iCheck);
			if( iCheck - 2000 < 0)
					iCheck = 0;	
			else	iCheck = iCheck - 2000;
			stSet[iCnt++] = DECtoBcd(iCheck);
				
			GetCtrlVal(m_hWndEventAutoSaveWait, 	PNLEVENTAT_NMR_Start_Month,  	&iCheck);
			stSet[iCnt++] = DECtoBcd(iCheck); 
			GetCtrlVal(m_hWndEventAutoSaveWait, 	PNLEVENTAT_NMR_Start_Day,  	&iCheck);
			stSet[iCnt++] = DECtoBcd(iCheck); 
			
			
			GetCtrlVal(m_hWndEventAutoSaveWait, 	PNLEVENTAT_NMR_Start_Year,  	&iCheck);
			if( iCheck - 2000 < 0)
					iCheck = 0;	
			else	iCheck = iCheck - 2000;
			stSet[iCnt++] = DECtoBcd(iCheck);
			GetCtrlVal(m_hWndEventAutoSaveWait, 	PNLEVENTAT_NMR_Start_Month,  &iCheck);
			stSet[iCnt++] = DECtoBcd(iCheck);
			GetCtrlVal(m_hWndEventAutoSaveWait, 	PNLEVENTAT_NMR_Start_Day,  	&iCheck);
			stSet[iCnt++] = DECtoBcd(iCheck);
			
			
			stSet[iCnt++] = LOBYTE(nCnt);
			stSet[iCnt++] = HIBYTE(nCnt);  
			
			if (m_hWndEventAutoSaveWait)
				SendRequest(iTarget, AHAUTODATA_REQ, stSet, 9);
				
			break;
		}

		
	}
	
}

void EventAutoDataSave(int iTarget, unsigned int nCnt)
{
	int 	i;
	int		iBit;
	int		iByte;
	char 	cTime[8];
	char	stTime[32];
	int 	iTemp;

	for(i=0; i<6; i++)
		cTime[i] = BCDtoDec(m_pEventHis->cTime[i]);
	
	memset(stTime, NULL, sizeof(stTime));
	sprintf(stTime, "20%02d/%02d/%02d - %02d:%02d:%02d", 
				cTime[0], cTime[1], cTime[2], cTime[3], cTime[4], cTime[5]); 
	
	iTemp = 0;
	
	
	if (m_pEventHis->iType == 0)
	{
		for(iByte=0; iByte<DATABYTE; iByte++)
		{
			for(iBit=0; iBit<8; iBit++)
			{ 
				if(!(strncmp((char *)m_pEventName[iByte*8+iBit][0], "-", 8)))
					continue;
			
				if(m_pEventHis->cFlag[iByte] & (1<<iBit))
				{
					iTemp = iByte*8+iBit;
					EventAutoDataSaveAddOneLine(iTarget, m_pEventHis->iType, nCnt, stTime, iTemp, m_pEventHis->cData[iByte] & (1<<iBit), (char *)m_pEventName[iByte*8+iBit][0]);
				}
		
			}										   
		}
	}
	else
	{
		for(iByte=0; iByte<DATABYTE; iByte++)
		{
			for(iBit=0; iBit<8; iBit++)
			{ 
				if(!(strncmp((char *)m_pEventCtrlName[iByte*8+iBit][0], "-", 8)))
					continue;
			
				if(m_pEventHis->cFlag[iByte] & (1<<iBit))
				{
					iTemp = iByte*8+iBit;
					EventAutoDataSaveAddOneLine(iTarget, m_pEventHis->iType, nCnt, stTime, iTemp, m_pEventHis->cData[iByte] & (1<<iBit), (char *)m_pEventCtrlName[iByte*8+iBit][0]);
				}
		
			}										   
		}
	}
}

void EventAutoDataSaveAddOneLine(int iTarget, int iType, unsigned int iIndex, char *pTime, int iTemp, BOOL bArm, char *pArmName)
{   
	int 	nLine;
	char	stHis[255];
	char 	stArmSts[20];
	char 	stArmClr[20];
	
	if (iType == 0)
	{
		if(bArm)
		{
			strcpy(stArmSts, (char *)m_pEventName[iTemp][1]);
			strcpy(stArmClr, "FF0000");
		}
		else
		{
			strcpy(stArmSts, (char *)m_pEventName[iTemp][2]);        
			strcpy(stArmClr, "004080");
		}
	}
	else
	{
		strcpy(stArmSts, "");
		strcpy(stArmClr, "");
	}  
	
	Fmt(stHis, "%d, \t%s, \t%s, \t%s", iIndex, pTime, stArmSts, pArmName);		  
	FmtFile(phEventSaveFile, "%s<%s\n", stHis);  								  
}


int CVICALLBACK OnCmdEventAuto (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int iIdx = -1;
	int i;
	
	if(event != EVENT_COMMIT)
		return 0;
	
		
	switch(control)
	{

		case PNLEVENTAT_BTN_CANCEL:
		{
			CloseFile(phEventSaveFile);	
			if(m_pEventHis)
				free(m_pEventHis);
			ClosePanel(&m_hWndEventAutoSaveWait);	   
			
			EnableTimer (m_hWndMain,	m_pCOMM[0].iPollingTimer,	TRUE);
			EnableTimer (m_hWndMain,	m_pCOMM[1].iPollingTimer,	TRUE); 
			break;
		}

	}
	
	return 0;
}


////
int CVICALLBACK OnTmrEventAuto (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int Y, M, D, h, m, s; 

	if(event != EVENT_TIMER_TICK)
		return 0;
	
	/*
	EnableTimer(panel, control, FALSE);
	
	GetSystemTime(&h, &m, &s);
	
	if ((h == m_iEventLogAutoSaveTime) && (m == 0))
	{
		EventAutoSaveStart();
	}
	else
		EnableTimer(panel, control, TRUE);
	*/
	
	StatusAutoSave();

	return 0;
}


void StatusAutoSave(void)
{
	char stBuf[16];

	
	
	GetCtrlVal(m_hWndMain, MAINPNL_CB_AUTOSAVE, &m_bEventLogAutoSave); 
	
	if(m_bEventLogAutoSave)
	{
	/////////////
		memset(stBuf, NULL, sizeof(stBuf));
		GetCtrlVal(m_hWndMain, MAINPNL_STR_CONNECT1,  stBuf);     		
		if (strcmp(stBuf, "DisConnect") != 0) 
		{
			AutoSave(0);//Channel1
		}

		memset(stBuf, NULL, sizeof(stBuf));
		GetCtrlVal(m_hWndMain, MAINPNL_STR_CONNECT2,  stBuf);     		
		if (strcmp(stBuf, "DisConnect") != 0) 
		{
			AutoSave(1);//Channel1
		} 
	}
}

void AutoSave(int iTarget)
{

	
}
