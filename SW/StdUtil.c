#include <userint.h>
#include <ansi_c.h>
#include "StdUtil.h"

int L_ModalDlg_Target;
int L_ModalDlg_TimerID;
char *L_pModalDlg_Title;
char *L_pModalDlg_Msg;

////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : MessagePopupEx
// Prameter : 
//				lpszTitle  - Title of MessgeBox
//				lpszFormat - Output Text Format
//				...	       - Format Info
// Return   : 
// Remart   : This function is like to printf(). So, You can use this Function As printf().
//
// Useage	: MessagePopupEx("Title", "Error");
//		    : MessagePopupEx("Title", "ErrorCode : %d", 3);
//			: MessagePopupEx("Title", "%d - %s - %f - %c%c\n", iValue, "str", 3.0, 'c', 'd');
////////////////////////////////////////////////////////////////////////////////////////////

void MessagePopupEx(char *lpszTitle, char *lpszFormat, ...)
{
	char *pstrText;
	int nTextLen;
	va_list args;

	if(!lpszFormat)
	{
		MessagePopup(lpszTitle, lpszFormat);
		return;
	}

	nTextLen = strlen(lpszFormat);
	if(nTextLen <= 0)
	{
		MessagePopup(lpszTitle, "");
		return;
	}

	pstrText = malloc(1024);

	if(!pstrText)
	{
		MessagePopup(lpszTitle, "MessagePopupEx Error !!!\n\n -Memeory Allocate Fail");
		return;
	}
	
	
	va_start(args, lpszFormat);	
	vsprintf(pstrText, lpszFormat, args);

	MessagePopup(lpszTitle, pstrText);	

	free(pstrText);

}


////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : SMemcopy
// Prameter : 
//				pDest  - Dest Memory Pointer
//				pSrc   - Source Memory Pointer
//				nSize  - Size to Copy ( byte )
// Return   : 
// Remart   : Safe Memory copy for overlab memory area
//			: Ex) 
//					Dest 0x00, Src 0x01, size 16  --> memory shift left
//												  --> memcpy is not suport overlab area copy
//
// Useage	: see memcpy usage
////////////////////////////////////////////////////////////////////////////////////////////

void SMemcopy(void *pDest, void *pSrc, int nSize)
{
	BYTE *pTemp;

	if(!pDest)
		return;

	if(!pSrc)
		return;

	if(nSize <= 0)
		return;

	pTemp = malloc(nSize);
	if(!pTemp)
		return;

	memcpy(pTemp, pSrc, nSize);
	memcpy(pDest, pTemp, nSize);

	free(pTemp);
}


////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : CVIShowWindow
// Prameter : 
//				iPanelID   - Control's Panel ID
//				iControlID - Control ID
//				bShow	   - Show or Hide Flag
// Return   : 
// Remart   : Show / Hide Panel or Contorl
// Useage	: 
//			  ShowWindow(PanelID, ControlID, TRUE)		: Show Control
//			  ShowWindow(PanelID, ControlID, FALSE)		: Hide Control
//			  ShowWindow(PanelID, -1, TRUE)				: Show Panel
//			  ShowWindow(PanelID, -1, FALSE)			: Hide Panel
////////////////////////////////////////////////////////////////////////////////////////////
void CVIShowWindow(int iPanelID, int iControlID, BOOL bShow)
{
	if(iPanelID < 0)
		return;

	if(iControlID < 0)	
		SetPanelAttribute(iPanelID, ATTR_VISIBLE, bShow);
	else
		SetCtrlAttribute(iPanelID, iControlID, ATTR_VISIBLE, bShow);
	
}


////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : CVIEnableWindow
// Prameter : 
//				iPanelID   - Control's Panel ID
//				iControlID - Control ID
//				bEnable	   - Enable or Disable Flag
// Return   : 
// Remart   : Enable / Disable Panel or Contorl
// Useage	: 
//			  EnableWindow(PanelID, ControlID, TRUE)		: Enable Control
//			  EnableWindow(PanelID, ControlID, FALSE)		: Disable Control
//			  EnableWindow(PanelID, -1, TRUE)				: Enable Panel
//			  EnableWindow(PanelID, -1, FALSE)				: Disable Panel
////////////////////////////////////////////////////////////////////////////////////////////
void CVIEnableWindow(int iPanelID, int iControlID, BOOL bEnable)
{
	if(iPanelID < 0)
		return;

	if(iControlID < 0)	
		SetPanelAttribute(iPanelID, ATTR_DIMMED, !bEnable);
	else
		SetCtrlAttribute(iPanelID, iControlID, ATTR_DIMMED, !bEnable);
}


////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : ConfirmValidData
// Prameter : 
//				iPanelID   - Control's Panel ID
//				iControlID - Control ID
//				bRepair	   - whether repair this control's value to safe range value 
//
// Return   : TRUE - Value is Safe, FALSE - Value is out of range
// Remart   : Confirm Control's data is Valid
// Useage	: 
//			  ConfirmValidData(PanelID, ControlID, TRUE)				: Fix wrong value
//			  if(ConfirmValidData(PanelID, ControlID, FALSE)) ...		: Confirm data but, NO Fixed
////////////////////////////////////////////////////////////////////////////////////////////
BOOL ConfirmValidData(int iPannelID, int iControlID, BOOL bRepair)
{
	int CtrlType;
	int iDataType;

	char			cValue[3];				//0-MinValue, 1-MaxValue, 2-CtrlValue
	double			dbValue[3];
	float			fValue[3];
	int				iValue[3];
	short			sValue[3];
	unsigned char	ucValue[3];
	unsigned int	uiValue[3];
	unsigned short  usValue[3];

	if(iPannelID < 0)
		return FALSE;

	if(iControlID < 0)
		return FALSE;

	GetCtrlAttribute(iPannelID, iControlID, ATTR_CTRL_STYLE, &CtrlType);

	if(CtrlType < CTRL_NUMERIC || CtrlType > CTRL_COLOR_NUMERIC)
		return FALSE;

	GetCtrlAttribute(iPannelID, iControlID, ATTR_DATA_TYPE, &iDataType);

	switch(iDataType)
	{
	case VAL_CHAR :								// A single byte character. 
		{
			GetCtrlAttribute(iPannelID, iControlID, ATTR_MIN_VALUE,  	&cValue[0]);
			GetCtrlAttribute(iPannelID, iControlID, ATTR_MAX_VALUE,	&cValue[1]);		
			GetCtrlVal(iPannelID, iControlID, &cValue[2]);

			if(cValue[0] > cValue[2])
			{
				if(bRepair)
					SetCtrlVal(iPannelID, iControlID, cValue[0]);

				return FALSE;
			}
			else if(cValue[1] < cValue[2])
			{
				if(bRepair)
					SetCtrlVal(iPannelID, iControlID, cValue[1]);

				return FALSE;
			}
			break;
		}
	case VAL_FLOAT :							// An 8 byte floating point value. 
		{
			GetCtrlAttribute(iPannelID, iControlID, ATTR_MIN_VALUE,  	&fValue[0]);
			GetCtrlAttribute(iPannelID, iControlID, ATTR_MAX_VALUE,	&fValue[1]);		
			GetCtrlVal(iPannelID, iControlID, &fValue[2]);

			if(fValue[0] > fValue[2])
			{
				if(bRepair)
					SetCtrlVal(iPannelID, iControlID, fValue[0]);

				return FALSE;
			}
			else if(fValue[1] < fValue[2])
			{
				if(bRepair)
					SetCtrlVal(iPannelID, iControlID, fValue[1]);

				return FALSE;
			}
			break;
		}
	case VAL_DOUBLE :							// A 4 byte floating point value. 
		{
			GetCtrlAttribute(iPannelID, iControlID, ATTR_MIN_VALUE,  	&dbValue[0]);
			GetCtrlAttribute(iPannelID, iControlID, ATTR_MAX_VALUE,	&dbValue[1]);		
			GetCtrlVal(iPannelID, iControlID, &dbValue[2]);

			if(dbValue[0] > dbValue[2])
			{
				if(bRepair)
					SetCtrlVal(iPannelID, iControlID, dbValue[0]);

				return FALSE;
			}
			else if(dbValue[1] < dbValue[2])
			{
				if(bRepair)
					SetCtrlVal(iPannelID, iControlID, dbValue[1]);

				return FALSE;
			}
			break;
		}
	case VAL_INTEGER :							// A 4 byte integer. 
		{
			GetCtrlAttribute(iPannelID, iControlID, ATTR_MIN_VALUE,  	&iValue[0]);
			GetCtrlAttribute(iPannelID, iControlID, ATTR_MAX_VALUE,	&iValue[1]);		
			GetCtrlVal(iPannelID, iControlID, &iValue[2]);

			if(iValue[0] > iValue[2])
			{
				if(bRepair)
					SetCtrlVal(iPannelID, iControlID, iValue[0]);

				return FALSE;
			}
			else if(iValue[1] < iValue[2])
			{
				if(bRepair)
					SetCtrlVal(iPannelID, iControlID, iValue[1]);

				return FALSE;
			}
			break;
		}

	case VAL_SHORT_INTEGER :					// A 2 byte integer. 
		{
			GetCtrlAttribute(iPannelID, iControlID, ATTR_MIN_VALUE,  	&sValue[0]);
			GetCtrlAttribute(iPannelID, iControlID, ATTR_MAX_VALUE,	&sValue[1]);		
			GetCtrlVal(iPannelID, iControlID, &sValue[2]);

			if(sValue[0] > sValue[2])
			{
				if(bRepair)
					SetCtrlVal(iPannelID, iControlID, sValue[0]);

				return FALSE;
			}
			else if(sValue[1] < sValue[2])
			{
				if(bRepair)
					SetCtrlVal(iPannelID, iControlID, sValue[1]);

				return FALSE;
			}
			break;
		}
	case VAL_UNSIGNED_CHAR :					// An unsigned single byte character. 
		{
			GetCtrlAttribute(iPannelID, iControlID, ATTR_MIN_VALUE,  	&ucValue[0]);
			GetCtrlAttribute(iPannelID, iControlID, ATTR_MAX_VALUE,	&ucValue[1]);		
			GetCtrlVal(iPannelID, iControlID, &ucValue[2]);

			if(ucValue[0] > ucValue[2])
			{
				if(bRepair)
					SetCtrlVal(iPannelID, iControlID, ucValue[0]);

				return FALSE;
			}
			else if(ucValue[1] < ucValue[2])
			{
				if(bRepair)
					SetCtrlVal(iPannelID, iControlID, ucValue[1]);

				return FALSE;
			}
			break;
		}
	case VAL_UNSIGNED_INTEGER :					// An unsigned 4 byte integer. 
		{
			GetCtrlAttribute(iPannelID, iControlID, ATTR_MIN_VALUE,  	&uiValue[0]);
			GetCtrlAttribute(iPannelID, iControlID, ATTR_MAX_VALUE,	&uiValue[1]);		
			GetCtrlVal(iPannelID, iControlID, &uiValue[2]);

			if(uiValue[0] > uiValue[2])
			{
				if(bRepair)
					SetCtrlVal(iPannelID, iControlID, uiValue[0]);

				return FALSE;
			}
			else if(uiValue[1] < uiValue[2])
			{
				if(bRepair)
					SetCtrlVal(iPannelID, iControlID, uiValue[1]);

				return FALSE;
			}
			break;
		}
	case VAL_UNSIGNED_SHORT_INTEGER :			// An unsigned 2 byte integer. 
		{
			GetCtrlAttribute(iPannelID, iControlID, ATTR_MIN_VALUE,  	&usValue[0]);
			GetCtrlAttribute(iPannelID, iControlID, ATTR_MAX_VALUE,	&usValue[1]);		
			GetCtrlVal(iPannelID, iControlID, &usValue[2]);

			if(usValue[0] > usValue[2])
			{
				if(bRepair)
					SetCtrlVal(iPannelID, iControlID, usValue[0]);

				return FALSE;
			}
			else if(usValue[1] < usValue[2])
			{
				if(bRepair)
					SetCtrlVal(iPannelID, iControlID, usValue[1]);

				return FALSE;
			}
			break;
		}
	default :
		return FALSE;
	}	

	return TRUE;
}


int CVICALLBACK MB_OCCUR(int panel, int control, int event, void *callbackData, int eventData1, int eventData2)
{
	if(event != EVENT_TIMER_TICK)
		return 0;
		
	ResetTimer(L_ModalDlg_Target, L_ModalDlg_TimerID);
	SetCtrlAttribute(L_ModalDlg_Target, L_ModalDlg_TimerID, ATTR_ENABLED, 0);
	
	MessagePopup(L_pModalDlg_Title, L_pModalDlg_Msg);
	
	ResetTimer(L_ModalDlg_Target, L_ModalDlg_TimerID);
	SetCtrlAttribute(L_ModalDlg_Target, L_ModalDlg_TimerID, ATTR_ENABLED, 0);
	
	L_ModalDlg_Target = 0;
	L_ModalDlg_TimerID = 0;
	free(L_pModalDlg_Title);
	free(L_pModalDlg_Msg);
	
	return 0;
}



////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : ModalMsgBox
// Prameter : 
//				iPanelID   - Main Pannel ID
//				lpszTitle  - Title of MessageBox
//				lpszFormat - String Format & Value
//
// Return   : 
// Remark   : Show Domal MessageBox in Thread
// Useage	: Look MessagePopupEx !!!
////////////////////////////////////////////////////////////////////////////////////////////
void ModalMsgBox(int iPannelID, int iTimerID, char *lpszTitle, char *lpszFormat, ...)
{   
	char *pstrText;
	int nTextLen;
	va_list args;
	
	if(iPannelID <= 0)
		return;
	
	L_pModalDlg_Title = malloc(strlen(lpszTitle)+1);
	strcpy(L_pModalDlg_Title, lpszTitle);
	
	
	L_pModalDlg_Msg = malloc(1024);
	if(!lpszFormat)
		strcpy(L_pModalDlg_Msg, "(null)");
	else
	{
		va_start(args, lpszFormat);	
		vsprintf(L_pModalDlg_Msg, lpszFormat, args);
	}
	

	L_ModalDlg_Target = iPannelID;
	L_ModalDlg_TimerID = iTimerID;

	SetCtrlAttribute(L_ModalDlg_Target, L_ModalDlg_TimerID, ATTR_CALLBACK_FUNCTION_POINTER, MB_OCCUR);    
	SetCtrlAttribute(L_ModalDlg_Target, L_ModalDlg_TimerID, ATTR_INTERVAL, 0.1);
	
	ResetTimer(L_ModalDlg_Target, L_ModalDlg_TimerID);
	SetCtrlAttribute(L_ModalDlg_Target, L_ModalDlg_TimerID, ATTR_ENABLED, 1);
}


////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : BCDtoDec
// Prameter : 
//				cBcdByte   - BDC Format Value
//
// Return   : Decimal Value of Inputed BCD Value
// Remark   : 
// Useage	: 
////////////////////////////////////////////////////////////////////////////////////////////
BYTE BCDtoDec(BYTE cBcdByte)
{
	char stTemp[3];
	
	memset(stTemp, 0, 3);
	stTemp[0]=((cBcdByte>>4) & 0x0F) + 0x30;
	stTemp[1]=(cBcdByte & 0x0F) + 0x30;
	
	return (BYTE)(atoi(stTemp));
}


////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : DECtoBcd
// Prameter : 
//				cDecByte   - Decimal Value
//
// Return   : BCD Format Value of Inputed Decimal Value
// Remark   : 
// Useage	: 
////////////////////////////////////////////////////////////////////////////////////////////
BYTE DECtoBcd(BYTE cDecByte)
{
	BYTE i10, i;
	
	i10 = cDecByte / 10;
	i10 = (i10 << 4) & 0xf0;
	
	i = cDecByte % 10;
	i = i & 0x0f; 
	
	i10 |= i;
	
	return i10;
}


////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : EnableTimer
// Prameter : 
//				hPanel   	- Panel Handle of Control
//				ControlID	- Control ID of Timer 
//				bEnable		- TRUE : Enable Timer
//							- FALSE : Disable Timer
// Return   : 
// Remark   : 
// Useage	: 
////////////////////////////////////////////////////////////////////////////////////////////
void EnableTimer(int hPanel, int ControlID, BOOL bEnable)
{
	if(hPanel <= 0)
		return;
		
	if(ControlID <= 0)
		return;
		
	ResetTimer(hPanel, ControlID); 
	SetCtrlAttribute(hPanel, ControlID, ATTR_ENABLED, bEnable);
}


////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : BitFix
// Prameter : 
//				pByte   	- DestByte Pointer
//				BitMask		- Bit Mask Byte
//				bSet		- TRUE : Set Bit by 1
//							- FALSE : Set Bit by 0
// Return   : 
// Remark   : 
// Useage	: 
////////////////////////////////////////////////////////////////////////////////////////////
void BitFix(UINT8 *pByte, UINT8 BitMask, BOOL bSet)
{
	if(bSet) 
		(*pByte) |= BitMask;
	else 
		(*pByte) &= ~(BitMask);
		
}


////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : IsCheckBoxSet
// Prameter : 
//				hPanel   	- Panel Handle of Control
//				CheckBoxID	- CheckBox ID
//
// Return   : TRUE : CheckBox Setted, FALSE : CheckBox Not Setted
// Remark   : 
// Useage	: 
////////////////////////////////////////////////////////////////////////////////////////////
BOOL IsCheckBoxSet(int hPanel, int CheckBoxID)
{
	int iCtrlStyle;
	int iValue;
	
	if(hPanel <= 0)
		return FALSE;
		
	if(CheckBoxID  <= 0)
		return FALSE;
	
	GetCtrlAttribute(hPanel, CheckBoxID, ATTR_CTRL_STYLE, &iCtrlStyle);
	
	if(iCtrlStyle != CTRL_CHECK_BOX)
		return FALSE;
		
	GetCtrlVal(hPanel, CheckBoxID, &iValue);
	
	return iValue;
	
}


////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : GetVersionString
// Prameter : 
//				cHexVer   	- Version Info ( ex : 0x34 -> 3.4 )
//
// Return   : Version String ( "3.4" )
// Remark   : 
// Useage	: 
////////////////////////////////////////////////////////////////////////////////////////////
char * GetVersionString(BYTE cHexVer)
{
	memset(Util_stBuff, NULL, 64);
	
	Util_stBuff[0]=((cHexVer>>4) & 0x0F)+0x30;
	Util_stBuff[1]='.';
	Util_stBuff[2]=(cHexVer & 0x0F)+0x30;
	
	return Util_stBuff;
}








