s��        �`   � S   �`   �   ����                                                               Programmer's Toolbox                                                                                     � ��PositionCtrlSide     � ��PositionCtrlJustification  �  � ��unsigned char *     � ��DWORD     � ��PanelCallbackPtr     � ��CtrlCallbackPtr     � ��time_t     � ��ListType     � ��CompareFunction     � ��ListApplicationFunc     � ��ShowColorChangeFunction     � ��DelayedCallbackFunc     
� 
��UInt64Type     � ��ItemOutputFunction     � ��FILE *     � ��Handle     � ��WinMsgCallback     � ��TrayIconCallback     � ��Rect     � ��DeferredCallbackPtr       ��const int *     � ��const void *     �  const char *[]     �  const unsigned char[]     � 
 const char[]   �    This instrument contains an assortment of useful and convenient functions.   

Most of the functions in this library are multi-threaded safe.  The functions that are not multi-threaded safe are marked with a text message on their panels.     =    This class contains assorted useful or convenient functions     m    This class provides functions to obtain times and dates in the format appropriate to the system's location.     �    This class provides miscellaneous helper functions for use with strings.  

Note: The C Library provides a large set of general string handling functions.      H    This class contains functions useful for implementing robust file I/O.     G    This class provides functions for searching, sorting, and comparison.    �    This class provides the functions for a list data type.

A list is an ordered sequence of items of the same size.
Lists are very useful for managing arrays of data when the size of the arrays cannot be known ahead of time.

Important:  The lists managed by this class of functions are lists of arbitrary data items.  They are not related to the User Interface Library functions which operate on List Boxes and Ring Controls.


Here is a short example using the list datatype which can be executed in the interactive window:

#include "toolbox.h"

   static ListType list;
   static double   value;
   static int      index;

   list = ListCreate(sizeof(double));

   for (index = 1; index <= 360; index++) {
       value = sin (PI * index / 180);
       ListInsertItem (list, &value, END_OF_LIST);
       }

   YGraphPopup ("Plot Of Data In List", ListGetDataPtr(list),
             ListNumItems(list), VAL_DOUBLE);



Lists are very general data structures which can be used to implement other data structures:

   Stacks - Always insert and delete from the end of the list.

   Queues - Insert at the front, delete from the end.

   N-ary Trees - Make each list item a structure node which
       includes a list of child structure nodes.

   etc...     i    This class contains list item manipulation functions which can operate on more than one item at a time.     h    This class contains functions useful for searching, sorting, and  removing duplicate items from lists.     {    This class contains functions useful for writing the contents of a list to a file or to the Standard Input/Output Window.     �    This class contains functions which access advanced or obscure features of lists.

It is unlikely that you will need to call a function in the class.
     ;    This class provides functions to manage memory handles.

     |    This class provides helpful functions for use with the User Interface Library and user-interface development in general.

    y    Every function in this class operates on several controls at a time.  The functions take variable number of control ID parameters so arbitrary numbers of controls can be specified.

For each function, there exists an equivalent function in the Control List Functions class which takes a list of controls as its argument, instead of a variable number of control ID parameters.    X    Every function in this class operates on several controls at a time.  The functions take lists of control IDs so arbitrary numbers of controls can be specified.

For each function, there exists an equivalent function in the Multiple Control Functions class which takes a variable number of controls ID arguments instead of a list of controls.     X    This class contains functions which create and control special purpose dialogs boxes.
     �    This class provides functions to create, update, and discard progress status dialogs.

A progress dialog is a popup which displays a horizontal bar indicating the amount of progress that has been made towards completing a lengthy task.
     S    This class provides functions useful for customizing the behaviour of controls.

    �    This class contains functions which enable a control to receive the following extended mouse events in addition to the built-in CVI mouse events.

    EVENT_MOUSE_MOVE
    EVENT_LEFT_MOUSE_UP
    EVENT_RIGHT_MOUSE_UP

The built-in CVI mouse events are:

    EVENT_LEFT_CLICK
    EVENT_LEFT_DOUBLE_CLICK
    EVENT_RIGHT_CLICK
    EVENT_RIGHT_DOUBLE_CLICK

each of which occur when the mouse button is pressed down.

For each event:

    eventData1 is the vertical mouse coordinate
    eventData2 is the horizontal mouse coordinate

EVENT_MOUSE_MOVE is sent when the mouse moves, regardless whether a mouse button is down.  You can specify minimum interval between mouse move events.
        Functions in this class allow you to install icons in the status area of the Windows desktop status bar (the System Tray).

System Tray icons are a useful mechanism through which you can allow a user quick-access to your application's features without crowding the taskbar or requiring a window to be open at all times.  Standard procedure dictates the following usage of System Tray icons and their popup menus:

 - When the user moves the mouse over the icon, the system will display the tooltip that you provided when you created the icon.
 
- When the user clicks the icon with the right mouse button, your application should display the popup shortcut menu.

- When the user double-clicks the icon, your application should execute the default command on the shortcut menu.      �    Functions in this class allow you to implement popup menus on your System Tray icons.  Standard procedure dictates the following usage of System Tray icon popup menus:

- When the user clicks the icon with the right mouse button, your application should display the shortcut menu.  This shortcut menu should have a default item (indicated in bold type).

- When the user left-clicks or double-left-clicks the icon, your application should execute the default command on the right-click popup menu.      �    Functions in this class allow you to communicate with the Windows Registry.

The Windows Registry is a hierarchical database of configuration information maintained by the operating system.  This database is made up of Keys, where each Key may have associated Subkeys and/or Values.  Each Value is made up of a Value Name and some Data, which may be of different types.  Using functions in this class, you may write and read Value data to and from specified Keys.  

The base of the Registry hierarchy is made up by the system Root keys:

     HKEY_LOCAL_MACHINE
     HKEY_CLASSES_ROOT 
     HKEY_CURRENT_USER 

Each of these Root keys contains any number of Subkeys, and each Subkey contains any number of Values.

Applications will typically store "system" settings, such as the installation directory, in Subkeys of HKEY_LOCAL_MACHINE\SOFTWARE, and user-oriented settings, like color schemes, in Subkeys of HKEY_CURRENT_USER\SOFTWARE.

Use Regedit or Regedt32 (on Windows NT) to view the contents of your system Registry.

As an example, LabWindows/CVI itself stores information in the \SOFTWARE\National Instruments\CVI\5.0 Subkeys of both the HKEY_LOCAL_MACHINE and HKEY_CURRENT_USER Root keys.

NOTE:  Do not store data in the LabWindows/CVI Keys.     �    Functions in this class allow you to query the Windows operating system for information on system settings, configurations, and resources.     �    This class contains functions to query/adjust Windows operating system user-interface hardware and software configurations and properties.     N    This class contains functions to query and adjust Windows keyboard settings.     L    This class contains functions to query available Windows system resources.    �    Functions in this class allow you to install a callback for any Windows message.  This is useful for communicating with DLLs or applications that want to notify your own application of events by posting or sending Windows messages, which they define, to one of your application's windows (panels).  This functionality is not available through the User Interface Library's RegisterWinMsgCallback.
     �    Functions in this class allow you to obtain information regarding the user currently logged on to the Windows operating system.     �    This class contains contains functions to enable and disable Windows Shell file Drag-And-Drop file notification for CVI panels.  Files may be dragged from Windows Explorer, the Windows Shell, or certain Windows applications, and dropped onto CVI panels.    
    This class contains functions that return the values of commonly used mathematical and physical constants.  This class also contains functions to convert between different types of units.

For every function, a macro which returns the same value is also available.     �    This class contains functions that return the values of commonly used mathematical constants.

For every function, a macro which returns the same value is also available.     �    This class contains function that return the values of commonly used physical constants.

For every function, a macro which returns the same value is also available.     �    This class also contains functions to convert between different types of units.

For every function, a macro which performs the same conversion is also available.    [    This class contains functions you use to schedule callback functions for execution.

Post Delayed Callback Function - Use this function to schedule a function to be called after a specific amount of time has elapsed.

Post Deferred Call To Thread And Wait - Use this function to schedule a function for synchronous execution in another thread.          This function returns English descriptions of error codes defined by the following libraries or instrument drivers:

The User Interface Library
The Easy I/O instrument driver
The Toolbox instrument driver.

This can be useful if you write code that contains calls to more than one of these libraries.  Since these error codes do not overlap, this function can query the correct library for the error string.

Note: "No description available. " is returned for EasyI/O error codes if the EasyI/O instrument driver is not loaded and linked. 

     T    Returns a pointer to a string containing an English description of the error code.     �    Pass the error code for which to retrieve the English description.

The error code must be a User Interface Library, an Easy I/O, or a Toolbox error code.    *����   x    Error Description                 +D = �      x    Error Code                         	               
    This function is used to implement the Assert() and AssertMsg() macros.

The Assert macros are useful for notifying you of errors in your code during development.  They should be called when a fatal programming error in your code has been detected.  They will display a dialog giving the line number and source file name
where the error was detected and then abort the program.

You may want to redefine the Assert macros to do nothing if you are compiling a final release version of your program.

Liberal use of Asserts can find large numbers of bugs that would otherwise escape detection.

Example:


void SomeFunction(int numBytes)
{
   /* numBytes must always be greater than zero. If it isn't,
      it indicates an error in programming.
   */

   
   /* if numBytes is not greater than zero, then a dialog will
      appear giving the file name and line number where this
      Assert is.  When the dialog is dismissed, the program will
      be aborted.
   */
   Assert(numBytes > 0);


   /* rest of function goes here */
}     �    If passed is FALSE (0) then the function will abort the program after displaying an error message.

If passed is TRUE (non-zero) then the function will do nothing.     �    The file name to display in the Assert dialog if Passed is not TRUE (non-zero).  This parameter is filled in automatically when using the Assert() macro.     �    The line number to display in the Assert dialog if Passed is not TRUE (non-zero).  This parameter is filled in automatically when using the Assert() macro.     �    The parameter specifies the message that the assert dialog will display.

If 0 is passed, the message "Programming error detected." will be displayed.    0t = 7           Passed                            1! = � �  x    File Name                         1� =     x    Line Number                       2i =� �  `    Message                         ���� w q��                                            On 1 Off 0    	__FILE__    	__LINE__    0   \Use the macro Assert(passed) defined in toolbox.h instead of
this function.  The macro calls this function, automatically passing
the correct file name and line number. Zero is passed to the message
parameter.

You may also use the AssertMsg(passed, msg) macro, which takes an
extra string parameter to pass to the message parameter of DoAssert().   ?    This function forces a value to be within a range specified by a minimum and a maximum.  If the value is between the minimum and the maximum, then it is returned unaltered.  If the value is less than the minimum, then the minimum is returned.  If the value is greater than the maximum, then the maximum is returned.

         Returns the pinned value.     =    The value to be pinned between the maximum and the minimum.     *    The minimum value that Pin() can return.     *    The maximum value that Pin() can return.    6����   x    Pinned Value                      7 = M     x    Value                             7K = �    x    Minimum                           7} =i    x    Maximum                            	                       6    Returns a random number between Minimum and Maximum.     X    The random number that is generated.  This number will be between Minimum and Maximum.     _    The minimum value Random can return.  Random will return a value between Minimum and Maximum.     _    The maximum value Random can return.  Random will return a value between Minimum and Maximum.    8����   x    Result                            99 = �     x    Minimum                           9� ='    x    Maximum                            	           0    100   |    This function sets the seed value used by the Random() function.

Setting the seed value causes the Random() function to begin a new sequence of random numbers.

If you set the seed to a previously set value, Random() will generate the same sequence of random numbers that it did when the seed was first set to that value.  

Pass 0 to set the seed value using the current time.     Y    Pass the seed value that the Random() function should use to generate random numbers.

    <B = �     x    Seed Value                         0    W    Starts a sound from the built-in PC speaker.  This function only works on MS Windows.     "    Specifies the pitch of the sound  ���� � ���                                         ���� � ���                                           =< = �     x    Frequency                          MS Windows version only    'This function is not multithread-safe.    1000    ]    This function is invaluable in that it stops the sound started by a call to StartPCSound().  ���� � ���                                         ���� � ���                                            MS Windows version only    'This function is not multithread-safe.    ;    This function swaps the contents of two blocks of memory.         A pointer to a memory block.         A pointer to a memory block.     3    Pass the size of the memory blocks to be swapped.    ?� = M     x    Memory Block 1                    ?� = �    x    Memory Block 2                    ?� =i    x    Number Of Bytes                               �    This function combines the functionality of the three utility library functions that control whether CVI breaks when a library function returns an error code.  It can be more convenient to use if you need to change the break-on-library-errors state often.

The three utility library functions that SetBOLE() combines are:
DisableBreakOnLibraryErrors();
EnableBreakOnLibraryErrors();
GetBreakOnLibraryErrors();     b    Returns the previous break-on-library-errors state.

Values:  1 - enabled
         0 - disabled
     2    Specifies the new break-on-library-errors state.    B~���    x    Previous State                    B� = �           New State                          	          ;  Break On Library Errors 1 Do Not Break On Library Errors 0        This function transposes a two dimensional array of the specified data type.  

This function is especially useful for separating the data from a multi-channel scanning operation into separate channels.  For example, if you had an array of data from 5 scans of channels A through C in the following form:

A1 B1 C1 A2 B2 C2 A3 B3 C3 A4 B4 C4 A5 B5 C5

Transposing the data would yield the following array in
which the data from each channel is stored contiguously:

A1 A2 A3 A4 A5 B1 B2 B3 B4 B5 C1 C2 C3 C4 C5     %    Pass the array of data to transpose     2    Pass the data type of the elements in the array      3    Pass the total number of points in the data array     �    If the data was generated by a series of multi-channel scans, then pass the number of channels in the data.  This is the same as the number of columns in the data.

    E� = I     x    Data Array                        F = �    �    Data Type                         FE =m     x    Number Of Points                  F� � I     x    Number Of Channels                               �char VAL_CHAR short integer VAL_SHORT_INTEGER integer VAL_INTEGER unsigned char VAL_UNSIGNED_CHAR unsigned short integer VAL_UNSIGNED_SHORT_INTEGER unsigned integer VAL_UNSIGNED_INTEGER float VAL_FLOAT double VAL_DOUBLE            V    This function converts an array of one data type to an array of another data type.

    b    The source array.  The data type must be of the same type selected by the Source Data Type control:
 
                 character
                 short integer
                 integer
                 float
                 double-precision
                 unsigned short integer
                 unsigned integer
                 unsigned character
    �    Specifies the data type of the Source Array.

Valid Data Types:   

  character                   VAL_CHAR
  short integer               VAL_SHORT_INTEGER
  integer                     VAL_INTEGER
  float                       VAL_FLOAT
  double-precision            VAL_DOUBLE
  unsigned short integer      VAL_UNSIGNED_SHORT_INTEGER
  unsigned integer            VAL_UNSIGNED_INTEGER
  unsigned character          VAL_UNSIGNED_CHAR

    b    The target array.  The data type must be of the same type selected by the Target Data Type control:
 
                 character
                 short integer
                 integer
                 float
                 double-precision
                 unsigned short integer
                 unsigned integer
                 unsigned character
    �    Specifies the data type of the Target Array.

Valid Data Types:   

  character                   VAL_CHAR
  short integer               VAL_SHORT_INTEGER
  integer                     VAL_INTEGER
  float                       VAL_FLOAT
  double-precision            VAL_DOUBLE
  unsigned short integer      VAL_UNSIGNED_SHORT_INTEGER
  unsigned integer            VAL_UNSIGNED_INTEGER
  unsigned character          VAL_UNSIGNED_CHAR

     Q    The number of points to be converted from the source array to the target array.    IX = �     x    Source Array                      J� ='    x    Source Data Type                  L � �    x    Target Array                      M� �'    x    Target Data Type                  O� � �     x    Number Of Array Elements                         �character VAL_CHAR short integer VAL_SHORT_INTEGER integer VAL_INTEGER floating point VAL_FLOAT double precision VAL_DOUBLE unsigned short integer VAL_UNSIGNED_SHORT_INTEGER unsigned integer VAL_UNSIGNED_INTEGER unsigned character VAL_UNSIGNED_CHAR    	                      �character VAL_CHAR short integer VAL_SHORT_INTEGER integer VAL_INTEGER floating point VAL_FLOAT double precision VAL_DOUBLE unsigned short integer VAL_UNSIGNED_SHORT_INTEGER unsigned integer VAL_UNSIGNED_INTEGER unsigned character VAL_UNSIGNED_CHAR       %    This function returns the time of the file in the same timebase used by the C Library function time().  

This allows the file's time to be compared with times obtained from C Library functions.  The returned time can also be passed to the C Library time formatting and conversion functions.     �    1 is returned if the function succeeded.

0 is returned if there was an error converting the file time to the C Library form.

A negative User Interface Library error code is returned if an error occurred while reading the file time.
     @    Specifies the pathname of the file for which to get the date.
     X    Returns the time of the file in the same units used by the C Library function time().     TY���    x    Result                            UM = �  �  x    File Name                         U� =' �  x    File Time                          	                	           �    This function compares two floating point numbers for equality using a small tolerance factor to compensate for the inability to precisely represent some numbers and the error inherent in successive floating point operations.


Comparing floating point numbers for equality can be tricky because results can differ infinitesimally depending on how the calculation is performed. 

For example, consider the results given by the following code when run under the listed compilers.

  #include <stdio.h>
  #include <math.h>

  /* from toolbox.h (more precision than can be represented) */
  #define PI  3.1415926535897932384626433832795028841971

  main()
  {
     /*  Test 1 */
  if (acos(-1) == PI)
     printf("acos(-1) == PI Succeeded\n");
  else
     printf("Huh? acos(-1) and PI should be equal!!\n");

     /*  Test 2 */
  if (1/acos(-1) == 1/PI)
     printf("1/acos(-1) == 1/PI Succeeded\n");
  else
     printf("Huh? 1/acos(-1) == 1/PI should be equal!!\n");
  }



Here are the results for a few compilers:

Compiler                            Test 1              Test 2
--------                            ------              ------
CVI for Windows 3.1                 Passed              Failed
CVI for Windows 95                  Passed              Failed
Visual C++ 1.5                      Passed              Failed
Visual C++ 2.0                      Failed              Passed
Think C 6.0 on Quadra (Macintosh)   Failed              Failed
CVI for Sparc                       Passed              Passed
ACC for Sparc                       Passed              Passed
CC for Sparc                        Failed              Failed


The reasons for the differing results on the previous tests can depend on subtle issues such as when the compiler chooses to remove values from the co-processor, whether an alternate math library is being used, or even if a Pentium division bug patch is installed.

Failing a test is NOT a sign the compiler is doing something wrong.  For example, keeping the operands of an operation in the co-processor can increase accuracy of a result.  If this more accurate result is compared with a result that required intermediate values to be stored outside of the co-processor, then an equality test on the resulting values may fail.
This is not the compiler's fault, since it was performing the arithmetic as precisely as was possible.

In addition, different tests and different ways of writing the above tests will give a different set of results.


The bottom line is that using "==" to compare floating point numbers may yield surprising results.

For instances where you want to test if two numbers are 'virtually equal', this function can be used to make the comparison.

FP_Compare() works by comparing the ratio of the two numbers and comparing how close this ratio is to 1.  This method is better than simply comparing the difference of the two numbers because it compensates for the magnitude of the numbers.

If the difference between A and B is less than FP_CompareEpsilon, then the comparison between A and B is made using the following formula (with appropriate handling of zero and negative values):

  1 - A/B > FP_CompareEpsilon

(The value of FP_CompareEpsilon is #defined in toolbox.h)




     �    Returns 0 if number A is equal to number B (within a small tolerance).

Returns 1 if number A is greater than number B.

Returns -1 if number A is less than number B.     '    Pass the value to compare to number B     '    Pass the value to compare to number A    cm���    x    Result                            d = �     x    Number A                          dL ='    x    Number B                           	            0.0    0.0    �    This function searches a table for the closest match to the specified color.

The closest matching color is determined by considering both the pythagorean distance between colors in the RGB color cube and the ratios between the color components.     h    Returns the zero-based index of the color in the table which most closely matches the specified color.     �    The color for which to search the table for the closest match.

The color is specified in RGB format.  See function panel for the User Interface Library function MakeColor() for more information on RGB colors.     �    An array of integers where each integer represents a color in RGB format.

See function panel for the User Interface Library function MakeColor() for more information on RGB colors.     ?    Pass the number of colors (integers) in the color table array    f3���    x    Index Of Closest Color            f� = M      x    Color                             g~ = �    x    Color Table                       h= =i     x    Table Size                         	                       �    Displays an HTML help (.chm) file and controls the HTML Help viewer.

This function requires that the HTML Help components server, specifically hhctrl.ocx, is present on your system.  

Do not call this function from more than one thread in your program.  Do not interactively run this function panel or call this function from the Interactive Execution window.  Because the HTML Help components are not multithread-safe,  your program might hang if you call this function under any of these circumstances.

After calling this function successfully, do not exit from your program till the HTML Help Windows have completely initialized. If you exit your program before these windows have initialized, you may get a General Protection Fault.     O    Returns 0 if the function succeeds.  Otherwise, returns a toolbox error code.     �    Pass the path to the help file.  Pass NULL in this parameter when you pass HH_CLOSE_ALL in the Command parameter of this function.     �    Contains additional data to pass to the HTML Help API.  The value you pass depends on the value you pass in the Command parameter of this function.  Refer to the help for the Command parameter for more information, and sample code.    C    Specifies the HTML Help API command to execute in this function.

The values you can pass are:

HH_DISPLAY_TOPIC: Opens a compiled help (.chm) file and optionally displays a specific topic within the help file. The
Additional Data parameter is either NULL or a pointer to a
topic (the name of the embedded html file containing the topic) within the compiled help file.

Example:
   ShowHtmlHelp ("c:\\help.chm", HH_DISPLAY_TOPIC, NULL);

HH_HELP_FINDER:  Provides the same functionality as HH_HELP_TOPIC.  This command is provided for backward compatibility with WinHelp(),

Example:
   ShowHtmlHelp ("c:\\help.chm", HH_HELPFINDER, NULL);

HH_DISPLAY_TOC:  Selects the Contents tab in the Navigation pane of the HTML Help Viewer. Pass NULL in the Additional Data parameter.

Example:
   ShowHtmlHelp ("c:\\help.chm", HH_DISPLAY_TOC, NULL);

HH_DISPLAY_INDEX: Selects the Index tab in the Navigation pane of the HTML Help Viewer.  Pass a string in the Additional Data parameter to indicate the name of a keyword to select in the index.  Pass NULL in the Additional Data parameter to indicate that you do not want to select a keyword in the index.

Example:
   ShowHtmlHelp ("c:\\cat.chm", HH_DISPLAY_INDEX, "meow");

HH_HELP_CONTEXT:  Displays a help topic that you specify with a a mapped topic ID.  This is similar to the HELP_CONTEXT command in WinHelp.  The topic IDs must be defined and mapped in the [MAP] section of the project (.hhp) file.  Pass the context number of the topic in the Additional Data parameter.

Example:
   ShowHtmlHelp ("c:\\help.chm", HH_HELP_CONTEXT, (void *)5000);

HH_CLOSE_ALL:  Closes all HTML Help windows opened directly or indirectly by the calling program.  You must pass NULL in the Help File parameter.  You must pass NULL in the Additional Data parameter. 

Example:        
   ShowHtmlHelp (NULL, HH_CLOSE_ALL, NULL);    l]���    `    Status                          ���� � ���                                           l� : ?  �  �    Help File                         m@ :�    `    Additional Data                   n1 : �   �    Command                            	            yThis function is not multithread-safe. 
Do not run this function from the function panel or from the Interactive window.        0               �HH_DISPLAY_TOPIC HH_DISPLAY_TOPIC HH_HELP_FINDER HH_HELP_FINDER HH_DISPLAY_TOC HH_DISPLAY_TOC HH_DISPLAY_INDEX HH_DISPLAY_INDEX HH_HELP_CONTEXT HH_HELP_CONTEXT HH_CLOSE_ALL HH_CLOSE_ALL       This function opens a document in its default viewer. This function has the same effect as double-clicking on a document in Windows Explorer.

You can register file types and their default viewers in Windows Explorer by selecting the File Types tab in the Folder Options dialog.     O    Returns 0 if the function succeeds.  Otherwise, returns a toolbox error code.     6    The full path to the document that you want to open.     9    This control specifies how the document will be opened.    x�	���    `    Status                            yV H y  �  �    Document Filename                 y� H(    �    Window Display                     	            ""               ANormal VAL_NO_ZOOM Minimized VAL_MINIMIZE Maximized VAL_MAXIMIZE   �    This function returns a string representation of the current time in a format appropriate to internationalization settings of the computer.

Note: This function depends on the operating system and C Library support for international time formats.  On some systems, the time returned may not be in correct format for the location.

This function is not safe for use in a multi-threaded program because it temporarily changes the current locale in the ANSI-C time library.     �    Returns the number of characters in the internationally formatted time string.

Zero is returned if the time could not be formatted into the buffer.     6    Pass a string buffer to store the formatted time in.     �    Pass the size (including space for the terminating null byte) of the string buffer in which the formatted time is to be stored.    |����    x    Number Of Character Formatted   ���� � ���                                           }T = g     �    Time                              }� =O     x    String Buffer Size                 	            'This function is not multithread-safe.    	               �    This function returns a string representation of the current date in a format appropriate to internationalization settings of the computer.

Note: This function depends on the operating system and C Library support for international date formats.  On some systems, the date returned may not be in correct format for the location.

This function is not safe for use in a multi-threaded program because it temporarily changes the current locale in the ANSI-C time library.     �    Returns the number of characters in the internationally formatted date string.

Zero is returned if the date could not be formatted into the buffer.     6    Pass a string buffer to store the formatted date in.     �    Pass the size (including space for the terminating null byte) of the string buffer that the formatted date is to be stored in.    ����    x    Number Of Character Formatted   ���� � ���                                           �� = g     �    Date                              �� =O     x    String Buffer Size                 	            'This function is not multithread-safe.    	               �    This function returns a string representation of the file time in a format appropriate to internationalization settings of the computer.

Note: This function depends on the operating system and C Library support for international time formats.  On some systems, the time returned may not be in correct format for the location.

This function is not safe for use in a multi-threaded program because it temporarily changes the current locale in the ANSI-C time library.     �    Returns the number of characters in the internationally formatted time string.

Zero is returned if the time could not be formatted into the buffer.

A negative User Interface Library error code is returned if the file's time could not be accessed.     @    Specifies the pathname of the file for which to get the time.
     6    Pass a string buffer to store the formatted time in.     �    Pass the size (including space for the terminating null byte) of the string buffer that the formatted time is to be stored in.    �p���    x    Number Of Character Formatted     �r =   �  �    File Name                         �� = �    �    Time                              �� =�     x    String Buffer Size              ���� � ���                                            	                	                'This function is not multithread-safe.   �    This function returns a string representation of the file date in a format appropriate to internationalization settings of the computer.

Note: This function depends on the operating system and C Library support for international date formats.  On some systems, the date returned may not be in correct format for the location.

This function is not safe for use in a multi-threaded program because it temporarily changes the current locale in the ANSI-C time library.     �    Returns the number of characters in the internationally formatted date string.

Zero is returned if the date could not be formatted into the buffer.

A negative User Interface Library error code is returned if the file's date could not be accessed.     @    Specifies the pathname of the file for which to get the date.
     6    Pass a string buffer to store the formatted date in.     �    Pass the size (including space for the terminating null byte) of the string buffer that the formatted date is to be stored in.    �����    x    Number Of Character Formatted     �� =   �  �    File Name                         �� = �    �    Date                              �8 =�     x    String Buffer Size              ���� � ���                                            	                	                'This function is not multithread-safe.   �    This function appends all or some of the characters from one string to the end of another string.

Here is an example that may be run in the Interactive Window:

#include "toolbox.h"

   static char *string;

      /* allocate a new string */
   string = StrDup("Cats");  

      /* prints 'Cats' */
   printf("string = '%s'\n", string);

      /* append to the string */
   AppendString(&string, " And Dogs", -1);

      /* prints 'Cats And Dogs' */
   printf("string = '%s'\n", string);

         The return value of this function is 1 if the append succeeded, or 0 if there was not enough memory to append the characters.    q    Pass a pointer to the string to which to append the other string.  

This parameter must be the address of variable that holds a pointer to a dynamically allocated nul-terminated string.
(The variable can contain NULL).   

A dynamically allocated string is a string whose memory was allocated by the malloc() or calloc() functions, or by a function which calls malloc() or calloc() (such as StrDup()).

The value in the variable will be changed if a new memory block is needed to provide room for the additional characters.  Otherwise, the string pointed to is reallocated in place to provide room for the new characters.
     ;    Pass the string to append to the end of the other string.        This parameter specifies how many characters of String_To_Append are to be appended.

If this value is greater than the number of character in the string, then only the characters in the string are appended.

Pass -1 to append the entire String_To_Append.    ����    x    Result                            �� = C     x    Pointer To String To Append To    � = � �  x    String To Append                  �J =p     x    Num Characters To Append           	            	                -1    �    This function inserts charcters at the beginning of a string that you dynamically allocate previously.

The function reallocates the string so that it is large enough to contain the additional characters.     �    Pass the address of the pointer to the string into which you want to insert additional characters.

The function reallocates the string so that it is large enough to contain the additional characters.     w    Pass the null-terminated string that contains the characters you want to insert at the beginning of the other string.     q    Pass the maximum number of characters to insert at the beginning of the string.

Pass -1 to add all characters.     u    Returns FALSE if the function cannot reallocate the string because of insufficient memory.

Returns TRUE otherwise.    � 8 P     �    string                            �� 8 �  `    stringPrefix                      �p 9�     `    lengthOfPrefix                    �����    `    Status                             	                -1    	            �    This function adds double quotes around a string that you dynamically allocate previously.

The function reallocates the string so that it is large enough to contain the additional characters.     �    Pass the address of the pointer to the string around which you want to place double quotes.

The function reallocates the string so that it is large enough to contain the additional characters.     u    Returns FALSE if the function cannot reallocate the string because of insufficient memory.

Returns TRUE otherwise.    �& 8 �     `    string                            �����    `    Status                             	            	            �    This function allocates and returns a copy of a string.  It is the responsibility of the caller to dispose of the string using the free() function.     �    Returns a pointer to a newly allocated memory block containing a copy of the String_To_Duplicate.  If there is not enough memory to copy the string or if String_To_Duplicate is passed a value of zero then a value of zero is returned.
     #    Pass the string to be duplicated.    �����   x    Duplicated String                 �� = �  �  x    String To Duplicate                	            ""    Y    Dynamically allocates a copy of a string, removing any leading or trailing white space.     �    Pointer to the dynamically allocated copy of the string.

If there was insufficient memory for the allocation, NULL (0) is returned.         The string to copy.      �����   x    Copy of String                    � = �  �  x    String                             	            ""    �    Returns a pointer to the first non-whitespace character in String.  

If there are no non-whitespace characters in String before the first ASCII NULL byte, the function returns a pointer to the NULL byte.     �    A pointer to the first non-whitespace character in String. 
If there are no non-whitespace characters in String before the first ASCII NUL byte, a pointer to the ASCII NUL byte.         A nul-terminated string.    �����   x    Pointer to Non-WhiteSpace         �C = �  �  x    String                             	            ""   �    This function copies the string pointed to by source to the array of characters pointed to by destination.  No more than the number of characters specified by Destination_Buffer_Size will be copied (including the nul byte).

This function is similar to the C Library function strncpy(), except for the following three advantages:

1) If the source string is larger than the destination buffer size, then as much of the source string as possible will be copied with the provision that the last byte in the destination buffer will be a nul string terminator.  strncpy() DOES NOT nul terminate the copied string when the source string is too long.

2) StringCopyMax() does not write into the destination string past the point necessary to copy the string.  strncpy() fills the end of the destination array with zeros, up to the specified buffer size.

3) StringCopyMax() is tolerant of overlaps between the source and destination strings. strncpy() is not.
     P    This control contains the target string to which source string will be copied.     �    This control contains a pointer to the null terminated source string which will be copied to the destination string subject to the length limit imposed by Destination_Buffer_Size.     �    Pass the size of the destination buffer (including space for the nul byte).

StringCopyMax() will copy no more than the specified number of bytes (including the nul byte) regardless of the length of the source string.    �� = M     x    Destination                       �� = � �  x    Source                            �� =i     x    Destination Buffer Size            	                    �    Returns a pointer to the first whitespace character in String.  

If there are no whitespace characters in String before the first ASCII NUL byte, the function returns a pointer to the NUL byte.     �    A pointer to the first whitespace character in String. 
If there are no whitespace characters in String before the first ASCII NUL byte, a pointer to the ASCII NUL byte.         A nul-terminated string.    ����   x    Pointer to WhiteSpace             �� = �  �  x    String                             	            ""    b    Determines if a string contains a non-whitespace character before the terminated ASCII NUL byte.     B    If String contains a non-whitespace character, 1.

Otherwise, 0.         A nul-terminated string.
    �����    x    Has Non-WhiteSpace                �! = �  �  x    String                             	            ""    �    This function removes leading and trailing white space from a string.  The string is modified in place and will be shortened by the number of white space characters removed.     <    Pass the string to remove the surrounding whitespace from.    �w = �     x    String                                 �    Performs a byte-by-byte, case-insensitive comparison of two nul-terminated strings.  Returns:

    A negative value if String1 is less than String2.
    Zero if String1 is identical to String2.
    A positive value if String1 is greater than String2.     �    A negative value if String1 is less than String2.
Zero if String1 is identical to String2.
A positive value if String1 is greater than String2.         A nul-terminated string.         A nul-terminated string.    �����    x    Result                            �� = �  �  x    String1                           �� =' �  x    String2                            	            ""    ""    �    Performs a byte-by-byte, case-insensitive comparison of two nul-terminated strings.  Returns:

    A negative value if String1 is less than String2.
    Zero if String1 is identical to String2.
    A positive value if String1 is greater than String2.     �    A negative value if String1 is less than String2.
Zero if String1 is identical to String2.
A positive value if String1 is greater than String2.         A nul-terminated string.         A nul-terminated string.     P    The maximum number of characters to compare between the two specified strings.    �����    x    Result                            �) = G  �  x    String1                           �K = � �  x    String2                           �m =c     x    Max Chars                          	            ""    ""       *    Performs a byte-by-byte, case-insensitive comparison of two nul-terminated strings.  Leading and trailing white space is ignored.

Returns:

    A negative value if String1 is less than String2.
    Zero if String1 is identical to String2.
    A positive value if String1 is greater than String2.     �    A negative value if String1 is less than String2.
Zero if String1 is identical to String2.
A positive value if String1 is greater than String2.         A nul-terminated string.         A nul-terminated string.    �����    x    Result                            �� = �  �  x    String1                           �� =' �  x    String2                            	            ""    ""   �    Attempts to convert a nul-terminated string to an integer value.

If the bytes in the nul-terminated string contain a valid ASCII decimal representation of an integer value, and there are no extraneous non-whitespace characters in the string, 1 is returned.  Otherwise, 0 is returned.

This function is similar to the C library function strtol() except in how extraneous characters are treated.     �    If the bytes in the nul-terminated string contain a valid ASCII decimal representation of an integer value, and there are no extraneous non-whitespace characters in the string, 1 is returned.  Otherwise, 0 is returned.         A nul-terminated string.     �    The integer value represented by the characters in String.

If the characters in String are not a valid ASCII decimal  representation of an integer value, or there are extraneous characters, this parameter is not modified.    ����    x    Success?                          �� = �  �  x    String                            � ='     x    Integer Value                      	            ""    	           �    Attempts to convert a nul-terminated string to an unsigned integer value.

If the bytes in the nul-terminated string contain a valid ASCII decimal representation of an unsigned integer value, and there are no extraneous non-whitespace characters in the string, 1 is returned.  Otherwise, 0 is returned.

This function is similar to the C library function strtoul() except in how extraneous characters are treated.     �    If the bytes in the nul-terminated string contain a valid ASCII decimal representation of an integer value, and there are no extraneous non-whitespace characters in the string, 1 is returned.  Otherwise, 0 is returned.         A nul-terminated string.     �    The unsigned integer value represented by the characters in String.

If the characters in String are not a valid ASCII decimal  representation of an unsigned integer value, or there are extraneous characters, this parameter is not modified.    �a���    x    Success?                          �E = �  �  x    String                            �g ='    x    Unsigned Integer Value             	            ""    	            �    Determines whether you have access (permissions) to write a file.  The determination is made without regard to whether the file already exists.    h    Indicates if function was able to determine the file writability.

If the function was successful, it returns a 0.  Otherwise, it returns a negative error code.  The error codes which can occur and the header files in which they are defined are listed below:

    -91  Too many files are open.   (userint.h)
    -93  Input/Output error.        (userint.h)


         The pathname of the file.   If it is not an absolute pathname, the file is located relative to the current working directory.     z    If the function determines that the file is writable, then this parameter is set to 1.  

Otherwise, it is set to 0.



    �����    x    Status                            �' = ?  �     Pathname                          �� =w     x    Is Writeable                       	            ""    	           ,    Creates a temporary file in the same directory as the file identified by Pathname.  This function is intended to be used when writing out a new version of the file identified by Pathname.  By writing out a temporary file in the same directory as the target file, you can make sure that there is enough room on the disk for the new file.  Otherwise, you might destroy the old contents of the file without being able to write the new version of the file.   When you have successfully written the temporary file, you can use the DeleteAndRename() function to delete the old version of the target file and then rename the temporary file to the target file.

The function operates as follows:

It generates temporary file names using:

  - the directory of the Pathname

  - the Temp Filename Prefix (which should be 5 characters in 
    length)

  - following by an index of up to three characters (e.g. "1",
    "20", "101")

  - followed by a dot

  - followed by the Temp Filename Extension (which should be 
    from 1 to 3 characters in length and should not contain a 
    dot).

For each temporary file name, the function checks to see if the file already exists.  If the file does not already exist, the function attempts to open the file for writing and returns success or failure based on whether the open call succeeded.

If files exist for each of the 1000 temporary file names that can be generated, an error is returned.

If the function succeeds, the pathname of the temporary file is returned in Temp File Pathname and the file handle is returned in Temp File Handle    y    Indicates if the temporary file was created.

If the function was successful, it returns a 0.  Otherwise, it returns a negative error code.  The error codes which can occur and the header files in which they are defined are listed below:


    -91  Too many files are open.                (userint.h)
    -93  Input/Output error.                     (userint.h)
  -5003  Could not generate an unused temporary 
         file name in the same directory as the 
         output file.                            (toolbox.h)
  -5004  Could not create a temporary file in 
         the same directory as the output file.  (toolbox.h)


        The pathname of a file.   If it is not an absolute pathname, the pathname is relative to the current working directory.

Only the drive and directory portions of the pathname are used in creating the temporary file.  The file name portion of this parameter is not used.      �    A string containing the characters to be used in generating the first portion of the temporary file names.  There should be from one to five characters only.      �    The extension to be used in the temporary file names.  It should contain from one to three characters, and it should not contain a dot.     �    The open mode string to be passed to the fopen call within this function.  Refer to the documentation for fopen in the ANSI C Library.     b    If this function succeeds in creating a temporary file, its pathname is returned in this buffer.     @    Returns the file pointer (type FILE *) for the temporary file.    �!���    x    Status                            ʢ = C  �     Pathname                          ˺ � C �  x    Temp Filename Prefix              �b � � �  x    Temp Filename Extension           �� �q �  x    Open Mode String                  ̓ � C       Temp File Pathname                �� �s �  x    Temp File Handle                   	            ""    ""    ""    "wb"    	            	            o    This function removes a file from the file system.   
It does not report an error if the file already exists.     �    The pathname of the file to remove.   If it is not an absolute pathname, the file is located relative to the current working directory.    �] = �  �     Pathname                           ""    ~    Deletes the file identified by Destination Pathname.
Renames the file identified by Source Pathname to Destination Pathname.        If the function was successful, it returns a 0.  Otherwise, it returns a negative error code.  The error codes which can occur and the header files in which they are defined are listed below:

    -12  Out of memory!                     (userint.h)
    -93  Input/Output error.                (userint.h)
    -94  File not found.                    (userint.h)
    -95  File access permission denied.     (userint.h)
    -97  Disk full.                         (userint.h)
   -100  Bad pathname.                      (userint.h)
    


     �    The pathname of the file to rename.   If it is not an absolute pathname, the file is located relative to the current working directory.     r    The pathname of the file to delete, and the pathname to rename the file currently identified by Source Pathname.    ѯ���    x    Status                            �� = �  �     Source Pathname                   �b � � �     Destination Pathname               	            ""    ""    P    This function determines if a file with the specified pathname exists on disk.     �    Returns 1 (TRUE) if the file exists.

Returns 0 (FALSE) if the file was not found.

Returns a negative user interface library error code if an
error occurred.

Possible errors:
   -12    UIEOutOfMemory
   -93    UIEIOError     


     y    The pathname of a file.   If it is not an absolute pathname, the pathname is relative to the current working directory.     �    This parameter returns the size of the file in bytes if the file exists, or -1 if the file does not exist or an error occurred.

Pass 0 if you do not need this information.    �����    x    Result                            �� = ?  �     Path                              �\ =w     x    File Size                          	            ""    	            �    This function writes a string to file, converting any newlines to the appropriate line terminators for the system.

This is convenient for writing text to a file that was opened in binary mode.     �    Returns 0 if the string was written succesfully.

Returns a negative user interface library error code if an
error occurred.

Possible errors:
  -97    UIEDiskFull
     �    This parameter specifies which FILE* stream to output the item to.  If stdout is specified, then the item is sent to the standard output device which is usually the Standard Input/Output window.     �    Pass the null terminated array of characters to write to the file.  The null terminator is not written to the file.

Any newline (\n) characters in the string are converted to the appropriate line terminators for the system.    ٚ���    x    Result                            �H = ;  �  x    Stream                            � = � �     String                             	                ""    '    This function saves a bitmap to file.    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     y    The pathname of a file.   If it is not an absolute pathname, the pathname is relative to the current working directory.    +    The id of the bitmap containing the image.  Must be a value obtained from one of the following functions:

    NewBitmap
    GetBitmapFromFile
    GetCtrlBitmap
    ClipboardGetBitmap
    GetCtrlDisplayBitmap
    GetPanelDisplayBitmap
    GetScaledPanelDisplayBitmap
    GetScaledCtrlDisplayBitmap    �� ����    `    Status                            �d Q 2  �     Path                              �� Q�     `    Bitmap ID                          	            ""           This function creates a bitmap "snapshot" of the specified control, scales it according to the New Height and New Width parameters and then saves it to file.


/*-------------------- Prototype ---------------------*/

int Status = SaveCtrlDisplayToFile (int Panel_Handle, 
                                    int Control_ID, 
                                    int Include_Label, 
                                    int New_Height, 
                                    int New_Width, 
                                    char Path[]);     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.     �    The defined constant (located in the UIR header file) which was assigned to the control in the User Interface Editor, or the ID returned by the NewCtrl() or DuplicateCtrl() function.     {    Specifies whether to include the control label (if any) in the bitmap created from the current appearance of the control.     \    Specifies the desired height of the resultant bitmap.

Use -1 to maintain the same height.     Z    Specifies the desired width of the resultant bitmap.

Use -1 to maintain the same width.     y    The pathname of a file.   If it is not an absolute pathname, the pathname is relative to the current working directory.    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.    �� = w      `    Panel Handle                      � = �     `    Control ID                        �^ :r          Include Label                     �� � w     `    New Height                        �E � �     `    New Width                         � � w �     Path                              �( ����    `    Status                                    Yes 1 No 0            ""    	           �    This function creates a bitmap "snapshot" of the specified panel, scales it according to the New Height and New Width parameters and then saves it to file.


/*-------------------- Prototype ---------------------*/

int Status = SavePanelDisplayToFile (int Panel_Handle, 
                                    int scope, Rect Area
                                    int New_Height, 
                                    int New_Width, 
                                    char Path[]);

    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.    |    Use this parameter to restrict the area of the panel scaled and copied into the bitmap.   

You must pass a Rect structure.  The rectangle coordinates, specified in pixels, are relative to the upper-left corner of the panel (directly below the title bar) before the panel is scaled.
   

Use the VAL_ENTIRE_OBJECT macro if you do not want to restrict the area scaled and copied.         If you pass VAL_ENTIRE_OBJECT for the Area parameter, this parameter specifies the desired height of the resultant bitmap.   (If you want to maintain the same height, pass -1.)

Otherwise, the resultant height equals

      new height
    -------------- * area height
    initial height        f you pass VAL_ENTIRE_OBJECT for the Area parameter, this parameter specifies the desired width of the resultant bitmap.   (If you want to maintain the same width, pass -1.)

Otherwise, the resultant width equals

      new width
    -------------- * area width
    initial width     y    The pathname of a file.   If it is not an absolute pathname, the pathname is relative to the current working directory.    �<	���    `    Status                            � A 2      `    Panel Handle                      �m AC �  �    Area                              �� � 2     `    New Height                        � � �     `    New Width                         �: �C �  �    Path                            ���� = �          Scope                              	                VAL_ENTIRE_OBJECT            ""   visible area 1 full panel 0   g    This function performs a Binary search in an array of Number_Of_Elements items of size Element_Size.

Important: The binary search algorithm assumes that the array is already sorted in ascending order.

This function is like the C-Library function bsearch() except that this function returns the index where the item should be placed if it is not found.
  
        Returns the index of the array element that matches the item (from 0 to Number_Of_Elements - 1).
       
If no match is found, it returns  (-i - 1) where i is the index (0 to numElements) where the item should be placed if it were to be inserted in order.      �    Pass the array of items that BinSearch() will search.

Important: The binary search algorithm assumes that the array is already sorted in ascending order.
     (    Pass the number of items in the array.     ;    Pass the item size (in bytes) for the items in the array.     8    Pass a pointer to the item to search for in the array.        Pass a comparison function that BinSearch should use to compare items in the array to the item pointed to by Item_Pointer.

The compare function should have the following prototype:

 int CVICALLBACK CompareFunction(void *item1, void *item2);
    
The compare function should return a negative number if item1 is less than item2, it should return 0 if item1 is equal to item2, and it should return a positive number is item1 is greater than item2.  When the compare function is called, item1 is always the item pointer passed to BinSearch() and item2 is always a pointer to an element in the array passed to BinSearch().

This instrument driver provides several commonly useful comparison functions:
   ShortCompare()
   IntCompare()
   FloatCompare()
   DoubleCompare()
   CStringCompare()
    �����    x    Result                            �� = L  �  x    Array To Search                   �� = �     x    Number Of Elements                �� =l     x    Element Size                      � � L �  x    Item Pointer                      �Q � � �  x    Comparison Function                	                               �    This function sorts an array of elements in ascending order according to a comparison function provided by the user.


This function is similar to the C library function qsort() except that its performance is guaranteed to be proportional to n*log n where n is the number of items to be sorted.

The qsort() function uses the quicksort algorithm with median of 3 partitioning.  The performance of this algorithm is n*log n for almost all initial orderings, including the case where the array is already sorted.  However, there are a very few orderings for which the performance of quicksort degenerates towards being proportional to n*n.  

In practice you will almost never encounter one of these orderings.  This makes the C library function qsort() a better choice since it is about three times faster that HeapSort().

Only use HeapSort() if sorting performance proportional to n*log n must be absolutely guaranteed.


     1    The array of items that HeapSort() will sort.

     (    Pass the number of items in the array.     ;    Pass the item size (in bytes) for the items in the array.    I    Pass a comparison function that HeapSort should use to compare the items in the array.

The compare function should have the following prototype:

 int CVICALLBACK CompareFunction(void *item1, void *item2);
    
The compare function should return a negative number if item1 is less than item2, it should return 0 if item1 is equal to item2, and it should return a positive number is item1 is greater than item2.


This instrument driver provides several commonly useful comparison functions:
   ShortCompare()
   IntCompare()
   FloatCompare()
   DoubleCompare()
   CStringCompare()
    �u = N     x    Array To Sort                     �� = �     x    Number Of Elements                �� =j     x    Element Size                      ! � N �  x    Comparison Function                               �    This function sorts an array of elements in ascending order according to a comparison function provided by the user.

This function is similar to the C library function qsort() except that it uses a plain vanilla n-squared sorting algorithm.  Its only advantage is that it is a stable sort.  

A stable sort can be used multiple times on the same data set with different keys.  For example, if InsertionSort() is called to sort an array of names by first name, and then called again to sort the same array by last name, then the names will be sorted by last name, and sorted by first name where the last names are the same (like a phone book).  The Quick Sort algorithm does not have this property (However, a comparison function could be written to compare based on last names with ties broken by comparing on first names.  This comparison function could be passed to qsort() or HeapSort() to achieve the same results with greater efficiency).

While this function can be fast for small numbers of items, its n-squared property makes it unsuitable for very large numbers of items.  For large numbers of items, qsort() or another n * log n sort should be used.       6    The array of items that InsertionSort() will sort.

     (    Pass the number of items in the array.     ;    Pass the item size (in bytes) for the items in the array.    L    Pass a comparison function that InsertionSort should use to compare the items in the array.

The compare function should have the following prototype:

 int CVICALLBACK CompareFunction(void *item1, void *item2);
    
The compare function should return a negative number if item1 is less than item2, it should return 0 if item1 is equal to item2, and it should return a positive number is item1 is greater than item2.

This instrument driver provides several commonly useful comparison functions:
   ShortCompare()
   IntCompare()
   FloatCompare()
   DoubleCompare()
   CStringCompare()   � = N     x    Array To Sort                    ( = �     x    Number Of Elements               X =j     x    Element Size                     � � N �  x    Comparison Function                               �    This function compares two short integers.  This function may be used anywhere a function of type CompareFunction is required.  Specifically, this function may be passed to:
   BinSearch(),
   InsertionSort
   ListInsertInOrder(),
   ListFindItem(),
   ListRemoveDuplicates(), 
   ListBinSearch(),
   ListQuickSort(),
   ListHeapSort(),
   ListInsertionSort(),
   or ListIsSorted().
     �    Returns the result of the comparison of the shorts pointed to by Item1 and Item2.  If Item1 > Item2 the result is a positive number ( > 0). If Item1 == Item2 the result is zero (0).  If Item1 < Item2 the result is a negative number ( < 0).
     ~    This parameter expects to be passed a pointer to a short int.  This short will be compared to the short pointed to by Item2.     ~    This parameter expects to be passed a pointer to a short int.  This short will be compared to the short pointed to by Item1.   \���    x    Comparison Result                V = �     x    Item1                            � ='    x    Item2                              	                   z    This function compares two integers.  This function may be used anywhere a function of type CompareFunction is required.  Specifically, this function may be passed to:
   BinSearch(),
   InsertionSort
   ListInsertInOrder(),
   ListFindItem(),
   ListRemoveDuplicates(), 
   ListBinSearch(),
   ListQuickSort(),
   ListHeapSort(),
   ListInsertionSort(),
   or ListIsSorted().     �    Returns the result of the comparison of the integers pointed to by Item1 and Item2.  If Item1 > Item2 the result is a positive number ( > 0). If Item1 == Item2 the result is zero (0).  If Item1 < Item2 the result is a negative number ( < 0).
     u    This parameter expects to be passed a pointer to an int.  This int will be compared to the int pointed to by Item2.     u    This parameter exoects to be passed a pointer to an int.  This int will be compared to the int pointed to by Item1.   ����    x    Comparison Result                � = �     x    Item1                             ='    x    Item2                              	                   x    This function compares two floats.  This function may be used anywhere a function of type CompareFunction is required.  Specifically, this function may be passed to:
   BinSearch(),
   InsertionSort
   ListInsertInOrder(),
   ListFindItem(),
   ListRemoveDuplicates(), 
   ListBinSearch(),
   ListQuickSort(),
   ListHeapSort(),
   ListInsertionSort(),
   or ListIsSorted().     �    Returns the result of the comparison of the floats pointed to by Item1 and Item2.  If Item1 > Item2 the result is a positive number ( > 0). If Item1 == Item2 the result is zero (0).  If Item1 < Item2 the result is a negative number ( < 0).
     z    This parameter expects to be passed a pointer to a float.  This float will be compared to the float pointed to by Item2.     z    This parameter expects to be passed a pointer to a float.  This float will be compared to the float pointed to by Item1.   ����    x    Comparison Result                � = �     x    Item1                            < ='    x    Item2                              	                   z    This function compares two doubles.  This function may be used anywhere a function of type CompareFunction is required.  Specifically, this function may be passed to:
   BinSearch(),
   InsertionSort
   ListInsertInOrder(),
   ListFindItem(),
   ListRemoveDuplicates(), 
   ListBinSearch(),
   ListQuickSort(),
   ListHeapSort(),
   ListInsertionSort(),
   or ListIsSorted().
     �    Returns the result of the comparison of the doubles pointed to by Item1 and Item2.  If Item1 > Item2 the result is a positive number ( > 0). If Item1 == Item2 the result is zero (0).  If Item1 < Item2 the result is a negative number ( < 0).
     }    This parameter expects to be passed a pointer to a double.  This double will be compared to the double pointed to by Item2.     }    This parameter expects to be passed a pointer to a double.  This double will be compared to the double pointed to by Item1.   ����    x    Comparison Result                � = �     x    Item1                            s ='    x    Item2                              	                   z    This function compares two strings.  This function may be used anywhere a function of type CompareFunction is required.  Specifically, this function may be passed to:
   BinSearch(),
   InsertionSort
   ListInsertInOrder(),
   ListFindItem(),
   ListRemoveDuplicates(), 
   ListBinSearch(),
   ListQuickSort(),
   ListHeapSort(),
   ListInsertionSort(),
   or ListIsSorted().
     �    Returns the result of the comparison of the strings pointed to by Item1 and Item2.  If Item1 > Item2 the result is a positive number ( > 0). If Item1 == Item2 the result is zero (0).  If Item1 < Item2 the result is a negative number ( < 0).
     }    This parameter expects to be passed a pointer to a string.  This string will be compared to the string pointed to by Item2.     ~    This parameter expects to be passed a pointer to an string.  This string will be compared to the string pointed to by Item1.   -���    x    Comparison Result                ( = �     x    Item1                            � ='    x    Item2                              	                   �    This function compares two strings without regard to case.  This function may be used anywhere a function of type CompareFunction is required.  Specifically, this function may be passed to:
   BinSearch(),
   InsertionSort
   ListInsertInOrder(),
   ListFindItem(),
   ListRemoveDuplicates(), 
   ListBinSearch(),
   ListQuickSort(),
   ListHeapSort(),
   ListInsertionSort(),
   or ListIsSorted().
    \    Returns one of the following values:

 -1 // if StringA is less than StringB without regard to case
 1  // if StringA is greater than StringB without regard to case
 0  // if StringA is equal to StringB without regard to case

where:

   StringA is the string pointed to by *(char **)Item1 
   StringB is the string pointed to by *(char **)Item2      }    This parameter expects to be passed a pointer to a string.  This string will be compared to the string pointed to by Item2.     ~    This parameter expects to be passed a pointer to an string.  This string will be compared to the string pointed to by Item1.   "���    x    Comparison Result                #� = �     x    Item1                            $h ='    x    Item2                              	                    I    This function creates a List which holds items of the specified size.       Q    Returns a new empty list.

If there is not enough memory, zero (0) is returned.    m    Pass the size in bytes of the items that the list will hold.

For example, to create a list of doubles:

#include "toolbox.h"
{
   double   value1 = 5.0;
   double   value2 = 7.0;
   ListType newList;

   newList = ListCreate(sizeof(double));
   
   if (newList)
      {
      ListInsertItem(newList, &value1, END_OF_LIST);
      ListInsertItem(newList, &value2, END_OF_LIST);    
      }
}


The size of every item in the list must be the same.  If you want to have a list of items that can be different sizes, make a list of pointers to the items instead.

For example, to make a list of strings:

#include "toolbox.h"
{
   char   *dog = "dog";
   char   *cat = "cat";
   ListType newList;

   newList = ListCreate(sizeof(char *));
   
   if (newList)
      {
      ListInsertItem(newList, &dog, END_OF_LIST);
      ListInsertItem(newList, &cat, END_OF_LIST);    
      }
}   %�����  x    New List                         &K = �      x    Item Size                          	               <    This function disposes of the memory allocated for a list.     t    Pass the list to dispose.  Zero (or NULL) may passed safely, and the function will return without doing anything.
   *~ = �  �  x    List To Dispose                        6    This function returns the number of items in a list.     *    Returns the number of items in the list.     F    The list for which the number of items it contains will be returned.   +q���    x    Number Of Items In The List      +� = �  �  x    List                               	                �    This function inserts a copy of an item into a list at the specified position.  

The item pointed to by Pointer_To_Item should be the same size as the item size specified when the list was created.     R    Returns non-zero if the item was inserted.

Returns zero (0) if an error occurs.     #    The list to insert the item into.     $    The pointer to the item to insert.    Y    The position at which to insert the item.  The position may be a number from 1 to the number of items in the list.  

To insert at the front of the list, the constant FRONT_OF_LIST may be passed.

To insert at the end of the list, the constant END_OF_LIST may be passed.

The items at or above the specified position are moved up one position.   -;���    x    Result                           -� = M  �  x    List                             -� = � �  x    Pointer To Item                  -� =i     x    Position To Insert At              	                    END_OF_LIST    {    This function copies the contents of the specified list item to the specified destination.  The item remains in the list.          The list to get the item from.     W    A pointer to where ListGetItem() should copy the contents of the specified list item.        The position of the item to be retrieved.  The position may be a number from 1 to the number of items in the list.  

To get the first item in the list, the constant FRONT_OF_LIST may be passed.

To get the last item in the list, the constant END_OF_LIST may be passed.   0� = M  �  x    List                             0� = �    x    Item Destination                 1N =i     x    Position Of Item To Get                    FRONT_OF_LIST    �    This function copies the contents of the data item pointed to by Pointer_To_Item over the contents of the item in the list at the specified position.      1    The list whose specified item will be replaced.     L    The pointer to the item which will replace the specified item in the list.        The position of the item to replace.  The position may be a number from 1 to the number of items in the list.  

To replace the first item in a list, the constant FRONT_OF_LIST may be passed.

To replace the last item in a list, the constant END_OF_LIST may be passed.   3� = M  �  x    List                             3� = � �  x    Pointer To Item                  4J =i     x    Position Of Item To Replace                FRONT_OF_LIST    �    This function removes an item from a list.  If Item_Destination is not zero (0) then the contents of the item will be copied to Item_Destination after the item is removed.     0    The list whose specified item will be removed.     �    A pointer to where the contents of the specified item should be copied after it is deleted from the list.

Zero (0) may be passed if the item is to be deleted from the list without being copied to another location.        The position of the item to remove.  The position may be a number from 1 to the number of items in the list.  

To remove the first item in a list, the constant FRONT_OF_LIST may be passed.

To remove the last item in a list, the constant END_OF_LIST may be passed.   6� = M  �  x    List                             7 = �    x    Item Destination                 7� =i     x    Position Of Item To Remove             0    FRONT_OF_LIST    G    This function inserts copies of the specified items into the list.  
     U    Returns non-zero if the items were inserted.

Returns zero (0) if an error occurs.
     $    The list to insert the items into.     %    The pointer to the items to insert.    {    The position at which to insert the items.  The position may be a number from 1 to the number of items in the list.  

To insert starting at the front of the list, the constant FRONT_OF_LIST may be passed.

To insert starting at the end of the list, the constant END_OF_LIST may be passed.

The items at or above the specified position are moved up to accomodate the new items.     ,    Specifies the number of items to insert.     : ���    x    Result                           :] = M  �  x    List                             :� = � �  x    Pointer To Items                 :� =i     x    Position To Insert At            <9 � c         Number Of Items To Insert          	                    END_OF_LIST    ���                 ~    This function copies the contents of the specified list items to the specified destination.  The items remain in the list.       !    The list to get the items from.     �    A pointer to where ListGetItem() should copy the contents of the specified list items.  The contents Number_Of_Item_To_Get items are returned, starting with the item specified by Position_Of_First_Item_To_Get.        The position of the first item to be retrieved.  The position may be a number from 1 to the number of items in the list.  

To start from the first item in the list, the constant FRONT_OF_LIST may be passed.

To get the last item in the list, the constant END_OF_LIST may be passed.     .    Specifies the number of items to retrieve.     >6 = M  �  x    List                             >_ = �    x    Items Destination                ?: =i     x    Position Of First Item To Get    @^ � c         Number Of Items To Get                     FRONT_OF_LIST    ���                 �    This function copies the contents of the data items pointed to by Pointer_To_Item over the contents of the items in the list starting at the specified position.      2    The list whose specified items will be replaced.     W    The pointer to the array of items which will replace the specified items in the list.    !    The position of the items to replace.  The position may be a number from 1 to the number of items in the list.  

To replace items starting at the first item in a list, the constant FRONT_OF_LIST may be passed.

To replace the last item in a list, the constant END_OF_LIST may be passed.     9    Specifies the number of items to replace in the list.     BC = M  �  x    List                             B} = � �  x    Pointer To Items                 B� =i     x    Position Of Items To Replace     D � c         Number Of Items To Replace                 FRONT_OF_LIST    ���                 �    This function removes a specified number of consecutive items from a list.  If Item_Destination is not zero (0) then the contents of the items will be copied to Item_Destination after the items are removed.     1    The list whose specified items will be removed.     �    A pointer to where the contents of the specified items will be copied after they are deleted from the list.

Zero (0) may be passed if the items are to be deleted from the list without being copied to another location.    #    The position of the first item to remove.  The position may be a number from 1 to the number of items in the list.  

To remove items starting at the first item in a list, the constant FRONT_OF_LIST may be passed.

To remove the last item in a list, the constant END_OF_LIST may be passed.     :    Specifies the number of items to remove from the list.     F" = M  �  x    List                             F[ = �    x    Items Destination                G? =i     x    First Item To Remove             Hj � c         Number Of Items To Remove              0        ���                 '    This function makes a copy of a list.     L    Returns a copy of the list.

Zero (0) will be returned if an error occurs.         The list to make a copy of.   I�����  x    Copy Of List                     J' = �  �  x    List To Copy                       	               G    This function appends the contents of one list to the end on another.     �    Returns non-zero if the operation was succesful.

Returns zero (0) if there was not enough memory or if bad arguments were passed to the function.     7    The list to which the other list will be appended to.     g    Every item in this list will be added to the end of the other list.  This list will remain unchanged.   K���    x    Result                           K� = �  �  x    List                             K� =' �  x    List To Append                     	                    �    This function removes every item from a list.  

The memory used by the removed items is saved to be reused when new items are inserted in the list (see ListCompact()).     *    The list to clear all of the items from.   M� = �  �  x    List To Clear                          R    This function examines two lists and returns whether or not they  are identical.     h    Returns a non-zero value if the lists are identical.

Returns zero (0) if the lists are not identical.     E    This list will be compared with List2 to see if they are identical.     E    This list will be compared with List1 to see if they are identical.   N����    x    Result                           N� = �  �  x    List1                            OF =' �  x    List2                              	                   .    This function inserts a copy of an item into a list at the correct position needed to maintain a sorted order.

The list must already be in sorted order or the results are undefined.

The item pointed to by Pointer_To_Item should be the same size as the item size specified when the list was created.     �    Returns non-zero if the item was inserted.

Returns zero (0) if there is not enough memory or if bad arguments are passed to the function.
     #    The list to insert the item into.     $    The pointer to the item to insert.    �    A comparison function which will be used to compare the item to the items already in the list so that it may be inserted at the correct position.

The compare function should have the following prototype:

 int CVICALLBACK CompareFunction(void *item1, void *item2);
    
The compare function should return a negative number if item1 is less than item2, it should return 0 if item1 is equal to item2, and it should return a positive number is item1 is greater than item2.

If zero (0) is passed for the compare function, the C Library function memcmp() will be called to compare items.

This instrument driver provides several commonly useful comparison functions:
   ShortCompare()
   IntCompare()
   FloatCompare()
   DoubleCompare()
   CStringCompare()   Q|���    x    Result                           R = M  �  x    List                             R< = � �  x    Pointer To Item                  Rh =i �  x    Compare Function                   	                           This function returns a pointer to the specified item in the list.

The pointer is only valid until the next operation which modifies the list occurs.

This function is provided for convenience of being able to examine a list item without copying it out of the list.       �    Returns a pointer directly to the item stored in the list.

The pointer is only valid until the next operation which modifies the list occurs.     >    The list from which to retrieve a pointer to specified item.    .    The position of the item to retrieve a pointer to.  The position may be a number from 1 to the number of items in the list.  

To get a pointer to the first item in a list, the constant FRONT_OF_LIST may be passed.

To get a pointer to the last item in a list, the constant END_OF_LIST may be passed.   Wf���   x    Item Pointer                     W� = �  �  x    List                             XD ='     x    Item Position                      	                   �    This function returns a pointer to the memory buffer the list uses to store its items.  Since the items are stored contiguously in ascending order, the pointer can be type cast as a pointer to the item type stored in the list and then directly indexed as an array.

This pointer is only valid until the next modification is made to the list.

This function is helpful for use with functions which take arrays of the same item type that the list stores.  The following example stores an array of doubles in a list and uses ListGetDataPtr() to get a pointer to the list data for use by the YGraphPopup() function.


#include "toolbox.h"

   static ListType list;
   static double   value;
   static int      index;

   list = ListCreate(sizeof(double));

   for (index = 1; index <= 360; index++) {
       value = sin (3.1415926 * index / 180);
       ListInsertItem (list, &value, END_OF_LIST);
       }

   YGraphPopup ("Plot Of Data In List", ListGetDataPtr(list),
             ListNumItems(list), VAL_DOUBLE);
     A    Returns a pointer the memory where the list's items are stored.     3    The list whose list data address will be returned   ^+���   x    List Data Pointer                ^t = �  �  x    List                               	                g    This function takes a list and a callback function and calls the callback for each item in the list.
     ?    Returns the result of the last call to Function_To_Apply().

     M    Pass the list for which the specified function will be called on its items.     |    Specifies whether the items in the list will be visited from first to last (ascending) or from last to first (descending).    �    Pass the function that is to be called for each item in the list.

The function must have the following prototype:

int CVICALLBACK ListApplicationFunc(int index, void *ptrToItem, void *callbackData);


When the function is called, it will be passed the index of the list item it is being called for, a pointer to the contents of the list item, and the user-defined callback data that was passed into ListApplyToEach().

If the list application function returns zero or a positive integer, the function will continue to be called for the remaining items in the list.  However, if the list application function returns a negative number, then ListApplyToEach() will stop and return this number as its result. 
     \    A user-defined value that will be passed on to Function_To_Apply each time it is called.

   _����    x    Result                           _� = W  �  x    List                             `4 = �          Order                            `� =_ �  x    Callback Function                c� � W    x    Callback Data                      	                Ascending 1 Descending 0        0    O    This function searches a list for an item which matches the specified item.       �    Returns the index of the first item in the list at or after the Starting Position whose contents match the specified item.

Zero (0) is returned if no matching item is found in the list.     5    The list in which to search for the specified item.     P    A pointer to the item for which the list will be searched for a matching item.     �    The position of the item in the list to start the search from.  The position may be a number from 1 to the number of items in the list.  

To start searching from the first item in a list, the constant FRONT_OF_LIST may be passed.

    �    A comparison function which will be used to compare the specified item to the items in the list.

The compare function should have the following prototype:

 int CVICALLBACK CompareFunction(void *item1, void *item2);
    
The compare function should return a negative number if item1 is less than item2, it should return 0 if item1 is equal to item2, and it should return a positive number is item1 is greater than item2.

If zero (0) is passed for the compare function, the C Library function memcmp() will be called to compare items.

This instrument driver provides several commonly useful comparison functions:
   ShortCompare()
   IntCompare()
   FloatCompare()
   DoubleCompare()
   CStringCompare()   e����    x    Found Position                   fD = M  �  x    List To Search                   f� = � �  x    Pointer To Item                  f� =i     x    Starting Position                g� � M �  x    Compare Function                   	                    FRONT_OF_LIST        �    This function searches a list for an item using the binary search algorithm.  The time taken by the search is proportional to log base 2 of the number of items in the list.

The list must be in sorted order.     �    Returns the index of the item in the list whose contents match the specified item.

Zero (0) is returned if no matching item is found in the list.     }    The list in which to search for the specified item.

The binary search algorithm requires that the list be in sorted order.     P    A pointer to the item for which the list will be searched for a matching item.    �    A comparison function which will be used to compare the specified item to the items in the list.

The compare function should have the following prototype:

 int CVICALLBACK CompareFunction(void *item1, void *item2);
    
The compare function should return a negative number if item1 is less than item2, it should return 0 if item1 is equal to item2, and it should return a positive number is item1 is greater than item2.

If zero (0) is passed for the compare function, the C Library function memcmp() will be called to compare items.

This instrument driver provides several commonly useful comparison functions:
   ShortCompare()
   IntCompare()
   FloatCompare()
   DoubleCompare()
   CStringCompare()   l����    x    Found Position                   m= = M  �  x    List To Search                   m� = � �  x    Pointer To Item                  n =i �  x    Compare Function                   	                        �    This function sorts the list in ascending order using the C Library qsort() function.  

The qsort() function is a fast (order n * log n) sort, but it is not stable (See the InsertionSort function panel).     *    The list to be sorted in ascending order    �    A comparison function which will be used to compare the items in the list.

The compare function should have the following prototype:

 int CVICALLBACK CompareFunction(void *item1, void *item2);
    
The compare function should return a negative number if item1 is less than item2, it should return 0 if item1 is equal to item2, and it should return a positive number is item1 is greater than item2.

If zero (0) is passed for the compare function, the C Library function memcmp() will be called to compare items.

This instrument driver provides several commonly useful comparison functions:
   ShortCompare()
   IntCompare()
   FloatCompare()
   DoubleCompare()
   CStringCompare()   r� = �  �  x    List To Sort                     r� =' �  x    Compare Function                              This function sorts the list using the HeapSort() function.

ListHeapSort(), like ListQuickSort(), is a n*log n algorithm.  However for almost all inputs, ListQuickSort() will run 2-3 times faster, making it a better choice.

See the help for the HeapSort() function panel.
     *    The list to be sorted in ascending order    �    A comparison function which will be used to compare the items in the list.

The compare function should have the following prototype:

 int CVICALLBACK CompareFunction(void *item1, void *item2);
    
The compare function should return a negative number if item1 is less than item2, it should return 0 if item1 is equal to item2, and it should return a positive number is item1 is greater than item2.

If zero (0) is passed for the compare function, the C Library function memcmp() will be called to compare items.

This instrument driver provides several commonly useful
comparison functions:
   ShortCompare()
   IntCompare()
   FloatCompare()
   DoubleCompare()
   CStringCompare()
   w = �  �  x    List To Sort                     wN =' �  x    Compare Function                           �    This function sorts the list using the InsertionSort() function.

InsertionSort() is a slow (n squared) but stable sort (see the InsertionSort() function panel).     *    The list to be sorted in ascending order    �    A comparison function which will be used to compare the items in the list.

The compare function should have the following prototype:

 int CVICALLBACK CompareFunction(void *item1, void *item2);
    
The compare function should return a negative number if item1 is less than item2, it should return 0 if item1 is equal to item2, and it should return a positive number is item1 is greater than item2.

If zero (0) is passed for the compare function, the C Library function memcmp() will be called to compare items.

This instrument driver provides several commonly useful
comparison functions:
   ShortCompare()
   IntCompare()
   FloatCompare()
   DoubleCompare()
   CStringCompare()   {! = �  �  x    List To Sort                     {S =' �  x    Compare Function                           T    This function examines a list and returns whether it is sorted in ascending order.     i    Returns non-zero if the list is sorted in ascending order.

Returns zero (0) if the list is not sorted.     (    The list to be checked if it is sorted    �    A comparison function which will be used to compare the items in the list.

The compare function should have the following prototype:

 int CVICALLBACK CompareFunction(void *item1, void *item2);
    
The compare function should return a negative number if item1 is less than item2, it should return 0 if item1 is equal to item2, and it should return a positive number is item1 is greater than item2.

If zero (0) is passed for the compare function, the C Library function memcmp() will be called to compare items.

This instrument driver provides several commonly useful comparison functions:
   ShortCompare()
   IntCompare()
   FloatCompare()
   DoubleCompare()
   CStringCompare()   ~����    x    Is Sorted                        G = �  �  x    List                             w =' �  x    Compare Function                   	                    4    This function removes duplicate items from a list.     *    The list to remove duplicate items from.    �    A comparison function which will be used to compare the items in the list.

The compare function should have the following prototype:

 int CVICALLBACK CompareFunction(void *item1, void *item2);
    
The compare function should return a negative number if item1 is less than item2, it should return 0 if item1 is equal to item2, and it should return a positive number is item1 is greater than item2.

If zero (0) is passed for the compare function, the C Library function memcmp() will be called to compare items.

This instrument driver provides several commonly useful comparison functions:
   ShortCompare()
   IntCompare()
   FloatCompare()
   DoubleCompare()
   CStringCompare()   � = �  �  x    List                             �M =' �  x    Compare Function                           �    This function is used for writing out lists of pointers to strings.

To output a list of pointers to strings, call OutputList() and pass this function as the Item_Output_Function parameter.     W    Returns the number of characters written, or a negative number if an error occurred.
     �    This parameter specifies which FILE* stream to output the item to.  If stdout is specified, then the item is sent to the standard output device which is usually the Standard Input/Output window.     �    This parameter is interpreted as a pointer to a string pointer. (type (char **)).  This string is written to the specified stream.   �;���    x    Number Of Character Written     ���� � d��                                          �� = �  �  x    Stream                           �f ='    x    Pointer To Item                    	            iThis function is intended to be used in the Item_Output_Function parameter
of the OutputList() function.    stdout        �    This function is used for writing out lists of short integers.

To output a list of short integers, call OutputList() and pass this function as the Item_Output_Function parameter.     W    Returns the number of characters written, or a negative number if an error occurred.
     �    This parameter specifies which FILE* stream to output the item to.  If stdout is specified, then the item is sent to the standard output device which is usually the Standard Input/Output window.     �    This parameter is interpreted as a pointer to a short integer. (type (short *)).  This short is written to the specified stream.   �	���    x    Number Of Character Written      �h = �  �  x    Stream                           �4 ='    x    Pointer To Item                 ���� � d��                                            	            stdout        iThis function is intended to be used in the Item_Output_Function parameter
of the OutputList() function.    �    This function is used for writing out lists of integers.

To output a list of integers, call OutputList() and pass this function as the Item_Output_Function parameter.     W    Returns the number of characters written, or a negative number if an error occurred.
     �    This parameter specifies which FILE* stream to output the item to.  If stdout is specified, then the item is sent to the standard output device which is usually the Standard Input/Output window.     }    This parameter is interpreted as a pointer to an integer. (type (int *)).  This integer is written to the specified stream.   �����    x    Number Of Character Written      �( = �  �  x    Stream                           �� ='    x    Pointer To Item                 ���� � d��                                            	            stdout        iThis function is intended to be used in the Item_Output_Function parameter
of the OutputList() function.    �    This function is used for writing out lists of doubles.

To output a list of doubles, call OutputList() and pass this function as the Item_Output_Function parameter.     W    Returns the number of characters written, or a negative number if an error occurred.
     �    This parameter specifies which FILE* stream to output the item to.  If stdout is specified, then the item is sent to the standard output device which is usually the Standard Input/Output window.     }    This parameter is interpreted as a pointer to a double. (type (double *)).  This double is written to the specified stream.   �����    x    Number Of Character Written      �� = �  �  x    Stream                           �� ='    x    Pointer To Item                 ���� � d��                                            	            stdout        iThis function is intended to be used in the Item_Output_Function parameter
of the OutputList() function.    �    This function is useful for writing the contents of a list to the standard I/O window, or to a file.  The list and item prefix and suffix parameters control the format of the output.     D    Returns the number of characters output, or a negative error code.     C    The list whose contents are to be output to the specified stream.     �    This parameter specifies which FILE* stream to output the item to.  If stdout is specified, then the contents of the list are sent to the standard output device which is usually the Standard Input/Output window.     �    Specifies a string that will be output before the list is output. 

This parameter is useful for preceding the contents of a list with a title or heading.

Pass 0 to specify that no list prefix should be output.
    U    Specifies a string that will be output after the contents of the list are output. 

This parameter is useful for following a list with summary information and to specify whether the last list item is followed by a newline or not.

If the string contains a "%d", it will be replaced with the index number of the last item that was output.

    C    Specifies a string that will be output before the contents of each list item is output. If the string contains a "%d", it will be replaced with the index number of the item being output.

This parameter is useful for preceding list items with their index numbers.

Pass 0 to specify that no item prefix should be output.
    �    Specifies a string that will be output after the contents of each list item is output. If the string contains a "%d", it will be replaced with the index number of the item being output.

The item suffix will not be output after the last item in a list.

This parameter is useful for separating list items with commas, spaces and/or newlines.

Pass 0 to specify that no item suffix should be output.
    �    This parameter specifies the function that OutputList() will call to output each list item.  The function must have the following prototype.

int CVICALLBACK ItemOutputFunction(FILE *stream, void *ptrToItem);

The function should return how many characters were written or return a negative value if an error occurs.

This parameter can either be a user defined function written to output a user defined item type, or it can be one of the predefined item output functions:
   OutputStringItem();    /* for lists of pointers to strings */
   OutputShortItem();     /* for lists of short integers */
   OutputIntegerItem();   /* for lists of integers */
   OutputDoubleItem();    /* for lists doubles */
   �L���    x    Num Characters Output            �� >    �  �    List                             �� > � �  �    Stream                           �� >x �  �    List Prefix                      �� �   �  �    List Suffix                      �� � � �  �    Item Prefix                      �F �x �  �    Item Suffix                      �� �   �  �    Item Output Function               	                stdout    "List Contents:\n"    "\n"    "Item #%d = "    ",\n"       �    This function configures how much new space a list will allocate for new items when it becomes full.

The space reserved for the list is increased by whichever is greatest: the number of items calculated from the specified percentage, or the value of Min Num Items Per Expansion.

The default allocation policy works well in almost any situation, therefore it is unlikely that you will want to call this function.     ,    The list to set the allocation policy for.     t    Pass the minimum number of items the list should allocate space for when the list if full and needs more space.           Pass the percentage of the number of its items the list should allocate space for when the list if full and needs more space.

For example if the percentage is set to 10 for a list with 200 items, then space for 20 new items will be allocated when the list needs more memory.   �L = -  �  x    List                             �� = �         Min Num Items Per Expansion      �� =�         Percent Increase Per Expansion         ���                  ���       
              This function re-allocates a list so that it only uses just enough memory to hold its current items.

Doing this will force a re-allocation the next time an item is added to the list, hurting performance.

It is unlikely that you will want to call this function.     ,    The list whose unused space is to be freed   �� = �  �  x    List To Compact                       *    This function reserves space in a list for the specified number of items.  Doing this insures that the specified number of items can be inserted into the list without checking for failure due to insufficient memory or without the possibility of a performance hit from a memory allocation.

The number of items in the list (as returned by ListNumItems()) is not changed.  

This function is useful for simplifying the error handling in certain complex situations and for ensuring predictable list insertion performance during time critical operations.

     v    Returns non-zero if the space for the specified number of items was allocated.

Returns zero (0) if an error occurs.     $    The list to pre-allocate space in.     �    The number of items to reserve space for in the list.

If ListPreAllocate() succeeds, then this is the number of items that can be inserted into the list without the possibility of running out of memory.   �����    x    Result                           � = �  �  x    List                             �C >'     x    Num Of Items To PreAllocate        	                    0    This function returns the item size of a list.     �    Returns the size in bytes of items that the list can store.

This value is the same value that was passed to ListCreate() when the list was created.     +    The list whose item size will be returned   ����    x    Item Size                        �� = �  �  x    List                               	               �    This function returns a handle to a newly allocated block of memory of the requested size.  The contents of the memory block will be all zeros.

Call DisposeHandle() to free the memory block.

The handle is a pointer to a second pointer.  It is this second
pointer which points to the allocated memory block.  This method allows the memory block to be resized (by calling SetHandleSize()) without changing the value of the handle.

     q    The handle to the memory block.
Zero (0) will be returned if there is not enough memory to allocate the block.
     *    The size of the memory block to allocate   �����  x    Mem Block Handle                 �� = �     x    Number Of Bytes                    	               E    This function disposes of a memory handle allocated by NewHandle().     �    The handle to the memory block to be disposed.
The handle must be a handle previously returned by
NewHandle() or the behavior is undefined.

Zero (or NULL) may passed safely, and the function will return without doing anything.
   �z = �  �  x    Mem Block Handle                       q    This function returns the size of the memory block referred to by a handle previously allocated by NewHandle().     J    Returns the size in bytes of the memory block referred to by the handle.     6    The handle whose memory block size will be returned.   ����   x    Size In Bytes                    �l = �  �  x    Handle                             	               �    This function changes the size of the memory block refered to by a handle.  If the new block is larger than the current block then all of the current data in the block is preserved and the new portion of the block is filled with zeros.  If the new block is smaller than the current block then as much of the data in the current block that can fit in the new block is preserved and the rest of the data is discarded.     q    Returns 1 (TRUE) if the size was set.  Returns 0 (FALSE) if the there was not enough memory to change the size.     5    The handle whose memory block size will be changed.     I    The new size (in bytes) for the memory block referred to by the handle.   �����    x    Result                           �F = �  �  x    Handle                           �� ='    x    New Size                           	                       This function places a control's label to the left of the control, centering it vertically with respect to the control.


Since many controls are created with their labels on top, this function can be useful when positioning controls for panels that are created programmatically.    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.     �    The defined constant (located in the UIR header file) which was assigned to the control in the User Interface Editor, or the ID returned by the NewCtrl() or DuplicateCtrl() function.
     c    Specifies the gap in pixels between the right edge of the label and the left edge of the control.   �����    x    Status                           �* = M      x    Panel                            �� = �     x    Ctrl                             �� =i     x    Label To Ctrl Gap                  	                    6    k    This function makes it easier to assign the usual shortcut keys to the OK and Cancel buttons of a dialog.    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.     �    Pass the control ID of the control whose shortcut key should be the Enter key. This is commonly the "OK" button on a dialog.

Pass zero to have the function ignore this parameter.     �    Pass the control ID of the control whose shortcut key should be the Escape key. This is commonly the "Cancel" button on a dialog.

Pass zero to have the function ignore this parameter.     �    Pass the control ID of the control which should be activated when the user tries to close the panel using the close box or the system menu. This is commonly the "Cancel" button on a dialog.

Pass zero to have the function ignore this parameter.   �e���    x    Status                           �� = M      x    Panel                            �� = �     x    Enter Control                    �S =i     x    Escape Control                   � � M     x    Close Ctrl                         	                            �    This function moves one control in front of another in Z-Plane order.  This means that if the controls overlap, then the control in front will obscure the other control.    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.        Pass the ID of the control which the other control will be moved in front of.

The ID is the defined constant (located in the UIR header file) which was assigned to the control in the User Interface Editor, or the ID returned by the NewCtrl() or DuplicateCtrl() function.
        Pass the ID of the control to move in front of the other control.

The ID is the defined constant (located in the UIR header file) which was assigned to the control in the User Interface Editor, or the ID returned by the NewCtrl() or DuplicateCtrl() function.
   �����    x    Status                           �m = M      x    Panel                            � = �     x    Ctrl                             �6 =i     x    Control To Move                    	                        �    This function moves one control behind of another in Z-Plane order.  This means that if the controls overlap, then the control in back will be obscured by the other control.    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.        Pass the ID of the control which the other control will be moved behind.

The ID is the defined constant (located in the UIR header file) which was assigned to the control in the User Interface Editor, or the ID returned by the NewCtrl() or DuplicateCtrl() function.
        Pass the ID of the control to move behind the other control.

The ID is the defined constant (located in the UIR header file) which was assigned to the control in the User Interface Editor, or the ID returned by the NewCtrl() or DuplicateCtrl() function.
   �����    x    Status                           �i = M      x    Panel                            � = �     x    Ctrl                             �- =i     x    Control To Move                    	                        A    This function moves one control relative to another on a panel.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.    @    Pass the ID of the control which the other control will be moved relative to. This control is called the reference control.

The ID is the defined constant (located in the UIR header file) which was assigned to the control in the User Interface Editor, or the ID returned by the NewCtrl() or DuplicateCtrl() function.
     |    Turn this option on if you want this function to include the label of the reference control when moving the other control.        Pass the ID of the control to move relative to the other control.

The ID is the defined constant (located in the UIR header file) which was assigned to the control in the User Interface Editor, or the ID returned by the NewCtrl() or DuplicateCtrl() function.
     f    Turn this option on if you also want to move the label of the control as well as the control itself.     8    The final distance in pixels between the two controls.     D    The second control is moved to this side of the reference control.     S    Specify the justification of the moved control relative to the reference control.    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.   �k $       �    Panel                            � $ �     �    Ctrl                             �b $�          Consider Label                   �� t �     �    Ctrl To Move                     �� t�          Consider Ctrl To Move Label      �b �      �    Gap                              Ң � � � �    Which Side                       �� �q � �    Justification                    �Ir��    �    Status                                    On 1 Off 0       On 1 Off 0                	  DTop Left kPositionCtrlTopLeft Top Center kPositionCtrlTopCenter Top Right kPositionCtrlTopRight Center Left kPositionCtrlCenterLeft Center Center kPositionCtrlCenterCenter Center Right kPositionCtrlCenterRight Bottom Left kPositionCtrlBottomLeft Bottom Center kPositionCtrlBottomCenter Bottom Right kPositionCtrlBottomRight               �Top or Left Justification kPositionCtrlTopOrLeftJustification Center Justification kPositionCtrlCenterJustification Bottom or Right Justication kPositionCtrlBottomOrRightJustication    	            �    This function centers a control in a specified area.  The control is centered with respect to its bounding rectangle which includes its label (if any) and any additional parts it might have.    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.        Pass the ID of the control to center in the specified rectangle.

The ID is the defined constant (located in the UIR header file) which was assigned to the control in the User Interface Editor, or the ID returned by the NewCtrl() or DuplicateCtrl() function.
     I    Specifies the top of the rectangular area to center the control within.     J    Specifies the left of the rectangular area to center the control within.     L    Specifies the height of the rectangular area to center the control within.         width   �����    x    Status                           �B = M      x    Panel                            �� = �     x    Ctrl                             �� =i     x    Top                              �O � M     x    Left                             ݡ � �     x    Height                           �� �i     x    Width                              	                                    �    This function resizes the specified ring control so that it is large enough to completely display the text of any of its entries.

The ring control must be a plain ring control or a recessed menu ring (styles CTRL_RING or CTRL_RECESSED_MENU_RING).
    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.     �    Pass the ID of the  ring control to resize.

The ID is the defined constant (located in the UIR header file) which was assigned to the control in the User Interface Editor, or the ID returned by the NewCtrl() or DuplicateCtrl() function.
   �����    x    Status                           � = �      x    Panel                            �� ='     x    Ring Ctrl                          	                   D    This function conforms the bitmap of a picture control, picture button, or picture ring to a specified color scheme.

This function is useful for making picture buttons conform to the system colors or for simply replacing one color with another in a control's bitmap.

Usually, when designing bitmaps for use in picture buttons, you choose a color which represents the 3d object color, and another color which represents the text or shadow color.  These are often VAL_PANEL_GRAY and VAL_BLACK, respectively.  But, if you are using the ATTR_CONFORM_TO_SYSTEM attribute on your panels and a user runs your program on a computer where the system color settings are different, your picture buttons will look wrong because the picture button background color will not match the panel background color.

To solve the problem, you simply call this function and pass it the panel and control you wish to convert, as well as the colors in the bitmap you wish to conform to a new 3d object and text color.  For example, the following code will conform all bitmaps in all the controls on the panel to match the current panel colors.  This assumes that 3d object pixels in the bitmap are VAL_PANEL_GRAY and text/shadow pixels are VAL_BLACK.

{
    ConformCtrlBitmapToPanel (panel, -1, -1, VAL_PANEL_GRAY,
                              -1, VAL_BLACK, -1);
}

    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.    �    The defined constant (located in the UIR header file) which was assigned to the control in the User Interface Editor, or the ID returned by the NewCtrl() or DuplicateCtrl() function.

The control should be a picture control, picture command button, picture toggle button, or picture ring.

If you pass -1 for this parameter, all appropriate controls on the panel will be conformed to the specified colors.

        If the control is a picture ring, specify the zero-based index of the bitmap to conform.  If the control is not a picture ring, this parameter is ignored.

If you pass -1 for the index, all bitmaps in the picture ring will be conformed to the desired colors.
    �    Specify the color of the pixels in the bitmap that you wish to conform to the new 3d object color.

Typically, bitmaps will use this color anywhere the 3d object color is required, such as for the background of a toolbar button image.

NOTE: Colors which are "close" to the specified color are also changed to the new color.  The definition of "close" is as follows for colors c1 and c2:

    diff = abs(red val of c1 - red val of c2) +
           abs(green val of c1 - green val of c2) +
           abs(blue val of c1 - blue val of c2);

If diff is less than MAX_COLOR_DIFF (defined in toolbox.c, the default value is 15), then colors c1 and c2 are considered "close".
     �    Specify the new color for any pixels in the bitmap whose color is the bitmap 3d object color.

If you pass -1 for this parameter, the current color of the specified panel is used as the new 3d object color.
    _    Specify the color of the pixels in the bitmap that you wish to conform to the new text color.

Typically, this color is used for text or dark shadow areas in the bitmap.

NOTE: Colors which are "close" to the specified color are also changed to the new color.  The definition of "close" is as follows for colors c1 and c2:

    diff = abs(red val of c1 - red val of c2) +
           abs(green val of c1 - green val of c2) +
           abs(blue val of c1 - blue val of c2);

If diff is less than MAX_COLOR_DIFF (defined in toolbox.c, the default value is 15), then colors c1 and c2 are considered "close".
     �    Specify the new color for any pixels in the bitmap which are in the bitmap text color.

If you pass -1 for this parameter, the current titlebar text color of the specified panel is used as the new text color.
   �����    x    Status                           �G = M      x    Panel                            �� = �     x    Ctrl                             � =i     x    Item Index                       � � M     x    Bitmap 3d Object Color           �L � �     x    New 3d Object Color              �% � M     x    Bitmap Text Color                � � �     x    New Text Color                     	                    0    VAL_PANEL_GRAY    -1    
VAL_BLACK    -1    �    This function returns a string that contains a copy of the value of a control.

The control must contain a string value.

The function dynamically allocates the string.  Call the free function to deallocate the string when you are done with it.     �    The specifier for a particular panel that is currently in memory

You obtain the handle by calling the LoadPanel, NewPanel, or DuplicatePanel function.     �    The defined constant (located in the UIR header file) that you assign to the control in the User Interface Editor, or the ID that you obtain by calling the NewCtrl or DuplicateCtrl function.     �    The dynamically allocated string that the function returns.  Call the free function to deallocate the string when you are done with it.     3    Returns a negative error code if an error occurs.   �Q 8 Y      `    Panel Handle                     �� 8 �     `    Control ID                       �� 8q    `    Allocated String Value           �K���    `    Status                                     	            	            a    Returns the number of bytes (excluding the ASCII NUL byte) in the text value for the control.

     �    The specifier for a particular panel that is currently in memory

You obtain the handle by calling the LoadPanel, NewPanel, or DuplicatePanel function.     �    The defined constant (located in the UIR header file) that you assign to the control in the User Interface Editor, or the ID that you obtain by calling the NewCtrl or DuplicateCtrl function.     3    Returns a negative error code if an error occurs.     _    Returns the number of bytes (excluding the ASCII NUL byte) in the text value for the control.   �� 6 N      `    Panel Handle                     �� 6 �     `    Control Id                       �L���    `    Status                           �� 4v     `    Number of Characters                       	            	            �    This function draws a recessed frame around the specified control.

Typically, you call this function only on string controls or text box controls.     3    Returns a negative error code if an error occurs.     �    The specifier for a particular panel that is currently in memory.

You obtain the handle by calling the LoadPanel, NewPanel, or DuplicatePanel function.     �    The defined constant (located in the UIR header file) that you assign to the control in the User Interface Editor, or the ID that you obtain by calling the NewCtrl or DuplicateCtrl function.
   � ����    `    Status                           �� ; �      `    Panel Handle                      \ ;6     `    Control ID                         	                    ~    This function calls RecessCtrlFrame on every string and text box control in the specified panel and all of its child panels.     3    Returns a negative error code if an error occurs.     �    The specifier for a particular panel that is currently in memory.

You obtain the handle by calling the LoadPanel, NewPanel, or DuplicatePanel function.   ^ ����    `    Status                           � C �      `    Panel Handle                       	               �    This function return a string containing the symbolic name for a User Interface Library event.  This function can be used as a debugging aid by printing the names of events as they are received by a callback function.

Example:


int CVICALLBACK CallbackToDebug(int panel, int control, 
                                int event, void *callbackData, 
                                int eventData1, int eventData2)
{
    printf("Event received is %s\n", UILEventString(event));
}    V    Returns a pointer to a string of characters containing the symbolic name of the event.  The string should not be freed or altered.  If the event is not a predefined User Interface Library Event, the string "Event #" is returned, where # is the event number.  In this case, the string is valid only until the next call to UILEventString().       -    Pass a User Interface Library event number.   ����   x    Event Name String                � = �      x    Event                              	                {    This function replaces all occurrences of the period with the localized decimal symbol.  The string is modified in place.     "    Pass the string to be localized.   . = �     x    Number String                          {    This function replaces all occurrences of the localized decimal symbol with the period.  The string is modified in place.     $    Pass the string to be delocalized.   	 = �     x    Number String                          F    This function sets tool tip attributes for the control you specify.      �    The specifier for a particular panel that is currently in memory

You obtain the handle by calling the LoadPanel, NewPanel, or DuplicatePanel function.     �    The defined constant (located in the UIR header file) that you assign to the control in the User Interface Editor, or the ID that you obtain by calling the NewCtrl or DuplicateCtrl function.     3    Returns a negative error code if an error occurs.    G    Select the tool tip attribute to set.

Currently there is only one tool tip attribute:

 TOOLTIP_ATTR_TEXT

  Set the value of the TOOLTIP_ATTR_TEXT attribute to the string  
  you want the control to display in its tool tip.

  Set the value of the TOOLTIP_ATTR_TEXT attribute to "" to
  remove the tool tip for the control.     5    Pass the value for the specified tool tip attribute   	� 6 F      `    Panel Handle                     
h 6 �     `    Control Id                       0���    `    Status                           k 6T    �    Tool Tip Attribute               � � F    `    Attribute Value                            	                       %Tool Tip Text CTRL_TOOLTIP_ATTR_TEXT       �    This function sets the value of the control attribute in each of the specified controls.  

This function can be very useful for sizing and positioning controls when programatically creating dialog boxes.

The following example would set three buttons to be the same width:
{
    int  maxWidth;

    AttributeMaxAndMin(thePanel, ATTR_WIDTH, 3,   
       &maxWidth, 0, 0, 0, button1, button2, button3);

    SetAttributeForCtrls(thePanel, ATTR_WIDTH,    
       maxWidth, 3, button1, button2, button3);
}    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.     �    Specify which attribute to set in the designated controls.  The data type of the attribute must be integer (example: ATTR_TOP, ATTR_WIDTH).     C    The value to set the attribute to for all the specified controls.    3    Specifies how many controls are in the control list.  

Pass zero for this parameter if you want the function to continue through the control list until it finds a control ID of zero.  This feature makes it unnecessary to count the number of controls passed if the last control ID passed is always a zero.    2    This parameter takes a variable number of comma separated control ID arguments. 


If the value of Number_Of_Ctrls is zero, then the last control ID passed should be a zero in order to mark the end of the list of controls.  Otherwise,  Number_Of_Ctrls should be set to the number of control IDs passed.

   M���    x    Status                           � = M      x    Panel                            ~ = �     x    Attribute                         =i     x    Value                            ^ � M     x    Number Of Ctrls                  � � �    x    Control List                       	                        1       �    This function examines the value of the control attribute in each of the specified controls.  It returns the minimum and maximum values of the attribute and the ID's of the controls it found those values on.


This function can be very useful for getting information needed to size and position controls when programatically creating dialog boxes.

The following example would set three buttons to be the same width:
{
    int  maxWidth;

    AttributeMaxAndMin(thePanel, ATTR_WIDTH, 3,   
       &maxWidth, 0, 0, 0, button1, button2, button3);

    SetAttributeForCtrls(thePanel, ATTR_WIDTH,    
       maxWidth, 3, button1, button2, button3);
}
    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.     �    Specify which attribute to examine in each control.  The data type of the attribute must be integer (example: ATTR_TOP, ATTR_LABEL_WIDTH).    3    Specifies how many controls are in the control list.  

Pass zero for this parameter if you want the function to continue through the control list until it finds a control ID of zero.  This feature makes it unnecessary to count the number of controls passed if the last control ID passed is always a zero.     y    Returns the maximum value of the attribute in the list of controls.  

You may pass zero if you do not need this value.     �    Returns the control ID of the control with the maximum value of the specified attribute.

You may pass zero if you do not need this value.     y    Returns the minimum value of the attribute in the list of controls.  

You may pass zero if you do not need this value.     �    Returns the control ID of the control with the minimum value of the specified attribute.

You may pass zero if you do not need this value.    2    This parameter takes a variable number of comma separated control ID arguments. 


If the value of Number_Of_Ctrls is zero, then the last control ID passed should be a zero in order to mark the end of the list of controls.  Otherwise,  Number_Of_Ctrls should be set to the number of control IDs passed.

   ����    x    Status                           E = M      x    Panel                            � = �     x    Attribute                        � =i     x    Number Of Ctrls                  � � M     x    Maximum                          D � �     x    Ctrl With Max                    � �h     x    Minimum                          Y � M     x    Ctrl With Min                    � � �    x    Control List                       	                    1    	            	            	            	                   This function examines the value of a control attribute for every control specified.  It then sets the value of that attribute in each control to either the largest or the smallest value it found, depending on the setting of the Which_Extreme parameter.


This function can be used to size and position controls when creating dialogs programmatically.  For example, the following code, which can be run in the interactive window, creates three buttons and then calls SetCtrlsToAttributeExtreme() to make all three buttons the same width.


{
   int panel, okBtn, defaultBtn, cancelBtn;

   panel = NewPanel(0, "Test Panel", 40, 40, 300, 300);

   defaultBtn = NewCtrl (panel, CTRL_SQUARE_COMMAND_BUTTON,
                        "__Default", 250, 20);
   okBtn = NewCtrl (panel, CTRL_SQUARE_COMMAND_BUTTON, "__Ok",
                   250, 120);
   cancelBtn = NewCtrl (panel, CTRL_SQUARE_COMMAND_BUTTON,
                       "__Cancel", 250, 220);


   DisplayPanel(panel);
   GetUserEvent(1,0,0); /* wait for any button to be pressed */

    /* make all buttons the same width */
   SetCtrlsToAttributeExtreme(panel, ATTR_WIDTH, VAL_TB_MAX, 3,
                            defaultBtn, okBtn, cancelBtn);

   GetUserEvent(1,0,0); /* wait for any button to be pressed */

   DiscardPanel(panel);
}
    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.     �    Specify which attribute to set in the designated controls.  The data type of the attribute must be integer (example: ATTR_TOP, ATTR_WIDTH).     �    This parameter specifies whether to set the value of the attribute in every designated control to the largest or to the smallest value of the attribute currently set for any of the designated controls.    H    Specifies how many control IDs are being passed to the function.  

Pass zero for this parameter if you want the function to continue through the control argument list until it finds a control ID of zero.  This feature makes it unnecessary to count the number of controls passed if the last control ID passed is always a zero.    2    This parameter takes a variable number of comma separated control ID arguments. 


If the value of Number_Of_Ctrls is zero, then the last control ID passed should be a zero in order to mark the end of the list of controls.  Otherwise,  Number_Of_Ctrls should be set to the number of control IDs passed.

   (l���    x    Status                           )� = M      x    Panel                            *� = �     x    Attribute                        +2 @x          Which Extreme                    , � M     x    Number Of Ctrls                  -U � �    x    Control List                       	                  &  Maximum VAL_TB_MAX Minimum VAL_TB_MIN    1        \    This function evenly distributes the specified controls either horizontally or vertically.    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.     T    This parameter controls the direction in which the controls will be distributed.      �    This parameter controls how the controls will be distributed.

Select "Fixed Gap Between Controls" to place a gap the size of the value passed to Gap_Or_Area_Size between each control.

Select "Distribute Over Area" to have your first control start at the value passed to Position and have the last control end on the value passed to Position plus the value passed to Gap_Or_Area_Size.  This has the effect of evenly distributing the controls in the area from Position to Position+Gap_Or_Area_Size.    B    This parameter specifies where to start distributing controls.

The position specifies the topmost position or the leftmost position of the distributed controls, depending on the value passed the the Direction parameter.

The coordinate of the bottom edge or right edge of the last control is returned in this parameter.        If the Spacing parameter is set to "Fixed Gap Between Controls" then this parameter specifies the number of pixels of gap to place between each control.

If Spacing parameter is set to "Distribute Over Area" then this parameter specifies the size of the area to distribute over.    H    Specifies how many control IDs are being passed to the function.  

Pass zero for this parameter if you want the function to continue through the control argument list until it finds a control ID of zero.  This feature makes it unnecessary to count the number of controls passed if the last control ID passed is always a zero.    2    This parameter takes a variable number of comma separated control ID arguments. 


If the value of Number_Of_Ctrls is zero, then the last control ID passed should be a zero in order to mark the end of the list of controls.  Otherwise,  Number_Of_Ctrls should be set to the number of control IDs passed.

   0w���    x    Status                           1� = M      x    Panel                            2� = �          Direction                        3 =i          Spacing                          5  � M     x    Position                         6J � �     x    Gap Or Area Size                 7j �i     x    Number Of Ctrls                  8� � M    x    Control List                       	              T  Vertically VAL_TB_VERTICAL_DISTRIBUTION Horizontally VAL_TB_HORIZONTAL_DISTRIBUTION  ] Fixed Gap Between Controls VAL_TB_FIXED_GAP_SPACING Distribute Over Area VAL_TB_AREA_SPACING    	                1       3    For each control passed, this function places the control's label to the left of the control, centering it vertically with respect to the control.

Since many controls are created with their labels on top, this function can be useful when positioning controls for panels that are created programmatically.    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.     c    Specifies the gap in pixels between the right edge of the label and the left edge of the control.    H    Specifies how many control IDs are being passed to the function.  

Pass zero for this parameter if you want the function to continue through the control argument list until it finds a control ID of zero.  This feature makes it unnecessary to count the number of controls passed if the last control ID passed is always a zero.    2    This parameter takes a variable number of comma separated control ID arguments. 


If the value of Number_Of_Ctrls is zero, then the last control ID passed should be a zero in order to mark the end of the list of controls.  Otherwise,  Number_Of_Ctrls should be set to the number of control IDs passed.

   =����    x    Status                           ?9 = M      x    Panel                            ?� = �     x    Label To Ctrl Gap                @S =i     x    Number Of Ctrls                  A� � M    x    Control List                       	                6    1        �    This function sets the value of the control attribute in each control in the list.

This function can be very useful for sizing and positioning controls when programatically creating dialog boxes.

    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.     Z    This parameter should be a list of control IDs (see the Lists... class of functions).


     �    Specify which attribute to set in the designated controls.  The data type of the attribute must be integer (example: ATTR_TOP, ATTR_WIDTH).     C    The value to set the attribute to for all the specified controls.   D����    x    Status                           FV = M      x    Panel                            G = � �  x    List Of Controls                 Gg =i     x    Attribute                        G� � M     x    Value                              	                           Z    This function examines the value of the control attribute for each control in the list.  It returns the minimum and maximum values of the attribute and the ID's of the controls it found those values on.


This function can be very useful for getting information needed to size and position controls when programatically creating dialog boxes.

    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.     Z    This parameter should be a list of control IDs (see the Lists... class of functions).


     �    Specify which attribute to examine in each control.  The data type of the attribute must be integer (example: ATTR_TOP, ATTR_LABEL_WIDTH).     y    Returns the maximum value of the attribute in the list of controls.  

You may pass zero if you do not need this value.     �    Returns the control ID of the control with the maximum value of the specified attribute.

You may pass zero if you do not need this value.     y    Returns the minimum value of the attribute in the list of controls.  

You may pass zero if you do not need this value.     �    Returns the control ID of the control with the minimum value of the specified attribute.

You may pass zero if you do not need this value.   J����    x    Status                           LP = M      x    Panel                            L� = � �  x    List Of Controls                 Ma =i     x    Attribute                        M� � M     x    Maximum                          Nv � �     x    Ctrl With Max                    O
 �i     x    Minimum                          O� � M     x    Ctrl With Min                      	                        	            	            	            	           e    This function examines the value of a control attribute for each control in the list.  It then sets the value of that attribute in each control to either the largest or the smallest value it found, depending on the setting of the Which_Extreme parameter.


This function can be used to size and position controls when creating dialogs programmatically. .
    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.     Z    This parameter should be a list of control IDs (see the Lists... class of functions).


     �    Specify which attribute to set in the designated controls.  The data type of the attribute must be integer (example: ATTR_TOP, ATTR_WIDTH).     �    This parameter specifies whether to set the value of the attribute in every control in the list to the largest or to the smallest value of the attribute currently set for any of the designated controls.   S|���    x    Status                           T� = M      x    Panel                            U� = � �  x    List Of Controls                 V =i     x    Attribute                        V� � i          Which Extreme                      	                      &  Maximum VAL_TB_MAX Minimum VAL_TB_MIN    X    This function evenly distributes a list of controls either horizontally or vertically.    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.     Z    This parameter should be a list of control IDs (see the Lists... class of functions).


     T    This parameter controls the direction in which the controls will be distributed.      �    This parameter controls how the controls will be distributed.

Select "Fixed Gap Between Controls" to place a gap the size of the value passed to Gap_Or_Area_Size between each control.

Select "Distribute Over Area" to have your first control start at the value passed to Position and have the last control end on the value passed to Position plus the value passed to Gap_Or_Area_Size.  This has the effect of evenly distributing the controls in the area from Position to Position+Gap_Or_Area_Size.    B    This parameter specifies where to start distributing controls.

The position specifies the topmost position or the leftmost position of the distributed controls, depending on the value passed the the Direction parameter.

The coordinate of the bottom edge or right edge of the last control is returned in this parameter.        If the Spacing parameter is set to "Fixed Gap Between Controls" then this parameter specifies the number of pixels of gap to place between each control.

If Spacing parameter is set to "Distribute Over Area" then this parameter specifies the size of the area to distribute over.   Y"���    x    Status                           Z� = M      x    Panel                            [S = � �  x    List Of Controls                 [� =|          Direction                        \ � *          Spacing                          ^ � �     x    Position                         _W �j     x    Gap Or Area Size                   	                  T  Vertically VAL_TB_VERTICAL_DISTRIBUTION Horizontally VAL_TB_HORIZONTAL_DISTRIBUTION  ] Fixed Gap Between Controls VAL_TB_FIXED_GAP_SPACING Distribute Over Area VAL_TB_AREA_SPACING    	               8    For each control in the list, this function places the control's label to the left of the control, centering it vertically with respect to the control.

Since many controls are created with their labels on top, this function can be useful when positioning controls for panels that are created programmatically.    z    Returns 0 if the function succeeded or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.     Z    This parameter should be a list of control IDs (see the Lists... class of functions).


     c    Specifies the gap in pixels between the right edge of the label and the left edge of the control.   d���    x    Status                           e� = M      x    Panel                            f6 = � �  x    List Of Controls                 f� =i     x    Label To Ctrl Gap                  	                    6   (    This function ensures that only one radio button in a group of radio buttons in a CVI user interface is set to the On state

Specify the active button and a list of buttons.  This list of buttons can also include the active button

The active button is set On and all other buttons are set Off.     �    The specifier for a particular panel that is currently in memory.

You obtain the handle by calling the LoadPanel, NewPanel, or DuplicatePanel function.     N    Pass the control ID of the radio button that you want to set to the On state     _    Specify the number of radio buttons that you pass in the List of all Radio Buttons parameter.     ^    Pass a comma separated list of control IDs for each radio button in the radio button group.      3    Returns a negative error code if an error occurs.   i  3 [      `    Panel Handle                     i� 3 �     `    Active Button                    j 3s     `    Num Buttons                      j � [    `    List of all Radio Buttons        j� �6��    `    Status                                             	            \    This function determines which button from a group of radio buttons is in the On state.        �    The specifier for a particular panel that is currently in memory

You obtain the handle by calling the LoadPanel, NewPanel, or DuplicatePanel function.     E    Returns the control ID of the radio button that is in the On state.     _    Specify the number of radio buttons that you pass in the List of all Radio Buttons parameter.     ]    Pass a comma separated list of control IDs for each radio button in the radio button group.     3    Returns a negative error code if an error occurs.   l� 5 ^      `    Panel Handle                     mJ 5 �     `    Active Button                    m� 5v     `    Num Buttons                      m� � ^    `    List of all Radio Buttons        nc �6��    `    Status                                 	                    	           �    This function displays a dialog which presents the user with a list of named colors and allows the user to change the values of these colors.

The dialog is similar in appearance and function to the Environment Color Preferences dialog in the CVI environment.



The following code can be run in the interactive window to demonstrate the ColorChangePopup():

/* Note: Since this example does not pass in a color change
         callback, the color selections will not be applied to
         anything.
*/

#include "toolbox.h"

static ColorChangeEntry entry;
static ListType         colorList = 0;
static int              result;

colorList = ListCreate(sizeof(ColorChangeEntry));

entry.name = "Cat Color";
entry.color = VAL_RED;
entry.defaultColor = VAL_BLUE;
ListInsertItem(colorList, &entry, END_OF_LIST);


entry.name = "Dog Color";
entry.color = VAL_GREEN;
entry.defaultColor = VAL_CYAN;
ListInsertItem(colorList, &entry, END_OF_LIST);

result = ColorChangePopup("Select Colors", colorList, 1, 0, 0);

ListDispose(colorList);

if (result > 0)
   MessagePopup("Color Change Result", "A Color Was Changed");
else
if (result == 0)
   MessagePopup("Color Change Result", "No Color Was Changed");
else
   MessagePopup("Color Change Result", "An Error Occurred");
     �    Returns 0 if no changes were made, returns > 0 if changes were made, or returns a negative User Interface Library error code if an error occurred.     1    Specifies the title of the color change dialog.    �    The color list should be a list of ColorChangeEntry structures.
Each ColorChangeEntry struct defines the name of a color, its default value (in RGB), and its current value (in RGB).  Each ColorChangeEntry struct in the list will be represented by an entry in the list box on the color change dialog.

The ColorChangeEntry struct has the following form (as defined in toolbox.h):

typedef struct
    {
    char   *name;        /* pointer to NULL terminated string */
    int    defaultColor; /* RGB value */
    int    color;        /* RGB value */
    } ColorChangeEntry;


Color List Hints:
-----------------
Use ListCreate(sizeof(ColorChangeEntry)) to create the list.

Use ListInsertItem() to insert ColorChangeEntry structures.
     �    Specifies whether the color change popup will have a default button.  If the color change popup has a default button and it is pressed, each color will revert to the default value specified by the color's entry in the Color_List.
    z    This parameter specifies a callback function that the color change dialog will call when a color is changed in the dialog.

This callback function should respond by applying the current values of the colors in the Color_List it is passed.  This allows the user to see the result of their color changes before exiting the dialog.

The callback function must have the following prototype:

void CVICALLBACK ShowColorChangeFunction(ListType color_List, 
                             void *color_Change_Callback_Data,
                             int color_List_Index);


The color_List parameter is the same list of ColorChangeEntry structures that was passed in to ColorChangePopup(), except that the color field of each structure reflects any changes the user has made while operating the dialog.

The color_Change_Callback_Data is the same value that was passed in to ColorChangePopup().

The color_List_Index parameter is the index in the color_List of the color that the user changed.  If the value of color_List_Index is -1, then the value of every color in the list should be applied (this occurs when the user selects cancel or default).

    u    A pointer to user-defined data that will be passed to the Color_Change_Callback function.  

This parameter can be useful to pass information to the callback function indicating which object(s) to apply the color list to without the need to resort to global variables.

If this parameter is not needed, you may pass zero (or any other  value if cast as a void pointer).

   t����    x    Result                           u` = 4  �  �    Title                            u� = � �  x    Color List                       x =�          Show Default Button              yo � f �  x    Color Change Callback            }� � �    x    Color Change Callback Data         	            "Select Colors"       On 1 Off 0        0    y    This function creates and displays a dialog which can be used to indicate the completion status of a lengthy operation.        This control returns a positive integer panel handle which can be used to refer to the progress dialog in subsequent calls to UpdateProgressDialog() or DiscardProgressDialog().


If an error occurred while creating the dialog, a negative User Interface Library error code is returned.     -    Specifies the title of the progress dialog.     �    This parameter specifies the label for the horizontal progress bar that appears on the progress dialog.

Pass "" if you do not want a label.     M    This parameter specifies whether the progress dialog is modal or non-modal.     Z    This control specifies how to mark the percentage values on the horizontal progress bar.    �    This parameter specifies the label for the dialog's cancel button.  The shortcut key for the cancel button is automatically set to be the ESC key.

Pass "" if you want the cancel button to be invisible.

Pass 0 if you do not want a cancel button.  If the dialog does not have a cancel button, then it will not detect if the user has pressed the ESC key.


The UpdateProgressDialog() function returns whether the user has requested to stop the operation by pressing the cancel button or the ESC key.
   �g���    x    Progress Dialog                  �� > &  �  �    Title                            �� > � �  �    Progress Indicator Label         �X =�          Dialog Is Modal                  �� � �   �      Indicator Marker Style           � � � �  �    Cancel Button Label                	            "Completion Status"    "Percent Complete"   Yes 1 No 0              ^No Markers VAL_NO_MARKERS No Inner Markers VAL_NO_INNER_MARKERS Full Markers VAL_FULL_MARKERS    "__Cancel"    �    This function should be called periodically to update the amount of progress indicated by the horizontal bar on a progress dialog.

    (    This control returns 1 if the user has requested to abort the operation by pressing the cancel button or the ESC key.

0 is returned if the user has not requested to abort the operation.


if an error occurred while updating the dialog, a negative User Interface Library error code is returned.     �    Pass the panel handle of the progress dialog to update.

This value must be a value returned by the CreateProgressDialog() function.     V    Pass the completion percentage (0 to 100) to display in the horizontal progress bar.    
    This parameter specifies whether or not to process system events while updating the dialog.


If you already process events while the operation is in progress, or if you want to lock out all user input and callbacks while the operation is progress, pass 0 (No).

If you are displaying the progress of an operation that does not allow events to be processed, but you would like events to be processed, pass 1 (Yes).

For more information, see the help for the ProcessSystemEvents() function in the User Interface Library.   �����    x    Cancel Requested                 �� = V      x    Progress Dialog                  �X = �         Percent Complete                 �� =�          Process System Events              	                ����                  Yes 1 No 0    �    Pass the panel handle of the progress dialog to discard.

This value must be a value returned by the CreateProgressDialog() function.   �� = �      x    Progress Dialog                       K    This function adds a new callback function to a control.  This function will be called first for every event the control receives.  If this new callback function returns zero (0), then the control's original callback function, if any, will be called with the same event.

This function is used to customize the behavior of controls.  Since the new callback does not replace the original callback, the behavior of controls which already have callback functions can be customized without disabling the control's currently defined behavior.

All the linked callbacks for a control are unchained when the control is discarded.

Once a control's callback is chained, the callback data and the callback function should not be changed using the InstallCtrlCallback() function or the ATTR_CALLBACK_DATA and ATTR_CALLBACK_FUNCTION_POINTER attributes.    5    Returns 0 if the function succeeded or a negative error code if the function failed.

If the ChainCtrlCallback() has previously been called for the specified control with the same Type_Name, then nothing is done and the error code UIEInvalidControlType (-45) is returned.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.        Pass the ID of the control to chain to new callback function to.

The ID is the defined constant (located in the UIR header file) which was assigned to the control in the User Interface Editor, or the ID returned by the NewCtrl() or DuplicateCtrl() function.
    !    Pass the callback function to be chained in with the control's current callback function.  This event function (type CtrlCallbackPtr) takes the form:

   int CVICALLBACK ChainCtrlCallback(int panel, int chainCtrlId,    
        int event, void *callbackData, int eventData1, int 
        eventData2);

For every event the control receives, the chained callback function will be called first.  If the chained callback function returns zero (0), then the original callback function will also be called with the same event.

Multiple callbacks can be chained to the same control.  For each event, the chained callbacks are called in order from the most recently chained callback to the first chained callback, followed by the control's original callback function (if any).  If one of the callback functions returns a non-zero value, then none of the subsequent callbacks are called for that event.

For the event EVENT_DISCARD, all the chained callbacks (and the original callback, if any) are called, regardless of what value each callback function returns.    %    This parameter specifies the callback data that will be passed to the New_Callback_Function in its callbackData parameter each time it is called.

The callback data parameter is typically used to hold a pointer to data related to the new behavior the chained callback function is intended to implement.  For example, if the chained callback function causes help to be displayed when the right mouse button is clicked on a control, then the callback data could be used to point to a string representing the name of the file with the help information, the topic name for the control's help, or even the help information itself.  Alternatively, the callback data could hold a pointer to a structure containing any number of data items that could then be used to implement the control's customized behavior.    >    Pass in the string to use as the type name of the newly created chained callback.  The type name is used to prevent ChainCtrlCallback() from being called twice on the same control for the same callback chaining.  The type name is also used by the GetChainedCallbackData() function to retrieve the callbackData for a chained callback.

Suggestion:  Use a type name which describes the behavior being added to the control.  For example, if a callback is being chained in to cause help to be displayed when the right mouse button is clicked on a control, then the type name might be "Help", "Right Click Help", or "Control With Help".


If the ChainCtrlCallback() function has previously been called for the specified control with the same Type_Name, then nothing is done and the error code UIEInvalidControlType (-45) is returned.   �����    x    Status                           �) = M      x    Panel                            �� = �     x    Ctrl                             �� =i �  x    New Callback Function            � � M    x    New Callback Data                �; � � �  x    Type Name                          	                        0       !    This function unchains the callback function that was linked to the control by the ChainCtrlCallback() function.

Note:  The behavior of this function is undefined if it attempts to unlink a callback function while inside of a DIFFERENT chained callback function for the same control.  
    �    Returns 1 if the callback was unchained, 0 if the type name was not found, or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.        Pass the ID of the control from which to unchain the callback function.

The ID is the defined constant (located in the UIR header file) which was assigned to the control in the User Interface Editor, or the ID returned by the NewCtrl() or DuplicateCtrl() function.
     �    Pass in the string that was passed to ChainCtrlCallback(). 

If the control has not been previously linked by ChainCtrlCallback() with specified type name, then nothing is done.   �	���    x    Status                           �� = M      x    Panel                            �a = �     x    Ctrl                             �u =i �  x    Type Name                          	                       O    This function retrieves the callback data for a callback function that was linked to a control by the ChainCtrlCallback() function.

This function is typically used to implement helper functions which operate on controls which have been customized using ChainCtrlCallback().

As an example, imagine that ChainCtrlCallback() is used to cause a help window to appear when the right mouse button is clicked on a control.  If the callback data for the chained callback contained a pointer to a structure which described what help information to display, then GetChainedCallbackData() could be used to implement hypothetical functions such as GetHelpInfo(panel, control, info) and SetHelpInfo(panel, control, newInfo).  These functions would use GetChainedCallbackData() to retrieve and/or modify the help information pointed to by the callback data.    ,    Returns 0 if the function succeeded or a negative error code if the function failed.

If the control has not been previously linked by  ChainCtrlCallback() with specified type name, then nothing is done and the error code UIEInvalidControlType (-45) is returned.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.    D    Pass the ID of the control for which the callback data of its chained callback with the specified type name is to be retrieved.

The ID is the defined constant (located in the UIR header file) which was assigned to the control in the User Interface Editor, or the ID returned by the NewCtrl() or DuplicateCtrl() function.
    �    Pass in the string that was passed to ChainCtrlCallback() to specify the type name of the link.  Pass an empty string if you want to get the original callback data (if it exists) before any 
ChainCtrlCallback was performed.

If the control has not been previously linked by ChainCtrlCallback() with specified type name, then nothing is done and the error code UIEInvalidControlType (-45) is returned.     �    Pass the address of a variable of type pointer to void (void *).  The callback data that was specified in the call to ChainCtrlCallback() will be placed in this variable.   �s���    x    Status                           �� = M      x    Panel                            �V = �     x    Ctrl                             �� =i �  x    Type Name                        �< � M    x    Callback Data                      	                        	           1    This function calls the callback function for a control with the specified parameters.

If the control does not have a callback function then nothing is done.

One use of this function is to send user defined events to a control whose behavior has been customized using the ChainCtrlCallback() function.    �    Returns a positive number if the control's callback was called, zero (0) if the control does not have a callback function, or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.        Pass the ID of the control for whose callback function is to be called.

The ID is the defined constant (located in the UIR header file) which was assigned to the control in the User Interface Editor, or the ID returned by the NewCtrl() or DuplicateCtrl() function.
    �    Pass the event number of the event to send to the control's callback.

This event can be a predefined event from userint.h (such as EVENT_COMMIT) or a user defined event (in the range from 1000 to 10000).  


This function only calls the callback function for the control (if any).  No control operation related the event will occur.  For example, calling a button control's callback with EVENT_LEFT_CLICK will NOT cause the button to operate.     ,    Pass data related to the event being sent.     7    Pass additional data related to the event being sent.     �    Pass the address of an integer variable in which to store the value returned by the callback function.

If the return value of the callback function is not needed, you can pass zero (0) for this parameter.   �V���    x    Status                           �/ = M      x    Panel                            �� = �     x    Ctrl                             �� =j     x    Event                            �� � M     x    EventData1                       �� � �     x    EventData2                       �* �j     x    Return Value                       	                        0    0    	           ;    This function adds a new callback function to a panel.  This function will be called first for every event the panel receives.  If this new callback function returns zero (0), then the panel's original callback function, if any, will be called with the same event.

This function is used to customize the behavior of panels.  Since the new callback does not replace the original callback, the behavior of a panel which already has a callback function can be customized without disabling the panel's currently defined behavior.

All the linked callbacks for a panel are unchained when the panel is discarded.

Once a panel's callback is chained, the callback data and the callback function should not be changed using the InstallPanelCallback() function or the ATTR_CALLBACK_DATA and ATTR_CALLBACK_FUNCTION_POINTER attributes.    6    Returns 0 if the function succeeded or a negative error code if the function failed.

If the ChainPanelCallback() has previously been called for the specified control with the same Type_Name, then nothing is done and the error code UIEInvalidControlType (-45) is returned.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.        Pass the callback function to be chained in with the panel's current callback function.  This event function (type PanelCallbackPtr) takes the form:

      int CVICALLBACK chainPnlCallback(int panel, int event, 
                void *callbackData, int eventData1, 
                int eventData2);

For every event the panel receives, the chained callback function will be called first.  If the chained callback function returns zero (0), then the original callback function will also be called with the same event.

Multiple callbacks can be chained to the same panel.  For each event, the chained callbacks are called in order from the most recently chained callback to the first chained callback, followed by the panel's original callback function (if any).  If one of the callback functions returns a non-zero value, then none of the subsequent callbacks are called for that event.

For the event EVENT_DISCARD, all the chained callbacks (and the original callback, if any) are called, regardless of what value each callback function returns.        This parameter specifies the callback data that will be passed to the New_Callback_Function in its callbackData parameter each time it is called.

The callback data parameter is typically used to hold a pointer to data related to the new behavior the chained callback function is intended to implement.  For example, if the chained callback function causes help to be displayed when the right mouse button is clicked on a panel, then the callback data could be used to point to a string representing the name of the file with the help information, the topic name for the panel's help, or even the help information itself.  Alternatively, the callback data could hold a pointer to a structure containing any number of data items that could then be used to implement the panel's customized behavior.    C    Pass in the string to use as the type name of the newly created chained callback.  The type name is used to prevent ChainPanelCallback() from being called twice on the same panel for the same callback chaining.  The type name is also used by the GetChainedPanelCallbackData() function to retrieve the callbackData for a chained callback.

Suggestion:  Use a type name which describes the behavior being added to the control.  For example, if a callback is being chained in to cause help to be displayed when the right mouse button is clicked on a control, then the type name might be "Help", "Right Click Help", or "Control With Help".


If the ChainPanelCallback() function has previously been called for the specified control with the same Type_Name, then nothing is done and the error code UIEInvalidControlType (-45) is returned.   �����    x    Status                           �# = M      x    Panel                            �� = � �  x    New Callback Function            �� =i    x    New Callback Data                � � M �  x    Type Name                          	                    0            This function unchains the callback function that was linked to the control by the ChainPanelCallback() function.

Note:  The behavior of this function is undefined if it attempts to unlink a callback function while inside of a DIFFERENT chained callback function for the same panel.  
    �    Returns 1 if the callback was unchained, 0 if the type name was not found, or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.     �    Pass in the string that was passed to ChainPanelCallback(). 

If the panel has not been previously linked by ChainPanelCallback() with specified type name, then nothing is done.   ϲ���    x    Status                           �[ = �      x    Panel                            �
 =' �  x    Type Name                          	                   F    This function retrieves the callback data for a callback function that was linked to a control by the ChainPanelCallback() function.

This function is typically used to implement helper functions which operate on panels which have been customized using ChainPanelCallback().

As an example, imagine that ChainPanelCallback() is used to cause a help window to appear when the right mouse button is clicked on a panel.  If the callback data for the chained callback contained a pointer to a structure which described what help information to display, then GetChainedPanelCallbackData() could be used to implement hypothetical functions such as GetHelpInfo(panel, info) and SetHelpInfo(panel, newInfo).  These functions would use GetChainedPanelCallbackData() to retrieve and/or modify the help information pointed to by the callback data.    -    Returns 0 if the function succeeded or a negative error code if the function failed.

If the control has not been previously linked by  ChainPanelCallback() with specified type name, then nothing is done and the error code UIEInvalidControlType (-45) is returned.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.        Pass in the string that was passed to ChainPanelCallback() to specify the type name of the link. 

If the control has not been previously linked by ChainPanelCallback() with specified type name, then nothing is done and the error code UIEInvalidControlType (-45) is returned.     �    Pass the address of a variable of type pointer to void (void *).  The callback data that was specified in the call to ChainPanelCallback() will be placed in this variable.   �����    x    Status                           �� = M      x    Panel                            ٪ = � �  x    Type Name                        �� =i    x    Callback Data                      	                    	           ,    This function calls the callback function for a panel with the specified parameters.

If the panel does not have a callback function then nothing is done.

One use of this function is to send user defined events to a panel whose behavior has been customized using the ChainPanelCallback() function.    �    Returns a positive number if the panel's callback was called, zero (0) if the panel does not have a callback function, or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.    �    Pass the event number of the event to send to the panel's callback.

This event can be a predefined event from userint.h (such as EVENT_COMMIT) or a user defined event (in the range from 1000 to 10000).  


This function only calls the callback function for the panel (if any).  No panel operation related the event will occur.  For example, calling a panels callback with EVENT_PANEL_MOVE will NOT cause the panel to move.     ,    Pass data related to the event being sent.     7    Pass additional data related to the event being sent.     �    Pass the address of an integer variable in which to store the value returned by the callback function.

If the return value of the callback function is not needed, you can pass zero (0) for this parameter.   ݤ���    x    Status                           �y = M      x    Panel                            �( = �     x    Event                            �� { K     x    EventData1                       � { �     x    EventData2                       �L {j     x    Return Value                       	                    0    0    	           �    This function enables a control to receive the following extended mouse events in addition to the usual mouse events.

    EVENT_MOUSE_MOVE
    EVENT_LEFT_MOUSE_UP
    EVENT_RIGHT_MOUSE_UP

The built-in CVI mouse events are:

    EVENT_LEFT_CLICK
    EVENT_LEFT_DOUBLE_CLICK
    EVENT_RIGHT_CLICK
    EVENT_RIGHT_DOUBLE_CLICK

each of which occur when the mouse button is pressed down.

For each event:

    eventData1 is the vertical mouse coordinate
    eventData2 is the horizontal mouse coordinate

EVENT_MOUSE_MOVE is sent when the mouse moves, regardless whether a mouse button is down.  You can specify minimum interval between mouse move events.

Note:
  This function can be called repeatedly to change the
  interval of a control's extended mouse events.
    �    Returns zero if the extended mouse events were enabled for the control, or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.        Pass the ID of the control whose callback function will receive extended mouse events

The ID is the defined constant (located in the UIR header file) which was assigned to the control in the User Interface Editor, or the ID returned by the NewCtrl() or DuplicateCtrl() function.
    }    This parameter specifies the minimum interval (in seconds) between extended mouse events of the same type.  If you set this parameter to 0.0, then you receive mouse events as frequently as possible.  

For example, if the minimum interval were 2 seconds, then you could receive at most one EVENT_MOUSE_MOVE, one EVENT_LEFT_MOUSE_UP, and one EVENT_RIGHT_MOUSE_UP every 2 seconds.
   ����    x    Status                           �8 = M      x    Panel                            �� = �     x    Ctrl                             �	 =�        Minimum Event Interval (Sec)       	                 ?��������              ?�������             J    This function stops a control from receiving the extended mouse events.
     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.        Pass the ID of the control for which to disable extended mouse events.

The ID is the defined constant (located in the UIR header file) which was assigned to the control in the User Interface Editor, or the ID returned by the NewCtrl() or DuplicateCtrl() function.
    P    Returns zero if the extended mouse events were disabled for the control, or a negative error code if the function failed.

If extended mouse events have not been enabled for the control by a previous call to EnableExtendedMouseEvents(), then the error code UIEInvalidControlType (-45) is returned.


The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.   �� = �      x    Panel                            � =+     x    Ctrl                             ����    x    Status                                     	               When control callback functions receive mouse events, eventData1 and eventData2 contain the vertical and horizontal mouse coordinates, respectively.  The coordinate values are relative to the top left corner of the panel under the title bar and within the frame.

This function allows you to adjust the coordinates so that they are relative to the top and left coordinates of a control on the panel.  It also allows you to restrict the coordinates to the rectangular area defined by the top, left, width, and height of the control.
    �    Returns zero if the extended mouse events were enabled for the control, or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     �    The specifier for a particular panel that is currently in memory. 

This handle will have been returned by the LoadPanel(), NewPanel(), or DuplicatePanel() function.     �    The ID is the defined constant (located in the UIR header file) which was assigned to the control in the User Interface Editor, or the ID returned by the NewCtrl() or DuplicateCtrl() function.
     �    This parameter specifies whether you want the mouse coordinates, which are assumed to be relative to the panel, to be adjusted to be relative to the top and left coordinates of the control.
    5    This parameter specifies whether you want the mouse coordinates to be restricted to the rectangular area defined by the size of the control. 

This option causes coordinates outside the control to be adjusted to the closest point on the rectangle defined by the top, left, height, and width of the control.
     u    On input, the original horizontal coordinate relative to the panel.

On output, the adjusted horizontal coordinate.     q    On input, the original vertical coordinate relative to the panel.

On output, the adjusted vertical coordinate.   �����    x    Status                           �� = M      x    Panel                            �3 = �     x    Ctrl                             �� =�          Make Relative to Control?        �� � u          Clip Coordinates To Control      � � �     x    X Coordinate                     �� �i     x    Y Coordinate                    ���� � ���                                            	                    Yes 1 No 0   On 1 Off 0    	            	            'This function is not multithread-safe.        This function installs a user-defined icon in the Status area of the Windows Desktop Taskbar (the System Tray).  The function specified in the Callback Function parameter will receive mouse events from the icon.

Use AttachTrayIconMenu () to attach popup menus to your System Tray icon.     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     X    A NULL-terminated string indicating the *.ico file to be displayed in the System Tray.        A NULL-temrinated string containing the text to be displayed when a user moves the mouse over the System Tray icon.  The popup message (a ToolTip) will be displayed in the default system colors.  The system will honor a maximum of 64 characters for this string.    W    The callback function to receive events from the System Tray icon.  The function should be of the following form:

    int CVICALLBACK Func (int iconHandle, int event,
                          int eventData);   

When an event occurs on the icon, CVI will call this function.  The iconHandle parameter will contain a handle to the icon which generated the event -- this handle will have been returned from InstallSysTrayIcon ().  The event parameter will be one of the following values:

    EVENT_LEFT_CLICK          
    EVENT_LEFT_MOUSE_UP            
    EVENT_RIGHT_CLICK         
    EVENT_RIGHT_MOUSE_UP           
    EVENT_LEFT_DOUBLE_CLICK           
    EVENT_RIGHT_DOUBLE_CLICK          
    EVENT_MENU_ITEM

The eventData parameter will be 0 for all events other than EVENT_MENU_ITEM. You will only receive EVENT_MENU_ITEM events if you install a popup menu for this icon.  In the case of this event, eventData will contain the ID of the menu item selected.

The callback function should return 0 unless you want to prevent an installed popup menu from appearing (return non-zero in this case).
     +    Returns a handle to the System tray icon.   �;���    x    Status                           �= = M  �  x    Icon Image File                  �� = � �  x    ToolTip Text                      � =i �  x    Callback Function                 � M     x    Icon Handle                        	            ""    ""        	            �    This function sets an attribute of the specified System Tray icon.

See the Class Help for more information on the recommended usage of System Tray icons and their popup menus.
     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    The handle of the System Tray icon for which you wish to set an  attribute.  This handle will have been returned from InstallSysTrayIcon.    �    The tray icon attribute you wish to set.

Currently, you may set the values of the following two attributes:

Constant:     ATTR_TRAY_ICOFILE
Data Type:    char *
Description:  Specify the icon that you want to appear in the 
              system tray.

Constant:     ATTR_TRAY_TOOLTIPTEXT
Data Type:    char *
Description:  Specify the tooltip text that you want to appear 
              when the mouse cursor moves over the system tray 
              icon.
     -    The new value of the specified attribute.

   *���    x    Status                           , = M      x    Icon Handle                      � = �    x    Tray Icon Attribute              
� =i �  x    Value                              	                           2ATTR_TRAY_ICOFILE 5004 ATTR_TRAY_TOOLTIPTEXT 5005        �    This function gets the current value of an attribute of the specified System Tray icon.

See the Class Help for more information on the recommended usage of System Tray icons and their popup menus.
     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    The handle of the System Tray icon for which you wish to get an  attribute value.  This handle will have been returned from InstallSysTrayIcon.        The tray icon attribute whose value you wish to get.

Currently, you may get the values of the following two attributes:

Constant:     ATTR_TRAY_TOOLTIPTEXT
Data Type:    char *
Description:  Return the tooltip text that appears when the 
              mouse cursor moves over the system tray icon.

Constant:     ATTR_TRAY_TOOLTIPTEXT_LENGTH
Data Type:    int
Description:  Return the length of the tooltip text that appears 
              when the mouse cursor moves over the system tray 
              icon.
     9    Returns the current value of the specified attribute.

   ����    x    Status                           � = M      x    Icon Handle                      ] = �    x    Tray Icon Attribute              g =i    x    Value                              	                           =ATTR_TRAY_TOOLTIPTEXT 5004 ATTR_TRAY_TOOLTIPTEXT_LENGTH 5006    	            �    This function removes the specied icon from the System Tray and frees all resources.  The function destroys any menus associated with the icon.
     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     m    The handle of the System Tray icon to remove.  This handle will have been returned from InstallSysTrayIcon.   ~���    x    Status                           � = �      x    Icon Handle                        	               
    This function attaches a right-click popup menu to the specified System Tray icon.  See the Class Help for more information on the recommended usage of System Tray icons and their popup menus.

You will add items to this popup menu with InsertTrayIconMenuItem ().
     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    The handle of the System Tray icon for which you wish to attach a popup menu.  This handle will have been returned from InstallSysTrayIcon ().   ����    x    Status                           � = �      x    Icon Handle                        	                �    This function sets an attribute of the specified System Tray icon's popup menu.

See the Class Help for more information on the recommended usage of System Tray icons and their popup menus.
     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    The handle of the System Tray icon for which you wish to set a  popup menu attribute.  This handle will have been returned from InstallSysTrayIcon.        The popup menu attribute you wish to set.

Currently, you may set the values of the following two attributes:

Constant:     ATTR_POPUP_DEFAULT_ITEM
Data Type:    int
Description:  Specify the 1-based index of the default item in 
              the popup menu as returned by 
              InsertTrayIconMenuItem, or 0 if you do not want a 
              default menu item.

Constant:     ATTR_VISIBLE
Data Type:    int
Description:  Specify whether the popup menu is visible.
Values:       0  not visible
              1  visible
     -    The new value of the specified attribute.

   ]���    x    Status                           _ = M      x    Icon Handle                      � = �    x    Popup Menu Attribute              =i     x    Value                              	                           .ATTR_POPUP_DEFAULT_ITEM 5002 ATTR_VISIBLE 530        �    This function gets the value of an attribute of the specified System Tray icon's popup menu.

See the Class Help for more information on the recommended usage of System Tray icons and their popup menus.
     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    The handle of the System Tray icon for which you wish to get a  popup menu attribute.  This handle will have been returned from InstallSysTrayIcon.    �    The popup menu attribute whose value you wish to get.

Currently, you may get the values of the following two attributes:

Constant:     ATTR_POPUP_DEFAULT_ITEM
Data Type:    int
Description:  Return the 1-based index of the default item in 
              the popup menu as returned by 
              InsertTrayIconMenuItem, or 0 if there is none.

Constant:     ATTR_VISIBLE
Data Type:    int
Description:  Return whether the popup menu is visible.
Values:       0  not visible
              1  visible
     1    The current value of the specified attribute.

   H���    x    Status                           J = M      x    Icon Handle                      � = �    x    Popup Menu Attribute             !� =i     x    Value                              	                           .ATTR_POPUP_DEFAULT_ITEM 5002 ATTR_VISIBLE 530    	           �    This function adds an item to the specified icon's right-click popup menu. Items are added in order, with the first item being item 1, and grow vertically on the popup menu.  

The Callback Function specified in InstallSysTrayIcon will receive EVENT_MENU_ITEM events when the user selects a menu item -- eventData will contain the 1-based index of the selected item.  The callback function will receive EVENT_RIGHT_CLICK events before any associated popups are diplayed.  If you return non-zero from the callback function upon receiving these events, the popup menu will not be displayed.   

See the Class Help for more information on the recommended usage of System Tray icons and their popup menus.
     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    The handle of the System Tray icon for which you wish to add a a popup menu item.  This handle will have been returned from InstallSysTrayIcon.     �    A NULL-terminated string indicating the name of the popup menu item.  This is the name which will appear on the popup menu.  If you pass NULL, then the created item will be a separator, for which you will not receive events.     1    The 1-based index of the newly inserted item.     &���    x    Status                           ' = M      x    Icon Handle                      '� = � �  x    Item Name                        (� =i     x    Item Index                         	                ""    	            �    This function sets an attribute of a particular item on the specified System Tray icon's popup menu.

See the Class Help for more information on the recommended usage of System Tray icons and their popup menus.
     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    The handle of the System Tray icon for which you wish to set a  popup menu item attribute.  This handle will have been returned from InstallSysTrayIcon.     �    The index of the item for which you wish to set an attribute.  The item index corresponds to its 1-based position on the popup menu, and will have been returned from InsertTrayIconMenuItem.    �    The attribute you wish to set.

Currently, you may set the values of the following two attributes:

Constant:     ATTR_DIMMED
Data Type:    int
Description:  Specify whether the menu item is dimmed.
Values:       0  not dimmed
              1  dimmed

Constant:     ATTR_CHECKED
Data Type:    int
Description:  Specify whether the menu item is checked.
Values:       0  not checked
              1  checked
     -    The new value of the specified attribute.

   *����    x    Status                           +� = �      x    Icon Handle                      ,L ='     x    Item Index                       - { �    x    Popup Menu Attribute             .� {'     x    Value                              	                1               "ATTR_DIMMED 500 ATTR_CHECKED 1040        �    This function gets the value of an attribute of a particular item on the specified System Tray icon's popup menu.

See the Class Help for more information on the recommended usage of System Tray icons and their popup menus.
     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    The handle of the System Tray icon for which you wish to get a  popup menu item attribute.  This handle will have been returned from InstallSysTrayIcon.     �    The index of the item for which you wish to get an attribute.  The item index corresponds to its 1-based position on the popup menu, and will have been returned from InsertTrayIconMenuItem.    �    The attribute you wish to get.

Currently, you may get the values of the following two attributes:

Constant:     ATTR_DIMMED
Data Type:    int
Description:  Return whether the menu item is dimmed.
Values:       0  not dimmed
              1  dimmed

Constant:     ATTR_CHECKED
Data Type:    int
Description:  Return whether the menu item is checked.
Values:       0  not checked
              1  checked
     1    The current value of the specified attribute.

   1&���    x    Status                           2( = �      x    Icon Handle                      2� ='     x    Item Index                       3� { �    x    Popup Menu Attribute             50 {'     x    Value                              	                1               "ATTR_DIMMED 500 ATTR_CHECKED 1040    	            K    This function removes a popup menu from the specified System Tray icon.

     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    The handle of the System Tray icon for which you wish to detach a popup menu.  This handle will have been returned from InstallSysTrayIcon.   7���    x    Status                           8 = �      x    Icon Handle                        	                   This function returns the typeface, size, and font style of the specified meta font.

CVI includes several predefined meta fonts.  You can create additional meta fonts by calling the CreateMetaFont() function in the User Interface Library.  See the function panel for CreateMetaFont() for more information on meta fonts.

Note: 
   This function assumes that the metafont name passed is a
   valid metafont.  You can use the IsMetaFont() function to
   determine if a particular font name belongs to a meta font.    �    Returns zero if the function was succesful, or a negative error code if the function failed.

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.    <    Meta Font can be one of the National Instrument meta fonts or a user-defined meta font saved by a previous CreateMetaFont() function call.

The following are National Instruments meta fonts:

  Platform Independent Meta Fonts:  Meta fonts contain typeface  
  information, point size, and text styles such as bold, 
  underline, italic, and strikeout.        
                         
                         VAL_MENU_META_FONT
                         VAL_DIALOG_META_FONT
                         VAL_EDITOR_META_FONT
                         VAL_APP_META_FONT       
   
  LW/CVI Supplied Meta Fonts: Meta fonts that are supplied with 
  LabWindows/CVI and installed on both PC and Sun systems while 
  LabWindows/CVI is running.

                         VAL_7SEG_META_FONT
                         VAL_SYSTEM_META_FONT
     �    The typeface name for the specified metafont is copied into this buffer.  The buffer should be at least 256 character long to hold all possible typeface names.


You may pass 0 (NULL) for this parameter if you do not need this information.     �    Returns whether the specified meta font has the bold font style attribute set.

You may pass 0 (NULL) for this parameter if you do not need this information.

     �    Returns whether the specified meta font has the underline font style attribute set.

You may pass 0 (NULL) for this parameter if you do not need this information.
     �    Returns whether the specified meta font has the strikeout font style attribute set.

You may pass 0 (NULL) for this parameter if you do not need this information.
     �    Returns whether the specified meta font has the italic font style attribute set.

You may pass 0 (NULL) for this parameter if you do not need this information.
     �    Returns the point size of the specified meta font.

You may pass 0 (NULL) for this parameter if you do not need this information.
   ;2���    x    Status                           <� = M  �  x    Meta Font Name                   @  = �    x    Type Face                        @� =i     x    Bold                             A� � M     x    Underline                        BO � �     x    Strike Out                       B� �i     x    Italic                           C� � M     x    Size                               	                	            	            	            	            	            	           8    This function returns whether the specified font name is a metafont name.

CVI includes several predefined meta fonts.  You can create additional meta fonts by calling the CreateMetaFont() function in the User Interface Library.  See the function panel for CreateMetaFont() for more information on meta fonts.
    �    Returns 0 (FALSE) if the font is not a meta font.
Returns 1 (TRUE) if the font is a meta font.
Returns a negative error code if the function fails

The possible negative error codes are:

-1 to -999        A User Interface Library error code.
                  (constants are available in userint.h)

A description of any of these error codes can be obtained using the GetGeneralErrorString() function in the toolbox.fp instrument driver.     ,    Pass the name of a typeface or metafont.

   Gr���    x    Result                           I2 = �  �     Font Name                          	               h    This function writes a NUL-terminated ASCII string to the specified Key Value in the Windows Registry.  You must specify a Root Key, a Subkey of that Root Key, and the actual Value of that Subkey to which you want to write the data.

Example:

RegWriteString (REGKEY_HKLM, "Software\\MySubKey",
                "MyStringValue", "This is a string buffer");
       �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    The Root Key under which you wish to access a Subkey and its value.

See the Windows Registry functions Class help for more information on Root Keys.     �    The name of the Subkey (relative to the Root Key), to which you want to write value data.

See the Windows Registry functions Class help for more information on Subkeys.
        The name of the value to which you want to write data. If the value name does not exist this function will create it.

You may pass NULL or an empty string to write to the Key's Default Value.

See the Windows Registry functions Class help for more information on Key Values.     e    The NUL-terminated ASCII string you wish to write to the specified Value of the specified Subkey.

   KP���    x    Status                           LR = B    �    Root Key                         L� = � �  x    Subkey Name                      M� =s �  x    Value Name                       N� � B �  �    String Buffer                      	                       �HKEY_LOCAL_MACHINE REGKEY_HKLM HKEY_CURRENT_USER REGKEY_HKCU HKEY_CLASSES_ROOT REGKEY_HKCR HKEY_USERS REGKEY_HKU HKEY_CURRENT_CONFIG REGKEY_HKCC HKEY_DYN_DATA REGKEY_HKDD    ""    ""    ""   �    This function writes an array of NUL-terminated ASCII strings to the specified Key Values in the Windows Registry.  You must specify a Root Key, a Subkey of that Root Key, and names of the actual Values of that Subkey to which you want to write the data.

Example:

static char *valueNames[]={"MyFirstValue",NULL,"MySecondValue"};
static char *values[]={"String1","DefaultValue","String2"};

RegWriteStringArray (REGKEY_HKLM, "Software\\MySubKey", 3,
                     valueNames, values);
       �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    The Root Key under which you wish to access a Subkey and its value.

See the Windows Registry functions Class help for more information on Root Keys.     �    The name of the Subkey (relative to the Root Key), to which you want to write value data.

See the Windows Registry functions Class help for more information on Subkeys.
     R    The number of names and values which you want to write to the Windows Registry.
    <    The array of names of the values to which you want to write data. If any value name does not exist this function will create it.

An array element may contain NULL or an empty string if you want to write to the Key's Default Value.

See the Windows Registry functions Class help for more information on Key Values.     u    The array of the NULL-terminated ASCII strings you wish to write to the specified Values of the specified Subkey.

   S	���    x    Status                           T = C    �    Root Key                         T� = � �  x    Subkey Name                      U^ =t     `    Number of items                  U� � C �  �    Array of Value Names             V� � � �  x    Array of String Buffer             	                       �HKEY_LOCAL_MACHINE REGKEY_HKLM HKEY_CURRENT_USER REGKEY_HKCU HKEY_CLASSES_ROOT REGKEY_HKCR HKEY_USERS REGKEY_HKU HKEY_CURRENT_CONFIG REGKEY_HKCC HKEY_DYN_DATA REGKEY_HKDD    ""    1           �    This function reads a NUL-terminated ASCII string from the specified Key Value in the Windows Registry.  You must specify a Root Key, a Subkey of that Root Key, and the actual Value of that Subkey which you want to read.

If you do not know the size of the string in the Registry, you may pass NULL for the "String Buffer" parameter.  The function will determine the size of the string and return it through "Real String Size".  You may then use this value to allocate appropriate space for the string, and call the function again.

Example:
unsigned char *buffer = NULL;
unsigned int  size;

RegReadString (REGKEY_HKLM, "Software\\MySubKey", "MyStrValue",
               NULL, 0, &size);
buffer = (unsigned char*) malloc(size * sizeof(unsigned char));
if (buffer != NULL) {
     RegReadString (REGKEY_HKLM, "Software\\MySubKey", 
                   "MyStrValue", buffer, size, &size);
     // Use the data in buffer ...
     free (buffer);
}
       �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    The Root Key under which you wish to access a Subkey and its value.

See the Windows Registry functions Class help for more information on Root Keys.     �    The name of the Subkey (relative to the Root Key), from which you wish to read value data.

See the Windows Registry functions Class help for more information on Subkeys.
     �    The name of the Value from which you want to read data.

You may pass NULL or an empty string to read from to the Key's Default Value.

See the Windows Registry functions Class help for more information on Key Values. 
    ]    Returns the ASCII NUL-terminated string contents of the specified Value of the specified Subkey.  This buffer must be large enough to receive the entire string.  If you are unsure of the size of the string, you may pass NULL -- the "Real String Size" parameter will return the actual size of the string (including the ASCII NUL) in the Registry.           The size of the buffer passed to "String Buffer".  This parameter is ignored when you pass NULL in place of an actual buffer.     �    Returns the actual size of the ASCII string associated with the specified registry Key.  You may pass NULL for the "String Buffer" parameter and use this value to allocate adequate memory before calling this function again.    ]K���    x    Status                           ^M = C    �    Root Key                         ^� = � �  x    Subkey Name                      _� =s �  x    Value Name                       `� � C    �    String Buffer                    a� � �    x    Buffer Size                      br �s    x    Real String Size                   	                       �HKEY_LOCAL_MACHINE REGKEY_HKLM HKEY_CURRENT_USER REGKEY_HKCU HKEY_CLASSES_ROOT REGKEY_HKCR HKEY_USERS REGKEY_HKU HKEY_CURRENT_CONFIG REGKEY_HKCC HKEY_DYN_DATA REGKEY_HKDD    ""    ""    	                	           �    This function writes an unsigned long (DWORD) value to the Windows Registry.  You must specify a Root Key, a Subkey of that Root Key, and the actual Value of that Subkey to which you want to write the data.

Example:

unsigned int number = 10;

// Writes 10 in Little-Endian format in MyULongValue
RegWriteULong (REGKEY_HKLM, "Software\\MySubKey",
                "MyULongValue", number, 0);     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    The Root Key under which you wish to access a Subkey and its value.

See the Windows Registry functions Class help for more information on Root Keys.     �    The name of the Subkey (relative to the Root Key), to which you want to write value data.

See the Windows Registry functions Class help for more information on Subkeys.     �    The name of the value to which you want to write data. If the value name does not exist this function will create it.

See the Windows Registry functions Class help for more information on Key Values.
     L    The data you wish to write to the specified Value of the specified Subkey.     �    This parameter controls how the 32-bit integer should be writen to the Registry.  Typically, you will specify Little-Endian (Intel) format.   gN���    x    Status                           hP = C    �    Root Key                         h� = � �  x    Subkey Name                      i� =s �  x    Value Name                       ju � C    �    Data Value                       j� � �          Data Format                        	                       �HKEY_LOCAL_MACHINE REGKEY_HKLM HKEY_CURRENT_USER REGKEY_HKCU HKEY_CLASSES_ROOT REGKEY_HKCR HKEY_USERS REGKEY_HKU HKEY_CURRENT_CONFIG REGKEY_HKCC HKEY_DYN_DATA REGKEY_HKDD    ""    ""    0    Big Endian 1 Little-Endian 0   �    This function reads unsigned 32-bit (unsigned long) data from the specified Key Value in the Windows Registry.  You must specify a Root Key, a Subkey of that Root Key, and the actual Value of that Subkey which you want to read.

Example:
unsigned long number;

// Reads the data in MyULongValue in Little-Endian format
RegReadULong (REGKEY_HKLM, "Software\\MySubKey", 
              "MyULongValue", &number, 0);     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    The Root Key under which you wish to access a Subkey and its value.

See the Windows Registry functions Class help for more information on Root Keys.     �    The name of the Subkey (relative to the Root Key), from which you wish to read value data.

See the Windows Registry functions Class help for more information on Subkeys.
     �    The name of the Value from which you want to read data.

See the Windows Registry functions Class help for more information on Key Values.       T    Returns the unsigned long contents of the specified Value of the specified Subkey.     �    This parameter specifies how the 32-bit integer is stored in the Registry.  Typically, you will specify Little-Endian (Intel) format.   o8���    x    Status                           p: = C    �    Root Key                         p� = � �  x    Subkey Name                      q� =s �  x    Value Name                       r$ � C    �    ULong Data                       r� � �          Data Format                        	                       �HKEY_LOCAL_MACHINE REGKEY_HKLM HKEY_CURRENT_USER REGKEY_HKCU HKEY_CLASSES_ROOT REGKEY_HKCR HKEY_USERS REGKEY_HKU HKEY_CURRENT_CONFIG REGKEY_HKCC HKEY_DYN_DATA REGKEY_HKDD    ""    ""    	            Big Endian 1 Little-Endian 0   �    This function writes a buffer of data into the Registry in binary format.  You must specify a Root Key, a Subkey of that Root Key, and the actual Value of that Subkey to which you want to write the data.  Any data that exists already at the specified Key value will be overwritten.

Example:

unsigned char buffer[100];

// Writes the first 10 bytes of buffer to MyBinaryValue
RegWriteBinary (REGKEY_HKLM, "Software\\MySubKey",
                "MyBinaryValue", buffer, 10);     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    The Root Key under which you wish to access a Subkey and its value.

See the Windows Registry functions Class help for more information on Root Keys.     �    The name of the Subkey (relative to the Root Key), to which you want to write value data.

See the Windows Registry functions Class help for more information on Subkeys.     �    The name of the value to which you want to write data. If the value name does not exist this function will create it.

See the Windows Registry functions Class help for more information on Key Values.
     �    The buffer whose contents you wish to write to the specified Value of the specified Subkey.  This function does not make any assumtions about the contents of this buffer, and data is written as-is to the Registry.

     �    The number of bytes you wish to copy from the input Buffer into the Registry.  This value must not be any larger than the size of the input Buffer.   w.���    x    Status                           x0 = C    �    Root Key                         x� = � �  x    Subkey Name                      y� =s �  x    Value Name                       zU � C �  �    Data Buffer                      {6 � �     x    Bytes to Copy                      	                       �HKEY_LOCAL_MACHINE REGKEY_HKLM HKEY_CURRENT_USER REGKEY_HKCU HKEY_CLASSES_ROOT REGKEY_HKCR HKEY_USERS REGKEY_HKU HKEY_CURRENT_CONFIG REGKEY_HKCC HKEY_DYN_DATA REGKEY_HKDD    ""    ""    ""       �    This function reads raw binary data from the specified Key Value in the Windows Registry.  You must specify a Root Key, a Subkey of that Root Key, and the actual Value of that Subkey which you want to read.

If you do not know the size of the data in the Registry, you may pass NULL for the "Data Buffer" parameter.  The function will determine the size of the data and return it through "Real Data Size".  You may then use this value to allocate appropriate space for the data, and call the function again.

Example:
unsigned char *buffer = NULL;
unsigned int  size;

RegReadBinary (REGKEY_HKLM, "Software\\MySubKey", 
               "MyBinaryValue", NULL, 0, &size);
buffer = (unsigned char*) malloc(size * sizeof(unsigned char));
if (buffer != NULL) {
     RegReadBinary (REGKEY_HKLM, "Software\\MySubKey", 
                   "MyBinaryValue", buffer, size, &size);
     // use the data in buffer ...
     free (buffer);
}     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    The Root Key under which you wish to access a Subkey and its value.

See the Windows Registry functions Class help for more information on Root Keys.     �    The name of the Subkey (relative to the Root Key), from which you wish to read value data.

See the Windows Registry functions Class help for more information on Subkeys.
     �    The name of the Value from which you want to read data.

See the Windows Registry functions Class help for more information on Key Values.      -    Returns the binary contents of the specified Value of the specified Subkey.  This buffer must be large enough to receive all of the binary data.  If you are unsure of the size of the data, you may pass NULL -- the "Real Data Size" parameter will return the actual size of the data in the Registry.       }    The size of the buffer passed to "Data Buffer".  This parameter is ignored when you pass NULL in place of an actual buffer.     �    Returns the actual size of the data associated with the specified registry Key.  You may pass NULL for the "Data Buffer" parameter and use this value to allocate adequate memory before calling this function again.    �����    x    Status                           �� = C    �    Root Key                         �5 = � �  x    Subkey Name                      �� =s �  x    Value Name                       �� � C    �    Data Buffer                      �� � �    x    Buffer Size                      �: �s    x    Real Data Size                     	                       �HKEY_LOCAL_MACHINE REGKEY_HKLM HKEY_CURRENT_USER REGKEY_HKCU HKEY_CLASSES_ROOT REGKEY_HKCR HKEY_USERS REGKEY_HKU HKEY_CURRENT_CONFIG REGKEY_HKCC HKEY_DYN_DATA REGKEY_HKDD    ""    ""    	                	           �    This function gathers information about the specified Key Value in the Windows Registry.  You must specify a Root Key, and a Subkey of that Root Key which you want to read.


Example:
unsigned char string[512];
unsigned int  size1,size2,values,i;
int           type;
char          valueName[MAX_PATH];

RegQueryInfoOnKey (REGKEY_HKLM, "Software\\MySubKey", 
                   NULL, &values, NULL, NULL, NULL);
for(i=0;i<values;i++) {
    size1 = MAX_PATH; size2 = 512;
    RegEnumerateValue (REGKEY_HKLM, "Software\\MySubKey",
                       i, valueName, &size1, string, &size2,
                       &type);
    if( type==_REG_SZ )
         // Process the data
}     �    The Root Key under which you wish to access a Subkey and its information.

See the Windows Registry functions Class help for more information on Root Keys.     �    The name of the Subkey (relative to the Root Key), about which you wish to obtain information.

See the Windows Registry functions Class help for more information on Subkeys.
     o    Returns the actual number of Subkeys associated with the specified registry Key.  This parameter may be NULL.     n    Returns the actual number of values associated with the specified registry Key.  This parameter may be NULL.     �    Returns the length of the key's subkey with the longest name.  This parameter may be NULL.

The length does not include the terminating NUL character.     �    Returns the length of the longest value name associated with the specified registry Key.  This parameter may be NULL.

The length does not include the terminating NUL character.     �    Returns the length of the longest value associated with the specified registry Key.  This parameter may be NULL.

If the data type is string (_REG_SZ), the length includes the terminating NUL character.     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
   �' = >    �    Root Key                         �� = � �  x    Subkey Name                      �� t >    `    Number of subkeys                �� t �    `    Number of values                 �r � >    `    Max subkey length                � � �    `    Max value name length            �� ��    `    Max value length                 �����    x    Status                                        �HKEY_LOCAL_MACHINE REGKEY_HKLM HKEY_CURRENT_USER REGKEY_HKCU HKEY_CLASSES_ROOT REGKEY_HKCR HKEY_USERS REGKEY_HKU HKEY_CURRENT_CONFIG REGKEY_HKCC HKEY_DYN_DATA REGKEY_HKDD    ""    	            	            	            	            	            	           9    This function enumerates the values for the specified key in the Windows Registry. This function copies one indexed value name and data block for the key each time it is called.  You must specify a Root Key, a Subkey of that Root Key, and index of the value which you want to read.

If you do not know the size of the data in the Registry, you may pass NULL for the "Data buffer" parameter.  The function will determine the size of the data and return it through "Data Size".  You may then use this value to allocate appropriate space for the data, and call the function again.


Example:
unsigned char string[512];
unsigned int  size1,size2,values,i;
int           type;
char          valueName[MAX_PATH];

RegQueryInfoOnKey (REGKEY_HKLM, "Software\\MySubKey", 
                   NULL, &values, NULL, NULL, NULL);
for(i=0;i<values;i++) {
    size1 = MAX_PATH; size2 = 512;
    RegEnumerateValue (REGKEY_HKLM, "Software\\MySubKey",
                       i, valueName, &size1, string, &size2,
                       &type);
    if( type==_REG_SZ )
         // Process the data
}     �    The Root Key under which you wish to access a Subkey and its value.

See the Windows Registry functions Class help for more information on Root Keys.     �    The name of the Subkey (relative to the Root Key), from which you wish to read the value.

See the Windows Registry functions Class help for more information on Subkeys.
     �    The index of the value which you wish to read.

This parameter should be zero for the first call to this function and then be incremented for subsequent calls.

See example in function help.     �    Buffer into which the name of the value will be stored.  The buffer has to be long enough to store also the terminating NUL character.    �    Returns the actual length of the value name.  This parameter may be NULL only if the parameter "Value Name" is NULL.

Use this parameter to pass the size of the buffer for the value name (including the terminating NUL character) and to receive the actual length of the value name read from the Windows Registry (the returned number of characters does not include the terminating NUL character).     �    Buffer into which the value will be stored.  If the value is of type string (_REG_SZ), the buffer has to be long enough to store also the terminating NUL character.    6    Returns the actual size of the data.  This parameter may be NULL only if the parameter "Data Buffer" is NULL.

Use this parameter to pass the size of the buffer for the data (including the terminating NUL character for string type) and to receive the actual length of the data read from the Windows Registry.    m    Returns the actual type of the data.  This parameter may be NULL.

The data type can be one of the following constants defined in the toolbox header file:
_REG_NONE
_REG_SZ
_REG_EXPAND_SZ
_REG_BINARY
_REG_DWORD
_REG_DWORD_LITTLE_ENDIAN
_REG_DWORD_BIG_ENDIAN
_REG_LINK
_REG_MULTI_SZ
_REG_RESOURCE_LIST
_REG_FULL_RESOURCE_DESCRIPTOR
_REG_RESOURCE_REQUIREMENTS_LIST
     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
   �� = H    �    Root Key                         �3 = � �  x    Subkey Name                      �� =�    `    Index                            �� q H    `    Value Name                       �? q �    `    Value Name Length                �� � H    `    Data Buffer                      �� � �    `    Data Length                      �� ��     `    Value Type                       �4���    x    Status                                        �HKEY_LOCAL_MACHINE REGKEY_HKLM HKEY_CURRENT_USER REGKEY_HKCU HKEY_CLASSES_ROOT REGKEY_HKCR HKEY_USERS REGKEY_HKU HKEY_CURRENT_CONFIG REGKEY_HKCC HKEY_DYN_DATA REGKEY_HKDD    ""    0    	            	            	            	            	            	            ^    This function returns detailed information on the current Windows operating system version.
     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    Returns the major version of the operating system.  On Windows NT 4.0, for example, this parameter will contain the integer 4.  You may pass NULL for this parameter.     �    Returns the minor version of the operating system.  On Windows NT 4.0, for example, this parameter will contain the integer 0.  You may pass NULL for this parameter.        Windows NT: Returns the build number of the operating system. 

Windows 95: Returns the build number of the operating system in
            the low-order word. The high-order word contains the
            major and minor version numbers.

You may pass NULL for this parameter.     �    Returns the platform ID of the operating system.  You may pass NULL for this parameter.

Possible values:

  0  PLATFORM_WIN32s 
  1  PLATFORM_WIN95   
  2  PLATFORM_WINNT

NOTE:  A platform value of PLATFORM_WIN95 is used to specify both Windows 95 and Windows 98.  If the platform is Windows 95, the Minor Version will be 0.  If the platform is Windows 98, the Minor Version will be greater than 0.

   �����    x    Status                           �� = �     x    Major Version                    �7 ='    x    Minor Version                    �� � �    x    Build                            � �'    x    Platform                           	            	            	            	            	            H    This function returns the Windows installation and System directories.     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     p    Returns the Windows installation directory.  If non-NULL, this buffer must be at least MAX_PATHNAME_LEN bytes.     j    Returns the Windows System directory.  If non-NULL, this buffer must be at least MAX_PATHNAME_LEN bytes.   �6���    x    Status                           �8 = �     x    Windows Directory                �� ='    x    System Directory                   	            	            	            ,    This function returns the Windows PC name.     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     [    Returns the Windows name of the PC.  This buffer must be at least MAX_COMPNAME_LEN bytes.   ����    x    Status                           � = �     x    Computer Name                      	            	               This function retrieves the 0RGB color of a Windows display element.  This is useful if you want to query the OS for its display colors and adjust your own application's display elements, such as Graph plot areas, which are not affected by the Conform To System Colors attribute.     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     6    The Windows display element to query for its color.     #    Returns the color of the display element.  The most significant byte of this four-byte value will be 0, and the lower three bytes will contain the Red, Green, and Blue components of the color, respectively.  This corresponds to the color format used throughout the CVI standard libraries.    �!���    x    Status                           �# = �     x    Display Element                  �a ='    x    Color                              	                      YScrollbar SYS_SCROLLBAR Background SYS_BACKGROUND Active Caption SYS_ACTIVECAPTION Inactive Caption SYS_INACTIVECAPTION Menu SYS_MENU Window SYS_WINDOW Window Frame SYS_WINDOWFRAME Menu Text SYS_MENUTEXT Window Text SYS_WINDOWTEXT Caption Text SYS_CAPTIONTEXT Active Border SYS_ACTIVEBORDER Inactive Border SYS_INACTIVEBORDER Application Workspace SYS_APPWORKSPACE Highlight SYS_HIGHLIGHT Highlight Text SYS_HIGHLIGHTTEXT Button Face SYS_BTNFACE Button Shadow SYS_BTNSHADOW Gray Text SYS_GRAYTEXT Button Text SYS_BTNTEXT Inactive Caption Text SYS_INACTIVECAPTIONTEXT Button Highlight SYS_BTNHIGHLIGHT    	           J    This function sets the current configuration of the Windows ScreenSaver.     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     L    This parameter specifies whether or not the ScreenSaver should be enabled.     �    This parameter specifies the time, in seconds, after which the ScreenSaver will activate.  The resolution of this value is 60s.       �    If the "Enabled" parameter is set to true, this parameter specifies the ScreenSaver application (*.scr) path you wish to use.  If the "Enabled" parameter is not set to true, this parameter is ignored and may be NULL.

   �����    x    Status                           �� = J    �      Enabled                          �S = �     x    Sleep Time                       �� =V �  x    ScreenSaver Path                   	            Yes 1 No 0        ""    M    This function obtains the current configuration of the Windows ScreenSaver.     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    Returns a boolean value indicating whether or not the ScreenSaver is currently enabled.  You may pass NULL for this parameter.     �    Returns the time, in seconds, after which the ScreenSaver will activate.  The resolution of this value is 60s.  You may pass NULL for this parameter.   ����    x    Status                           � = �      x    Enabled                          �� ='     x    Sleep Time                         	            	            	            ;    This function sets the current Windows desktop wallpaper.     �    The pathname of the bitmap (*.BMP) file to set as the Windows desktop wallpaper.  If you do not want any wallpaper, pass an empty string.

Example:

     "C:\\WinNT\\winnt.bmp" or ""     6    The orientation of the bitmap on the desktop window.     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
   �> =' �  x    Bitmap File                      �� = �           Display Style                    �<���    x    Status                             ""    Tiled 1 Centered 0    	            �    This function gets the current Windows desktop wallpaper.  This is useful if your application is going to change the wallpaper, as it must be able to restore the original settings when it completes.     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    Returns a boolean indicating whether or not the walpaper is tiled.  If this value is 0, then the wallpaper is centered on the desktop.         A buffer into which the function will copy the full NULL-terminated pathname of the desktop wallpaper file (a bitmap).  This buffer must be at least MAX_PATHNAME_LEN bytes.  If no wallpaper is currently configured, the function will return an empty string.
   �����    x    Status                           �� = �      x    Tiled                            �h ='    x    Bitmap File                        	            	            	            `    This function gets the type and timing attributes of the currently installed Windows keyboard.     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
    R    Returns an integer indicating the type of currently installed keyboard.  You may pass NULL for this parameter.

Possible output values:

  1  KYBD_IBM_PCXT         
  2  KYBD_OLIVETTI_ICO     
  3  KYBD_IBM_PCAT          
  4  KYBD_IBM_Enh          
  5  KYBD_NOKIA_1050       
  6  KYBD_NOKIA_9140       
  7  KYBD_JAPANESE         


     �    Returns an integer indicating the number of function keys on the currently installed keyboard.  You may pass NULL for this parameter.     �    Returns an integer indicating the repeat delay of the currently installed keyboard (0-3 increasing).  You may pass NULL for this parameter.     �    Returns an integer containing the speed of the currently installed keyboard (0-31 increasing).  You may pass NULL for this parameter.   Ş���    x    Status                           Ơ = �      x    Keyboard Type                    �� ='     x    Num Function Keys                ȉ � �    x    Repeat Delay                     � �'    x    Speed                              	            	            	            	            	            W    This function sets the timing attributes of the currently installed Windows keyboard.     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     F    This parameter specifies the keyboard repeat delay (0-3 increasing).     @    This parameter specifies the keyboard speed (0-31 increasing).   �Q���    x    Status                           �S = �     x    Repeat Delay                     ̡ ='    x    Speed                              	            1    31    �    This function determines the free space in a specified directory on disk.  On the original OEM release of Windows 95, this function is not guaranteed to be reliable when handling partitions over 2G.     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     �    The directory for which to obtain size information.  This paramater can be a root drive, or any subdirectory off of that drive.  This function will obtain the space available within this directory (including subdirectories).    �    Returns the total number of bytes in the specified directory.

This 64-bit unsigned integer value is returned in the following structure:

    typedef struct {
        unsigned long int loBytes;
        unsigned long int hiBytes;
    } UInt64Type;

The "loBytes" member will contain the low-order 32-bits of the value, and the "hiBytes" member will contain the high-order 32-bits of the value.  Use the function UInt64TypeCompareUInt to compare this value with a native 32-bit unsigned long integer.        Returns the number of bytes free to the calling process in the specified directory.

This 64-bit unsigned integer value is returned in the following structure:

    typedef struct {
        unsigned long int loBytes;
        unsigned long int hiBytes;
    } UInt64Type;

The "loBytes" member will contain the low-order 32-bits of the value, and the "hiBytes" member will contain the high-order 32-bits of the value.  Use the function UInt64TypeCompareUInt to compare this value with a native 32-bit unsigned long integer.   �o���    x    Status                           �q = M  �  x    Directory                        �[ = � �  x    Total Bytes                      �X =i �  x    Total Bytes Free                   	            "C:"    	            	            j    This function compares a UInt64Type value returned from GetDiskSpace to a native unsigned long int type.     �    The result of the comparison:

  -1 if UInt64Type is less than UInt
   0 if UInt64Type is equal to UInt
   1 if UInt64Type is greater than UInt
         A UInt64Type to compare.     "    An unsigned long int to compare.   �����    x    Result                           �w = �  �  x    UInt64Type                       ֙ ='    x    UInt                               	                    �    This function determines the size of the Windows operating system's Physical, Page File, and Virtual memory, as well as the portions of each which are available to the calling process.     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     T    Returns the load on available memory as a percentage.  This paramater may be NULL.     Q    Returns the total physical system memory in bytes.  This paramater may be NULL.     R    Returns the total system Page File memory in bytes.  This paramater may be NULL.     P    Returns the total virtual system memory in bytes.  This paramater may be NULL.     U    Returns the available physical system memory in bytes.  This paramater may be NULL.     M    Returns the available Page File size in bytes.  This paramater may be NULL.     T    Returns the available virtual system memory in bytes.  This paramater may be NULL.   �8���    x    Status                           �: = �     x    Percent Load                     ٖ � L    x    Total Physical                   �� � �    x    Total Page File                  �I �h    x    Total Virtual                    ڡ � M    x    Available Physical               �� � �    x    Available Page File              �S �h    x    Available Virtual                  	            	            	            	            	            	            	            	           �    This function installs a callback function for a specific Windows message posted or sent to a CVI panel.  A ProcessSystemEvents () or RunUserInterface () call will cause the panel to receive the message, and the specified callback function will be passed the message data. 

You will have to include windows.h in your source code if you use Windows constants like WM_CLOSE, WM_PAINT, etc to denote the message numbers. You can install the same callback function to intercept more than one Windows message by calling InstallWinMsgCallback once for each message and passing the callback function's address in each call. After done intercepting messages, you should call RemoveWinMsgCallback to uninstall the callback functions that were installed using the InstallWinMsgCallback function, and free associated memory.

DISCLAIMER: Windows Messaging is a complicated paradigm, and so some things may not work as expected. This is especially true when intercepting messages like WM_PAINT that occur very frequently during an application's lifetime. Note that these messages are correctly handled by the internal Window Procedures, and that anomalies may be seen when trying to intercept and handle them externally.

NOTE:  A message callback cannot be installed for a CVI Child panel. 

Example:

int CVICALLBACK MyCallback (int panelHandle, int message,
                                 unsigned int* wParam,
                                 unsigned int* lParam,
                                 void* callbackData);
int panelHandle;
int postHandle;

void main (void)
{
   if ((panelHandle = LoadPanel (0, "MyUIR.uir", PANEL)) < 0)
        return;
   /* To intercept the WM_CLOSE message with MyCallback */
   InstallWinMsgCallback (panelHandle, WM_CLOSE,
                          MyCallback, VAL_MODE_INTERCEPT, NULL,
                          &postHandle);
   ........

   /* Uninstall the event handler and free associated memory */
   RemoveWinMsgCallback (panelHandle, WM_CLOSE);
}      �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     `    The handle of the CVI panel for which you wish to install a Windows message callback function.     �    The number of the message for which you would like to install a callback function.

NOTE: You may not respond to WM_DISCARD, WM_DESTROY or WM_DROPFILES messages.        �    The callback function to be associated with the specified Windows message.  The driver will call this function when the message handler detects the specified message.  It will pass all Windows message data to the function.  

The function must be of the prototype:

     int CVICALLBACK MyCallback (int panelHandle, int message,
                                 unsigned int* wParam,
                                 unsigned int* lParam,
                                 void* callbackData);

If your callback is configured to intercept the message (execution mode = VAL_MODE_INTERCEPT), the function should return 0 if you want the message to be passed on to CVI's normal panel message-handler.  If the function returns a 1, then the message will not be detected by CVI's message-handler.  Note that the function has access to message data pointers.  If passing the messages on to CVI's message handler, you may alter the contents of these pointers and CVI's message-handler will receive the "adjusted" message data.

If your callback is configured for queued execution, then the return value or changing the message contents has no effect.

An event EVENT_NEWHANDLE may be passed to your callback through the "message" parameter.  When this occurs, it is because the Windows handle of the CVI panel has changed internally.  The new Windows handle, which you must now use to post messages, will be passed in via the contents of the wParam pointer.    p    This parameter controls when your callback will actually execute.  

In In-Queue Mode, CVI's handler sees the message and a call to your callback is placed in CVI's standard UI event queue.

In Intercept Mode, the callback is executed before CVI sees the message.  You would use this mode if you needed to adjust or swallow messages before CVI's handler  sees them.
     j    A pointer to be passed to your message callback function, similar to the standard UI callback mechanism.    9    The Window Handle (HWND) that should be used to post a message to this CVI panel.  On certain conditions, this handle may change internally.  Therefore, it is necessary to respond to the EVENT_NEWHANDLE message in your callback function and then use the new handle, passed via the contents of the wParam input.
   ����    x    Status                           � = M      x    Panel Handle                     �� = �     x    Message Number                   � =i �  x    Callback Function                �N � g   �      Calllback Mode                   �� � �    x    Callback Data                    �8 �i     x    Posting Handle                     	                      8 In-Queue VAL_MODE_IN_QUEUE Intercept VAL_MODE_INTERCEPT    NULL    	            m    This function sets an attribute of the callback mechanism linked to the specified message number and panel.     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     D    The handle of the panel for which a message-callback is installed.     Z    The number of the message for whose callback mechanism you wish to set an attribute.        6    The attribute you would like to set for the specified callback function.

Currently, you may set the values of the following two attributes:

Constant:     ATTR_CALLBACK_MODE
Data Type:    int
Description:  Specify the callback mode for the callback. See 
              the documentation for InstallWinMsgCallback for 
              more details.
Values:       VAL_MODE_IN_QUEUE
              VAL_MODE_INTERCEPT

Constant:     ATTR_ENABLED
Data Type:    int
Description:  Specify whether the callback is enabled.
Values:       0  disabled
              1  enabled
     +    The new value of the specified attribute.   �����    x    Status                           �� = M      x    Panel Handle                     � = �     x    Message Number                   �x =i    �    Callback Attribute               �� � �    x    Attribute Value                    	                               @ATTR_CALLBACK_MODE ATTR_CALLBACK_MODE ATTR_ENABLED ATTR_ENABLED        m    This function gets an attribute of the callback mechanism linked to the specified message number and panel.     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     D    The handle of the panel for which a message-callback is installed.     Z    The number of the message for whose callback mechanism you wish to get an attribute.        4    The attribute you would like to get for the specified callback function.

Currently, you may get the values of the following two attributes:

Constant:     ATTR_CALLBACK_MODE
Data Type:    int
Description:  Return the selected mode for the callback. See 
              the documentation for InstallWinMsgCallback for 
              more details.
Values:       VAL_MODE_IN_QUEUE
              VAL_MODE_INTERCEPT

Constant:     ATTR_ENABLED
Data Type:    int
Description:  Return whether the callback is enabled.
Values:       0  disabled
              1  enabled
     /    The current value of the specified attribute.   �����    x    Status                           �� = M      x    Panel Handle                     � = �     x    Message Number                   �~ =i    �    Callback Attribute               �� � �     x    Attribute Value                    	                               @ATTR_CALLBACK_MODE ATTR_CALLBACK_MODE ATTR_ENABLED ATTR_ENABLED    	            s    This function removes the callback function for a specific panel and message number, and frees associated memory.     M    The handle of the CVI panel for which a message-callback should be removed.     _    The number of the message for which you would like to remove an installed callback function.
     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
   �� = �      x    Panel Handle                     �9 =%     x    Message Number                   �����    x    Status                                     	            a    This function returns the name of the user currently logged-in to the Windows operating system.     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
    x    Returns the name of the currently logged-in user.  This buffer must be large enough to hold the complete name.  If you are unsure of the size of the name, you may safely pass NULL in this parameter -- the function will return the User-Interface Library error UIEBufTooSmall and the required buffer size, in bytes, will be passed out through the "Actual Name Size" parameter.     �    The size of the buffer, in bytes, passed as the "User Name" parameter.  If you are passing NULL into that parameter, then this value is ignored.      �    Returns the exact size of the logged-in user's name, including the terminating NULL.  The buffer you pass to the "User Name" parameter must be at least this large (or NULL).   ����    x    Status                           � = M     x    User Name                        @ = �     x    Buffer Size                      � =i    x    Actual Name Size                   	            	                	           �    This function enables traditional Windows file Drag-And-Drop notification for the specified panel.  After calling this function, the panel's standard callback function will be passed an event EVENT_FILESDROPPED whenever files are dropped onto the panel from the Windows explorer shell.  

The eventData1 parameter will be an array of strings indicating the received files.  This scheme follows exactly that used by MultiFileSelectPopup -- see its Function Panel for more details.

eventData2 will be a pointer to a CVI Point structure indicating the position at which the files were dropped, relative to the top-left of the panel.

NOTE:  The panel's ATTR_CALLBACK_DATA pointer will be passed to the panel callback with the EVENT_FILESDROPPED event.

NOTE:  This function should not be called for a CVI Child panel -- Child panels will respond to Drag-And-Drop notifications as long as their Parent panels do.     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     U    The handle of the CVI panel for which Drag-And-Drop notification should be enabled.   
%���    x    Status                           ' = �      x    Panel Handle                       	                M    This function disables Drag-And-Drop notification for the specified panel.      �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString to obtain a text description of the error.
     V    The handle of the CVI panel for which Drag-And-Drop notification should be disabled.   S���    x    Status                           U = �      x    Panel Handle                       	                j    This function returns the value of Pi. Pi is the ratio of the circumference to the diameter of a circle.         Returns the value of Pi   ����   x    Pi                              ���� � z��                                            	           NThe value returned by this function is also available as the defined
macro PI    d    This function returns the value of two times Pi.  This value is the number of radians in a circle.     #    Returns the value of two times Pi   ����   x    Two Pi                          ���� � z��                                            	           RThe value returned by this function is also available as the defined
macro TWO_PI    7    This function returns the value of Pi divided by two.     )    Returns the value of Pi divided by two.   (���   x    Half Pi                         ���� � z��                                            	           SThe value returned by this function is also available as the defined
macro HALF_PI    C    This function returns the value of three times Pi divided by two.     5    Returns the value of three times Pi divided by two.   p���   x    Three Halves Pi                 ���� � z��                                            	           [The value returned by this function is also available as the defined
macro THREE_HALVES_PI    8    This functions returns the value of one divided by Pi.     )    Returns the value of one divided by Pi.   ����   x    Reciprocal Of Pi                ���� � z��                                            	           \The value returned by this function is also available as the defined
macro RECIPROCAL_OF_PI    A    This function returns the value of the natural logarithm of Pi.     3    Returns the value of the natural logarithm of Pi.   ���   x    Natural Logarithm Of Pi         ���� � z��                                            	           TThe value returned by this function is also available as the defined
macro LN_OF_PI    B    This function returns the value of the base ten logarithm of Pi.     4    Returns the value of the base ten logarithm of Pi.   b���   x    Base Ten Logarithm Of Pi        ���� � z��                                            	           XThe value returned by this function is also available as the defined
macro LOG_10_OF_PI    |    This function returns the value of the Euler's Constant.  Euler's Constant is the base for the natural logarithm function.     ,    Returns the value of the Euler's Constant.   ����   x    Euler's Constant                ���� � z��                                            	           QThe value returned by this function is also available as the defined
macro EULER    E    This function returns the value of one divided by Euler's Constant.     7    Returns the value of one divided by Euler's Constant.   >���   x    Reciprocal Of Euler's Constant  ���� � z��                                            	           _The value returned by this function is also available as the defined
macro RECIPROCAL_OF_EULER    B    This function returns the base 10 logarithm of Euler's Constant.     4    Returns the base 10 logarithm of Euler's Constant.   ����   x    Log Base 10 Of Euler's Constant ���� � z��                                            	           [The value returned by this function is also available as the defined
macro LOG_10_OF_EULER    5    This function returns the natural logarithm of ten.     '    Returns the natural logarithm of ten.   ����   x    Natural Logarithm Of Ten        ���� � z��                                            	           TThe value returned by this function is also available as the defined
macro LN_OF_10    5    This function returns the natural logarithm of two.     '    Returns the natural logarithm of two.   %���   x    Natural Logarithm Of Two        ���� � z��                                            	           SThe value returned by this function is also available as the defined
macro LN_OF_2       This function returns the value of Planck's Constant. The units are J/Hz and the uncertainty of the value is 5.4 ppm. Planck's constant is a universal constant of nature which relates the energy of a quantum of radiation to the frequency of the oscillator which emitted it.      )    Returns the value of Planck's Constant.   <���   x    Planck's Constant               ���� � z��                                            	           [The value returned by this function is also available as the defined
macro PLANCK_CONSTANT    t    This function returns the value of the elementary charge.  The units are Coulombs, and the uncertainty is 2.9 ppm.     ,    Returns the value of the elementary charge    ����   x    Elementary Charge               ���� � z��                                            	           ]The value returned by this function is also available as the defined
macro ELEMENTARY_CHARGE    �    This function returns the value of the speed of light in a vacuum.  The units are Meters per Second and the uncertainty is 0.004 ppm.     5    Returns the value of the speed of light in a vacuum   "V���   x    Speed Of Light                  ���� � z��                                            	           ZThe value returned by this function is also available as the defined
macro SPEED_OF_LIGHT   [    This function returns the value of Avogadro's number.  The units are 1/mol and the uncertainty is 5.1 ppm.  Avogadro's number is the number of molecules in one mole or gram-molecular weight of a substance.  Avogadro's principle states that the number of molecules present in equal volumes of gases at the same temperature and pressure are equal.     (    Returns the value of Avogadro's number   $����   x    Avogadro's Number               ���� � z��                                            	           ]The value returned by this function is also available as the defined
macro AVOGADRO_CONSTANT   8    This function returns the value of the gravitational constant. 
The units are N*M^2/K^2 and the uncertainty is 615 ppm.  The gravitational constant is the coefficient of proportionality in Newton's law of gravitation:

Force_Of_Gravity = (Gravitational_Constant * Mass_Of_A * Mass_Of B) / (distance * distance)     2    Returns the value of the gravitational constant.   '���   x    Gravitational Constant          ���� � z��                                            	           bThe value returned by this function is also available as the defined
macro GRAVITATIONAL_CONSTANT    o    This function returns the value of the Rydberg constant.  The units are 1/m and the uncertainty is 0.075 ppm.     ,    Returns the value of the Rydberg constant.   (����   x    Rydberg Constant                ���� � z��                                            	           \The value returned by this function is also available as the defined
macro RYDBERG_CONSTANT    t    This function returns the value of the Molar Gas Constant.  The units are 1/(m * K) and the uncertainty is 31 ppm.     .    Returns the value of the Molar Gas Constant.   * ���   x    Molar Gas Constant              ���� � z��                                            	           ^The value returned by this function is also available as the defined
macro MOLAR_GAS_CONSTANT    1    This function converts from degrees to radians.     (    Returns the converted value in radians     D    Pass the value in units of degrees to convert to units of radians.   +f���   x    Radians                         ���� � ���                                          +� = �     x    Degrees                            	           fAn alternate method of converting Degrees to Radians is to
use the defined macro: DEG_TO_RAD(degrees)        1    This function converts from radians to degrees.     )    Returns the converted value in degrees.     D    Pass the value in units of radians to convert to units of degrees.   -3���   x    Degrees                         ���� � ���                                          -d = �     x    Radians                            	           fAn alternate method of converting Degrees to Radians is to
use the defined macro: RAD_TO_DEG(radians)        @    This function converts from Celsius degrees to Kelvin degrees.     0    Returns the converted value in Kelvin degrees.     C    Pass the value in units of Celsius to convert to units of Kelvin.   /���   x    Kelvin                          ���� � ���                                          /H = �     x    Celsius                            	           qAn alternate method of converting from Celsius to Kelvin is to
use the defined macro: CELSIUS_TO_KELVIN(celsius)        @    This function converts from Kelvin degrees to Celsius degrees.     1    Returns the converted value in Celsius degrees.     C    Pass the value in units of Kelvin to convert to units of Celsius.   0����   x    Celsius                         ���� � ���                                          17 = �     x    Kelvin                             	           pAn alternate method of converting from Kelvin to Celsius is to
use the defined macro: KELVIN_TO_CELSIUS(kelvin)        D    This function converts from Celsius degrees to Fahrenheit degrees.     4    Returns the converted value in Fahrenheit degrees.     G    Pass the value in units of Celsius to convert to units of Fahrenheit.   2����   x    Fahrenheit                      ���� � }��                                          3, = �     x    Celsius                            	           yAn alternate method of converting from Celsius to Fahrenheit is to
use the defined macro: CELSIUS_TO_FAHRENHEIT(celsius)        D    This function converts from Fahrenheit degrees to Celsius degrees.     1    Returns the converted value in Celsius degrees.     G    Pass the value in units of Fahrenheit to convert to units of Celsius.   4����   x    Celsius                         ���� � }��                                          5+ = �     x    Fahrenheit                         	           |An alternate method of converting from Fahrenheit to Celsius is to
use the defined macro: FAHRENHEIT_TO_CELSIUS(fahrenheit)        -    This function converts from Meters to Feet.     /    Returns the converted value in units of Feet.     @    Pass the value in units of Meters to convert to units of Feet.   6����   x    Feet                            ���� � ���                                          7 = �     x    Meters                             	           jAn alternate method of converting from Meters to Feet is to
use the defined macro: METERS_TO_FEET(meters)        -    This function converts from Feet to Meters.     1    Returns the converted value in units of Meters.     @    Pass the value in units of Feet to convert to units of Meters.   8����   x    Meters                          ���� � ���                                          8� = �     x    Feet                               	           hAn alternate method of converting from Feet to Meters is to
use the defined macro: FEET_TO_METERS(feet)        <    This function converts from Kilometers to Miles (statute).     0    Returns the converted value in units of Miles.     E    Pass the value in units of Kilometers to convert to units of Miles.   :����   x    Miles                           ���� � ���                                          :� = �     x    Kilometers                         	           xAn alternate method of converting from Kilometers to Miles is to
use the defined macro: KILOMETERS_TO_MILES(kilometers)        <    This function converts from Miles (statute) to Kilometers.     5    Returns the converted value in units of Kilometers.     E    Pass the value in units of Miles to convert to units of Kilometers.   <���   x    Kilometers                      ���� � ���                                          <� = �     x    Miles                              	           sAn alternate method of converting from Miles to Kilometers is to
use the defined macro: MILES_TO_KILOMETERS(miles)        :    This function converts from Kilograms to Pounds (avdp.).     1    Returns the converted value in units of Pounds.     E    Pass the value in units of Kilograms to convert to units of Pounds.   >p���   x    Pounds                          ���� � ~��                                          >� = �     x    Kilograms                          	           wAn alternate method of converting from Kilograms to Pounds is to
use the defined macro: KILOGRAMS_TO_POUNDS(kilograms)        :    This function converts from Pounds (avdp.) to Kilograms.     4    Returns the converted value in units of Kilograms.     E    Pass the value in units of Pounds to convert to units of Kilograms.   @a���   x    Kilograms                       ���� � ~��                                          @� = �     x    Pounds                             	           tAn alternate method of converting from Pounds to Kilograms is to
use the defined macro: POUNDS_TO_KILOGRAMS(pounds)        >    This function converts from Liters to Gallons (U.S. Liquid).     2    Returns the converted value in units of Gallons.     C    Pass the value in units of Liters to convert to units of Gallons.   BV���   x    Gallons                         ���� � ���                                          B� = �     x    Liters                             	           pAn alternate method of converting from Liters to Gallons is to
use the defined macro: LITERS_TO_GALLONS(liters)        >    This function converts from Gallons (U.S. Liquid) to Liters.     1    Returns the converted value in units of Liters.     C    Pass the value in units of Gallons to convert to units of Liters.   DC���   x    Liters                          ���� � ���                                          D| = �     x    Gallons                            	           qAn alternate method of converting from Gallons to Liters is to
use the defined macro: GALLONS_TO_LITERS(gallons)        ^    This function causes a callback function to be called at the specified time in the future.

     �    Returns 1 if the callback has been queued for execution at the specified time.  Returns 0 if there was not enough memory to queue the callback.     �    This parameter specifies which function to call after the specified delay has elapsed.

The function must have the following prototype:

void CVICALLBACK DelayedFunction(void *callbackData);
     w    This parameter specifies a user-defined value that will be passed to the delayed callback function when it is called.        This parameter specifies the amount on time in seconds, from the current time, when the callback function is to be called.  This time is a minimum value.  Delays may be longer depending on the system's load and the frequency at which the system is processing events.
   FP���    x    Result                           F� = U  �  �    Delayed Callback Function        G� =    x    Callback Data                    H1 =�        Delay                              	                0 ?�      �              ?�                      Call this function when you want to execute a callback function in a particular thread and you want to wait until that callback function finishes executing before continuing execution in the  current thread.

You pass a callback function pointer, callback data, a thread id, and a timeout to PostDeferredCallToThreadAndWait().  If the thread id you pass is the same as the current thread's id, PostDeferredCallToThreadAndWait() calls the callback function directly.  If the thread id you pass is different than the current thread's id, PostDeferredCallToThreadAndWait() calls PostDeferredCallToThread() and then sleeps until your callback finishes executing or until the timeout expires.

To obtain the id of a thread, call the Utility Library function CmtGetCurrentThreadId().
     �    The status code that the function returns.

0 indicates success

A negative value indicates an error.

This function may return a Programmer's Toolbox or UI Library error-code.  Call GetGeneralErrorString() to obtain a text description of the error.
     �    Pass a pointer to the function that you want to execute in a particular thread.  The function you pass in this parameter must have the following prototype:

    void CVICALLBACK FunctionName (void *callbackData);
    /    Pass the thread id of the thread that you want to execute the Deferred Function.  The thread that you post the call to must process messages by calling RunUserInterface(), GetEvent(), or ProcessSystemEvents().

To obtain the id of a thread, call the Utility Library function CmtGetCurrentThreadId().

    +    Pass the maximum amount of time, in milliseconds, that PostDeferredCallToThreadAndWait() should wait for the callback function to be executed.  Pass POST_CALL_WAIT_TIMEOUT_INFINITE to indicate that PostDeferredCallToThreadAndWait() should not return until the callback function finishes executing.   Mn ����    `    Status                           Nr B e  �  `    Deferred Function               ���� B �    `    Callback Data                    OQ B_    `    Target Thread Id                 P� � �         Timeout (ms)                       	                0         POST_CALL_WAIT_TIMEOUT_INFINITE ����         (�  +�             K.        GetGeneralErrorString                                                                                                                   ����         ,b  3	             K.        DoAssert                                                                                                                                ����         5�  7�             K.        Pin                                                                                                                                     ����         8�  :             K.        Random                                                                                                                                  ����         :�  <�             K.        SetRandomSeed                                                                                                                           ����         <�  =f             K.        StartPCSound                                                                                                                            ����         >R  >�             K.        StopPCSound                                                                                                                             ����         ?f  @0             K.        SwapBlock                                                                                                                               ����         @�  C"             K.        SetBOLE                                                                                                                                 ����         C�  G/             K.        TransposeData                                                                                                                           ����         H�  O�             K.        ConvertArrayType                                                                                                                        ����         S,  U�             K.        GetFileCLibTime                                                                                                                         ����         V�  d{             K.        FP_Compare                                                                                                                              ����         e4  h�             K.        FindClosestColorInTable                                                                                                                 ����         ip  u|             K.        ShowHtmlHelp                                                                                                                            ����         w�  y�             K.        OpenDocumentInDefaultViewer                                                                                                             ����         z�  ~             K.        InternationalTime                                                                                                                       ����         5  �y             K.        InternationalDate                                                                                                                       ����         ��  ��             K.        InternationalFileTime                                                                                                                   ����         ��  ��             K.        InternationalFileDate                                                                                                                   ����         �  �S             K.        AppendString                                                                                                                            ����         �I  �f             K.        AddStringPrefix                                                                                                                         ����         �\  �n             K.        AddStringQuotes                                                                                                                         ����         ��  ��             K.        StrDup                                                                                                                                  ����         �(  �6             K.        StrDupWithoutSurrWhiteSpace                                                                                                             ����         ��  �e             K.        SkipWhiteSpace                                                                                                                          ����         ��  ��             K.        StringCopyMax                                                                                                                           ����         �P  ��             K.        SkipNonWhiteSpace                                                                                                                       ����         �m  �D             K.        HasNonWhiteSpace                                                                                                                        ����         ��  ��             K.        RemoveSurroundingWhiteSpace                                                                                                             ����         ��  ��             K.        StrICmp                                                                                                                                 ����         ��  ��             K.        StrNICmp                                                                                                                                ����         ��  ��             K.        StrICmpWithoutSurrWhiteSpace                                                                                                            ����         �{  ��             K.        StrToInt                                                                                                                                ����         ��  �a             K.        StrToUInt                                                                                                                               ����         �  �0             K.        GetFileWritability                                                                                                                      ����         ��  �5             K.        CreateAndOpenTemporaryFile                                                                                                              ����         ��  ��             K.        RemoveFileIfExists                                                                                                                      ����         �)  ��             K.        DeleteAndRename                                                                                                                         ����         Փ  �             K.        FileExists                                                                                                                              ����         ��  ��             K.        WriteStringToFile                                                                                                                       ����         ܳ  �             K.        SaveBitmapToFile                                                                                                                        ����         ��  �             K.        SaveCtrlDisplayToFile                                                                                                                   ����         �M  �             K.        SavePanelDisplayToFile                                                                                                                  ����         �  �r             K.        BinSearch                                                                                                                               ����         �� r             K.        HeapSort                                                                                                                                ����        V 
�             K.        InsertionSort                                                                                                                           ����        � b             K.        ShortCompare                                                                                                                            ����         �             K.        IntCompare                                                                                                                              ����        @ �             K.        FloatCompare                                                                                                                            ����        q �             K.        DoubleCompare                                                                                                                           ����        �  3             K.        CStringCompare                                                                                                                          ����         � $�             K.        CStringNoCaseCompare                                                                                                                    ����        %� )�             K.        ListCreate                                                                                                                              ����        *: *�             K.        ListDispose                                                                                                                             ����        +3 +�             K.        ListNumItems                                                                                                                            ����        ,k /M             K.        ListInsertItem                                                                                                                          ����        0D 2e             K.        ListGetItem                                                                                                                             ����        3 5`             K.        ListReplaceItem                                                                                                                         ����        6 8�             K.        ListRemoveItem                                                                                                                          ����        9� <m             K.        ListInsertItems                                                                                                                         ����        =� @�             K.        ListGetItems                                                                                                                            ����        A� DF             K.        ListReplaceItems                                                                                                                        ����        EJ H�             K.        ListRemoveItems                                                                                                                         ����        I� JL             K.        ListCopy                                                                                                                                ����        J� L_             K.        ListAppend                                                                                                                              ����        M M�             K.        ListClear                                                                                                                               ����        N/ O�             K.        ListEqual                                                                                                                               ����        PF Ud             K.        ListInsertInOrder                                                                                                                       ����        VP Yz             K.        ListGetPtrToItem                                                                                                                        ����        Z- ^�             K.        ListGetDataPtr                                                                                                                          ����        _) c�             K.        ListApplyToEach                                                                                                                         ����        e) j�             K.        ListFindItem                                                                                                                            ����        k� p�             K.        ListBinSearch                                                                                                                           ����        q� u�             K.        ListQuickSort                                                                                                                           ����        v  z             K.        ListHeapSort                                                                                                                            ����        zv ~             K.        ListInsertionSort                                                                                                                       ����        ~z �,             K.        ListIsSorted                                                                                                                            ����        �� �             K.        ListRemoveDuplicates                                                                                                                    ����        �t ��             K.        OutputStringItem                                                                                                                        ����        �L ��             K.        OutputShortItem                                                                                                                         ����        � �y             K.        OutputIntegerItem                                                                                                                       ����        �� �2             K.        OutputDoubleItem                                                                                                                        ����        �� ��             K.        OutputList                                                                                                                              ����        �� �             K.        ListSetAllocationPolicy                                                                                                                 ����        �� �.             K.        ListCompact                                                                                                                             ����        �g �             K.        ListPreAllocate                                                                                                                         ����        �� ��             K.        ListGetItemSize                                                                                                                         ����        �N ��             K.        NewHandle                                                                                                                               ����        �- �h             K.        DisposeHandle                                                                                                                           ����        �� ��             K.        GetHandleSize                                                                                                                           ����        �$ ��             K.        SetHandleSize                                                                                                                           ����        �� �             K.        PutLabelOnLeft                                                                                                                          ����        �� �             K.        SetCommonDialogShortcutKeys                                                                                                             ����        �8 �D             K.        MoveInFront                                                                                                                             ����        �0 �6             K.        MoveBehind                                                                                                                              ����        �" ��         	    K.        PositionCtrlRelativeToCtrl                                                                                                              ����        �� �             K.        CenterCtrl                                                                                                                              ����        ߛ ��             K.        SizeRingCtrlToText                                                                                                                      ����        �y �g             K.        ConformCtrlBitmapToPanel                                                                                                                ����        �S ��             K.        AllocateCtrlValString                                                                                                                   ����        �z ��             K.        GetCtrlValStringLength                                                                                                                  ����        �� %             K.        RecessCtrlFrame                                                                                                                         ����        � ;             K.        RecessAllCtrlFrames                                                                                                                     ����        � 1             K.        UILEventString                                                                                                                          ����        � X             K.        LocalizeNumberString                                                                                                                    ����        � 	@             K.        DelocalizeNumberString                                                                                                                  ����        	y �             K.        SetCtrlToolTipAttribute                                                                                                                 ����        L �             K.        SetAttributeForCtrls                                                                                                                    ����        2 !'         	    K.        AttributeMaxAndMin                                                                                                                      ����        #Q .�             K.        SetCtrlsToAttributeExtreme                                                                                                              ����        0 9�             K.        DistributeCtrls                                                                                                                         ����        <| B�             K.        PutLabelsOnLeft                                                                                                                         ����        D HG             K.        SetAttributeForList                                                                                                                     ����        Il P             K.        AttrMaxAndMinForList                                                                                                                    ����        R Wx             K.        SetAttributeToExtremeForList                                                                                                            ����        X� `w             K.        DistributeCtrlsInList                                                                                                                   ����        b� g             K.        PutLabelsOnLeftForList                                                                                                                  ����        g� k              K.        SetSelectedRadioButtons                                                                                                                 ����        lE n�             K.        GetSelectedRadioButtons                                                                                                                 ����        o� n             K.        ColorChangePopup                                                                                                                        ����        �� �             K.        CreateProgressDialog                                                                                                                    ����        � ��             K.        UpdateProgressDialog                                                                                                                    ����       ���� �`             K.        DiscardProgressDialog                                                                                                                   ����        �� ��             K.        ChainCtrlCallback                                                                                                                       ����        �� �0             K.        UnchainCtrlCallback                                                                                                                     ����        � ��             K.        GetChainedCallbackData                                                                                                                  ����        � �             K.        CallCtrlCallback                                                                                                                        ����        �� �d             K.        ChainPanelCallback                                                                                                                      ����        Ί ��             K.        UnchainPanelCallback                                                                                                                    ����        �x �|             K.        GetChainedPanelCallbackData                                                                                                             ����        �p �#             K.        CallPanelCallback                                                                                                                       ����        � �             K.        EnableExtendedMouseEvents                                                                                                               ����        �� �             K.        DisableExtendedMouseEvents                                                                                                              ����        �� ��             K.        ConvertMouseCoordinates                                                                                                                 ����        � >             K.        InstallSysTrayIcon                                                                                                                      ����        o 
�             K.        SetTrayIconAttr                                                                                                                         ����        � �             K.        GetTrayIconAttr                                                                                                                         ����        � �             K.        RemoveSysTrayIcon                                                                                                                       ����        o              K.        AttachTrayIconMenu                                                                                                                      ����        � N             K.        SetTrayIconMenuAttr                                                                                                                     ����        s ""             K.        GetTrayIconMenuAttr                                                                                                                     ����        #O (�             K.        InsertTrayIconMenuItem                                                                                                                  ����        )� .�             K.        SetTrayIconMenuItemAttr                                                                                                                 ����        0< 5i             K.        GetTrayIconMenuItemAttr                                                                                                                 ����        6� 8�             K.        DetachTrayIconMenu                                                                                                                      ����        9( D2             K.        GetMetaFontInfo                                                                                                                         ����        F2 If             K.        IsMetaFont                                                                                                                              ����        I� O/             K.        RegWriteString                                                                                                                          ����        Q Wy             K.        RegWriteStringArray                                                                                                                     ����        Y� c\             K.        RegReadString                                                                                                                           ����        e� k^             K.        RegWriteULong                                                                                                                           ����        m� s             K.        RegReadULong                                                                                                                            ����        uK {�             K.        RegWriteBinary                                                                                                                          ����        }� �             K.        RegReadBinary                                                                                                                           ����        �{ ��             K.        RegQueryInfoOnKey                                                                                                                       ����        �S �6         	    K.        RegEnumerateValue                                                                                                                       ����        �  ��             K.        GetWinOSVersion                                                                                                                         ����        �� �"             K.        GetWindowsDirs                                                                                                                          ����        �� �~             K.        GetCompName                                                                                                                             ����        �  ��             K.        GetWindowsColor                                                                                                                         ����        �� ��             K.        SetScreenSaver                                                                                                                          ����        �� �8             K.        GetScreenSaver                                                                                                                          ����        �� �>             K.        SetWallpaper                                                                                                                            ����        � �s             K.        GetWallpaper                                                                                                                            ����        �6 ɭ             K.        GetKeyboardPreferences                                                                                                                  ����        �� ��             K.        SetKeyboardPreferences                                                                                                                  ����        ͟ �k             K.        GetDiskSpace                                                                                                                            ����        �k ��             K.        UInt64TypeCompareUInt                                                                                                                   ����        �v ۯ             K.        GetMemoryInfo                                                                                                                           ����        ݷ �y             K.        InstallWinMsgCallback                                                                                                                   ����        �S ��             K.        SetMsgCallbackAttribute                                                                                                                 ����        �Y ��             K.        GetMsgCallbackAttribute                                                                                                                 ����        �i  �             K.        RemoveWinMsgCallback                                                                                                                    ����        U �             K.        GetCurrentUser                                                                                                                          ����        � �             K.        EnableDragAndDrop                                                                                                                       ����        � �             K.        DisableDragAndDrop                                                                                                                      ����        - �             K.        Pi                                                                                                                                      ����        �              K.        TwoPi                                                                                                                                   ����        � Y             K.        HalfPi                                                                                                                                  ����        % �             K.        ThreeHalvesPi                                                                                                                           ����        � �             K.        ReciprocalOfPi                                                                                                                          ����        � K             K.        LnOfPi                                                                                                                                  ����         �             K.        Log10OfPi                                                                                                                               ����        o '             K.        Euler                                                                                                                                   ����        � }             K.        ReciprocalOfEuler                                                                                                                       ����        U �             K.        Log10OfEuler                                                                                                                            ����        �              K.        Ln10                                                                                                                                    ����        � T             K.        Ln2                                                                                                                                     ����          m             K.        PlanckConstant                                                                                                                          ����         A  �             K.        ElementaryCharge                                                                                                                        ����        !� "�             K.        SpeedOfLight                                                                                                                            ����        #f $�             K.        AvogadroConstant                                                                                                                        ����        %� 'I             K.        GravitationalConstant                                                                                                                   ����        ($ (�             K.        RydbergConstant                                                                                                                         ����        )� *V             K.        MolarGasConstant                                                                                                                        ����        +- +�             K.        DegToRad                                                                                                                                ����        ,� -�             K.        RadToDeg                                                                                                                                ����        .� /�             K.        CelsiusToKelvin                                                                                                                         ����        0� 1�             K.        KelvinToCelsius                                                                                                                         ����        2� 3{             K.        CelsiusToFahrenheit                                                                                                                     ����        4� 5z             K.        FahrenheitToCelsius                                                                                                                     ����        6� 7\             K.        MetersToFeet                                                                                                                            ����        8x 9.             K.        FeetToMeters                                                                                                                            ����        :H ;             K.        KilometersToMiles                                                                                                                       ����        <; =	             K.        MilesToKilometers                                                                                                                       ����        >. >�             K.        KilogramsToPounds                                                                                                                       ����        @ @�             K.        PoundsToKilograms                                                                                                                       ����        B B�             K.        LitersToGallons                                                                                                                         ����        C� D�             K.        GallonsToLiters                                                                                                                         ����        E� IF             K.        PostDelayedCall                                                                                                                         ����        JZ Q�             K.        PostDeferredCallToThreadAndWait                                                                                                               Q                                                                                    HMiscellaneous                                                                        �Get General Error String                                                             �Do Assert                                                                            �Pin Value To Range                                                                   �Random                                                                               �Set Random Seed                                                                      �Start PC Sound                                                                       �Stop PC Sound                                                                        �Swap Memory Blocks                                                                   �Set Break On Library Errors                                                          �Transpose Data                                                                       �Convert Array Type                                                                   �Get File Time (C Library Form)                                                       �Floating Point Comparison                                                            �Find Closest Color Match                                                             �Show Html Help                                                                       �Open Document in Default Viewer                                                     �International Time And Date                                                          �International Time                                                                   �International Date                                                                   �International File Time                                                              �International File Date                                                             String Handling                                                                      �Append String                                                                        �Prefix String                                                                        �Quote String                                                                         �Duplicate String                                                                     �StrDup Without Surr White Space                                                      �Skip White Space                                                                     �String Copy With Maximum Limit                                                       �Skip Non-White Space                                                                 �Has Non White Space                                                                  �Remove Surrounding White Space                                                       �Case-Insensitive String Compare                                                      �Case-Insensitive Character Compare                                                   �Case-Insensitive Compare/No WS                                                       �Convert String To Integer                                                            �Convert String To Unsigned Int                                                      �File                                                                                 �Get File Writability                                                                 �Create and Open Temp File                                                            �Remove File If It Exists                                                             �Delete And Rename                                                                    �File Exists?                                                                         �Write String To File                                                                 �Save Bitmap To File                                                                  �Save Control Display to File                                                         �Save Panel Display to File                                                          �Searching and Sorting                                                                �Binary Search                                                                        �Heap Sort                                                                            �Insertion Sort                                                                       �Compare (short)                                                                      �Compare (int)                                                                        �Compare (float)                                                                      �Compare (double)                                                                     �Compare (C String)                                                                   �Compare (C String case insensitive)                                                 FLists                                                                                �Create List                                                                          �Dispose List                                                                         �Number Of Items In List                                                              �Insert Item In List                                                                  �Get Item From List                                                                   �Replace Item In List                                                                 �Remove Item From List                                                               3Multiple Item Functions                                                              �Insert Items In List                                                                 �Get Items From List                                                                  �Replace Items In List                                                                �Remove Items From List                                                               �Copy A List                                                                          �Append A List To Another List                                                        �Clear All Items                                                                      �Are Lists Equal                                                                      �Insert Item In Order                                                                 �Get Pointer To Item                                                                  �Get Pointer To List Data                                                             �Apply Function To Each Item                                                         �List Searching And Sorting                                                           �Find Item In List                                                                    �Binary Search For Item In List                                                       �Quick Sort A List                                                                    �Heap Sort A List                                                                     �Insertion Sort A List                                                                �Is A List Sorted                                                                     �Remove Duplicates From A List                                                       List Output/Printing                                                                 �Output String Item                                                                   �Output Short Item                                                                    �Output Integer Item                                                                  �Output Double Item                                                                   �Output List                                                                         �Advanced List Operations                                                             �Set List Allocation Policy                                                           �Compact List                                                                         �Pre-Allocate Items In List                                                           �Get List Item Size                                                                  7Memory Block Handles                                                                 �New Handle                                                                           �Dispose Handle                                                                       �Get Handle Size                                                                      �Set Handle Size                                                                     zUser Interface Utilities                                                             �Center Ctrl Label On Left Side                                                       �Set Common Dialog Shortcut Keys                                                      �Move Ctrl In Front Of Another                                                        �Move Ctrl Behind Another                                                             �Move Ctrl Relative To Another Ctrl                                                   �Center Control                                                                       �Size Ring Control To Text                                                            �Conform Control Bitmap To Panel                                                      �Allocate Control Value String                                                        �Get String Value Length                                                              �Recess Control Frame                                                                 �Recess All Control Frames                                                            �Get Event Name String                                                                �Localize Number String                                                               �Delocalize Number String                                                             �Set Ctrl Tool Tip Attribute                                                         �Multiple Control Functions                                                           �Set Attribute For Several Ctrls                                                      �Get Attribute Max And Min                                                            �Set Ctrls To Attribute Extreme                                                       �Distribute Controls                                                                  �Center Multiple Labels On Left                                                      Control List Functions                                                               �Set Attribute For Ctrls In List                                                      �Get Attr Max And Min For List                                                        �Set Ctrls To Attribute Extreme                                                       �Distribute Controls                                                                  �Center Labels On Left For Ctrls                                                      �Set Selected Radio Buttons                                                           �Get Selected Radio Buttons                                                          �Dialogs                                                                              �Color Change Popup                                                                  ?Progress Dialog                                                                      �Create A Progress Dialog                                                             �Update A Progress Dialog                                                             �Discard A Progress Dialog                                                           5Callback Chaining                                                                    �Chain Control Callback Function                                                      �Unchain Control Callback                                                             �Get Chained Callback Data                                                            �Call Control Callback                                                                �Chain Panel Callback Function                                                        �Unchain Panel Callback                                                               �Get Chained Panel Callback Data                                                      �Call Panel Callback                                                                 �Extended Mouse Events                                                                �Enable Extended Mouse Events                                                         �Disable Extended Mouse Events                                                        �Convert Mouse Coordinates                                                           DSystem Tray Icons                                                                    �Install System Tray Icon                                                             �Set Tray Icon Attribute                                                              �Get Tray Icon Attribute                                                              �Remove System Tray Icon                                                             YPopup Menus                                                                          �Attach Tray Icon Popup Menu                                                          �Set Popup Menu Attribute                                                             �Get Popup Menu Attribute                                                             �Add Tray Icon Popup Menu Item                                                        �Set Popup Menu Item Attribute                                                        �Get Popup Menu Item Attribute                                                        �Detach Tray Icon Popup Menu                                                       ����Meta Fonts                                                                           �Get Meta Font Information                                                            �Is Meta Font?                                                                       VWindows Registry                                                                     �Write String to Registry                                                             �Write Array of String to Registry                                                    �Read String from Registry                                                            �Write ULong to Registry                                                              �Read ULong from Registry                                                             �Write Binary to Registry                                                             �Read Binary from Registry                                                            �Query Information On Key From Registry                                               �Enumerate One Value From Registry                                                   ESystem Configuration                                                                 �Get Windows Version                                                                  �Get Windows Directories                                                              �Get Computer Name                                                                   �GUI                                                                                  �Get System Color                                                                     �Set ScreenSaver                                                                      �Get ScreenSaver                                                                      �Set Desktop Wallpaper                                                                �Get Desktop Wallpaper                                                                mKeyboard                                                                             �Get Keyboard Preferences                                                             �Set Keyboard Preferences                                                             �Resources                                                                            �Get Disk Space                                                                       �Compare UInt64Type to UInt                                                           �Get Memory Information                                                              !Windows Messaging                                                                    �Install Win Message Callback                                                         �Set Callback Attribute                                                               �Get Callback Attribute                                                               �Remove Win Message Callback                                                         "�User Information                                                                     �Get User Name                                                                       #6Drag-And-Drop                                                                        �Enable Drag-And-Drop                                                                 �Disable Drag-And-Drop                                                               $=Constants And Conversions                                                           %OMathematical Constants                                                               �Pi                                                                                   �Two Times Pi                                                                         �Half Of Pi                                                                           �Three Times Pi Divided By Two                                                        �Reciprocal Of Pi                                                                     �Natural Logarithm Of Pi                                                              �Base Ten Logarithm Of Pi                                                             �Euler's Constant (e)                                                                 �Reciprocal Of Euler's Constant                                                       �Log Base 10 Of Euler's Constant                                                      �Natural Logarithm Of Ten                                                             �Natural Logarithm Of Two                                                            &Physical Constants                                                                   �Planck's Constant                                                                    �Elementary Charge                                                                    �Speed Of Light In A Vacuum                                                           �Avogadro's Number                                                                    �Gravitational Constant                                                               �Rydberg's Constant                                                                   �Molar Gas Constant                                                                  &�Conversions                                                                          �Convert Degrees To Radians                                                           �Convert Radians To Degrees                                                           �Convert Celsius To Kelvin                                                            �Convert Kelvin To Celsius                                                            �Convert Celsius To Fahrenheit                                                        �Convert Fahrenheit To Celsius                                                        �Convert Meters To Feet                                                               �Convert Feet To Meters                                                               �Convert Kilometers To Miles                                                          �Convert Miles To Kilometers                                                          �Convert Kilograms To Pounds                                                          �Convert Pounds To Kilograms                                                          �Convert Liters To Gallons                                                            �Convert Gallons To Liters                                                           '^Callback Posting                                                                     �Post Delayed Callback Function                                                       �Post Deferred Call To Thread And Wait                                           