/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2018. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  AMPPNL                           1
#define  AMPPNL_BTN_SET                   2       /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1316              3
#define  AMPPNL_BTN_STS                   4       /* callback function: OnCmdAMP */
#define  AMPPNL_BTN_REQ                   5       /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_878               6
#define  AMPPNL_BTN_CLOSE                 7       /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1470              8
#define  AMPPNL_TEXTMSG_1361              9
#define  AMPPNL_TEXTMSG_1362              10
#define  AMPPNL_DECORATION_35             11
#define  AMPPNL_TEXTMSG_1294              12
#define  AMPPNL_TEXTMSG_1082              13
#define  AMPPNL_TEXTMSG_1317              14
#define  AMPPNL_TEXTMSG_1293              15
#define  AMPPNL_TG_DIR_19                 16      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_DIR_18                 17      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_DIR_17                 18      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_DIR_16                 19      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_DIR_15                 20      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_DIR_14                 21      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_DIR_13                 22      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_DIR_12                 23      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_DIR_11                 24      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_DIR_10                 25      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_DIR_9                  26      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_DIR_8                  27      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_DIR_7                  28      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_DIR_6                  29      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_DIR_5                  30      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_DIR_4                  31      /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1448              32
#define  AMPPNL_TEXTMSG_1363              33
#define  AMPPNL_TG_DIR_3                  34      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_DIR_2                  35      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_AMP_19                 36      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_AMP_18                 37      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_AMP_17                 38      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_AMP_16                 39      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_AMP_15                 40      /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1472              41
#define  AMPPNL_TEXTMSG_1473              42
#define  AMPPNL_TEXTMSG_1474              43
#define  AMPPNL_TG_AMP_14                 44      /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1365              45
#define  AMPPNL_TEXTMSG_1366              46
#define  AMPPNL_TEXTMSG_1367              47
#define  AMPPNL_TEXTMSG_1369              48
#define  AMPPNL_TEXTMSG_1370              49
#define  AMPPNL_TEXTMSG_1371              50
#define  AMPPNL_TG_AMP_13                 51      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_AMP_12                 52      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_DIR_0                  53      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_DIR_1                  54      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_AMP_11                 55      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_ALC_19                 56      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_ALC_18                 57      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_ALC_17                 58      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_ALC_16                 59      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_ALC_15                 60      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_ALC_14                 61      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_ALC_13                 62      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_ALC_12                 63      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_ALC_11                 64      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_ALC_10                 65      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_AMP_10                 66      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_ALC_9                  67      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_ALC_8                  68      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_ALC_7                  69      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_AMP_9                  70      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_AMP_8                  71      /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1475              72
#define  AMPPNL_TEXTMSG_1476              73
#define  AMPPNL_TG_ALC_6                  74      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_ALC_5                  75      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_AMP_7                  76      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_ALC_4                  77      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_AMP_6                  78      /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1373              79
#define  AMPPNL_TEXTMSG_1374              80
#define  AMPPNL_TG_AMP_5                  81      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_ALC_3                  82      /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_877               83
#define  AMPPNL_TG_AMP_4                  84      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_AMP_3                  85      /* callback function: OnCmdAMP */
#define  AMPPNL_DECORATION_99             86
#define  AMPPNL_TEXTMSG_1318              87
#define  AMPPNL_TEXTMSG_1319              88
#define  AMPPNL_TEXTMSG_1320              89
#define  AMPPNL_TEXTMSG_1321              90
#define  AMPPNL_TG_ALC_2                  91      /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1322              92
#define  AMPPNL_TG_AMP_2                  93      /* callback function: OnCmdAMP */
#define  AMPPNL_TG_ALC_0                  94      /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1324              95
#define  AMPPNL_TG_ALC_1                  96      /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1325              97
#define  AMPPNL_TEXTMSG_1477              98
#define  AMPPNL_TEXTMSG_1478              99
#define  AMPPNL_TEXTMSG_1296              100
#define  AMPPNL_TG_AMP_0                  101     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1297              102
#define  AMPPNL_TEXTMSG_1298              103
#define  AMPPNL_TG_AMP_1                  104     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1378              105
#define  AMPPNL_TEXTMSG_1379              106
#define  AMPPNL_TEXTMSG_1381              107
#define  AMPPNL_TEXTMSG_1382              108
#define  AMPPNL_TEXTMSG_1479              109
#define  AMPPNL_TEXTMSG_1092              110
#define  AMPPNL_TEXTMSG_1094              111
#define  AMPPNL_TEXTMSG_1096              112
#define  AMPPNL_TEXTMSG_1480              113
#define  AMPPNL_TEXTMSG_1386              114
#define  AMPPNL_STR_PATH                  115
#define  AMPPNL_TEXTMSG_1102              116
#define  AMPPNL_TEXTMSG_1387              117
#define  AMPPNL_RING_ID_19                118     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_RING_ID_18                119     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_RING_ID_17                120     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_RING_ID_16                121     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_RING_ID_15                122     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_RING_ID_14                123     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_RING_ID_13                124     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_RING_ID_12                125     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_RING_ID_11                126     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_RING_ID_10                127     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_RING_ID_9                 128     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_RING_ID_8                 129     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_RING_ID_7                 130     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_RING_ID_6                 131     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_TEXTMSG_1388              132
#define  AMPPNL_RING_ID_5                 133     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_TEXTMSG_1389              134
#define  AMPPNL_RING_ID_4                 135     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_TEXTMSG_1449              136
#define  AMPPNL_TEXTMSG_1326              137
#define  AMPPNL_TEXTMSG_1390              138
#define  AMPPNL_TEXTMSG_1327              139
#define  AMPPNL_RING_ID_3                 140     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_TEXTMSG_1328              141
#define  AMPPNL_TEXTMSG_1329              142
#define  AMPPNL_TEXTMSG_1330              143
#define  AMPPNL_TEXTMSG_1450              144
#define  AMPPNL_TEXTMSG_1331              145
#define  AMPPNL_TEXTMSG_1451              146
#define  AMPPNL_TEXTMSG_1391              147
#define  AMPPNL_RING_ID_2                 148     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_TEXTMSG_1452              149
#define  AMPPNL_TEXTMSG_1392              150
#define  AMPPNL_TEXTMSG_1299              151
#define  AMPPNL_TEXTMSG_1393              152
#define  AMPPNL_TEXTMSG_1481              153
#define  AMPPNL_TEXTMSG_1482              154
#define  AMPPNL_TEXTMSG_1300              155
#define  AMPPNL_TEXTMSG_1483              156
#define  AMPPNL_TEXTMSG_1484              157
#define  AMPPNL_TEXTMSG_1485              158
#define  AMPPNL_TEXTMSG_1492              159
#define  AMPPNL_TEXTMSG_1486              160
#define  AMPPNL_TEXTMSG_1453              161
#define  AMPPNL_TEXTMSG_1487              162
#define  AMPPNL_TEXTMSG_1488              163
#define  AMPPNL_TEXTMSG_1489              164
#define  AMPPNL_TEXTMSG_1490              165
#define  AMPPNL_TEXTMSG_1491              166
#define  AMPPNL_TEXTMSG_1395              167
#define  AMPPNL_TEXTMSG_1396              168
#define  AMPPNL_TEXTMSG_1397              169
#define  AMPPNL_RING_TYPE_19              170     /* callback function: OnCmdAMP */
#define  AMPPNL_RING_TYPE_18              171     /* callback function: OnCmdAMP */
#define  AMPPNL_RING_TYPE_17              172     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1454              173
#define  AMPPNL_RING_TYPE_16              174     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1398              175
#define  AMPPNL_TEXTMSG_1399              176
#define  AMPPNL_TEXTMSG_1401              177
#define  AMPPNL_TEXTMSG_1403              178
#define  AMPPNL_TEXTMSG_1405              179
#define  AMPPNL_TEXTMSG_1493              180
#define  AMPPNL_TEXTMSG_1407              181
#define  AMPPNL_TEXTMSG_1409              182
#define  AMPPNL_RING_TYPE_15              183     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1410              184
#define  AMPPNL_TEXTMSG_1412              185
#define  AMPPNL_TEXTMSG_1413              186
#define  AMPPNL_TEXTMSG_1414              187
#define  AMPPNL_RING_TYPE_14              188     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1415              189
#define  AMPPNL_RING_TYPE_13              190     /* callback function: OnCmdAMP */
#define  AMPPNL_RING_TYPE_12              191     /* callback function: OnCmdAMP */
#define  AMPPNL_RING_TYPE_11              192     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1455              193
#define  AMPPNL_RING_TYPE_10              194     /* callback function: OnCmdAMP */
#define  AMPPNL_RING_TYPE_9               195     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1419              196
#define  AMPPNL_TEXTMSG_1456              197
#define  AMPPNL_TEXTMSG_1457              198
#define  AMPPNL_TEXTMSG_1458              199
#define  AMPPNL_RING_TYPE_8               200     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1459              201
#define  AMPPNL_TEXTMSG_1494              202
#define  AMPPNL_RING_TYPE_7               203     /* callback function: OnCmdAMP */
#define  AMPPNL_RING_TYPE_6               204     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1460              205
#define  AMPPNL_TEXTMSG_1461              206
#define  AMPPNL_TEXTMSG_1462              207
#define  AMPPNL_TEXTMSG_1463              208
#define  AMPPNL_TEXTMSG_1421              209
#define  AMPPNL_TEXTMSG_1464              210
#define  AMPPNL_RING_TYPE_5               211     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1465              212
#define  AMPPNL_RING_TYPE_4               213     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1466              214
#define  AMPPNL_RING_TYPE_3               215     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1467              216
#define  AMPPNL_TEXTMSG_1422              217
#define  AMPPNL_TEXTMSG_1423              218
#define  AMPPNL_TEXTMSG_1424              219
#define  AMPPNL_RING_TYPE_2               220     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1425              221
#define  AMPPNL_TEXTMSG_1495              222
#define  AMPPNL_TEXTMSG_1426              223
#define  AMPPNL_TEXTMSG_1469              224
#define  AMPPNL_TEXTMSG_1427              225
#define  AMPPNL_TEXTMSG_1428              226
#define  AMPPNL_TEXTMSG_1429              227
#define  AMPPNL_TEXTMSG_1430              228
#define  AMPPNL_TEXTMSG_1431              229
#define  AMPPNL_TEXTMSG_1432              230
#define  AMPPNL_TEXTMSG_1496              231
#define  AMPPNL_TEXTMSG_1433              232
#define  AMPPNL_TEXTMSG_1434              233
#define  AMPPNL_TEXTMSG_1435              234
#define  AMPPNL_TEXTMSG_1436              235
#define  AMPPNL_RING_ID_0                 236     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_TEXTMSG_1437              237
#define  AMPPNL_RING_TYPE_0               238     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1438              239
#define  AMPPNL_TEXTMSG_1439              240
#define  AMPPNL_TEXTMSG_1440              241
#define  AMPPNL_TEXTMSG_1441              242
#define  AMPPNL_TEXTMSG_1442              243
#define  AMPPNL_TEXTMSG_1444              244
#define  AMPPNL_TEXTMSG_1447              245
#define  AMPPNL_RING_TYPE_1               246     /* callback function: OnCmdAMP */
#define  AMPPNL_RING_ID_1                 247     /* callback function: OnCmdAMP_Check */
#define  AMPPNL_TEXTMSG_1104              248
#define  AMPPNL_TEXTMSG_1105              249
#define  AMPPNL_TEXTMSG_1106              250
#define  AMPPNL_TEXTMSG_917               251
#define  AMPPNL_TEXTMSG_979               252
#define  AMPPNL_TEXTMSG_1107              253
#define  AMPPNL_TEXTMSG_1301              254
#define  AMPPNL_TEXTMSG_1302              255
#define  AMPPNL_TEXTMSG_1108              256
#define  AMPPNL_TEXTMSG_1109              257
#define  AMPPNL_TEXTMSG_1111              258
#define  AMPPNL_TEXTMSG_1497              259
#define  AMPPNL_TEXTMSG_1112              260
#define  AMPPNL_TEXTMSG_1113              261
#define  AMPPNL_TEXTMSG_1114              262
#define  AMPPNL_TEXTMSG_918               263
#define  AMPPNL_TEXTMSG_1332              264
#define  AMPPNL_TEXTMSG_1333              265
#define  AMPPNL_TEXTMSG_1334              266
#define  AMPPNL_TEXTMSG_919               267
#define  AMPPNL_TEXTMSG_1335              268
#define  AMPPNL_TEXTMSG_1303              269
#define  AMPPNL_TEXTMSG_1336              270
#define  AMPPNL_TEXTMSG_1115              271
#define  AMPPNL_TEXTMSG_1337              272
#define  AMPPNL_TEXTMSG_1304              273
#define  AMPPNL_NMR_ALC_19                274     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_ALC_18                275     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_ALC_17                276     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_ALC_16                277     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_ALC_15                278     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_ALC_14                279     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_ALC_13                280     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_ALC_12                281     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_ALC_11                282     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_ALC_10                283     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_ALC_9                 284     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_ALC_8                 285     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_ALC_7                 286     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_TEMP_19               287
#define  AMPPNL_NMR_TEMP_18               288
#define  AMPPNL_NMR_TEMP_17               289
#define  AMPPNL_NMR_TEMP_16               290
#define  AMPPNL_NMR_TEMP_15               291
#define  AMPPNL_NMR_TEMP_14               292
#define  AMPPNL_NMR_TEMP_13               293
#define  AMPPNL_NMR_TEMP_12               294
#define  AMPPNL_NMR_TEMP_11               295
#define  AMPPNL_NMR_TEMP_10               296
#define  AMPPNL_NMR_TEMP_9                297
#define  AMPPNL_NMR_TEMP_8                298
#define  AMPPNL_NMR_TEMP_7                299
#define  AMPPNL_NMR_TEMP_6                300
#define  AMPPNL_NMR_TEMP_5                301
#define  AMPPNL_NMR_TEMP_4                302
#define  AMPPNL_NMR_TEMP_3                303
#define  AMPPNL_NMR_TEMP_2                304
#define  AMPPNL_NMR_TEMP_0                305
#define  AMPPNL_NMR_TEMP_1                306
#define  AMPPNL_NMR_ALC_6                 307     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_ALC_5                 308     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1338              309
#define  AMPPNL_NMR_ALC_4                 310     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_PWR_19                311
#define  AMPPNL_NMR_OutATTN_19            312
#define  AMPPNL_NMR_ALC_3                 313     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_PWR_18                314
#define  AMPPNL_LED_ASD_19                315
#define  AMPPNL_LED_ALM_19                316
#define  AMPPNL_NMR_OutATTN_18            317
#define  AMPPNL_TEXTMSG_1339              318
#define  AMPPNL_NMR_PWR_17                319
#define  AMPPNL_LED_ASD_18                320
#define  AMPPNL_LED_ALM_18                321
#define  AMPPNL_NMR_OutATTN_17            322
#define  AMPPNL_NMR_ALC_2                 323     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_PWR_16                324
#define  AMPPNL_LED_ASD_17                325
#define  AMPPNL_LED_ALM_17                326
#define  AMPPNL_NMR_OutATTN_16            327
#define  AMPPNL_TEXTMSG_1340              328
#define  AMPPNL_NMR_PWR_15                329
#define  AMPPNL_LED_ASD_16                330
#define  AMPPNL_LED_ALM_16                331
#define  AMPPNL_NMR_OutATTN_15            332
#define  AMPPNL_TEXTMSG_1341              333
#define  AMPPNL_NMR_PWR_14                334
#define  AMPPNL_LED_ASD_15                335
#define  AMPPNL_LED_ALM_15                336
#define  AMPPNL_NMR_OutATTN_14            337
#define  AMPPNL_TEXTMSG_1342              338
#define  AMPPNL_NMR_PWR_13                339
#define  AMPPNL_LED_ASD_14                340
#define  AMPPNL_LED_ALM_14                341
#define  AMPPNL_NMR_OutATTN_13            342
#define  AMPPNL_TEXTMSG_1343              343
#define  AMPPNL_NMR_PWR_12                344
#define  AMPPNL_LED_ASD_13                345
#define  AMPPNL_LED_ALM_13                346
#define  AMPPNL_NMR_OutATTN_12            347
#define  AMPPNL_NMR_PWR_11                348
#define  AMPPNL_LED_ASD_12                349
#define  AMPPNL_LED_ALM_12                350
#define  AMPPNL_NMR_OutATTN_11            351
#define  AMPPNL_TEXTMSG_1344              352
#define  AMPPNL_NMR_PWR_10                353
#define  AMPPNL_LED_ASD_11                354
#define  AMPPNL_LED_ALM_11                355
#define  AMPPNL_NMR_OutATTN_10            356
#define  AMPPNL_NMR_ATTN_19               357     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_PWR_9                 358
#define  AMPPNL_LED_ASD_10                359
#define  AMPPNL_LED_ALM_10                360
#define  AMPPNL_NMR_OutATTN_9             361
#define  AMPPNL_NMR_ATTN_18               362     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_PWR_8                 363
#define  AMPPNL_LED_ASD_9                 364
#define  AMPPNL_LED_ALM_9                 365
#define  AMPPNL_NMR_OutATTN_8             366
#define  AMPPNL_TEXTMSG_1345              367
#define  AMPPNL_NMR_PWR_7                 368
#define  AMPPNL_LED_ASD_8                 369
#define  AMPPNL_LED_ALM_8                 370
#define  AMPPNL_NMR_OutATTN_7             371
#define  AMPPNL_NMR_ATTN_17               372     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_PWR_6                 373
#define  AMPPNL_LED_ASD_7                 374
#define  AMPPNL_LED_ALM_7                 375
#define  AMPPNL_NMR_OutATTN_6             376
#define  AMPPNL_TEXTMSG_1346              377
#define  AMPPNL_NMR_PWR_5                 378
#define  AMPPNL_LED_ASD_6                 379
#define  AMPPNL_LED_ALM_6                 380
#define  AMPPNL_NMR_OutATTN_5             381
#define  AMPPNL_NMR_ATTN_16               382     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_PWR_4                 383
#define  AMPPNL_LED_ASD_5                 384
#define  AMPPNL_LED_ALM_5                 385
#define  AMPPNL_NMR_PWR_0                 386
#define  AMPPNL_NMR_OutATTN_4             387
#define  AMPPNL_NMR_OutATTN_0             388
#define  AMPPNL_NMR_PWR_3                 389
#define  AMPPNL_LED_ASD_4                 390
#define  AMPPNL_LED_ASD_0                 391
#define  AMPPNL_LED_ALM_0                 392
#define  AMPPNL_LED_ALM_4                 393
#define  AMPPNL_NMR_OutATTN_3             394
#define  AMPPNL_NMR_ATTN_15               395     /* callback function: OnCmdAMP */
#define  AMPPNL_NMR_PWR_2                 396
#define  AMPPNL_LED_ASD_3                 397
#define  AMPPNL_LED_ALM_3                 398
#define  AMPPNL_NMR_OutATTN_2             399
#define  AMPPNL_TEXTMSG_1347              400
#define  AMPPNL_NMR_PWR_1                 401
#define  AMPPNL_TEXTMSG_1498              402
#define  AMPPNL_LED_ASD_2                 403
#define  AMPPNL_LED_ALM_2                 404
#define  AMPPNL_NMR_OutATTN_1             405
#define  AMPPNL_TEXTMSG_1118              406
#define  AMPPNL_NMR_ATTN_14               407     /* callback function: OnCmdAMP */
#define  AMPPNL_LED_ASD_1                 408
#define  AMPPNL_LED_ALM_1                 409
#define  AMPPNL_TEXTMSG_1120              410
#define  AMPPNL_NMR_ATTN_13               411     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1348              412
#define  AMPPNL_NMR_ATTN_12               413     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1349              414
#define  AMPPNL_NMR_ATTN_11               415     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1350              416
#define  AMPPNL_NMR_ATTN_10               417     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1122              418
#define  AMPPNL_TEXTMSG_1351              419
#define  AMPPNL_NMR_ATTN_9                420     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1124              421
#define  AMPPNL_TEXTMSG_1352              422
#define  AMPPNL_NMR_ATTN_8                423     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1126              424
#define  AMPPNL_TEXTMSG_1353              425
#define  AMPPNL_NMR_ATTN_7                426     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1128              427
#define  AMPPNL_NMR_ALC_0                 428     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1354              429
#define  AMPPNL_NMR_ATTN_6                430     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_975               431
#define  AMPPNL_TEXTMSG_1355              432
#define  AMPPNL_NMR_ATTN_5                433     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1130              434
#define  AMPPNL_TEXTMSG_1499              435
#define  AMPPNL_TEXTMSG_1132              436
#define  AMPPNL_NMR_ATTN_4                437     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1133              438
#define  AMPPNL_TEXTMSG_1356              439
#define  AMPPNL_NMR_ATTN_3                440     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1500              441
#define  AMPPNL_TEXTMSG_1358              442
#define  AMPPNL_NMR_ATTN_2                443     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_1359              444
#define  AMPPNL_TEXTMSG_1305              445
#define  AMPPNL_TEXTMSG_1306              446
#define  AMPPNL_NMR_ALC_1                 447     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_897               448
#define  AMPPNL_TEXTMSG_1138              449
#define  AMPPNL_TEXTMSG_898               450
#define  AMPPNL_TEXTMSG_1307              451
#define  AMPPNL_TEXTMSG_1501              452
#define  AMPPNL_TEXTMSG_987               453
#define  AMPPNL_TEXTMSG_1308              454
#define  AMPPNL_TEXTMSG_899               455
#define  AMPPNL_TEXTMSG_1309              456
#define  AMPPNL_TEXTMSG_981               457
#define  AMPPNL_TEXTMSG_1310              458
#define  AMPPNL_TEXTMSG_976               459
#define  AMPPNL_TEXTMSG_1311              460
#define  AMPPNL_TEXTMSG_900               461
#define  AMPPNL_TEXTMSG_1312              462
#define  AMPPNL_TEXTMSG_1313              463
#define  AMPPNL_TEXTMSG_1314              464
#define  AMPPNL_TEXTMSG_970               465
#define  AMPPNL_TEXTMSG_1315              466
#define  AMPPNL_TEXTMSG_901               467
#define  AMPPNL_TEXTMSG_902               468
#define  AMPPNL_NMR_ATTN_0                469     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_903               470
#define  AMPPNL_TEXTMSG_923               471
#define  AMPPNL_TEXTMSG_924               472
#define  AMPPNL_TEXTMSG_925               473
#define  AMPPNL_TEXTMSG_1502              474
#define  AMPPNL_TEXTMSG_926               475
#define  AMPPNL_TEXTMSG_988               476
#define  AMPPNL_TEXTMSG_922               477
#define  AMPPNL_TEXTMSG_982               478
#define  AMPPNL_TEXTMSG_927               479
#define  AMPPNL_TEXTMSG_928               480
#define  AMPPNL_TEXTMSG_920               481
#define  AMPPNL_TEXTMSG_977               482
#define  AMPPNL_TEXTMSG_1503              483
#define  AMPPNL_TEXTMSG_929               484
#define  AMPPNL_TEXTMSG_930               485
#define  AMPPNL_TEXTMSG_931               486
#define  AMPPNL_TEXTMSG_895               487
#define  AMPPNL_TEXTMSG_971               488
#define  AMPPNL_TEXTMSG_894               489
#define  AMPPNL_TEXTMSG_932               490
#define  AMPPNL_TEXTMSG_893               491
#define  AMPPNL_TEXTMSG_973               492
#define  AMPPNL_NMR_ATTN_1                493     /* callback function: OnCmdAMP */
#define  AMPPNL_TEXTMSG_989               494
#define  AMPPNL_TEXTMSG_888               495
#define  AMPPNL_TEXTMSG_983               496
#define  AMPPNL_TEXTMSG_967               497
#define  AMPPNL_TEXTMSG_892               498
#define  AMPPNL_TEXTMSG_912               499
#define  AMPPNL_TEXTMSG_891               500
#define  AMPPNL_TEXTMSG_913               501
#define  AMPPNL_TEXTMSG_890               502
#define  AMPPNL_TEXTMSG_985               503
#define  AMPPNL_TEXTMSG_889               504
#define  AMPPNL_TEXTMSG_914               505
#define  AMPPNL_TEXTMSG                   506
#define  AMPPNL_TEXTMSG_915               507
#define  AMPPNL_TEXTMSG_916               508
#define  AMPPNL_TEXTMSG_1134              509
#define  AMPPNL_TEXTMSG_969               510
#define  AMPPNL_TEXTMSG_1135              511
#define  AMPPNL_TEXTMSG_896               512
#define  AMPPNL_TEXTMSG_1137              513

#define  BROPNL                           2
#define  BROPNL_BTN_SET                   2       /* callback function: OnCmdBOARD */
#define  BROPNL_BTN_STS                   3       /* callback function: OnCmdBOARD */
#define  BROPNL_BTN_REQ                   4       /* callback function: OnCmdBOARD */
#define  BROPNL_BTN_CLOSE                 5       /* callback function: OnCmdBOARD */
#define  BROPNL_DECORATION_35             6
#define  BROPNL_TEXTMSG_1528              7
#define  BROPNL_TEXTMSG_1506              8
#define  BROPNL_TEXTMSG_1464              9
#define  BROPNL_TEXTMSG_1422              10
#define  BROPNL_TEXTMSG_1380              11
#define  BROPNL_TEXTMSG_1507              12
#define  BROPNL_TEXTMSG_1508              13
#define  BROPNL_TEXTMSG_1465              14
#define  BROPNL_TEXTMSG_1466              15
#define  BROPNL_TEXTMSG_1509              16
#define  BROPNL_TEXTMSG_1510              17
#define  BROPNL_TEXTMSG_1467              18
#define  BROPNL_TEXTMSG_1468              19
#define  BROPNL_TEXTMSG_1469              20
#define  BROPNL_TEXTMSG_1470              21
#define  BROPNL_TEXTMSG_1471              22
#define  BROPNL_TEXTMSG_1472              23
#define  BROPNL_TEXTMSG_1473              24
#define  BROPNL_TEXTMSG_1474              25
#define  BROPNL_TEXTMSG_1475              26
#define  BROPNL_TEXTMSG_1476              27
#define  BROPNL_TEXTMSG_1477              28
#define  BROPNL_TEXTMSG_1515              29
#define  BROPNL_TEXTMSG_1478              30
#define  BROPNL_TEXTMSG_1479              31
#define  BROPNL_TEXTMSG_1480              32
#define  BROPNL_TEXTMSG_1481              33
#define  BROPNL_TEXTMSG_1482              34
#define  BROPNL_TEXTMSG_1516              35
#define  BROPNL_TEXTMSG_1517              36
#define  BROPNL_TEXTMSG_1518              37
#define  BROPNL_TEXTMSG_1519              38
#define  BROPNL_TEXTMSG_1520              39
#define  BROPNL_TEXTMSG_1521              40
#define  BROPNL_TEXTMSG_1483              41
#define  BROPNL_TEXTMSG_1527              42
#define  BROPNL_TEXTMSG_1529              43
#define  BROPNL_TEXTMSG_1484              44
#define  BROPNL_TEXTMSG_1485              45
#define  BROPNL_TEXTMSG_1486              46
#define  BROPNL_TEXTMSG_1487              47
#define  BROPNL_TEXTMSG_1488              48
#define  BROPNL_RING_NUM_26               49      /* callback function: OnCmdBOARD */
#define  BROPNL_RING_NUM_24               50      /* callback function: OnCmdBOARD */
#define  BROPNL_TEXTMSG_1489              51
#define  BROPNL_TEXTMSG_1490              52
#define  BROPNL_TEXTMSG_1491              53
#define  BROPNL_TEXTMSG_1492              54
#define  BROPNL_TEXTMSG_1493              55
#define  BROPNL_TEXTMSG_1494              56
#define  BROPNL_TEXTMSG_1495              57
#define  BROPNL_TEXTMSG_1496              58
#define  BROPNL_TEXTMSG_1497              59
#define  BROPNL_TEXTMSG_1498              60
#define  BROPNL_TEXTMSG_1499              61
#define  BROPNL_TEXTMSG_1500              62
#define  BROPNL_TEXTMSG_1501              63
#define  BROPNL_TEXTMSG_1502              64
#define  BROPNL_TEXTMSG_1503              65
#define  BROPNL_TEXTMSG_1504              66
#define  BROPNL_RING_NUM_25               67      /* callback function: OnCmdBOARD */
#define  BROPNL_TEXTMSG_1293              68
#define  BROPNL_RING_NUM_23               69      /* callback function: OnCmdBOARD */
#define  BROPNL_TEXTMSG_1505              70
#define  BROPNL_RING_NUM_22               71      /* callback function: OnCmdBOARD */
#define  BROPNL_TEXTMSG_1423              72
#define  BROPNL_TEXTMSG_1424              73
#define  BROPNL_TEXTMSG_1425              74
#define  BROPNL_TEXTMSG_1426              75
#define  BROPNL_TEXTMSG_1427              76
#define  BROPNL_TEXTMSG_1428              77
#define  BROPNL_TEXTMSG_1429              78
#define  BROPNL_TEXTMSG_1430              79
#define  BROPNL_TEXTMSG_1431              80
#define  BROPNL_TEXTMSG_1432              81
#define  BROPNL_TEXTMSG_1433              82
#define  BROPNL_TEXTMSG_1434              83
#define  BROPNL_TEXTMSG_1435              84
#define  BROPNL_TEXTMSG_1436              85
#define  BROPNL_TEXTMSG_1437              86
#define  BROPNL_TEXTMSG_1438              87
#define  BROPNL_TEXTMSG_1439              88
#define  BROPNL_TEXTMSG_1440              89
#define  BROPNL_TEXTMSG_1441              90
#define  BROPNL_TEXTMSG_1442              91
#define  BROPNL_TEXTMSG_1443              92
#define  BROPNL_TEXTMSG_1444              93
#define  BROPNL_TEXTMSG_1445              94
#define  BROPNL_TEXTMSG_1446              95
#define  BROPNL_TEXTMSG_1447              96
#define  BROPNL_TEXTMSG_1448              97
#define  BROPNL_TEXTMSG_1449              98
#define  BROPNL_TEXTMSG_1450              99
#define  BROPNL_TEXTMSG_1451              100
#define  BROPNL_TEXTMSG_1452              101
#define  BROPNL_TEXTMSG_1453              102
#define  BROPNL_TEXTMSG_1454              103
#define  BROPNL_TEXTMSG_1455              104
#define  BROPNL_TEXTMSG_1456              105
#define  BROPNL_TEXTMSG_1457              106
#define  BROPNL_TEXTMSG_1458              107
#define  BROPNL_TEXTMSG_1459              108
#define  BROPNL_TEXTMSG_1460              109
#define  BROPNL_TEXTMSG_1461              110
#define  BROPNL_TEXTMSG_1462              111
#define  BROPNL_DECORATION_99             112
#define  BROPNL_TEXTMSG_1463              113
#define  BROPNL_RING_NUM_21               114     /* callback function: OnCmdBOARD */
#define  BROPNL_TEXTMSG_1381              115
#define  BROPNL_TEXTMSG_1382              116
#define  BROPNL_TEXTMSG_1383              117
#define  BROPNL_TEXTMSG_1384              118
#define  BROPNL_TEXTMSG_1385              119
#define  BROPNL_TEXTMSG_1386              120
#define  BROPNL_TEXTMSG_1387              121
#define  BROPNL_TEXTMSG_1388              122
#define  BROPNL_TEXTMSG_1389              123
#define  BROPNL_TEXTMSG_1390              124
#define  BROPNL_TEXTMSG_1391              125
#define  BROPNL_TEXTMSG_1392              126
#define  BROPNL_TEXTMSG_1393              127
#define  BROPNL_TEXTMSG_1394              128
#define  BROPNL_TEXTMSG_1395              129
#define  BROPNL_TEXTMSG_1396              130
#define  BROPNL_TEXTMSG_1397              131
#define  BROPNL_TEXTMSG_1398              132
#define  BROPNL_TEXTMSG_1399              133
#define  BROPNL_TEXTMSG_1400              134
#define  BROPNL_TEXTMSG_1401              135
#define  BROPNL_TEXTMSG_1402              136
#define  BROPNL_TEXTMSG_1403              137
#define  BROPNL_TEXTMSG_1404              138
#define  BROPNL_TEXTMSG_1405              139
#define  BROPNL_TEXTMSG_1406              140
#define  BROPNL_TEXTMSG_1407              141
#define  BROPNL_TEXTMSG_1408              142
#define  BROPNL_TEXTMSG_1409              143
#define  BROPNL_TEXTMSG_1410              144
#define  BROPNL_TEXTMSG_1411              145
#define  BROPNL_TEXTMSG_1412              146
#define  BROPNL_TEXTMSG_1413              147
#define  BROPNL_TEXTMSG_1414              148
#define  BROPNL_TEXTMSG_1415              149
#define  BROPNL_TEXTMSG_1416              150
#define  BROPNL_TEXTMSG_1417              151
#define  BROPNL_TEXTMSG_1418              152
#define  BROPNL_TEXTMSG_1419              153
#define  BROPNL_TEXTMSG_1420              154
#define  BROPNL_STR_PATH                  155
#define  BROPNL_RING_NUM_20               156     /* callback function: OnCmdBOARD */
#define  BROPNL_TEXTMSG_1421              157
#define  BROPNL_RING_ID_1                 158     /* callback function: OnCmdBOARD */
#define  BROPNL_TEXTMSG_1360              159
#define  BROPNL_TEXTMSG_1361              160
#define  BROPNL_TEXTMSG_1108              161
#define  BROPNL_TEXTMSG_1109              162
#define  BROPNL_TEXTMSG_1362              163
#define  BROPNL_TEXTMSG_1363              164
#define  BROPNL_TEXTMSG_1113              165
#define  BROPNL_TEXTMSG_1114              166
#define  BROPNL_TEXTMSG_1364              167
#define  BROPNL_TEXTMSG_1115              168
#define  BROPNL_TEXTMSG_1365              169
#define  BROPNL_TEXTMSG_1366              170
#define  BROPNL_TEXTMSG_1367              171
#define  BROPNL_TEXTMSG_1117              172
#define  BROPNL_TEXTMSG_1120              173
#define  BROPNL_TEXTMSG_1122              174
#define  BROPNL_TEXTMSG_1368              175
#define  BROPNL_TEXTMSG_1369              176
#define  BROPNL_TEXTMSG_1370              177
#define  BROPNL_TEXTMSG_1371              178
#define  BROPNL_TEXTMSG_1372              179
#define  BROPNL_TEXTMSG_1373              180
#define  BROPNL_TEXTMSG_1374              181
#define  BROPNL_TEXTMSG_976               182
#define  BROPNL_TEXTMSG_1375              183
#define  BROPNL_TEXTMSG_1376              184
#define  BROPNL_TEXTMSG_1377              185
#define  BROPNL_TEXTMSG_1378              186
#define  BROPNL_TEXTMSG_1379              187
#define  BROPNL_TEXTMSG_970               188
#define  BROPNL_TEXTMSG_923               189
#define  BROPNL_TEXTMSG_924               190
#define  BROPNL_TEXTMSG_925               191
#define  BROPNL_TEXTMSG_926               192
#define  BROPNL_TEXTMSG_988               193
#define  BROPNL_TEXTMSG_982               194
#define  BROPNL_TEXTMSG_927               195
#define  BROPNL_TEXTMSG_928               196
#define  BROPNL_TEXTMSG_929               197
#define  BROPNL_TEXTMSG_930               198
#define  BROPNL_TEXTMSG_931               199
#define  BROPNL_TEXTMSG_932               200
#define  BROPNL_RING_NUM_19               201     /* callback function: OnCmdBOARD */

#define  CHAPNL                           3
#define  CHAPNL_RING_NUM_39               2       /* callback function: CtrlCHACheck */
#define  CHAPNL_RING_NUM_38               3       /* callback function: CtrlCHACheck */
#define  CHAPNL_RING_NUM_37               4       /* callback function: CtrlCHACheck */
#define  CHAPNL_RING_NUM_36               5       /* callback function: CtrlCHACheck */
#define  CHAPNL_RING_NUM_35               6       /* callback function: CtrlCHACheck */
#define  CHAPNL_RING_NUM_34               7       /* callback function: CtrlCHACheck */
#define  CHAPNL_RING_NUM_33               8       /* callback function: CtrlCHACheck */
#define  CHAPNL_RING_NUM_32               9       /* callback function: CtrlCHACheck */
#define  CHAPNL_RING_NUM_31               10      /* callback function: CtrlCHACheck */
#define  CHAPNL_RING_NUM_30               11      /* callback function: CtrlCHACheck */
#define  CHAPNL_RING_NUM_29               12      /* callback function: CtrlCHACheck */
#define  CHAPNL_RING_NUM_28               13      /* callback function: CtrlCHACheck */
#define  CHAPNL_RING_NUM_27               14      /* callback function: CtrlCHACheck */
#define  CHAPNL_BTN_STS                   15      /* callback function: OnCmdCHA */
#define  CHAPNL_RING_NUM_26               16      /* callback function: CtrlCHACheck */
#define  CHAPNL_TG_CH_39                  17
#define  CHAPNL_NMR_GAIN_39               18      /* callback function: OnCmdCHA */
#define  CHAPNL_RING_NUM_25               19      /* callback function: CtrlCHACheck */
#define  CHAPNL_TG_CH_37                  20
#define  CHAPNL_NMR_GAIN_37               21      /* callback function: OnCmdCHA */
#define  CHAPNL_RING_NUM_24               22      /* callback function: CtrlCHACheck */
#define  CHAPNL_TEXTMSG_1232              23
#define  CHAPNL_TG_CH_38                  24
#define  CHAPNL_NMR_GAIN_38               25      /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1233              26
#define  CHAPNL_TEXTMSG_1234              27
#define  CHAPNL_TEXTMSG_1235              28
#define  CHAPNL_TG_CH_31                  29
#define  CHAPNL_RING_NUM_23               30      /* callback function: CtrlCHACheck */
#define  CHAPNL_TEXTMSG_1236              31
#define  CHAPNL_NMR_GAIN_31               32      /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1237              33
#define  CHAPNL_TEXTMSG_1238              34
#define  CHAPNL_TG_CH_36                  35
#define  CHAPNL_NMR_GAIN_36               36      /* callback function: OnCmdCHA */
#define  CHAPNL_RING_NUM_22               37      /* callback function: CtrlCHACheck */
#define  CHAPNL_TEXTMSG_1239              38
#define  CHAPNL_TEXTMSG_1240              39
#define  CHAPNL_NMR_GAIN_35               40      /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1241              41
#define  CHAPNL_RING_NUM_21               42      /* callback function: CtrlCHACheck */
#define  CHAPNL_TG_CH_35                  43
#define  CHAPNL_TEXTMSG_1242              44
#define  CHAPNL_NMR_GAIN_34               45      /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1243              46
#define  CHAPNL_TEXTMSG_1244              47
#define  CHAPNL_TG_CH_34                  48
#define  CHAPNL_RING_NUM_20               49      /* callback function: CtrlCHACheck */
#define  CHAPNL_TEXTMSG_1245              50
#define  CHAPNL_NMR_GAIN_33               51      /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1246              52
#define  CHAPNL_TEXTMSG_1247              53
#define  CHAPNL_TG_CH_33                  54
#define  CHAPNL_RING_NUM_19               55      /* callback function: CtrlCHACheck */
#define  CHAPNL_TEXTMSG_1248              56
#define  CHAPNL_TEXTMSG_1249              57
#define  CHAPNL_NMR_GAIN_32               58      /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1250              59
#define  CHAPNL_RING_NUM_18               60      /* callback function: CtrlCHACheck */
#define  CHAPNL_TG_CH_32                  61
#define  CHAPNL_TEXTMSG_1251              62
#define  CHAPNL_TEXTMSG_1252              63
#define  CHAPNL_NMR_GAIN_25               64      /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1253              65
#define  CHAPNL_TG_CH_25                  66
#define  CHAPNL_NMR_GAIN_30               67      /* callback function: OnCmdCHA */
#define  CHAPNL_RING_NUM_17               68      /* callback function: CtrlCHACheck */
#define  CHAPNL_TEXTMSG_1254              69
#define  CHAPNL_TG_CH_30                  70
#define  CHAPNL_RING_NUM_16               71      /* callback function: CtrlCHACheck */
#define  CHAPNL_TEXTMSG_1255              72
#define  CHAPNL_TEXTMSG_1256              73
#define  CHAPNL_TEXTMSG_1257              74
#define  CHAPNL_NMR_GAIN_29               75      /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1258              76
#define  CHAPNL_TEXTMSG_1259              77
#define  CHAPNL_RING_NUM_15               78      /* callback function: CtrlCHACheck */
#define  CHAPNL_NMR_GAIN_28               79      /* callback function: OnCmdCHA */
#define  CHAPNL_TG_CH_29                  80
#define  CHAPNL_TEXTMSG_1260              81
#define  CHAPNL_TEXTMSG_1261              82
#define  CHAPNL_TEXTMSG_1262              83
#define  CHAPNL_RING_NUM_14               84      /* callback function: CtrlCHACheck */
#define  CHAPNL_TG_CH_28                  85
#define  CHAPNL_NMR_GAIN_27               86      /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1263              87
#define  CHAPNL_TEXTMSG_1264              88
#define  CHAPNL_TEXTMSG_1265              89
#define  CHAPNL_RING_NUM_13               90      /* callback function: CtrlCHACheck */
#define  CHAPNL_NMR_GAIN_26               91      /* callback function: OnCmdCHA */
#define  CHAPNL_TG_CH_27                  92
#define  CHAPNL_TEXTMSG_1266              93
#define  CHAPNL_TEXTMSG_1267              94
#define  CHAPNL_TEXTMSG_1268              95
#define  CHAPNL_RING_NUM_12               96      /* callback function: CtrlCHACheck */
#define  CHAPNL_NMR_GAIN_24               97      /* callback function: OnCmdCHA */
#define  CHAPNL_TG_CH_26                  98
#define  CHAPNL_TEXTMSG_1269              99
#define  CHAPNL_NMR_GAIN_19               100     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1270              101
#define  CHAPNL_TEXTMSG_1271              102
#define  CHAPNL_TG_CH_19                  103
#define  CHAPNL_TEXTMSG_1272              104
#define  CHAPNL_RING_NUM_11               105     /* callback function: CtrlCHACheck */
#define  CHAPNL_TG_CH_24                  106
#define  CHAPNL_TEXTMSG_1273              107
#define  CHAPNL_NMR_GAIN_23               108     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1274              109
#define  CHAPNL_TEXTMSG_1275              110
#define  CHAPNL_TEXTMSG_1276              111
#define  CHAPNL_RING_NUM_10               112     /* callback function: CtrlCHACheck */
#define  CHAPNL_NMR_GAIN_22               113     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1277              114
#define  CHAPNL_TG_CH_23                  115
#define  CHAPNL_TEXTMSG_1278              116
#define  CHAPNL_TEXTMSG_1279              117
#define  CHAPNL_TEXTMSG_1280              118
#define  CHAPNL_TG_CH_22                  119
#define  CHAPNL_NMR_GAIN_21               120     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1281              121
#define  CHAPNL_TEXTMSG_1282              122
#define  CHAPNL_TEXTMSG_1283              123
#define  CHAPNL_TEXTMSG_1284              124
#define  CHAPNL_TG_CH_21                  125
#define  CHAPNL_RING_NUM_9                126     /* callback function: CtrlCHACheck */
#define  CHAPNL_TEXTMSG_1285              127
#define  CHAPNL_TEXTMSG_1286              128
#define  CHAPNL_TEXTMSG_1287              129
#define  CHAPNL_TEXTMSG_1288              130
#define  CHAPNL_NMR_GAIN_20               131     /* callback function: OnCmdCHA */
#define  CHAPNL_TG_CH_20                  132
#define  CHAPNL_RING_NUM_8                133     /* callback function: CtrlCHACheck */
#define  CHAPNL_NMR_OUT_PWR               134     /* callback function: OnCmdCHA */
#define  CHAPNL_RING_NUM_7                135     /* callback function: CtrlCHACheck */
#define  CHAPNL_TEXTMSG_1289              136
#define  CHAPNL_TEXTMSG_1290              137
#define  CHAPNL_NMR_GAIN_17               138     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1291              139
#define  CHAPNL_TEXTMSG_1292              140
#define  CHAPNL_RING_NUM_6                141     /* callback function: CtrlCHACheck */
#define  CHAPNL_TG_CH_17                  142
#define  CHAPNL_DECORATION_105            143
#define  CHAPNL_TEXTMSG_1293              144
#define  CHAPNL_RING_NUM_5                145     /* callback function: CtrlCHACheck */
#define  CHAPNL_TEXTMSG_1294              146
#define  CHAPNL_NMR_GAIN_18               147     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1295              148
#define  CHAPNL_TEXTMSG_1296              149
#define  CHAPNL_TEXTMSG_1297              150
#define  CHAPNL_RING_NUM_4                151     /* callback function: CtrlCHACheck */
#define  CHAPNL_NMR_GAIN_11               152     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1298              153
#define  CHAPNL_TEXTMSG_1226              154
#define  CHAPNL_TG_CH_18                  155
#define  CHAPNL_TEXTMSG_1227              156
#define  CHAPNL_TEXTMSG_1228              157
#define  CHAPNL_NMR_GAIN_16               158     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1229              159
#define  CHAPNL_TG_CH_11                  160
#define  CHAPNL_RING_NUM_3                161     /* callback function: CtrlCHACheck */
#define  CHAPNL_TEXTMSG_1208              162
#define  CHAPNL_TEXTMSG_1230              163
#define  CHAPNL_TEXTMSG_1231              164
#define  CHAPNL_NMR_GAIN_15               165     /* callback function: OnCmdCHA */
#define  CHAPNL_TG_CH_16                  166
#define  CHAPNL_NMR_GAIN_14               167     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1209              168
#define  CHAPNL_TEXTMSG_1210              169
#define  CHAPNL_RING_NUM_2                170     /* callback function: CtrlCHACheck */
#define  CHAPNL_TEXTMSG_1211              171
#define  CHAPNL_TG_CH_15                  172
#define  CHAPNL_TEXTMSG_1212              173
#define  CHAPNL_NMR_GAIN_13               174     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1213              175
#define  CHAPNL_TEXTMSG_1214              176
#define  CHAPNL_TG_CH_14                  177
#define  CHAPNL_RING_NUM_1                178     /* callback function: CtrlCHACheck */
#define  CHAPNL_TEXTMSG_1215              179
#define  CHAPNL_NMR_GAIN_12               180     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1216              181
#define  CHAPNL_TEXTMSG_1217              182
#define  CHAPNL_TG_CH_13                  183
#define  CHAPNL_TEXTMSG_1218              184
#define  CHAPNL_RING_TYPE                 185     /* callback function: OnCmdCHA */
#define  CHAPNL_RING_NUM_0                186     /* callback function: CtrlCHACheck */
#define  CHAPNL_TEXTMSG_1219              187
#define  CHAPNL_TEXTMSG_1220              188
#define  CHAPNL_NMR_GAIN_5                189     /* callback function: OnCmdCHA */
#define  CHAPNL_TG_CH_12                  190
#define  CHAPNL_TEXTMSG_1221              191
#define  CHAPNL_NMR_GAIN_10               192     /* callback function: OnCmdCHA */
#define  CHAPNL_BTN_LOAD                  193     /* callback function: OnCmdCHA */
#define  CHAPNL_BTN_SET                   194     /* callback function: OnCmdCHA */
#define  CHAPNL_BTN_SAVE                  195     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1222              196
#define  CHAPNL_TEXTMSG_1223              197
#define  CHAPNL_BTN_REQ                   198     /* callback function: OnCmdCHA */
#define  CHAPNL_TG_CH_5                   199
#define  CHAPNL_NMR_GAIN_9                200     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1190              201
#define  CHAPNL_TG_CH_10                  202
#define  CHAPNL_TEXTMSG_1224              203
#define  CHAPNL_TEXTMSG_1225              204
#define  CHAPNL_TEXTMSG_1191              205
#define  CHAPNL_NMR_GAIN_8                206     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1188              207
#define  CHAPNL_TEXTMSG_1192              208
#define  CHAPNL_TEXTMSG_1193              209
#define  CHAPNL_TG_CH_9                   210
#define  CHAPNL_TEXTMSG_1194              211
#define  CHAPNL_TEXTMSG_1299              212
#define  CHAPNL_NMR_GAIN_7                213     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1189              214
#define  CHAPNL_TEXTMSG_1195              215
#define  CHAPNL_TEXTMSG_1196              216
#define  CHAPNL_TG_CH_8                   217
#define  CHAPNL_TEXTMSG_1197              218
#define  CHAPNL_NMR_GAIN_6                219     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1187              220
#define  CHAPNL_TEXTMSG_1198              221
#define  CHAPNL_TEXTMSG_1199              222
#define  CHAPNL_TG_CH_7                   223
#define  CHAPNL_TEXTMSG_1200              224
#define  CHAPNL_TG_AGC                    225     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1201              226
#define  CHAPNL_NMR_GAIN_4                227     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1202              228
#define  CHAPNL_TG_CH_6                   229
#define  CHAPNL_TEXTMSG_1203              230
#define  CHAPNL_NMR_CFR_THE               231     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1204              232
#define  CHAPNL_TEXTMSG_1205              233
#define  CHAPNL_TG_CFR                    234     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1103              235
#define  CHAPNL_STR_PATH                  236
#define  CHAPNL_NMR_GAIN_3                237     /* callback function: OnCmdCHA */
#define  CHAPNL_TG_CH_4                   238
#define  CHAPNL_TEXTMSG_1206              239
#define  CHAPNL_TEXTMSG_1207              240
#define  CHAPNL_TEXTMSG_1100              241
#define  CHAPNL_BTN_CLOSE                 242     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1104              243
#define  CHAPNL_TEXTMSG_1105              244
#define  CHAPNL_NMR_GAIN_2                245     /* callback function: OnCmdCHA */
#define  CHAPNL_TG_CH_3                   246
#define  CHAPNL_TEXTMSG_1097              247
#define  CHAPNL_DECORATION                248
#define  CHAPNL_TEXTMSG_1101              249
#define  CHAPNL_TEXTMSG_1102              250
#define  CHAPNL_TG_CH_2                   251
#define  CHAPNL_TEXTMSG_1094              252
#define  CHAPNL_TEXTMSG_1090              253
#define  CHAPNL_NMR_GAIN_1                254     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_1098              255
#define  CHAPNL_TEXTMSG_1099              256
#define  CHAPNL_TG_CH_1                   257
#define  CHAPNL_TEXTMSG_1091              258
#define  CHAPNL_TEXTMSG_877               259
#define  CHAPNL_TEXTMSG_1095              260
#define  CHAPNL_TEXTMSG_1096              261
#define  CHAPNL_TG_CH_0                   262
#define  CHAPNL_NMR_GAIN_0                263     /* callback function: OnCmdCHA */
#define  CHAPNL_TEXTMSG_926               264
#define  CHAPNL_TEXTMSG_921               265
#define  CHAPNL_TEXTMSG_1092              266
#define  CHAPNL_TEXTMSG_1093              267
#define  CHAPNL_DECORATION_106            268
#define  CHAPNL_DECORATION_104            269
#define  CHAPNL_DECORATION_103            270
#define  CHAPNL_DECORATION_102            271
#define  CHAPNL_DECORATION_101            272
#define  CHAPNL_TEXTMSG_932               273
#define  CHAPNL_TEXTMSG_888               274
#define  CHAPNL_TEXTMSG_892               275
#define  CHAPNL_TEXTMSG_879               276
#define  CHAPNL_TEXTMSG_1089              277
#define  CHAPNL_TEXTMSG_907               278

#define  DEBPNL                           4
#define  DEBPNL_BTN_EXIT_2                2
#define  DEBPNL_BTN_EXIT                  3       /* callback function: OnCmdDebug */
#define  DEBPNL_BOX                       4
#define  DEBPNL_FORMSEL_1                 5       /* callback function: OnCmdDebug */
#define  DEBPNL_TOPSEL                    6       /* callback function: OnCmdDebug */
#define  DEBPNL_MODESEL                   7       /* callback function: OnCmdDebug */
#define  DEBPNL_BTN_MODECH                8       /* callback function: OnCmdModeChange */
#define  DEBPNL_TMSG_MODE                 9
#define  DEBPNL_BTN_CLEAR                 10      /* callback function: OnCmdDebug */
#define  DEBPNL_BTN_STOP                  11      /* callback function: OnCmdDebug */
#define  DEBPNL_BTN_GO                    12      /* callback function: OnCmdDebug */
#define  DEBPNL_FORMSEL_2                 13      /* callback function: OnCmdDebug */

#define  DLPNL                            5
#define  DLPNL_BTN_EXIT                   2       /* callback function: OnCmdDown */
#define  DLPNL_DLPATH                     3
#define  DLPNL_RING_SYSTEM                4       /* callback function: OnChangeIDRingDn */
#define  DLPNL_DLFRAME                    5
#define  DLPNL_PROGTIME_2                 6
#define  DLPNL_PROGTIME                   7
#define  DLPNL_TIMER_DLRESP               8       /* callback function: DLTimer_Response */
#define  DLPNL_TIMER_DLPROG               9       /* callback function: DLTimer_Progress */
#define  DLPNL_DECORATION_4               10
#define  DLPNL_DECORATION                 11
#define  DLPNL_DECORATION_7               12
#define  DLPNL_DECORATION_2               13
#define  DLPNL_TEXTMSG                    14
#define  DLPNL_DECORATION_5               15
#define  DLPNL_DECORATION_6               16
#define  DLPNL_BTN_DLSTART                17      /* callback function: OnCmdDown */
#define  DLPNL_BTN_FILEOPEN               18      /* callback function: OnCmdDown */
#define  DLPNL_DLSIZE                     19
#define  DLPNL_TEXTMSG_3                  20
#define  DLPNL_TEXTMSG_4                  21
#define  DLPNL_TEXTMSG_5                  22
#define  DLPNL_TEXTMSG_7                  23
#define  DLPNL_TEXTMSG_8                  24
#define  DLPNL_TEXTMSG_9                  25
#define  DLPNL_TEXTMSG_6                  26
#define  DLPNL_TEXTMSG_2                  27

#define  EMCUPNL                          6
#define  EMCUPNL_TEXT_TYPE                2
#define  EMCUPNL_DECORATION_74            3
#define  EMCUPNL_BTN_SET                  4       /* callback function: OnCmdEMCU */
#define  EMCUPNL_BTN_CLOSE                5       /* callback function: OnCmdEMCU */
#define  EMCUPNL_DECPANEL_101             6
#define  EMCUPNL_DECPANEL_100             7
#define  EMCUPNL_DECPANEL_84              8
#define  EMCUPNL_DECORATION               9
#define  EMCUPNL_TEXTMSG_2                10
#define  EMCUPNL_NMR_SERVER1              11
#define  EMCUPNL_NMR_SERVER2              12      /* callback function: IPInfoAutoCheck */
#define  EMCUPNL_NMR_SERVER3              13      /* callback function: IPInfoAutoCheck */
#define  EMCUPNL_NMR_SERVER4              14      /* callback function: IPInfoAutoCheck */
#define  EMCUPNL_TEXTMSG_20               15
#define  EMCUPNL_TEXTMSG_19               16
#define  EMCUPNL_TEXTMSG_21               17
#define  EMCUPNL_TEXTMSG_18               18

#define  ENVPNL                           7
#define  ENVPNL_BTN_SET                   2       /* callback function: OnCmdENV */
#define  ENVPNL_BTN_REQ                   3       /* callback function: OnCmdENV */
#define  ENVPNL_STR_PATH                  4
#define  ENVPNL_BTN_CLOSE                 5       /* callback function: OnCmdENV */
#define  ENVPNL_DECORATION_179            6
#define  ENVPNL_DECORATION_177            7
#define  ENVPNL_DECORATION_176            8
#define  ENVPNL_DECORATION                9
#define  ENVPNL_TEXTMSG_1171              10
#define  ENVPNL_TEXTMSG_1172              11
#define  ENVPNL_TEXTMSG_1173              12
#define  ENVPNL_TEXTMSG_1170              13
#define  ENVPNL_TEXTMSG_1169              14
#define  ENVPNL_TEXTMSG_1168              15
#define  ENVPNL_TEXTMSG_1091              16
#define  ENVPNL_DECORATION_173            17
#define  ENVPNL_DECORATION_174            18
#define  ENVPNL_DECORATION_175            19
#define  ENVPNL_DECORATION_172            20
#define  ENVPNL_DECORATION_171            21
#define  ENVPNL_DECORATION_170            22
#define  ENVPNL_STR_TRS_FPGA              23
#define  ENVPNL_STR_TRS_CPU               24
#define  ENVPNL_STR_DMB_FPGA              25
#define  ENVPNL_STR_DMB_CPU               26
#define  ENVPNL_STR_FM_FPGA               27
#define  ENVPNL_STR_AM_FPGA               28
#define  ENVPNL_STR_FM_CPU                29
#define  ENVPNL_STR_EBU2_FPGA             30
#define  ENVPNL_STR_AM_CPU                31
#define  ENVPNL_STR_EBU1_FPGA             32
#define  ENVPNL_STR_EBU2_CPU              33
#define  ENVPNL_STR_EBU1_CPU              34
#define  ENVPNL_DECORATION_178            35
#define  ENVPNL_DECORATION_154            36
#define  ENVPNL_STR_NAME                  37
#define  ENVPNL_DECPANEL_347              38
#define  ENVPNL_DECPANEL_367              39
#define  ENVPNL_DECPANEL_348              40
#define  ENVPNL_TEXTMSG_1199              41
#define  ENVPNL_STR_CPU                   42
#define  ENVPNL_DECPANEL_353              43
#define  ENVPNL_DECPANEL_354              44
#define  ENVPNL_TEXTMSG_1202              45
#define  ENVPNL_DECPANEL_351              46
#define  ENVPNL_DECPANEL_352              47
#define  ENVPNL_TEXTMSG_1201              48
#define  ENVPNL_DECPANEL_363              49
#define  ENVPNL_DECPANEL_364              50
#define  ENVPNL_DECPANEL_365              51
#define  ENVPNL_TEXTMSG_1207              52
#define  ENVPNL_DECPANEL_359              53
#define  ENVPNL_DECPANEL_366              54
#define  ENVPNL_TEXTMSG_1208              55
#define  ENVPNL_DECPANEL_360              56
#define  ENVPNL_DECPANEL_361              57
#define  ENVPNL_NMR_AC_V                  58
#define  ENVPNL_TEXTMSG_1205              59
#define  ENVPNL_DECPANEL_357              60
#define  ENVPNL_DECPANEL_368              61
#define  ENVPNL_DECPANEL_362              62
#define  ENVPNL_NMR_AC_A                  63
#define  ENVPNL_TEXTMSG_1206              64
#define  ENVPNL_DECPANEL_355              65
#define  ENVPNL_DECPANEL_358              66
#define  ENVPNL_NMR_PSU_N                 67
#define  ENVPNL_TEXTMSG_1204              68
#define  ENVPNL_DECPANEL_349              69
#define  ENVPNL_DECPANEL_356              70
#define  ENVPNL_NMR_PSU_P                 71
#define  ENVPNL_TEXTMSG_1203              72
#define  ENVPNL_NMR_TEMP_LOW              73
#define  ENVPNL_DECPANEL_350              74
#define  ENVPNL_NMR_FAN_OFF               75
#define  ENVPNL_TEXTMSG_1200              76
#define  ENVPNL_DECPANEL_345              77
#define  ENVPNL_NMR_TEMP_HIGH             78
#define  ENVPNL_NMR_FAN_ON                79
#define  ENVPNL_DECPANEL_346              80
#define  ENVPNL_TEXTMSG_1198              81
#define  ENVPNL_DECPANEL_343              82
#define  ENVPNL_TEXTMSG_1214              83
#define  ENVPNL_NMR_TEMP                  84
#define  ENVPNL_TEXTMSG_1212              85
#define  ENVPNL_DECPANEL_344              86
#define  ENVPNL_TEXTMSG_1211              87
#define  ENVPNL_TEXTMSG_1197              88
#define  ENVPNL_RING_TYPE                 89
#define  ENVPNL_NMR_ID                    90      /* callback function: OnCmdHPA */
#define  ENVPNL_TEXTMSG_1209              91
#define  ENVPNL_TEXTMSG_1213              92
#define  ENVPNL_TEXTMSG_1215              93
#define  ENVPNL_TEXTMSG_1210              94
#define  ENVPNL_TEXTMSG_988               95

#define  HPAPNL                           8
#define  HPAPNL_BTN_STS                   2       /* callback function: OnCmdHPA */
#define  HPAPNL_BTN_SET                   3       /* callback function: OnCmdHPA */
#define  HPAPNL_BTN_REQ                   4       /* callback function: OnCmdHPA */
#define  HPAPNL_STR_PATH                  5
#define  HPAPNL_BTN_CLOSE                 6       /* callback function: OnCmdHPA */
#define  HPAPNL_DECORATION                7
#define  HPAPNL_TEXTMSG_1138              8
#define  HPAPNL_TEXTMSG_1122              9
#define  HPAPNL_TEXTMSG_1123              10
#define  HPAPNL_TEXTMSG_1106              11
#define  HPAPNL_TEXTMSG_1107              12
#define  HPAPNL_TEXTMSG_1098              13
#define  HPAPNL_TEXTMSG_1069              14
#define  HPAPNL_TEXTMSG_1146              15
#define  HPAPNL_TEXTMSG_1147              16
#define  HPAPNL_TEXTMSG_1148              17
#define  HPAPNL_TEXTMSG_1149              18
#define  HPAPNL_TEXTMSG_1150              19
#define  HPAPNL_TEXTMSG_1151              20
#define  HPAPNL_TEXTMSG_1152              21
#define  HPAPNL_TEXTMSG_1153              22
#define  HPAPNL_TEXTMSG_1154              23
#define  HPAPNL_TEXTMSG_1155              24
#define  HPAPNL_DECORATION_100            25
#define  HPAPNL_TEXTMSG_1156              26
#define  HPAPNL_TEXTMSG_1157              27
#define  HPAPNL_TEXTMSG_1158              28
#define  HPAPNL_TEXTMSG_1159              29
#define  HPAPNL_TEXTMSG_1160              30
#define  HPAPNL_TEXTMSG_1161              31
#define  HPAPNL_TEXTMSG_1162              32
#define  HPAPNL_TEXTMSG_1091              33
#define  HPAPNL_TEXTMSG_1163              34
#define  HPAPNL_TEXTMSG_1164              35
#define  HPAPNL_TEXTMSG_1165              36
#define  HPAPNL_TEXTMSG_1092              37
#define  HPAPNL_TEXTMSG_1166              38
#define  HPAPNL_TEXTMSG_1167              39
#define  HPAPNL_TEXTMSG_1168              40
#define  HPAPNL_TEXTMSG_1139              41
#define  HPAPNL_TEXTMSG_1169              42
#define  HPAPNL_TEXTMSG_1170              43
#define  HPAPNL_TEXTMSG_1171              44
#define  HPAPNL_TEXTMSG_1172              45
#define  HPAPNL_TEXTMSG_1173              46
#define  HPAPNL_TEXTMSG_1174              47
#define  HPAPNL_TEXTMSG_1175              48
#define  HPAPNL_TEXTMSG_972               49
#define  HPAPNL_TEXTMSG_1176              50
#define  HPAPNL_TEXTMSG_1177              51
#define  HPAPNL_TEXTMSG_1178              52
#define  HPAPNL_TEXTMSG_1140              53
#define  HPAPNL_TEXTMSG_1179              54
#define  HPAPNL_TEXTMSG_1180              55
#define  HPAPNL_TEXTMSG_1181              56
#define  HPAPNL_TEXTMSG_1182              57
#define  HPAPNL_TEXTMSG_1183              58
#define  HPAPNL_TEXTMSG_1184              59
#define  HPAPNL_TEXTMSG_1185              60
#define  HPAPNL_TEXTMSG_1186              61
#define  HPAPNL_TEXTMSG_1187              62
#define  HPAPNL_TEXTMSG_1124              63
#define  HPAPNL_TEXTMSG_1188              64
#define  HPAPNL_TEXTMSG_1189              65
#define  HPAPNL_NMR_Temp_TRS_UL           66
#define  HPAPNL_NMR_Temp_TRS_DL           67
#define  HPAPNL_NMR_Temp_DMB              68
#define  HPAPNL_NMR_Temp_FM2              69
#define  HPAPNL_NMR_Temp_FM1              70
#define  HPAPNL_NMR_Temp_AM2              71
#define  HPAPNL_NMR_Temp_AM1              72
#define  HPAPNL_TEXTMSG_973               73
#define  HPAPNL_TEXTMSG_1141              74
#define  HPAPNL_TEXTMSG_1190              75
#define  HPAPNL_TEXTMSG_1191              76
#define  HPAPNL_TEXTMSG_1125              77
#define  HPAPNL_TEXTMSG_1192              78
#define  HPAPNL_TEXTMSG_1193              79
#define  HPAPNL_TEXTMSG_1142              80
#define  HPAPNL_TEXTMSG_1143              81
#define  HPAPNL_TEXTMSG_1144              82
#define  HPAPNL_TEXTMSG_1145              83
#define  HPAPNL_TEXTMSG_1126              84
#define  HPAPNL_TEXTMSG_974               85
#define  HPAPNL_TEXTMSG_1127              86
#define  HPAPNL_TEXTMSG_1128              87
#define  HPAPNL_TEXTMSG_1129              88
#define  HPAPNL_TEXTMSG_1130              89
#define  HPAPNL_TEXTMSG_1131              90
#define  HPAPNL_TEXTMSG_1132              91
#define  HPAPNL_TEXTMSG_1108              92
#define  HPAPNL_TEXTMSG_1206              93
#define  HPAPNL_TEXTMSG_1205              94
#define  HPAPNL_TEXTMSG_1204              95
#define  HPAPNL_TEXTMSG_1203              96
#define  HPAPNL_TEXTMSG_1202              97
#define  HPAPNL_TEXTMSG_1201              98
#define  HPAPNL_TEXTMSG_1093              99
#define  HPAPNL_TEXTMSG_1133              100
#define  HPAPNL_TEXTMSG_1109              101
#define  HPAPNL_TEXTMSG_1134              102
#define  HPAPNL_TEXTMSG_1135              103
#define  HPAPNL_TEXTMSG_1136              104
#define  HPAPNL_TEXTMSG_1137              105
#define  HPAPNL_TEXTMSG_1110              106
#define  HPAPNL_TEXTMSG_975               107
#define  HPAPNL_TEXTMSG_1111              108
#define  HPAPNL_TEXTMSG_1112              109
#define  HPAPNL_TEXTMSG_1113              110
#define  HPAPNL_TEXTMSG_1114              111
#define  HPAPNL_TEXTMSG_1115              112
#define  HPAPNL_TEXTMSG_1116              113
#define  HPAPNL_TEXTMSG_1099              114
#define  HPAPNL_TG_ALC_TRS_UL             115     /* callback function: OnCmdHPA */
#define  HPAPNL_TG_ALC_TRS_DL             116     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_1094              117
#define  HPAPNL_TG_ALC_DMB                118     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_1117              119
#define  HPAPNL_TG_ALC_FM2                120     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_1100              121
#define  HPAPNL_TG_ALC_FM1                122     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_1118              123
#define  HPAPNL_TG_AMP_TRS_UL             124     /* callback function: OnCmdHPA */
#define  HPAPNL_TG_AMP_TRS_DL             125     /* callback function: OnCmdHPA */
#define  HPAPNL_TG_ALC_AM2                126     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_1119              127
#define  HPAPNL_TG_AMP_DMB                128     /* callback function: OnCmdHPA */
#define  HPAPNL_TG_ALC_AM1                129     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_1120              130
#define  HPAPNL_TG_AMP_FM2                131     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_1121              132
#define  HPAPNL_TEXTMSG_1095              133
#define  HPAPNL_TG_AMP_FM1                134     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_986               135
#define  HPAPNL_NMR_ASD_TEMP_TRS_UL       136     /* callback function: OnCmdHPA */
#define  HPAPNL_NMR_ASD_TRS_UL            137     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_1101              138
#define  HPAPNL_NMR_ASD_TEMP_TRS_DL       139     /* callback function: OnCmdHPA */
#define  HPAPNL_NMR_ASD_TRS_DL            140     /* callback function: OnCmdHPA */
#define  HPAPNL_NMR_SetAttn_TRS_UL        141     /* callback function: OnCmdHPA */
#define  HPAPNL_NMR_ALC_TRS_UL            142     /* callback function: OnCmdHPA */
#define  HPAPNL_TG_AMP_AM2                143     /* callback function: OnCmdHPA */
#define  HPAPNL_NMR_SetAttn_TRS_DL        144     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_1096              145
#define  HPAPNL_NMR_ALC_TRS_DL            146     /* callback function: OnCmdHPA */
#define  HPAPNL_NMR_ATTN_TRS_UL           147
#define  HPAPNL_NMR_PWR_TRS_UL            148
#define  HPAPNL_NMR_ATTN_TRS_DL           149
#define  HPAPNL_LED_TEMP_TRS_UL           150
#define  HPAPNL_LED_PWR_TRS_UL            151
#define  HPAPNL_LED_ASD_TRS_UL            152
#define  HPAPNL_NMR_PWR_TRS_DL            153
#define  HPAPNL_LED_TEMP_TRS_DL           154
#define  HPAPNL_LED_PWR_TRS_DL            155
#define  HPAPNL_LED_ASD_TRS_DL            156
#define  HPAPNL_TEXTMSG_1102              157
#define  HPAPNL_NMR_ASD_TEMP_DMB          158     /* callback function: OnCmdHPA */
#define  HPAPNL_NMR_ASD_DMB               159     /* callback function: OnCmdHPA */
#define  HPAPNL_TG_AMP_AM1                160     /* callback function: OnCmdHPA */
#define  HPAPNL_NMR_SetAttn_DMB           161     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_1103              162
#define  HPAPNL_NMR_ALC_DMB               163     /* callback function: OnCmdHPA */
#define  HPAPNL_NMR_ATTN_DMB              164
#define  HPAPNL_NMR_PWR_DMB               165
#define  HPAPNL_LED_TEMP_DMB              166
#define  HPAPNL_LED_PWR_DMB               167
#define  HPAPNL_LED_ASD_DMB               168
#define  HPAPNL_TEXTMSG_1104              169
#define  HPAPNL_NMR_ASD_TEMP_FM2          170     /* callback function: OnCmdHPA */
#define  HPAPNL_NMR_ASD_FM2               171     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_1105              172
#define  HPAPNL_NMR_SetAttn_FM2           173     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_976               174
#define  HPAPNL_NMR_ALC_FM2               175     /* callback function: OnCmdHPA */
#define  HPAPNL_NMR_ATTN_FM2              176
#define  HPAPNL_NMR_PWR_FM2               177
#define  HPAPNL_LED_TEMP_FM2              178
#define  HPAPNL_LED_PWR_FM2               179
#define  HPAPNL_LED_ASD_FM2               180
#define  HPAPNL_TEXTMSG_977               181
#define  HPAPNL_NMR_ASD_TEMP_FM1          182     /* callback function: OnCmdHPA */
#define  HPAPNL_NMR_ASD_FM1               183     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_978               184
#define  HPAPNL_NMR_SetAttn_FM1           185     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_979               186
#define  HPAPNL_NMR_ALC_FM1               187     /* callback function: OnCmdHPA */
#define  HPAPNL_NMR_ATTN_FM1              188
#define  HPAPNL_NMR_PWR_FM1               189
#define  HPAPNL_LED_TEMP_FM1              190
#define  HPAPNL_LED_PWR_FM1               191
#define  HPAPNL_LED_ASD_FM1               192
#define  HPAPNL_TEXTMSG_1097              193
#define  HPAPNL_NMR_ASD_TEMP_AM2          194     /* callback function: OnCmdHPA */
#define  HPAPNL_NMR_ASD_AM2               195     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_980               196
#define  HPAPNL_NMR_SetAttn_AM2           197     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_981               198
#define  HPAPNL_NMR_ALC_AM2               199     /* callback function: OnCmdHPA */
#define  HPAPNL_NMR_ATTN_AM2              200
#define  HPAPNL_NMR_PWR_AM2               201
#define  HPAPNL_LED_TEMP_AM2              202
#define  HPAPNL_LED_PWR_AM2               203
#define  HPAPNL_LED_ASD_AM2               204
#define  HPAPNL_TEXTMSG_982               205
#define  HPAPNL_NMR_ASD_TEMP_AM1          206     /* callback function: OnCmdHPA */
#define  HPAPNL_NMR_ASD_AM1               207     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_984               208
#define  HPAPNL_NMR_SetAttn_AM1           209     /* callback function: OnCmdHPA */
#define  HPAPNL_TEXTMSG_988               210
#define  HPAPNL_NMR_ALC_AM1               211     /* callback function: OnCmdHPA */
#define  HPAPNL_NMR_ATTN_AM1              212
#define  HPAPNL_NMR_PWR_AM1               213
#define  HPAPNL_LED_TEMP_AM1              214
#define  HPAPNL_LED_PWR_AM1               215
#define  HPAPNL_LED_ASD_AM1               216

#define  IPPNL                            9
#define  IPPNL_TEXT_TYPE                  2
#define  IPPNL_DECORATION_74              3
#define  IPPNL_BTN_LOAD                   4       /* callback function: OnCmdIP */
#define  IPPNL_BTN_SET                    5       /* callback function: OnCmdIP */
#define  IPPNL_BTN_CLOSE                  6       /* callback function: OnCmdIP */
#define  IPPNL_DECPANEL_106               7
#define  IPPNL_DECPANEL_111               8
#define  IPPNL_DECPANEL_108               9
#define  IPPNL_DECPANEL_112               10
#define  IPPNL_DECPANEL_151               11
#define  IPPNL_DECPANEL_152               12
#define  IPPNL_DECPANEL_147               13
#define  IPPNL_COMRING_2                  14
#define  IPPNL_DECPANEL_148               15
#define  IPPNL_BTN_PORTOPEN               16      /* callback function: OnUdpConnect */
#define  IPPNL_DECPANEL_143               17
#define  IPPNL_DECPANEL_144               18
#define  IPPNL_DECPANEL_153               19
#define  IPPNL_DECPANEL_139               20
#define  IPPNL_DECPANEL_154               21
#define  IPPNL_DECPANEL_140               22
#define  IPPNL_DECPANEL_149               23
#define  IPPNL_DECPANEL_135               24
#define  IPPNL_DECPANEL_150               25
#define  IPPNL_BTN_PORTOPEN_3             26      /* callback function: OnCmdMain */
#define  IPPNL_DECPANEL_136               27
#define  IPPNL_DECPANEL_145               28
#define  IPPNL_DECPANEL_131               29
#define  IPPNL_DECPANEL_146               30
#define  IPPNL_DECPANEL_132               31
#define  IPPNL_DECPANEL_141               32
#define  IPPNL_DECPANEL_127               33
#define  IPPNL_DECPANEL_142               34
#define  IPPNL_DECPANEL_128               35
#define  IPPNL_DECPANEL_137               36
#define  IPPNL_DECPANEL_123               37
#define  IPPNL_DECPANEL_138               38
#define  IPPNL_DECPANEL_124               39
#define  IPPNL_DECPANEL_133               40
#define  IPPNL_DECPANEL_119               41
#define  IPPNL_NMR_SERVER1_22             42      /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER2_22             43      /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_22             44      /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_22             45      /* callback function: IPInfoAutoCheck */
#define  IPPNL_DECPANEL_134               46
#define  IPPNL_DECPANEL_120               47
#define  IPPNL_NMR_SERVER1_23             48      /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER2_23             49      /* callback function: IPInfoAutoCheck */
#define  IPPNL_MODE_BACK_3                50
#define  IPPNL_NMR_SERVER3_23             51      /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_23             52      /* callback function: IPInfoAutoCheck */
#define  IPPNL_COMPORT                    53
#define  IPPNL_DECPANEL_129               54
#define  IPPNL_DECPANEL_117               55
#define  IPPNL_DECORATION_16              56
#define  IPPNL_NMR_SERVER1_20             57      /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER2_20             58      /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_20             59      /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_20             60      /* callback function: IPInfoAutoCheck */
#define  IPPNL_SMODE_TEXT2                61
#define  IPPNL_DECPANEL_130               62
#define  IPPNL_INFO_TEXT1_3               63
#define  IPPNL_TEXTMSG_113                64
#define  IPPNL_SMODE_TEXT1                65
#define  IPPNL_TEXTMSG_114                66
#define  IPPNL_TEXTMSG_115                67
#define  IPPNL_DECPANEL_114               68
#define  IPPNL_NMR_SERVER1_21             69      /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_116                70
#define  IPPNL_NMR_SERVER2_21             71      /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_21             72      /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_21             73      /* callback function: IPInfoAutoCheck */
#define  IPPNL_DECPANEL_125               74
#define  IPPNL_TEXTMSG_117                75
#define  IPPNL_TEXTMSG_118                76
#define  IPPNL_TEXTMSG_119                77
#define  IPPNL_DECPANEL_101               78
#define  IPPNL_NMR_SERVER1_18             79      /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_120                80
#define  IPPNL_NMR_SERVER2_18             81      /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_18             82      /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_18             83      /* callback function: IPInfoAutoCheck */
#define  IPPNL_DECPANEL_126               84
#define  IPPNL_TEXTMSG_105                85
#define  IPPNL_TEXTMSG_106                86
#define  IPPNL_TEXTMSG_107                87
#define  IPPNL_DECPANEL_113               88
#define  IPPNL_NMR_SERVER1_19             89      /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_108                90
#define  IPPNL_NMR_SERVER2_19             91      /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_19             92      /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_19             93      /* callback function: IPInfoAutoCheck */
#define  IPPNL_DECPANEL_121               94
#define  IPPNL_TEXTMSG_109                95
#define  IPPNL_TEXTMSG_110                96
#define  IPPNL_TEXTMSG_111                97
#define  IPPNL_DECPANEL_109               98
#define  IPPNL_NMR_SERVER1_16             99      /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_112                100
#define  IPPNL_NMR_SERVER2_16             101     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_16             102     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_16             103     /* callback function: IPInfoAutoCheck */
#define  IPPNL_DECPANEL_122               104
#define  IPPNL_TEXTMSG_97                 105
#define  IPPNL_TEXTMSG_98                 106
#define  IPPNL_TEXTMSG_99                 107
#define  IPPNL_DECPANEL_107               108
#define  IPPNL_NMR_SERVER1_17             109     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_100                110
#define  IPPNL_NMR_SERVER2_17             111     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_17             112     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_17             113     /* callback function: IPInfoAutoCheck */
#define  IPPNL_DECPANEL_118               114
#define  IPPNL_TEXTMSG_101                115
#define  IPPNL_TEXTMSG_102                116
#define  IPPNL_TEXTMSG_103                117
#define  IPPNL_DECPANEL_110               118
#define  IPPNL_NMR_SERVER1_14             119     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_104                120
#define  IPPNL_NMR_SERVER2_14             121     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_14             122     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_14             123     /* callback function: IPInfoAutoCheck */
#define  IPPNL_DECPANEL_115               124
#define  IPPNL_TEXTMSG_89                 125
#define  IPPNL_TEXTMSG_90                 126
#define  IPPNL_TEXTMSG_91                 127
#define  IPPNL_TEXTMSG_35                 128
#define  IPPNL_NMR_SERVER1_15             129     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_92                 130
#define  IPPNL_NMR_SERVER2_15             131     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_15             132     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_15             133     /* callback function: IPInfoAutoCheck */
#define  IPPNL_DECPANEL_100               134
#define  IPPNL_TEXTMSG_93                 135
#define  IPPNL_TEXTMSG_94                 136
#define  IPPNL_TEXTMSG_95                 137
#define  IPPNL_NMR_EBU_L_1                138     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER1_12             139     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_96                 140
#define  IPPNL_NMR_SERVER2_12             141     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_12             142     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_12             143     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_EBU_L_2                144     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_81                 145
#define  IPPNL_TEXTMSG_82                 146
#define  IPPNL_TEXTMSG_83                 147
#define  IPPNL_NMR_EBU_L_3                148     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER1_13             149     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_84                 150
#define  IPPNL_NMR_SERVER2_13             151     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_13             152     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_13             153     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_EBU_L_4                154     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_85                 155
#define  IPPNL_TEXTMSG_86                 156
#define  IPPNL_TEXTMSG_87                 157
#define  IPPNL_DECORATION_75              158
#define  IPPNL_NMR_SERVER1_10             159     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_88                 160
#define  IPPNL_NMR_SERVER2_10             161     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_10             162     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_10             163     /* callback function: IPInfoAutoCheck */
#define  IPPNL_DECPANEL_116               164
#define  IPPNL_TEXTMSG_73                 165
#define  IPPNL_TEXTMSG_74                 166
#define  IPPNL_TEXTMSG_75                 167
#define  IPPNL_DECPANEL_104               168
#define  IPPNL_NMR_SERVER1_11             169     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_76                 170
#define  IPPNL_NMR_SERVER2_11             171     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_11             172     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_11             173     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_30                 174
#define  IPPNL_TEXTMSG_77                 175
#define  IPPNL_TEXTMSG_78                 176
#define  IPPNL_TEXTMSG_79                 177
#define  IPPNL_DECPANEL_84                178
#define  IPPNL_NMR_SERVER1_8              179     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_80                 180
#define  IPPNL_NMR_SERVER2_8              181     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_8              182     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_8              183     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_EBU_U_1                184     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_65                 185
#define  IPPNL_TEXTMSG_66                 186
#define  IPPNL_TEXTMSG_67                 187
#define  IPPNL_NMR_EBU_U_2                188     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER1_9              189     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_68                 190
#define  IPPNL_NMR_SERVER2_9              191     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_9              192     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_9              193     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_EBU_U_3                194     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_69                 195
#define  IPPNL_TEXTMSG_70                 196
#define  IPPNL_TEXTMSG_71                 197
#define  IPPNL_NMR_EBU_U_4                198     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER1_6              199     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_72                 200
#define  IPPNL_NMR_SERVER2_6              201     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_6              202     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_6              203     /* callback function: IPInfoAutoCheck */
#define  IPPNL_DECPANEL_105               204
#define  IPPNL_TEXTMSG_57                 205
#define  IPPNL_TEXTMSG_58                 206
#define  IPPNL_TEXTMSG_59                 207
#define  IPPNL_DECORATION_76              208
#define  IPPNL_NMR_SERVER1_7              209     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_60                 210
#define  IPPNL_NMR_SERVER2_7              211     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_7              212     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_7              213     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_36                 214
#define  IPPNL_TEXTMSG_61                 215
#define  IPPNL_TEXTMSG_62                 216
#define  IPPNL_TEXTMSG_63                 217
#define  IPPNL_TEXTMSG_37                 218
#define  IPPNL_NMR_SERVER1_5              219     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_64                 220
#define  IPPNL_NMR_SERVER2_5              221     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_5              222     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_5              223     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_40                 224
#define  IPPNL_TEXTMSG_49                 225
#define  IPPNL_TEXTMSG_50                 226
#define  IPPNL_TEXTMSG_51                 227
#define  IPPNL_TEXTMSG_38                 228
#define  IPPNL_NMR_SERVER1_4              229     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_52                 230
#define  IPPNL_NMR_SERVER2_4              231     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER3_4              232     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4_4              233     /* callback function: IPInfoAutoCheck */
#define  IPPNL_DECORATION                 234
#define  IPPNL_TEXTMSG_53                 235
#define  IPPNL_TEXTMSG_54                 236
#define  IPPNL_TEXTMSG_55                 237
#define  IPPNL_DECPANEL_102               238
#define  IPPNL_TEXTMSG_39                 239
#define  IPPNL_TEXTMSG_56                 240
#define  IPPNL_TEXTMSG_2                  241
#define  IPPNL_DECPANEL_103               242
#define  IPPNL_NMR_SERVER1                243
#define  IPPNL_NMR_SERVER2                244     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_45                 245
#define  IPPNL_TEXTMSG_46                 246
#define  IPPNL_TEXTMSG_47                 247
#define  IPPNL_NMR_SERVER3                248     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SERVER4                249     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_48                 250
#define  IPPNL_TEXTMSG_31                 251
#define  IPPNL_TEXTMSG_32                 252
#define  IPPNL_TEXTMSG_33                 253
#define  IPPNL_NMR_PORT                   254     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_41                 255
#define  IPPNL_TEXTMSG_42                 256
#define  IPPNL_TEXTMSG_43                 257
#define  IPPNL_NMR_GW1                    258     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_34                 259
#define  IPPNL_TEXTMSG_44                 260
#define  IPPNL_NMR_GW2                    261     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_GW3                    262     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_GW4                    263     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SM1                    264     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SM2                    265     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SM3                    266     /* callback function: IPInfoAutoCheck */
#define  IPPNL_NMR_SM4                    267     /* callback function: IPInfoAutoCheck */
#define  IPPNL_TEXTMSG_20                 268
#define  IPPNL_TEXTMSG_19                 269
#define  IPPNL_TEXTMSG_21                 270
#define  IPPNL_TEXTMSG_9                  271
#define  IPPNL_TEXTMSG_11                 272
#define  IPPNL_TEXTMSG_18                 273
#define  IPPNL_TEXTMSG_10                 274
#define  IPPNL_TEXTMSG_29                 275
#define  IPPNL_TEXTMSG_28                 276
#define  IPPNL_TEXTMSG_27                 277
#define  IPPNL_TEXTMSG_26                 278
#define  IPPNL_TEXTMSG_25                 279
#define  IPPNL_TEXTMSG_24                 280

#define  LNAPNL                           10
#define  LNAPNL_BTN_STS                   2       /* callback function: OnCmdLNA */
#define  LNAPNL_BTN_SET                   3       /* callback function: OnCmdLNA */
#define  LNAPNL_BTN_REQ                   4       /* callback function: OnCmdLNA */
#define  LNAPNL_STR_PATH                  5
#define  LNAPNL_BTN_CLOSE                 6       /* callback function: OnCmdLNA */
#define  LNAPNL_DECORATION                7
#define  LNAPNL_TEXTMSG_1135              8
#define  LNAPNL_TEXTMSG_1106              9
#define  LNAPNL_TEXTMSG_1107              10
#define  LNAPNL_TEXTMSG_1098              11
#define  LNAPNL_TEXTMSG_1069              12
#define  LNAPNL_TEXTMSG_1122              13
#define  LNAPNL_TEXTMSG_1123              14
#define  LNAPNL_DECORATION_100            15
#define  LNAPNL_TEXTMSG_972               16
#define  LNAPNL_TEXTMSG_973               17
#define  LNAPNL_TEXTMSG_974               18
#define  LNAPNL_TEXTMSG_1119              19
#define  LNAPNL_TG_ALC_TRS_UL             20      /* callback function: OnCmdLNA */
#define  LNAPNL_TEXTMSG_1124              21
#define  LNAPNL_TG_ALC_TRS                22      /* callback function: OnCmdLNA */
#define  LNAPNL_TG_ALC_DMB                23      /* callback function: OnCmdLNA */
#define  LNAPNL_TG_ALC_FM                 24      /* callback function: OnCmdLNA */
#define  LNAPNL_TG_ALC_AM                 25      /* callback function: OnCmdLNA */
#define  LNAPNL_TEXTMSG_1125              26
#define  LNAPNL_TEXTMSG_1120              27
#define  LNAPNL_TEXTMSG_1126              28
#define  LNAPNL_TEXTMSG_1121              29
#define  LNAPNL_TEXTMSG_1136              30
#define  LNAPNL_TEXTMSG_986               31
#define  LNAPNL_TEXTMSG_1137              32
#define  LNAPNL_TG_AMP_TRS_UL             33      /* callback function: OnCmdLNA */
#define  LNAPNL_TEXTMSG_1127              34
#define  LNAPNL_TEXTMSG_1128              35
#define  LNAPNL_TEXTMSG_1101              36
#define  LNAPNL_TEXTMSG_1129              37
#define  LNAPNL_TG_AMP_TRS                38      /* callback function: OnCmdLNA */
#define  LNAPNL_TEXTMSG_1130              39
#define  LNAPNL_TG_AMP_DMB                40      /* callback function: OnCmdLNA */
#define  LNAPNL_TEXTMSG_1131              41
#define  LNAPNL_TG_AMP_FM                 42      /* callback function: OnCmdLNA */
#define  LNAPNL_TEXTMSG_1102              43
#define  LNAPNL_TG_AMP_AM                 44      /* callback function: OnCmdLNA */
#define  LNAPNL_TEXTMSG_1103              45
#define  LNAPNL_TEXTMSG_1138              46
#define  LNAPNL_TEXTMSG_1104              47
#define  LNAPNL_TEXTMSG_1105              48
#define  LNAPNL_TEXTMSG_1139              49
#define  LNAPNL_TEXTMSG_975               50
#define  LNAPNL_TEXTMSG_976               51
#define  LNAPNL_TEXTMSG_1140              52
#define  LNAPNL_TEXTMSG_1132              53
#define  LNAPNL_TEXTMSG_1111              54
#define  LNAPNL_TEXTMSG_1141              55
#define  LNAPNL_TEXTMSG_1134              56
#define  LNAPNL_NMR_SetAttn_TRS_UL        57      /* callback function: OnCmdLNA */
#define  LNAPNL_NMR_ALC_TRS_UL            58      /* callback function: OnCmdLNA */
#define  LNAPNL_NMR_ATTN_TRS_UL           59
#define  LNAPNL_NMR_PWR_TRS_UL            60
#define  LNAPNL_TEXTMSG_1142              61
#define  LNAPNL_TEXTMSG_977               62
#define  LNAPNL_TEXTMSG_1113              63
#define  LNAPNL_TEXTMSG_978               64
#define  LNAPNL_TEXTMSG_979               65
#define  LNAPNL_TEXTMSG_1114              66
#define  LNAPNL_TEXTMSG_1097              67
#define  LNAPNL_TEXTMSG_980               68
#define  LNAPNL_TEXTMSG_1115              69
#define  LNAPNL_TEXTMSG_981               70
#define  LNAPNL_NMR_SetAttn_TRS           71      /* callback function: OnCmdLNA */
#define  LNAPNL_NMR_ALC_TRS               72      /* callback function: OnCmdLNA */
#define  LNAPNL_NMR_ATTN_TRS              73
#define  LNAPNL_NMR_PWR_TRS               74
#define  LNAPNL_TEXTMSG_1116              75
#define  LNAPNL_TEXTMSG_982               76
#define  LNAPNL_NMR_SetAttn_DMB           77      /* callback function: OnCmdLNA */
#define  LNAPNL_NMR_ALC_DMB               78      /* callback function: OnCmdLNA */
#define  LNAPNL_NMR_ATTN_DMB              79
#define  LNAPNL_NMR_PWR_DMB               80
#define  LNAPNL_TEXTMSG_1117              81
#define  LNAPNL_TEXTMSG_984               82
#define  LNAPNL_NMR_SetAttn_FM            83      /* callback function: OnCmdLNA */
#define  LNAPNL_NMR_ALC_FM                84      /* callback function: OnCmdLNA */
#define  LNAPNL_NMR_ATTN_FM               85
#define  LNAPNL_NMR_PWR_FM                86
#define  LNAPNL_TEXTMSG_988               87
#define  LNAPNL_TEXTMSG_1118              88
#define  LNAPNL_NMR_SetAttn_AM            89      /* callback function: OnCmdLNA */
#define  LNAPNL_NMR_ALC_AM                90      /* callback function: OnCmdLNA */
#define  LNAPNL_NMR_ATTN_AM               91
#define  LNAPNL_NMR_PWR_AM                92

#define  MAINPNL                          11
#define  MAINPNL_TEXTMSG_1171             2
#define  MAINPNL_TEXTMSG_1172             3
#define  MAINPNL_TEXTMSG_1173             4
#define  MAINPNL_TEXTMSG_1170             5
#define  MAINPNL_TEXTMSG_1169             6
#define  MAINPNL_TEXTMSG_1168             7
#define  MAINPNL_TEXTMSG_1091             8
#define  MAINPNL_DECORATION_173           9
#define  MAINPNL_DECORATION_174           10
#define  MAINPNL_DECORATION_175           11
#define  MAINPNL_NMR_EMCU_1               12
#define  MAINPNL_NMR_EMCU_2               13      /* callback function: IPInfoAutoCheck */
#define  MAINPNL_NMR_EMCU_3               14      /* callback function: IPInfoAutoCheck */
#define  MAINPNL_NMR_EMCU_4               15      /* callback function: IPInfoAutoCheck */
#define  MAINPNL_TEXTMSG_22               16
#define  MAINPNL_TEXTMSG_23               17
#define  MAINPNL_DECORATION_172           18
#define  MAINPNL_TEXTMSG_24               19
#define  MAINPNL_DECORATION_171           20
#define  MAINPNL_DECORATION_170           21
#define  MAINPNL_STR_TRS_FPGA             22
#define  MAINPNL_STR_TRS_CPU              23
#define  MAINPNL_STR_DMB_FPGA             24
#define  MAINPNL_DECORATION_154           25
#define  MAINPNL_STR_DMB_CPU              26
#define  MAINPNL_STR_FM_FPGA              27
#define  MAINPNL_STR_AM_FPGA              28
#define  MAINPNL_STR_FM_CPU               29
#define  MAINPNL_STR_EBU2_FPGA            30
#define  MAINPNL_STR_AM_CPU               31
#define  MAINPNL_STR_EBU1_FPGA            32
#define  MAINPNL_STR_EBU2_CPU             33
#define  MAINPNL_STR_EBU1_CPU             34
#define  MAINPNL_STR_NAME                 35
#define  MAINPNL_STR_CPU                  36
#define  MAINPNL_BTN_AMP_39               37      /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_19               38      /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_38               39      /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_18               40      /* callback function: OnCmdMain2 */
#define  MAINPNL_DECPANEL_317             41
#define  MAINPNL_DECPANEL_316             42
#define  MAINPNL_DECPANEL_315             43
#define  MAINPNL_DECPANEL_314             44
#define  MAINPNL_DECPANEL_313             45
#define  MAINPNL_DECPANEL_312             46
#define  MAINPNL_DECPANEL_305             47
#define  MAINPNL_LED_PWR_AC_V             48
#define  MAINPNL_LED_PWR_AC_A             49
#define  MAINPNL_LED_PWR_PSU_N            50
#define  MAINPNL_LED_PWR_PSU_P            51
#define  MAINPNL_LED_HPA_TRS_U            52
#define  MAINPNL_LED_HPA_TRS_D            53
#define  MAINPNL_LED_HPA_DMB              54
#define  MAINPNL_LED_HPA_FM2              55
#define  MAINPNL_LED_HPA_FM1              56
#define  MAINPNL_LED_HPA_AM2              57
#define  MAINPNL_LED_HPA_AM1              58
#define  MAINPNL_LED_LNA_TRS_U            59
#define  MAINPNL_LED_LNA_TRS_D            60
#define  MAINPNL_LED_LNA_DMB              61
#define  MAINPNL_LED_LNA_FM               62
#define  MAINPNL_LED_LNA_AM               63
#define  MAINPNL_BTN_AMP_37               64      /* callback function: OnCmdMain2 */
#define  MAINPNL_DECPANEL_306             65
#define  MAINPNL_DECPANEL_311             66
#define  MAINPNL_DECPANEL_310             67
#define  MAINPNL_DECPANEL_309             68
#define  MAINPNL_DECPANEL_308             69
#define  MAINPNL_DECPANEL_307             70
#define  MAINPNL_DECPANEL_304             71
#define  MAINPNL_TEXTMSG_1185             72
#define  MAINPNL_DECPANEL_322             73
#define  MAINPNL_TEXTMSG_1184             74
#define  MAINPNL_DECPANEL_320             75
#define  MAINPNL_TEXTMSG_1183             76
#define  MAINPNL_DECPANEL_318             77
#define  MAINPNL_TEXTMSG_1132             78
#define  MAINPNL_DECPANEL_202             79
#define  MAINPNL_TEXTMSG_1190             80
#define  MAINPNL_TEXTMSG_1191             81
#define  MAINPNL_TEXTMSG_1192             82
#define  MAINPNL_DECPANEL_332             83
#define  MAINPNL_TEXTMSG_1195             84
#define  MAINPNL_TEXTMSG_1193             85
#define  MAINPNL_DECPANEL_333             86
#define  MAINPNL_RING_TYPE                87
#define  MAINPNL_NMR_ID                   88      /* callback function: OnCmdHPA */
#define  MAINPNL_DECPANEL_334             89
#define  MAINPNL_LED_FAN_I5               90
#define  MAINPNL_5                        91
#define  MAINPNL_DECPANEL_335             92
#define  MAINPNL_DECPANEL_336             93
#define  MAINPNL_LED_FAN_I4               94
#define  MAINPNL_TEXTMSG_1188             95
#define  MAINPNL_DECPANEL_337             96
#define  MAINPNL_DECPANEL_338             97
#define  MAINPNL_LED_FAN_I3               98
#define  MAINPNL_TEXTMSG_1187             99
#define  MAINPNL_DECPANEL_339             100
#define  MAINPNL_DECPANEL_340             101
#define  MAINPNL_LED_FAN_I2               102
#define  MAINPNL_DECPANEL_330             103
#define  MAINPNL_TEXTMSG_1186             104
#define  MAINPNL_DECPANEL_341             105
#define  MAINPNL_LED_FAN_I1               106
#define  MAINPNL_DECPANEL_328             107
#define  MAINPNL_DECPANEL_331             108
#define  MAINPNL_DECPANEL_342             109
#define  MAINPNL_TEXTMSG_1194             110
#define  MAINPNL_LED_FAN_D5               111
#define  MAINPNL_TEXTMSG_1143             112
#define  MAINPNL_DECPANEL_326             113
#define  MAINPNL_DECPANEL_329             114
#define  MAINPNL_LED_FAN_D4               115
#define  MAINPNL_DECPANEL_323             116
#define  MAINPNL_DECPANEL_324             117
#define  MAINPNL_DECPANEL_327             118
#define  MAINPNL_LED_FAN_D3               119
#define  MAINPNL_LED_FIRE_4               120
#define  MAINPNL_DECPANEL_222             121
#define  MAINPNL_DECPANEL_325             122
#define  MAINPNL_LED_FAN_D2               123
#define  MAINPNL_DECPANEL_321             124
#define  MAINPNL_LED_FIRE_3               125
#define  MAINPNL_DECPANEL_223             126
#define  MAINPNL_LED_FAN_D1               127
#define  MAINPNL_DECPANEL_319             128
#define  MAINPNL_LED_FIRE_2               129
#define  MAINPNL_DECPANEL_224             130
#define  MAINPNL_TEXTMSG_1144             131
#define  MAINPNL_DECPANEL_203             132
#define  MAINPNL_BTN_AMP_17               133     /* callback function: OnCmdMain2 */
#define  MAINPNL_LED_FIRE_1               134
#define  MAINPNL_BTN_AMP_36               135     /* callback function: OnCmdMain2 */
#define  MAINPNL_DECPANEL_204             136
#define  MAINPNL_BTN_AMP_16               137     /* callback function: OnCmdMain2 */
#define  MAINPNL_TEXTMSG_1133             138
#define  MAINPNL_BTN_AMP_35               139     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_15               140     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_34               141     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_33               142     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_14               143     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_32               144     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_13               145     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_12               146     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_31               147     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_11               148     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_30               149     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_10               150     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_29               151     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_9                152     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_28               153     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_8                154     /* callback function: OnCmdMain2 */
#define  MAINPNL_TEXTMSG_1182             155
#define  MAINPNL_BTN_AMP_27               156     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_7                157     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_26               158     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_6                159     /* callback function: OnCmdMain2 */
#define  MAINPNL_TEXTMSG_1181             160
#define  MAINPNL_BTN_AMP_25               161     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_5                162     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_24               163     /* callback function: OnCmdMain2 */
#define  MAINPNL_TEXTMSG_1180             164
#define  MAINPNL_BTN_AMP_23               165     /* callback function: OnCmdMain2 */
#define  MAINPNL_TEXTMSG_1179             166
#define  MAINPNL_BTN_AMP_4                167     /* callback function: OnCmdMain2 */
#define  MAINPNL_TEXTMSG_1126             168
#define  MAINPNL_BTN_AMP_22               169     /* callback function: OnCmdMain2 */
#define  MAINPNL_BTN_AMP_3                170     /* callback function: OnCmdMain2 */
#define  MAINPNL_DECPANEL_191             171
#define  MAINPNL_DECPANEL_192             172
#define  MAINPNL_BTN_AMP_2                173     /* callback function: OnCmdMain2 */
#define  MAINPNL_LED_DOOR_R               174
#define  MAINPNL_BTN_AMP_21               175     /* callback function: OnCmdMain2 */
#define  MAINPNL_PORTTIMER                176     /* callback function: PortDropTimer */
#define  MAINPNL_TIMER                    177     /* callback function: PollingTimer */
#define  MAINPNL_BTN_AMP_1                178     /* callback function: OnCmdMain2 */
#define  MAINPNL_WAITTIMER                179     /* callback function: ResponseTimer */
#define  MAINPNL_STR_PORT_OUT_ID_14       180
#define  MAINPNL_BTN_AMP_20               181     /* callback function: OnCmdMain2 */
#define  MAINPNL_STR_PORT_OUT_ID_13       182
#define  MAINPNL_STR_PORT_OUT_ID_12       183
#define  MAINPNL_BTN_AMP_0                184     /* callback function: OnCmdMain2 */
#define  MAINPNL_STR_PORT_OUT_ID_11       185
#define  MAINPNL_STR_DMB                  186
#define  MAINPNL_STR_PORT_OUT_ID_19       187
#define  MAINPNL_STR_PORT_OUT_ID_10       188
#define  MAINPNL_STR_PORT_OUT_ID_18       189
#define  MAINPNL_STR_FM1                  190
#define  MAINPNL_STR_PORT_OUT_ID_17       191
#define  MAINPNL_STR_PORT_OUT_ID_9        192
#define  MAINPNL_STR_PORT_OUT_ID_16       193
#define  MAINPNL_STR_AM2                  194
#define  MAINPNL_STR_PORT_OUT_ID_15       195
#define  MAINPNL_STR_PORT_OUT_ID_8        196
#define  MAINPNL_STR_AM1                  197
#define  MAINPNL_STR_FM2                  198
#define  MAINPNL_STR_PORT_OUT_ID_24       199
#define  MAINPNL_STR_PORT_OUT_ID_23       200
#define  MAINPNL_STR_PORT_OUT_ID_22       201
#define  MAINPNL_STR_PORT_OUT_ID_21       202
#define  MAINPNL_STR_PORT_OUT_ID_20       203
#define  MAINPNL_STR_TRS_DL               204
#define  MAINPNL_STR_PORT_OUT_ID          205
#define  MAINPNL_DECPANEL_302             206
#define  MAINPNL_DECPANEL_300             207
#define  MAINPNL_NMR_AC_V                 208
#define  MAINPNL_NMR_AC_A                 209
#define  MAINPNL_TEXTMSG_1202             210
#define  MAINPNL_TEXTMSG_1201             211
#define  MAINPNL_TEXTMSG_1203             212
#define  MAINPNL_TEXTMSG_1199             213
#define  MAINPNL_TEXTMSG_1127             214
#define  MAINPNL_NMR_PSU_N                215
#define  MAINPNL_DECPANEL_193             216
#define  MAINPNL_NMR_PSU_P                217
#define  MAINPNL_NMR_TEMP                 218
#define  MAINPNL_STR_LEFTPULSE            219
#define  MAINPNL_TEXTMSG_1151             220
#define  MAINPNL_DECPANEL_298             221
#define  MAINPNL_TEXTMSG_1152             222
#define  MAINPNL_DECPANEL_296             223
#define  MAINPNL_DECPANEL_345             224
#define  MAINPNL_DECPANEL_346             225
#define  MAINPNL_TEXTMSG_1198             226
#define  MAINPNL_DECPANEL_343             227
#define  MAINPNL_DECPANEL_344             228
#define  MAINPNL_DECPANEL_293             229
#define  MAINPNL_TEXTMSG_1197             230
#define  MAINPNL_DECPANEL_295             231
#define  MAINPNL_TEXTMSG_1174             232
#define  MAINPNL_NMR_AM_PWR               233
#define  MAINPNL_NMR_FM_PWR               234
#define  MAINPNL_NMR_DMB_PWR              235
#define  MAINPNL_NMR_TRS_D_PWR            236
#define  MAINPNL_NMR_TRS_U_PWR            237
#define  MAINPNL_NMR_HPA_TRS_U            238
#define  MAINPNL_NMR_HPA_TRS_D            239
#define  MAINPNL_NMR_HPA_DMB              240
#define  MAINPNL_NMR_HPA_FM1              241
#define  MAINPNL_NMR_HPA_AM1              242
#define  MAINPNL_DECPANEL_350             243
#define  MAINPNL_NMR_HPA_FM2              244
#define  MAINPNL_DECPANEL_349             245
#define  MAINPNL_DECPANEL_351             246
#define  MAINPNL_NMR_HPA_AM2              247
#define  MAINPNL_DECPANEL_347             248
#define  MAINPNL_NMR_LNA_TRS_U            249
#define  MAINPNL_DECPANEL_194             250
#define  MAINPNL_NMR_LNA_TRS_D            251
#define  MAINPNL_LED_DOOR_F               252
#define  MAINPNL_NMR_LNA_DMB              253
#define  MAINPNL_DECPANEL_348             254
#define  MAINPNL_TEXTMSG_1200             255
#define  MAINPNL_DECPANEL_195             256
#define  MAINPNL_TEXTMSG_1128             257
#define  MAINPNL_NMR_LNA_FM               258
#define  MAINPNL_TEXTMSG_1123             259
#define  MAINPNL_TEXTMSG_33               260
#define  MAINPNL_DECPANEL_186             261
#define  MAINPNL_NMR_LNA_AM               262
#define  MAINPNL_DECORATION_169           263
#define  MAINPNL_LED_AMP_39               264
#define  MAINPNL_LED_AMP_19               265
#define  MAINPNL_LED_AMP_38               266
#define  MAINPNL_LED_AMP_37               267
#define  MAINPNL_LED_AMP_18               268
#define  MAINPNL_LED_AMP_17               269
#define  MAINPNL_LED_AMP_36               270
#define  MAINPNL_LED_AMP_16               271
#define  MAINPNL_LED_AMP_35               272
#define  MAINPNL_LED_AMP_34               273
#define  MAINPNL_LED_AMP_15               274
#define  MAINPNL_LED_AMP_14               275
#define  MAINPNL_LED_AMP_13               276
#define  MAINPNL_LED_AMP_33               277
#define  MAINPNL_LED_AMP_12               278
#define  MAINPNL_LED_AMP_32               279
#define  MAINPNL_LED_AMP_11               280
#define  MAINPNL_LED_AMP_31               281
#define  MAINPNL_LED_AMP_30               282
#define  MAINPNL_LED_AMP_10               283
#define  MAINPNL_LED_AMP_29               284
#define  MAINPNL_LED_AMP_9                285
#define  MAINPNL_LED_AMP_28               286
#define  MAINPNL_LED_AMP_27               287
#define  MAINPNL_LED_AMP_8                288
#define  MAINPNL_LED_AMP_7                289
#define  MAINPNL_LED_AMP_26               290
#define  MAINPNL_LED_AMP_6                291
#define  MAINPNL_LED_AMP_25               292
#define  MAINPNL_LED_AMP_24               293
#define  MAINPNL_LED_AMP_5                294
#define  MAINPNL_LED_AMP_23               295
#define  MAINPNL_LED_TEMP_LOW             296
#define  MAINPNL_LED_AMP_4                297
#define  MAINPNL_TEXTMSG_1124             298
#define  MAINPNL_DECPANEL_188             299
#define  MAINPNL_LED_AMP_22               300
#define  MAINPNL_LED_TEMP_HIGH            301
#define  MAINPNL_LED_AMP_3                302
#define  MAINPNL_LED_AMP_21               303
#define  MAINPNL_LED_AMP_2                304
#define  MAINPNL_LED_AMP_1                305
#define  MAINPNL_LED_AMP_20               306
#define  MAINPNL_LED_AMP_0                307
#define  MAINPNL_STR_RIGHTPULSE           308
#define  MAINPNL_RX_LED                   309
#define  MAINPNL_DECTXT_8                 310
#define  MAINPNL_BTN_UDPOPEN              311     /* callback function: OnUdpConnect */
#define  MAINPNL_BTN_DOWNLOAD             312     /* callback function: OnCmdMain */
#define  MAINPNL_BTN_IP                   313     /* callback function: OnCmdMain */
#define  MAINPNL_BTN_TABLE                314     /* callback function: OnCmdMain */
#define  MAINPNL_BTN_DUBUGOPEN            315     /* callback function: OnCmdMain */
#define  MAINPNL_TX_LED                   316
#define  MAINPNL_DECORATION_176           317
#define  MAINPNL_DECORATION_13            318
#define  MAINPNL_DECORATION_12            319
#define  MAINPNL_BTN_EMCU_IP              320     /* callback function: OnCmdMain */
#define  MAINPNL_BTN_EXIT                 321     /* callback function: OnCmdMain */
#define  MAINPNL_BTN_EMECH                322     /* callback function: OnCmdMain */
#define  MAINPNL_BTN_EMERGENCY            323     /* callback function: OnCmdMain */
#define  MAINPNL_BTN_HPA                  324     /* callback function: OnCmdMain */
#define  MAINPNL_BTN_LNA                  325     /* callback function: OnCmdMain */
#define  MAINPNL_BTN_SPEAKER              326     /* callback function: OnCmdMain */
#define  MAINPNL_BTN_CHA                  327     /* callback function: OnCmdMain */
#define  MAINPNL_BTN_ENV                  328     /* callback function: OnCmdMain */
#define  MAINPNL_BTN_AMP                  329     /* callback function: OnCmdMain */
#define  MAINPNL_DECORATION_168           330
#define  MAINPNL_PICTURE_5                331

#define  PNLMSG                           12
#define  PNLMSG_TXT_MSG2                  2
#define  PNLMSG_TXT_MSG                   3

#define  PROPNL                           13
#define  PROPNL_COMMANDBUTTON             2       /* callback function: OnCmdProgress */
#define  PROPNL_PROGRESS                  3
#define  PROPNL_TEXTMSG                   4

#define  TABPNL                           14
#define  TABPNL_RING_SYSTEM4              2       /* callback function: OnCmdTable */
#define  TABPNL_RING_SYSTEM3              3       /* callback function: OnCmdTable */
#define  TABPNL_RING_SYSTEM2              4       /* callback function: OnCmdSelSys */
#define  TABPNL_RING_ID3                  5       /* callback function: OnChangeIDRing */
#define  TABPNL_RING_ID                   6       /* callback function: OnChangeIDRing */
#define  TABPNL_CURRAD                    7
#define  TABPNL_CURRVO                    8
#define  TABPNL_BTN_CLOSE                 9       /* callback function: OnCmdTable */
#define  TABPNL_BTN_VOLT                  10      /* callback function: OnCmdTable */
#define  TABPNL_VO_140                    11      /* callback function: VoltageChang */
#define  TABPNL_VO_139                    12      /* callback function: VoltageChang */
#define  TABPNL_VO_138                    13      /* callback function: VoltageChang */
#define  TABPNL_VO_137                    14      /* callback function: VoltageChang */
#define  TABPNL_VO_136                    15      /* callback function: VoltageChang */
#define  TABPNL_VO_135                    16      /* callback function: VoltageChang */
#define  TABPNL_VO_134                    17      /* callback function: VoltageChang */
#define  TABPNL_VO_133                    18      /* callback function: VoltageChang */
#define  TABPNL_VO_132                    19      /* callback function: VoltageChang */
#define  TABPNL_VO_131                    20      /* callback function: VoltageChang */
#define  TABPNL_VO_130                    21      /* callback function: VoltageChang */
#define  TABPNL_VO_129                    22      /* callback function: VoltageChang */
#define  TABPNL_VO_128                    23      /* callback function: VoltageChang */
#define  TABPNL_VO_127                    24      /* callback function: VoltageChang */
#define  TABPNL_VO_126                    25      /* callback function: VoltageChang */
#define  TABPNL_VO_125                    26      /* callback function: VoltageChang */
#define  TABPNL_VO_124                    27      /* callback function: VoltageChang */
#define  TABPNL_VO_123                    28      /* callback function: VoltageChang */
#define  TABPNL_VO_122                    29      /* callback function: VoltageChang */
#define  TABPNL_VO_121                    30      /* callback function: VoltageChang */
#define  TABPNL_VO_120                    31      /* callback function: VoltageChang */
#define  TABPNL_VO_119                    32      /* callback function: VoltageChang */
#define  TABPNL_VO_118                    33      /* callback function: VoltageChang */
#define  TABPNL_VO_117                    34      /* callback function: VoltageChang */
#define  TABPNL_VO_116                    35      /* callback function: VoltageChang */
#define  TABPNL_VO_115                    36      /* callback function: VoltageChang */
#define  TABPNL_VO_114                    37      /* callback function: VoltageChang */
#define  TABPNL_VO_113                    38      /* callback function: VoltageChang */
#define  TABPNL_VO_112                    39      /* callback function: VoltageChang */
#define  TABPNL_VO_111                    40      /* callback function: VoltageChang */
#define  TABPNL_VO_110                    41      /* callback function: VoltageChang */
#define  TABPNL_VO_109                    42      /* callback function: VoltageChang */
#define  TABPNL_VO_108                    43      /* callback function: VoltageChang */
#define  TABPNL_VO_107                    44      /* callback function: VoltageChang */
#define  TABPNL_VO_106                    45      /* callback function: VoltageChang */
#define  TABPNL_VO_105                    46      /* callback function: VoltageChang */
#define  TABPNL_VO_104                    47      /* callback function: VoltageChang */
#define  TABPNL_VO_103                    48      /* callback function: VoltageChang */
#define  TABPNL_VO_102                    49      /* callback function: VoltageChang */
#define  TABPNL_VO_101                    50      /* callback function: VoltageChang */
#define  TABPNL_VO_100                    51      /* callback function: VoltageChang */
#define  TABPNL_VO_99                     52      /* callback function: VoltageChang */
#define  TABPNL_VO_98                     53      /* callback function: VoltageChang */
#define  TABPNL_VO_97                     54      /* callback function: VoltageChang */
#define  TABPNL_VO_96                     55      /* callback function: VoltageChang */
#define  TABPNL_VO_95                     56      /* callback function: VoltageChang */
#define  TABPNL_VO_94                     57      /* callback function: VoltageChang */
#define  TABPNL_VO_93                     58      /* callback function: VoltageChang */
#define  TABPNL_VO_92                     59      /* callback function: VoltageChang */
#define  TABPNL_VO_91                     60      /* callback function: VoltageChang */
#define  TABPNL_VO_90                     61      /* callback function: VoltageChang */
#define  TABPNL_VO_89                     62      /* callback function: VoltageChang */
#define  TABPNL_VO_88                     63      /* callback function: VoltageChang */
#define  TABPNL_VO_87                     64      /* callback function: VoltageChang */
#define  TABPNL_VO_86                     65      /* callback function: VoltageChang */
#define  TABPNL_VO_85                     66      /* callback function: VoltageChang */
#define  TABPNL_VO_84                     67      /* callback function: VoltageChang */
#define  TABPNL_VO_83                     68      /* callback function: VoltageChang */
#define  TABPNL_VO_82                     69      /* callback function: VoltageChang */
#define  TABPNL_VO_81                     70      /* callback function: VoltageChang */
#define  TABPNL_VO_80                     71      /* callback function: VoltageChang */
#define  TABPNL_VO_79                     72      /* callback function: VoltageChang */
#define  TABPNL_VO_78                     73      /* callback function: VoltageChang */
#define  TABPNL_VO_77                     74      /* callback function: VoltageChang */
#define  TABPNL_VO_76                     75      /* callback function: VoltageChang */
#define  TABPNL_VO_75                     76      /* callback function: VoltageChang */
#define  TABPNL_VO_74                     77      /* callback function: VoltageChang */
#define  TABPNL_VO_73                     78      /* callback function: VoltageChang */
#define  TABPNL_VO_72                     79      /* callback function: VoltageChang */
#define  TABPNL_VO_71                     80      /* callback function: VoltageChang */
#define  TABPNL_VO_70                     81      /* callback function: VoltageChang */
#define  TABPNL_VO_69                     82      /* callback function: VoltageChang */
#define  TABPNL_VO_68                     83      /* callback function: VoltageChang */
#define  TABPNL_VO_67                     84      /* callback function: VoltageChang */
#define  TABPNL_VO_66                     85      /* callback function: VoltageChang */
#define  TABPNL_VO_65                     86      /* callback function: VoltageChang */
#define  TABPNL_VO_64                     87      /* callback function: VoltageChang */
#define  TABPNL_VO_63                     88      /* callback function: VoltageChang */
#define  TABPNL_VO_62                     89      /* callback function: VoltageChang */
#define  TABPNL_VO_61                     90      /* callback function: VoltageChang */
#define  TABPNL_VO_60                     91      /* callback function: VoltageChang */
#define  TABPNL_VO_59                     92      /* callback function: VoltageChang */
#define  TABPNL_VO_58                     93      /* callback function: VoltageChang */
#define  TABPNL_VO_57                     94      /* callback function: VoltageChang */
#define  TABPNL_VO_56                     95      /* callback function: VoltageChang */
#define  TABPNL_VO_55                     96      /* callback function: VoltageChang */
#define  TABPNL_VO_54                     97      /* callback function: VoltageChang */
#define  TABPNL_VO_53                     98      /* callback function: VoltageChang */
#define  TABPNL_VO_52                     99      /* callback function: VoltageChang */
#define  TABPNL_VO_51                     100     /* callback function: VoltageChang */
#define  TABPNL_VO_50                     101     /* callback function: VoltageChang */
#define  TABPNL_VO_49                     102     /* callback function: VoltageChang */
#define  TABPNL_VO_48                     103     /* callback function: VoltageChang */
#define  TABPNL_VO_47                     104     /* callback function: VoltageChang */
#define  TABPNL_VO_46                     105     /* callback function: VoltageChang */
#define  TABPNL_VO_45                     106     /* callback function: VoltageChang */
#define  TABPNL_VO_44                     107     /* callback function: VoltageChang */
#define  TABPNL_VO_43                     108     /* callback function: VoltageChang */
#define  TABPNL_VO_42                     109     /* callback function: VoltageChang */
#define  TABPNL_VO_41                     110     /* callback function: VoltageChang */
#define  TABPNL_VO_40                     111     /* callback function: VoltageChang */
#define  TABPNL_VO_39                     112     /* callback function: VoltageChang */
#define  TABPNL_VO_38                     113     /* callback function: VoltageChang */
#define  TABPNL_VO_37                     114     /* callback function: VoltageChang */
#define  TABPNL_VO_36                     115     /* callback function: VoltageChang */
#define  TABPNL_VO_35                     116     /* callback function: VoltageChang */
#define  TABPNL_VO_34                     117     /* callback function: VoltageChang */
#define  TABPNL_VO_33                     118     /* callback function: VoltageChang */
#define  TABPNL_VO_32                     119     /* callback function: VoltageChang */
#define  TABPNL_VO_31                     120     /* callback function: VoltageChang */
#define  TABPNL_VO_30                     121     /* callback function: VoltageChang */
#define  TABPNL_VO_29                     122     /* callback function: VoltageChang */
#define  TABPNL_VO_28                     123     /* callback function: VoltageChang */
#define  TABPNL_VO_27                     124     /* callback function: VoltageChang */
#define  TABPNL_VO_26                     125     /* callback function: VoltageChang */
#define  TABPNL_VO_25                     126     /* callback function: VoltageChang */
#define  TABPNL_NMR_ATTR2                 127     /* callback function: OnCmdAttrChange */
#define  TABPNL_NMR_ATTR1                 128     /* callback function: OnCmdAttrChange */
#define  TABPNL_VO_24                     129     /* callback function: VoltageChang */
#define  TABPNL_VO_23                     130     /* callback function: VoltageChang */
#define  TABPNL_VO_22                     131     /* callback function: VoltageChang */
#define  TABPNL_VO_21                     132     /* callback function: VoltageChang */
#define  TABPNL_OFFSET                    133
#define  TABPNL_VO_20                     134     /* callback function: VoltageChang */
#define  TABPNL_VO_19                     135     /* callback function: VoltageChang */
#define  TABPNL_VO_18                     136     /* callback function: VoltageChang */
#define  TABPNL_VO_17                     137     /* callback function: VoltageChang */
#define  TABPNL_VO_16                     138     /* callback function: VoltageChang */
#define  TABPNL_VO_15                     139     /* callback function: VoltageChang */
#define  TABPNL_VO_14                     140     /* callback function: VoltageChang */
#define  TABPNL_VO_13                     141     /* callback function: VoltageChang */
#define  TABPNL_VO_12                     142     /* callback function: VoltageChang */
#define  TABPNL_VO_11                     143     /* callback function: VoltageChang */
#define  TABPNL_VO_10                     144     /* callback function: VoltageChang */
#define  TABPNL_VO_9                      145     /* callback function: VoltageChang */
#define  TABPNL_VO_8                      146     /* callback function: VoltageChang */
#define  TABPNL_VO_7                      147     /* callback function: VoltageChang */
#define  TABPNL_VO_6                      148     /* callback function: VoltageChang */
#define  TABPNL_VO_5                      149     /* callback function: VoltageChang */
#define  TABPNL_VO_4                      150     /* callback function: VoltageChang */
#define  TABPNL_VO_3                      151     /* callback function: VoltageChang */
#define  TABPNL_VO_2                      152     /* callback function: VoltageChang */
#define  TABPNL_AD_140                    153
#define  TABPNL_AD_139                    154
#define  TABPNL_AD_138                    155
#define  TABPNL_AD_137                    156
#define  TABPNL_AD_136                    157
#define  TABPNL_AD_135                    158
#define  TABPNL_AD_134                    159
#define  TABPNL_AD_133                    160
#define  TABPNL_AD_132                    161
#define  TABPNL_AD_131                    162
#define  TABPNL_AD_130                    163
#define  TABPNL_AD_129                    164
#define  TABPNL_AD_128                    165
#define  TABPNL_AD_127                    166
#define  TABPNL_AD_126                    167
#define  TABPNL_AD_125                    168
#define  TABPNL_AD_124                    169
#define  TABPNL_AD_123                    170
#define  TABPNL_AD_122                    171
#define  TABPNL_AD_121                    172
#define  TABPNL_AD_120                    173
#define  TABPNL_AD_119                    174
#define  TABPNL_AD_118                    175
#define  TABPNL_AD_117                    176
#define  TABPNL_AD_116                    177
#define  TABPNL_AD_115                    178
#define  TABPNL_AD_114                    179
#define  TABPNL_AD_113                    180
#define  TABPNL_AD_112                    181
#define  TABPNL_AD_111                    182
#define  TABPNL_AD_110                    183
#define  TABPNL_AD_109                    184
#define  TABPNL_AD_108                    185
#define  TABPNL_AD_107                    186
#define  TABPNL_AD_106                    187
#define  TABPNL_AD_105                    188
#define  TABPNL_AD_104                    189
#define  TABPNL_AD_103                    190
#define  TABPNL_AD_102                    191
#define  TABPNL_AD_101                    192
#define  TABPNL_AD_100                    193
#define  TABPNL_AD_99                     194
#define  TABPNL_AD_98                     195
#define  TABPNL_AD_97                     196
#define  TABPNL_AD_96                     197
#define  TABPNL_AD_95                     198
#define  TABPNL_AD_94                     199
#define  TABPNL_AD_93                     200
#define  TABPNL_AD_92                     201
#define  TABPNL_AD_91                     202
#define  TABPNL_AD_90                     203
#define  TABPNL_AD_89                     204
#define  TABPNL_AD_88                     205
#define  TABPNL_AD_87                     206
#define  TABPNL_AD_86                     207
#define  TABPNL_AD_85                     208
#define  TABPNL_AD_84                     209
#define  TABPNL_AD_83                     210
#define  TABPNL_AD_82                     211
#define  TABPNL_GRAPH                     212
#define  TABPNL_AD_81                     213
#define  TABPNL_AD_80                     214
#define  TABPNL_AD_79                     215
#define  TABPNL_AD_78                     216
#define  TABPNL_AD_77                     217
#define  TABPNL_AD_76                     218
#define  TABPNL_AD_75                     219
#define  TABPNL_AD_74                     220
#define  TABPNL_AD_73                     221
#define  TABPNL_AD_72                     222
#define  TABPNL_AD_71                     223
#define  TABPNL_AD_70                     224
#define  TABPNL_AD_69                     225
#define  TABPNL_AD_68                     226
#define  TABPNL_AD_67                     227
#define  TABPNL_AD_66                     228
#define  TABPNL_AD_65                     229
#define  TABPNL_AD_64                     230
#define  TABPNL_AD_63                     231
#define  TABPNL_AD_62                     232
#define  TABPNL_AD_61                     233
#define  TABPNL_AD_60                     234
#define  TABPNL_AD_59                     235
#define  TABPNL_AD_58                     236
#define  TABPNL_AD_57                     237
#define  TABPNL_AD_56                     238
#define  TABPNL_AD_55                     239
#define  TABPNL_AD_54                     240
#define  TABPNL_AD_53                     241
#define  TABPNL_AD_52                     242
#define  TABPNL_AD_51                     243
#define  TABPNL_AD_50                     244
#define  TABPNL_AD_49                     245
#define  TABPNL_AD_48                     246
#define  TABPNL_AD_47                     247
#define  TABPNL_AD_46                     248
#define  TABPNL_AD_45                     249
#define  TABPNL_AD_44                     250
#define  TABPNL_AD_43                     251
#define  TABPNL_AD_42                     252
#define  TABPNL_AD_41                     253
#define  TABPNL_AD_40                     254
#define  TABPNL_AD_39                     255
#define  TABPNL_AD_38                     256
#define  TABPNL_AD_37                     257
#define  TABPNL_AD_36                     258
#define  TABPNL_AD_35                     259
#define  TABPNL_AD_34                     260
#define  TABPNL_AD_33                     261
#define  TABPNL_AD_32                     262
#define  TABPNL_AD_31                     263
#define  TABPNL_AD_30                     264
#define  TABPNL_AD_29                     265
#define  TABPNL_AD_28                     266
#define  TABPNL_AD_27                     267
#define  TABPNL_AD_26                     268
#define  TABPNL_AD_25                     269
#define  TABPNL_AD_24                     270
#define  TABPNL_AD_23                     271
#define  TABPNL_AD_22                     272
#define  TABPNL_AD_21                     273
#define  TABPNL_AD_20                     274
#define  TABPNL_AD_19                     275
#define  TABPNL_AD_18                     276
#define  TABPNL_AD_17                     277
#define  TABPNL_AD_16                     278
#define  TABPNL_AD_15                     279
#define  TABPNL_AD_14                     280
#define  TABPNL_AD_13                     281
#define  TABPNL_AD_12                     282
#define  TABPNL_AD_11                     283
#define  TABPNL_AD_10                     284
#define  TABPNL_AD_9                      285
#define  TABPNL_AD_8                      286
#define  TABPNL_AD_7                      287
#define  TABPNL_AD_6                      288
#define  TABPNL_AD_5                      289
#define  TABPNL_AD_4                      290
#define  TABPNL_AD_3                      291
#define  TABPNL_AD_2                      292
#define  TABPNL_AD_1                      293
#define  TABPNL_VO_1                      294     /* callback function: VoltageChang */
#define  TABPNL_TMSGVOLT                  295
#define  TABPNL_TIMER                     296     /* callback function: TablePolling */
#define  TABPNL_CB_140                    297
#define  TABPNL_CB_139                    298
#define  TABPNL_CB_138                    299
#define  TABPNL_CB_137                    300
#define  TABPNL_CB_136                    301
#define  TABPNL_CB_135                    302
#define  TABPNL_CB_134                    303
#define  TABPNL_CB_133                    304
#define  TABPNL_CB_132                    305
#define  TABPNL_CB_131                    306
#define  TABPNL_CB_130                    307
#define  TABPNL_CB_129                    308
#define  TABPNL_CB_128                    309
#define  TABPNL_CB_127                    310
#define  TABPNL_CB_126                    311
#define  TABPNL_CB_125                    312
#define  TABPNL_CB_124                    313
#define  TABPNL_CB_123                    314
#define  TABPNL_CB_122                    315
#define  TABPNL_CB_121                    316
#define  TABPNL_CB_120                    317
#define  TABPNL_CB_119                    318
#define  TABPNL_CB_118                    319
#define  TABPNL_CB_117                    320
#define  TABPNL_CB_116                    321
#define  TABPNL_CB_115                    322
#define  TABPNL_CB_114                    323
#define  TABPNL_CB_113                    324
#define  TABPNL_CB_112                    325
#define  TABPNL_CB_111                    326
#define  TABPNL_CB_110                    327
#define  TABPNL_CB_109                    328
#define  TABPNL_CB_108                    329
#define  TABPNL_CB_107                    330
#define  TABPNL_CB_106                    331
#define  TABPNL_CB_105                    332
#define  TABPNL_CB_104                    333
#define  TABPNL_CB_103                    334
#define  TABPNL_CB_102                    335
#define  TABPNL_CB_101                    336
#define  TABPNL_CB_100                    337
#define  TABPNL_CB_99                     338
#define  TABPNL_CB_98                     339
#define  TABPNL_CB_97                     340
#define  TABPNL_CB_96                     341
#define  TABPNL_CB_95                     342
#define  TABPNL_CB_94                     343
#define  TABPNL_CB_93                     344
#define  TABPNL_CB_92                     345
#define  TABPNL_CB_91                     346
#define  TABPNL_CB_90                     347
#define  TABPNL_CB_89                     348
#define  TABPNL_CB_88                     349
#define  TABPNL_CB_87                     350
#define  TABPNL_CB_86                     351
#define  TABPNL_CB_85                     352
#define  TABPNL_CB_84                     353
#define  TABPNL_CB_83                     354
#define  TABPNL_CB_82                     355
#define  TABPNL_CB_81                     356
#define  TABPNL_CB_80                     357
#define  TABPNL_CB_79                     358
#define  TABPNL_CB_78                     359
#define  TABPNL_CB_77                     360
#define  TABPNL_CB_76                     361
#define  TABPNL_CB_75                     362
#define  TABPNL_CB_74                     363
#define  TABPNL_CB_73                     364
#define  TABPNL_CB_72                     365
#define  TABPNL_CB_71                     366
#define  TABPNL_CB_70                     367
#define  TABPNL_CB_69                     368
#define  TABPNL_CB_68                     369
#define  TABPNL_CB_67                     370
#define  TABPNL_CB_66                     371
#define  TABPNL_CB_65                     372
#define  TABPNL_CB_64                     373
#define  TABPNL_CB_63                     374
#define  TABPNL_CB_62                     375
#define  TABPNL_CB_61                     376
#define  TABPNL_CB_60                     377
#define  TABPNL_CB_59                     378
#define  TABPNL_CB_58                     379
#define  TABPNL_CB_57                     380
#define  TABPNL_CB_56                     381
#define  TABPNL_CB_55                     382
#define  TABPNL_CB_54                     383
#define  TABPNL_CB_53                     384
#define  TABPNL_CB_52                     385
#define  TABPNL_CB_51                     386
#define  TABPNL_CB_50                     387
#define  TABPNL_CB_49                     388
#define  TABPNL_CB_48                     389
#define  TABPNL_CB_47                     390
#define  TABPNL_CB_46                     391
#define  TABPNL_CB_45                     392
#define  TABPNL_CB_44                     393
#define  TABPNL_CB_43                     394
#define  TABPNL_CB_42                     395
#define  TABPNL_CB_41                     396
#define  TABPNL_CB_40                     397
#define  TABPNL_CB_39                     398
#define  TABPNL_CB_38                     399
#define  TABPNL_CB_37                     400
#define  TABPNL_CB_36                     401
#define  TABPNL_CB_35                     402
#define  TABPNL_CB_34                     403
#define  TABPNL_CB_33                     404
#define  TABPNL_CB_32                     405
#define  TABPNL_CB_31                     406
#define  TABPNL_CB_30                     407
#define  TABPNL_CB_29                     408
#define  TABPNL_CB_28                     409
#define  TABPNL_CB_27                     410
#define  TABPNL_CB_26                     411
#define  TABPNL_CB_25                     412
#define  TABPNL_CB_24                     413
#define  TABPNL_CB_23                     414
#define  TABPNL_CB_22                     415
#define  TABPNL_CB_21                     416
#define  TABPNL_CB_20                     417
#define  TABPNL_CB_19                     418
#define  TABPNL_CB_18                     419
#define  TABPNL_CB_17                     420
#define  TABPNL_CB_16                     421
#define  TABPNL_CB_15                     422
#define  TABPNL_CB_14                     423
#define  TABPNL_CB_13                     424
#define  TABPNL_CB_12                     425
#define  TABPNL_CB_11                     426
#define  TABPNL_CB_10                     427
#define  TABPNL_CB_9                      428
#define  TABPNL_CB_8                      429
#define  TABPNL_CB_7                      430
#define  TABPNL_CB_6                      431
#define  TABPNL_CB_5                      432
#define  TABPNL_CB_4                      433
#define  TABPNL_CB_3                      434
#define  TABPNL_CB_2                      435
#define  TABPNL_CB_1                      436
#define  TABPNL_TEXT1_7                   437
#define  TABPNL_TEXT1_6                   438
#define  TABPNL_TEXT1_5                   439
#define  TABPNL_TEXT1_4                   440
#define  TABPNL_TEXT1_3                   441
#define  TABPNL_TEXT1_2                   442
#define  TABPNL_TEXT2_7                   443
#define  TABPNL_TEXT2_6                   444
#define  TABPNL_TEXT2_5                   445
#define  TABPNL_TEXT2_4                   446
#define  TABPNL_TEXT2_3                   447
#define  TABPNL_TEXT2_2                   448
#define  TABPNL_TEXT2_1                   449
#define  TABPNL_TEXT3_7                   450
#define  TABPNL_TEXT3_6                   451
#define  TABPNL_TEXT3_5                   452
#define  TABPNL_TEXT3_4                   453
#define  TABPNL_TEXT3_3                   454
#define  TABPNL_TEXT3_2                   455
#define  TABPNL_TEXT3_1                   456
#define  TABPNL_TEXT1_1                   457
#define  TABPNL_DECORATION_16             458
#define  TABPNL_DECORATION                459
#define  TABPNL_DECORATION_8              460
#define  TABPNL_DECORATION_7              461
#define  TABPNL_DECORATION_6              462
#define  TABPNL_DECORATION_5              463
#define  TABPNL_DECORATION_4              464
#define  TABPNL_DECORATION_3              465
#define  TABPNL_DECORATION_15             466
#define  TABPNL_DECORATION_14             467
#define  TABPNL_DECORATION_13             468
#define  TABPNL_DECORATION_17             469
#define  TABPNL_TMSG_ATTR1                470
#define  TABPNL_DECORATION_12             471
#define  TABPNL_DECORATION_11             472
#define  TABPNL_DECORATION_10             473
#define  TABPNL_DECORATION_9              474
#define  TABPNL_DECORATION_2              475
#define  TABPNL_TOG_CAL                   476     /* callback function: OnCmdTable */
#define  TABPNL_BTN_SETUP                 477     /* callback function: OnCmdTable */
#define  TABPNL_BTN_LOAD                  478     /* callback function: OnCmdTable */
#define  TABPNL_BTN_SAVE                  479     /* callback function: OnCmdTable */
#define  TABPNL_BTN_DEFSET                480     /* callback function: OnCmdTable */
#define  TABPNL_BTN_DEFGET                481     /* callback function: OnCmdTable */
#define  TABPNL_BTN_DEFAULTSET            482     /* callback function: OnCmdTable */
#define  TABPNL_BTN_DEFAULTMAKE           483     /* callback function: OnCmdTable */
#define  TABPNL_BTN_SPREADING             484     /* callback function: OnCmdTable */
#define  TABPNL_BTN_APPLY                 485     /* callback function: OnCmdTable */
#define  TABPNL_RING_SYSTEM1              486     /* callback function: OnCmdSelSys */
#define  TABPNL_TEXTMSG                   487

#define  TBLDEFPNL                        15
#define  TBLDEFPNL_aa_3                   2
#define  TBLDEFPNL_aa_2                   3
#define  TBLDEFPNL_aa                     4
#define  TBLDEFPNL_TEXTMSG_2              5
#define  TBLDEFPNL_TXT_TIME               6
#define  TBLDEFPNL_NUMERIC                7
#define  TBLDEFPNL_TIMER                  8       /* callback function: TableBKTimer */
#define  TBLDEFPNL_DECORATION             9

#define  WAITPNL                          16
#define  WAITPNL_aa_3                     2
#define  WAITPNL_aa_2                     3
#define  WAITPNL_aa                       4
#define  WAITPNL_TEXTMSG_2                5
#define  WAITPNL_TXT_TIME                 6
#define  WAITPNL_COMMANDBUTTON            7       /* callback function: DownloadWaitQuitButton */


     /* Menu Bars, Menus, and Menu Items: */

#define  MENU_2                           1
#define  MENU_2_FILE                      2
#define  MENU_2_FILE_ITEM2                3
#define  MENU_2_FILE_ITEM1_2              4
#define  MENU_2_FILE_ITEM1                5
#define  MENU_2_FILE_ITEM3                6
#define  MENU_2_FILE_SEPARATOR_4          7
#define  MENU_2_FILE_DEG1                 8       /* callback function: OnMainMenu */
#define  MENU_2_FILE_DEG2                 9       /* callback function: OnMainMenu */
#define  MENU_2_FILE_EXIT                 10      /* callback function: OnMainMenu */
#define  MENU_2_WIN                       11
#define  MENU_2_WIN_LOGIN                 12      /* callback function: OnMainMenu */
#define  MENU_2_WIN_CONSET                13      /* callback function: OnMainMenu */
#define  MENU_2_TOOL                      14
#define  MENU_2_TOOL_LOGSET               15      /* callback function: OnMainMenu */
#define  MENU_2_TOOL_SEPARATOR_2          16
#define  MENU_2_TOOL_IPSET1               17      /* callback function: OnMainMenu */
#define  MENU_2_TOOL_IPSET2               18      /* callback function: OnMainMenu */
#define  MENU_2_TOOL_SEPARATOR            19
#define  MENU_2_TOOL_MAPSET1              20      /* callback function: OnMainMenu */
#define  MENU_2_TOOL_MAPSET2              21      /* callback function: OnMainMenu */
#define  MENU_2_TOOL_SEPARATOR_3          22
#define  MENU_2_TOOL_PWRSET1              23      /* callback function: OnMainMenu */
#define  MENU_2_TOOL_PWRSET2              24      /* callback function: OnMainMenu */
#define  MENU_2_WINDOWS                   25
#define  MENU_2_WINDOWS_SDMSET            26      /* callback function: OnMainMenu */
#define  MENU_2_WINDOWS_MAPINS            27      /* callback function: OnMainMenu */
#define  MENU_2_WINDOWS_ITEM1_3           28
#define  MENU_2_WINDOWS_ITEM3_2           29
#define  MENU_2_WINDOWS_ITEM2_2           30
#define  MENU_2_WINDOWS_ITEM_4            31
#define  MENU_2_HELP                      32
#define  MENU_2_HELP_VER                  33      /* callback function: OnMainMenu */

#define  MENU_MAIN                        2
#define  MENU_MAIN_FILE                   2
#define  MENU_MAIN_FILE_SETUP             3       /* callback function: OnMainMenu */
#define  MENU_MAIN_FILE_SEPARATOR_2       4
#define  MENU_MAIN_FILE_EXIT              5       /* callback function: OnMainMenu */
#define  MENU_MAIN_WIN                    6
#define  MENU_MAIN_WIN_SSPA               7
#define  MENU_MAIN_WIN_SSPA_SUBMENU       8
#define  MENU_MAIN_WIN_SSPA_ASDE1         9       /* callback function: OnMainMenu */
#define  MENU_MAIN_WIN_SSPA_ASDE2         10      /* callback function: OnMainMenu */
#define  MENU_MAIN_WIN_UCM                11
#define  MENU_MAIN_WIN_UCM_SUBMENU        12
#define  MENU_MAIN_WIN_UCM_ASDE1          13      /* callback function: OnMainMenu */
#define  MENU_MAIN_WIN_UCM_ASDE2          14      /* callback function: OnMainMenu */
#define  MENU_MAIN_WIN_SPM                15
#define  MENU_MAIN_WIN_SPM_SUBMENU        16
#define  MENU_MAIN_WIN_SPM_ASDE1          17      /* callback function: OnMainMenu */
#define  MENU_MAIN_WIN_SPM_ASDE2          18      /* callback function: OnMainMenu */
#define  MENU_MAIN_WIN_SPSM               19
#define  MENU_MAIN_WIN_SPSM_SUBMENU       20
#define  MENU_MAIN_WIN_SPSM_ASDE1         21      /* callback function: OnMainMenu */
#define  MENU_MAIN_WIN_SPSM_ASDE2         22      /* callback function: OnMainMenu */
#define  MENU_MAIN_WIN_SCM                23
#define  MENU_MAIN_WIN_SCM_SUBMENU        24
#define  MENU_MAIN_WIN_SCM_ASDE1          25      /* callback function: OnMainMenu */
#define  MENU_MAIN_WIN_SCM_ASDE2          26      /* callback function: OnMainMenu */
#define  MENU_MAIN_WIN_FAN                27
#define  MENU_MAIN_WIN_FAN_SUBMENU        28
#define  MENU_MAIN_WIN_FAN_ASDE1          29      /* callback function: OnMainMenu */
#define  MENU_MAIN_WIN_FAN_ASDE2          30      /* callback function: OnMainMenu */
#define  MENU_MAIN_WIN_ACU                31
#define  MENU_MAIN_WIN_ANTENNA            32
#define  MENU_MAIN_TOOL                   33
#define  MENU_MAIN_TOOL_IPSET1            34      /* callback function: OnMainMenu */
#define  MENU_MAIN_TOOL_IPSET2            35      /* callback function: OnMainMenu */
#define  MENU_MAIN_TOOL_SEPARATOR         36
#define  MENU_MAIN_TOOL_USER              37      /* callback function: OnMainMenu */
#define  MENU_MAIN_HELP                   38
#define  MENU_MAIN_HELP_VER               39      /* callback function: OnMainMenu */


     /* Callback Prototypes: */

int  CVICALLBACK CtrlCHACheck(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DLTimer_Progress(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DLTimer_Response(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DownloadWaitQuitButton(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK IPInfoAutoCheck(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnChangeIDRing(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnChangeIDRingDn(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnCmdAMP(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnCmdAMP_Check(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnCmdAttrChange(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnCmdBOARD(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnCmdCHA(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnCmdDebug(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnCmdDown(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnCmdEMCU(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnCmdENV(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnCmdHPA(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnCmdIP(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnCmdLNA(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnCmdMain(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnCmdMain2(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnCmdModeChange(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnCmdProgress(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnCmdSelSys(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OnCmdTable(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
void CVICALLBACK OnMainMenu(int menubar, int menuItem, void *callbackData, int panel);
int  CVICALLBACK OnUdpConnect(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK PollingTimer(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK PortDropTimer(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ResponseTimer(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK TableBKTimer(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK TablePolling(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK VoltageChang(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
