#include "toolbox.h"
#include <utility.h>
#include <userint.h>     
#include <ansi_c.h>      
#include <formatio.h>    
#include "G_MAIN.h"    
#include "H_EXTERN.H"    




void GetBitFlagVal(int hWnd, int iCtrl, BYTE *pByte, BYTE iBit)
{
	BOOL bOn;

	GetCtrlVal(hWnd, iCtrl, &bOn);
	
	BitFix(pByte, iBit, bOn);
}


UINT8 Ascii2Hex ( UINT8 Upper, UINT8 Lower )
{
	if ( Upper >= 0x41 ) Upper = Upper - 0x07;
	if ( Lower >= 0x41 ) Lower = Lower - 0x07;
	return ( ((Upper-0x30)<<4)|((Lower-0x30) & 0x0f) );
}


