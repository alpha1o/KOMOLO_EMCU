#ifndef GLOBAL_ROID_DEFINE_HEADER
#define GLOBAL_ROID_DEFINE_HEADER


// OPTIC_Alarm
#define	LD_Alarm		BITSET(7) 
#define	PD_1_Alarm		BITSET(6) 
#define	PD_2_Alarm		BITSET(5) 
#define	PD_3_Alarm		BITSET(4) 
#define	PD_4_Alarm		BITSET(3) 

// OPTIC_Type
#define	AID_OM4			0 
#define	AID_OM1			1
#define	AID_R_OPTIC		2
#define	AID_E_OPTIC		3

//	OPTIC_MUX
#define	PATH_ALL_OFF		0x00
#define	PATH_1_ON			0x01
#define	PATH_2_ON			0x02
#define	PATH_3_ON			0x03
#define	PATH_4_ON			0x04
#define	PATH_ALL_ON			0x05



//=====================================================================//
// AID
//=====================================================================//
//	장비 구성정보
#define	OPTIC_Alarm_AID			0x0000		// S
#define	Version_AID				0x0200		// S
#define	OPTIC_Maker_AID			0x0400		// S
#define	Serial_Number_AID		0x0900		// S, C
#define	RESET_AID				0x0A00		// C
#define	OPTIC_Type_AID			0x0B00		// S, C
#define	OPTIC_Alarm_MASK_AID	0x0C00		// S, C

//	ATT
#define	OPTIC_ATT_1_AID			0x1000		// S, C
#define	OPTIC_ATT_2_AID			0x1100		// S, C
#define	OPTIC_ATT_3_AID			0x1200		// S, C
#define	OPTIC_ATT_4_AID			0x1300		// S, C

#define	OPTIC_ATT_Offset_1_AID	0x1600		// S, C
#define	OPTIC_ATT_Offset_2_AID	0x1700		// S, C
#define	OPTIC_ATT_Offset_3_AID	0x1800		// S, C
#define	OPTIC_ATT_Offset_4_AID	0x1900		// S, C

#define	OPTIC_ATT_Out_1_AID		0x1C00		// S
#define	OPTIC_ATT_Out_2_AID		0x1D00		// S
#define	OPTIC_ATT_Out_3_AID		0x1E00		// S
#define	OPTIC_ATT_Out_4_AID		0x1F00		// S

//	POWER
#define	TX_Power_AID			0x3000		// S
#define	RX_Power_AID			0x3080		// S

#define	LD_Power_1_AID			0x3400		// S
#define	LD_Power_2_AID			0x3401		// S 
#define	LD_Power_3_AID			0x3402		// S 
#define	LD_Power_4_AID			0x3403		// S 

#define	PD_Power_1_AID			0x3480		// S
#define	PD_Power_2_AID			0x3481		// S
#define	PD_Power_3_AID			0x3482		// S
#define	PD_Power_4_AID			0x3483		// S

#define	LD_Power_Offset_1_AID	0x3500		// S, C
#define	LD_Power_Offset_2_AID	0x3501		// S, C
#define	LD_Power_Offset_3_AID	0x3502		// S, C
#define	LD_Power_Offset_4_AID	0x3503		// S, C

#define	PD_Power_Offset_1_AID	0x3580		// S, C
#define	PD_Power_Offset_2_AID	0x3581		// S, C
#define	PD_Power_Offset_3_AID	0x3582		// S, C
#define	PD_Power_Offset_4_AID	0x3583		// S, C

#define	LD_1_OnOff_AID			0xAC00
#define	LD_2_OnOff_AID			0xAC01
#define	LD_3_OnOff_AID			0xAC02
#define	LD_4_OnOff_AID			0xAC03


//	Enable
#define	OPTIC_MUX_AID			0x6600		// S, C

//	OPTIC
#define	LED_Enable_AID			0xA800		// S, C

#define	TX_Offset_LOW_AID		0xA900
#define	TX_Offset_middle_AID	0xAA00
#define	TX_Offset_High_AID		0xAB00
#define	RX_Offset_LOW_AID		0xA980
#define	RX_Offset_middle_AID	0xAA80
#define	RX_Offset_High_AID		0xAB80

//  ETC
#define	ATT_DAC_AID				0xEE00		// S
#define	EQ_DAC_AID				0xEE01		// S
#define	Temp_AID				0xEE02		// S

#define	Det_AD_AID				0xEE03		// S

#define	LD_High_Limit_AID		0xEE04		// S, C
#define	PD_High_Limit_1_AID		0xEE05		// S, C
#define	PD_High_Limit_2_AID		0xEE06		// S, C
#define	PD_High_Limit_3_AID		0xEE07		// S, C
#define	PD_High_Limit_4_AID		0xEE08		// S, C

#define	LD_Low_Limit_AID		0xEE09		// S, C
#define	PD_Low_Limit_1_AID		0xEE0A		// S, C
#define	PD_Low_Limit_2_AID		0xEE0B		// S, C
#define	PD_Low_Limit_3_AID		0xEE0C		// S, C
#define	PD_Low_Limit_4_AID		0xEE0D		// S, C

#define	Temp_OnOff_AID			0xEE0E		// S, C

#define	DOU_ID_AID				0xEE0F

#define	Test_Attn_1_AID			0xEE10
#define	Test_Attn_2_AID			0xEE11
#define	Test_Attn_3_AID			0xEE12
#define	Test_Attn_4_AID			0xEE13

#define	Hidden_Offset_1_AID		0xEE14
#define	Hidden_Offset_2_AID		0xEE15
#define	Hidden_Offset_3_AID		0xEE16
#define	Hidden_Offset_4_AID		0xEE17

#endif
