#include "pwctrl.h"
#include "toolbox.h"
#include <userint.h>
#include <utility.h>
#include <ansi_c.h>
#include <formatio.h>	  
#include "H_EXTERN.H"
#include "G_MAIN.h"    



/*********************************************************************************/

BOOL OpenNewHidPanel(int *hWnd, int iPanel, BOOL bAlwaysTop, char *strTitle, char *strUirFile)
{
	if(*hWnd > 0)
		SetActivePanel(*hWnd);
	else
	{
		*hWnd = LoadPanel(0, strUirFile, iPanel);
		if(*hWnd <= 0)
			return FALSE;
	}
	
	SetPanelPos(*hWnd, VAL_AUTO_CENTER, VAL_AUTO_CENTER);
	SetPanelAttribute (*hWnd, ATTR_TITLE, strTitle);
	if(bAlwaysTop)
		SetPanelAttribute(*hWnd, ATTR_FLOATING, VAL_FLOAT_APP_ACTIVE);

	PreOperation(*hWnd, 0);
	
//	DisplayPanel(*hWnd);

	return TRUE;
}				

BOOL OpenNewPanel_Main(int *hWnd, int iPanel, BOOL bAlwaysTop, char *strTitle, char *strUirFile, BOOL bPopup)
{
	*hWnd = LoadPanel(0, strUirFile, iPanel);
	if(*hWnd <= 0)
		return FALSE;
	SetPanelPos(*hWnd, VAL_AUTO_CENTER, VAL_AUTO_CENTER);
	return TRUE;
}										 
	

BOOL OpenNewPanel(int *hWnd, int iPanel, BOOL bAlwaysTop, char *strTitle, char *strUirFile, BOOL bPopup)
{
	if(*hWnd > 0)
		SetActivePanel(*hWnd);
	else
	{
		*hWnd = LoadPanel(0, strUirFile, iPanel);
		if(*hWnd <= 0)
			return FALSE;
	}
	
	SetPanelPos(*hWnd, VAL_AUTO_CENTER, VAL_AUTO_CENTER);

	
	SetPanelAttribute (*hWnd, ATTR_TITLE, strTitle);
	if(bAlwaysTop)
		SetPanelAttribute(*hWnd, ATTR_FLOATING, VAL_FLOAT_APP_ACTIVE);

	PreOperation(*hWnd, 0);
	
	if (bPopup)
		InstallPopup(*hWnd);   
	
	DisplayPanel(*hWnd);

	
	

	return TRUE;
}										 
	

BOOL OpenSubPanel(int *hWnd, int iParentPanel, int iSubPanel, int iPanelTop, int iPanelLeft, char *strUirFile, int iSeld)
{
	if(*hWnd > 0)
		SetActivePanel(*hWnd);
	else
	{
		*hWnd = LoadPanel(iParentPanel, strUirFile, iSubPanel);
		if(*hWnd <= 0)
			return FALSE;
	}

	SetPanelPos(*hWnd, iPanelTop, iPanelLeft);
	SetPanelAttribute (*hWnd, ATTR_TITLEBAR_VISIBLE, 0);
	SetPanelAttribute (*hWnd, ATTR_FRAME_STYLE, VAL_HIDDEN_FRAME);
	PreOperation(*hWnd, iSeld);
	DisplayPanel(*hWnd);

	
	return TRUE;
}



void ClosePanel(int *hWnd)
{
	if(*hWnd <= 0)
		return;

	DiscardPanel(*hWnd);
	
	*hWnd = 0;	
}

//----------------------------------------------------------------------------------------------------//

void CVIReturnDouble(double *dVal, int Power)
{
	int iVal;
	
	if (*dVal >= 0)	 *dVal += 0.0001;
	else			 *dVal -= 0.0001; 
	
	iVal = *dVal * (pow(10., Power));
	*dVal = iVal / (pow(10., Power));
}

//----------------------------------------------------------------------------------------------------//







// Init File Load or Read
////////////////////////////////////////////////////////////////////////////////////


//----------------------------------------------------------------------------------------------------//
void SendRequest(int iIdx, int iCmd, void *pData, int iSize)
{
	int i;
	unsigned short iCodeCRC;
	BYTE cCrcBuf[1024*2];
/*	
	7e 00 47 80 05 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 

00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 

00 00 00 00 00 00 00 00 00 76 78
*/

	
#if Test_COMM
	
	iSize = 0x40;
	iCmd = 0x8005;
	
	m_pSendIO->STX 	= SYNC;
	
	m_pSendIO->DataLen[0] 	= HIBYTE(iSize + 7);
	m_pSendIO->DataLen[1] 	= LOBYTE(iSize + 7);	
	
  	m_pSendIO->Command[0] 	= HIBYTE(iCmd);
	m_pSendIO->Command[1] 	= LOBYTE(iCmd);
	
	for(i =0; i<71 ; i++) m_pSendIO->DataBuf[i] = 0;

	memcpy(cCrcBuf, m_pSendIO, sizeof(COMMIO));
	iCodeCRC = sum_CRC(&cCrcBuf[0], iSize + 5);	
	m_pSendIO->DataBuf[iSize] = HIBYTE(iCodeCRC);
	m_pSendIO->DataBuf[iSize + 1] = LOBYTE(iCodeCRC);
	
#else
	

	m_pSendIO->STX 	= SYNC;
	
	m_pSendIO->DataLen[0] 	= HIBYTE(iSize + 7);
	m_pSendIO->DataLen[1] 	= LOBYTE(iSize + 7);	
	
  	m_pSendIO->Command[0] 	= HIBYTE(iCmd);
	m_pSendIO->Command[1] 	= LOBYTE(iCmd);
	

	
	if(iSize > 0 && pData)		memcpy(m_pSendIO->DataBuf, pData, iSize);
	

	memcpy(cCrcBuf, m_pSendIO, sizeof(COMMIO));
	iCodeCRC = sum_CRC(&cCrcBuf[0], iSize + 5);	
	m_pSendIO->DataBuf[iSize] = HIBYTE(iCodeCRC);
	m_pSendIO->DataBuf[iSize + 1] = LOBYTE(iCodeCRC);	
	
#endif	
	if((RS232IsOpen(iIdx))&&(m_pCOMM[iIdx].iCommMethod == COMM_SERIAL))
	{
		RS232Send(iIdx, (BYTE *)m_pSendIO, iSize + 7);
	}
	else if (m_pCOMM[iIdx].iCommMethod == COMM_UDP)
	{
			
		UDPSendData(iIdx, (BYTE *)m_pSendIO, iSize + 7, m_pCOMM_UDP[iIdx].m_cUdpIp, m_pCOMM_UDP[iIdx].m_iUdpPort);	      
	} 
 
}


//----------------------------------------------------------------------------------------------------//
void CtrlsAttribute(int hWnd, int iFlagCtrl, BOOL bSetMode, int iValCtrl, int iCtrlType)
{
	if (!hWnd)
		return;
		
	if(iFlagCtrl)
	{
		SetCtrlVal(hWnd, iFlagCtrl, 0);
		SetCtrlAttribute(hWnd, iFlagCtrl, ATTR_VISIBLE, bSetMode);
	}
	
	if((iCtrlType!=iCT_TEXT)&&(iCtrlType!=iCT_STRING))
		SetCtrlAttribute(hWnd, iValCtrl, ATTR_CTRL_MODE, bSetMode);
	
	if((iCtrlType!=iCT_CHECKBOX)&&(iCtrlType!=iCT_TEXT))
	{
		SetCtrlAttribute (hWnd, iValCtrl, ATTR_TEXT_COLOR, VAL_BLACK);
		SetCtrlAttribute (hWnd, iValCtrl, ATTR_TEXT_BOLD, 0);
	}
	
	if(iCtrlType==iCT_TEXT)
	{
		char stInsText[16];
		GetCtrlVal(hWnd, iValCtrl, stInsText);
		if(!strcmp(stInsText, "[ Insert ]"))	SetCtrlAttribute(hWnd, iValCtrl, ATTR_TEXT_COLOR, VAL_BLUE);
		else									SetCtrlAttribute(hWnd, iValCtrl, ATTR_TEXT_COLOR, VAL_BLACK);
		
	}
	
	if(iCtrlType==iCT_NUMERIC)
	{
		SetCtrlAttribute(hWnd, iValCtrl, ATTR_TEXT_BGCOLOR, VAL_WHITE);
	}
	
	if(iCtrlType==iCT_RING)
	{
		// SetCtrlAttribute(hWnd, iValCtrl, ATTR_TEXT_BGCOLOR, VAL_WHITE);
	}
}

//----------------------------------------------------------------------------------------------------//
void SetInsertValue(int hWnd, int iCtrl, BYTE iByte, BYTE iBit)
{
	SetCtrlVal(hWnd, iCtrl, (iByte & iBit)?"[ Insert ]":"[ Not Insert ]");
	SetCtrlAttribute(hWnd, iCtrl, ATTR_TEXT_COLOR, (iByte & iBit)?VAL_BLUE:VAL_BLACK);	
}

//----------------------------------------------------------------------------------------------------//
BOOL SetOnOffValue(BYTE iByte)
{
	if(iByte == '1')
		return TRUE;
	else
		return FALSE;
}

//----------------------------------------------------------------------------------------------------//
void SetBitDefVal(int hWnd, int iByte, int iCtrl, int iBit)
{
	SetCtrlVal(hWnd, iCtrl, iByte & iBit);
}

//----------------------------------------------------------------------------------------------------//
void GetOnOffVal(int hWnd, int iCtrl, BYTE *iSetByte)
{
	BOOL bOn;

	GetCtrlVal(hWnd, iCtrl, &bOn);
	if(bOn)
		*iSetByte = '1';
	else
		*iSetByte = '0';
}

//----------------------------------------------------------------------------------------------------//
void GetBitVal(int hWnd, int iCtrl, BYTE *pByte, BYTE iBit)
{
	BOOL bOn;
	
	GetCtrlVal(hWnd, iCtrl, &bOn);
	BitFix(pByte, iBit, bOn);
}

//----------------------------------------------------------------------------------------------------//
void GetInsertVal(int hWnd, int iCtrl, BYTE *pByte, BYTE iBit)
{
	char stInsText[16];

	GetCtrlVal(hWnd, iCtrl, stInsText);
	BitFix(pByte, iBit, !strcmp(stInsText, "[ Insert ]"));
}

//----------------------------------------------------------------------------------------------------//
void GetInDoorVal(int hWnd, int iCtrl, BYTE *iSetByte) 
{
	int iVal;

	GetCtrlVal(hWnd, iCtrl, &iVal);
	
	*iSetByte = iVal;
}
//----------------------------------------------------------------------------------------------------//

//-------------------------------------------------------------------------------//
void DisplayRS232Error(int error)
{
	char ErrorMessage[200];
	
	int RS232Error = 1; // 추후 수정해야 함..
	
	switch (error)
	{
		default:
			if(error < 0) 
			{
				Fmt(ErrorMessage, "%s<RS232 error number %i", RS232Error);
				MessagePopup("RS232 Message", ErrorMessage);
			}
			break;
		
		case 0:
		   	MessagePopup("RS232 Message", "No errors.");
			break;
		
		case -2:
			Fmt(ErrorMessage, "%s", "Invalid port number (must be in the range 1 to 8).");
			MessagePopup("RS232 Message", ErrorMessage);
			break;
		
		case -3:
			Fmt(ErrorMessage, "%s", "No port is open.\n"
				"Check COM Port setting in Configure.");
			MessagePopup("RS232 Message", ErrorMessage);
			break;
		
		case -99:
			Fmt(ErrorMessage, "%s", "Timeout error.\n\n"
				"Either increase timeout value,\n"
				"       check COM Port setting, or\n"
				"       check device.");
			MessagePopup("RS232 Message", ErrorMessage);       
			break;
	}
}


///////////

void LastEnvInfoSave(void)
{
	int hFile;
	char strDir[MAX_PATHNAME_LEN];
	int i, j ;
	
	
		GetCtrlVal(m_hWndEMCU, EMCUPNL_NMR_SERVER1, &m_pIPSet->EMCU_IP[0]);
		GetCtrlVal(m_hWndEMCU, EMCUPNL_NMR_SERVER2, &m_pIPSet->EMCU_IP[1]);
		GetCtrlVal(m_hWndEMCU, EMCUPNL_NMR_SERVER3, &m_pIPSet->EMCU_IP[2]);
		GetCtrlVal(m_hWndEMCU, EMCUPNL_NMR_SERVER4, &m_pIPSet->EMCU_IP[3]);
		
	//	GetCtrlVal(m_hWndIP, IPPNL_NMR_PORT, &m_pIPSet->EMCU_PORT);
		
		m_pIPSet->EMCU_PORT = 11120;
	/*	
		GetCtrlVal(m_hWndIP, IPPNL_NMR_GW1, &m_pIPSet->EMCU_GATEWAY[0]);
		GetCtrlVal(m_hWndIP, IPPNL_NMR_GW2, &m_pIPSet->EMCU_GATEWAY[1]);
		GetCtrlVal(m_hWndIP, IPPNL_NMR_GW3, &m_pIPSet->EMCU_GATEWAY[2]);
		GetCtrlVal(m_hWndIP, IPPNL_NMR_GW4, &m_pIPSet->EMCU_GATEWAY[3]);
		
		GetCtrlVal(m_hWndIP, IPPNL_NMR_SM1, &m_pIPSet->EMCU_SUBNET[0]);
		GetCtrlVal(m_hWndIP, IPPNL_NMR_SM2, &m_pIPSet->EMCU_SUBNET[1]);
		GetCtrlVal(m_hWndIP, IPPNL_NMR_SM3, &m_pIPSet->EMCU_SUBNET[2]);
		GetCtrlVal(m_hWndIP, IPPNL_NMR_SM4, &m_pIPSet->EMCU_SUBNET[3]);
		
		GetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_U_1, &m_pIPSet->EBU_U_IP[0]);
		GetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_U_2, &m_pIPSet->EBU_U_IP[1]);
		GetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_U_3, &m_pIPSet->EBU_U_IP[2]);
		GetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_U_4, &m_pIPSet->EBU_U_IP[3]);
		
		GetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_L_1, &m_pIPSet->EBU_L_IP[0]);
		GetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_L_2, &m_pIPSet->EBU_L_IP[1]);
		GetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_L_3, &m_pIPSet->EBU_L_IP[2]);
		GetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_L_4, &m_pIPSet->EBU_L_IP[3]);
		
		
	 */

		GetProjectDir(strDir);
		strcat(strDir, "\\config.dat");  

		hFile = OpenFile(strDir, VAL_WRITE_ONLY, VAL_TRUNCATE, VAL_ASCII); 

		for (j = 0; j<4; j++)
		{
			FmtFile(hFile, "%s<%i\n", m_pIPSet->EMCU_IP[j]); 
		}

		FmtFile(hFile, "%s<%i\n", m_pIPSet->EMCU_PORT);  

		CloseFile(hFile);
		
		
		GetProjectDir(strDir);
		strcat(strDir, "\\EBSC\\network_info.cfg ");  

		hFile = OpenFile(strDir, VAL_WRITE_ONLY, VAL_TRUNCATE, VAL_ASCII); 
	
		FmtFile(hFile, "[Device IP Address] = %i.%i.%i.%i\n", m_pIPSet->EMCU_IP[0], m_pIPSet->EMCU_IP[1], m_pIPSet->EMCU_IP[2], m_pIPSet->EMCU_IP[3]); 
	
		CloseFile(hFile);
		
		
		MessagePopup("Success!", " IP Setup is Completed.");
		
}

void LastEnvInfoLoad(void)
{
	int hFile;
	char strDir[MAX_PATHNAME_LEN];
	char stTemp[64];
	int intSize;
	int iVal;
	int i, j ;
	
	GetProjectDir (strDir);
//	strcat(strDir, "\\EtcInfo.dat");	
	strcat(strDir, "\\config.dat");
	
	if (FileExists (strDir, &intSize)!=1) 
	{
		
		m_pIPSet->EMCU_IP[0] = 192;
		m_pIPSet->EMCU_IP[1] = 168; 
		m_pIPSet->EMCU_IP[2] = 10; 
		m_pIPSet->EMCU_IP[3] = 121;
		
		m_pIPSet->EMCU_PORT = 11120;
		

		hFile = OpenFile(strDir, VAL_WRITE_ONLY, VAL_TRUNCATE, VAL_ASCII); 
		///////////////////////////	

		for (j = 0; j<4; j++)
		{
			FmtFile(hFile, "%s<%i\n", m_pIPSet->EMCU_IP[j]); 
		}
		
		FmtFile(hFile, "%s<%i\n", m_pIPSet->EMCU_PORT);  
		
		CloseFile(hFile);
			
		
	
		
	}
	else
	{
		hFile = OpenFile (strDir, VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);   


		for (j = 0; j<4; j++)
		{
			memset (stTemp, NULL, 16);
			ReadLine (hFile, stTemp, -1);
			m_pIPSet->EMCU_IP[j] = atoi(stTemp);
		}
		
		memset (stTemp, NULL, 16);
		ReadLine (hFile, stTemp, -1);
		
		//m_pIPSet->EMCU_PORT  = atoi(stTemp);
		
		m_pIPSet->EMCU_PORT = 11120; 
		
	
		CloseFile (hFile);	
	}
	
	if(m_hWndEMCU) 
	{
		SetCtrlVal(m_hWndEMCU, EMCUPNL_NMR_SERVER1, m_pIPSet->EMCU_IP[0]);
		SetCtrlVal(m_hWndEMCU, EMCUPNL_NMR_SERVER2, m_pIPSet->EMCU_IP[1]);
		SetCtrlVal(m_hWndEMCU, EMCUPNL_NMR_SERVER3, m_pIPSet->EMCU_IP[2]);
		SetCtrlVal(m_hWndEMCU, EMCUPNL_NMR_SERVER4, m_pIPSet->EMCU_IP[3]);
	}
	
	m_pCOMM[0].iSetupInfo[2] = m_pIPSet->EMCU_IP[0];
	m_pCOMM[0].iSetupInfo[3] = m_pIPSet->EMCU_IP[1];
	m_pCOMM[0].iSetupInfo[4] = m_pIPSet->EMCU_IP[2];
	m_pCOMM[0].iSetupInfo[5] = m_pIPSet->EMCU_IP[3];
	m_pCOMM_UDP[0].m_iUdpPort = m_pCOMM[0].iSetupInfo[6] = m_pIPSet->EMCU_PORT;
	
	m_pCOMM_SER[0].m_ComAttr.iComport = m_pCOMM[0].iSetupInfo[8] = 5;
	m_pCOMM_SER[0].m_ComAttr.iBaudrate = m_pCOMM[0].iSetupInfo[9] = 115200;
	
	
	
#if Test_COMM

#else
		SetupDataInit();  		

#endif
		
	

}




void SetupDataSave(void)
{
	int hFile;
	char strDir[MAX_PATHNAME_LEN];
	int iAdd1,iAdd2,iAdd3,iAdd4; 
	int iVal;
	int i, j;
	
	GetProjectDir(strDir);
	strcat(strDir, "\\config.dat");

	hFile = OpenFile(strDir, VAL_WRITE_ONLY, VAL_TRUNCATE, VAL_ASCII); 
///////////////////////////	

	
	
	CloseFile(hFile);	

	SetupDataInit();
	
	MessagePopup("SETUP", "저장하였습니다.");
	
}	


void SetupDataInit(void)
{
	char stTemp[64];
	
	int i, j;
	
	i = 0;
	
	strcpy(m_pCOMM_UDP[i].m_cUdpIp, "");
//	strcpy(m_cUdpIp_Part1, "");
	Fmt(m_pCOMM_UDP[i].m_cUdpIp, "%s<%d.%d.%d.%d", m_pCOMM[i].iSetupInfo[2], m_pCOMM[i].iSetupInfo[3], m_pCOMM[i].iSetupInfo[4], m_pCOMM[i].iSetupInfo[5]);
	m_pCOMM_UDP[i].m_iUdpPort = m_pCOMM[i].iSetupInfo[6];
	
	m_pCOMM_SER[i].m_ComAttr.iComport = m_pCOMM[i].iSetupInfo[8];
	m_pCOMM_SER[i].m_ComAttr.iBaudrate = m_pCOMM[i].iSetupInfo[9];  

	memset (stTemp, NULL, 64);
	
}


////////////
int CVICALLBACK OnCmdWinSSPA (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int iIdx = -1;
	int i;
	
	if(event != EVENT_COMMIT)
		return 0;
	
	for (i=0; i<DEF_COMM_CNT;i++)
	{
		if (m_pCOMM[i].ihWndSSPA == panel)
		{	
			iIdx = i;
			break;
		}
	}
	
	if (iIdx == -1)
		return 0;
	

	return 0; 
}

int CVICALLBACK OnCmdWinSPSM (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int iIdx = -1;
	int i;
	
	if(event != EVENT_COMMIT)
		return 0;
	
	for (i=0; i<DEF_COMM_CNT;i++)
	{
		if (m_pCOMM[i].ihWndSPSM == panel)
		{	
			iIdx = i;
			break;
		}
	}
	
	if (iIdx == -1)
		return 0;
	

	return 0;
}

int CVICALLBACK OnCmdWinMPSM (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int iIdx = -1;
	int i;
	
	if(event != EVENT_COMMIT)
		return 0;
	
	for (i=0; i<DEF_COMM_CNT;i++)
	{
		if (m_pCOMM[i].ihWndMPSM == panel)
		{	
			iIdx = i;
			break;
		}
	}
	
	if (iIdx == -1)
		return 0;
	

	return 0;
}

int CVICALLBACK OnCmdWinUCM (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int iIdx = -1;
	int i;
	
	if(event != EVENT_COMMIT)
		return 0;
	
	for (i=0; i<DEF_COMM_CNT;i++)
	{
		if (m_pCOMM[i].ihWndUCM == panel)
		{	
			iIdx = i;
			break;
		}
	}
	
	if (iIdx == -1)
		return 0;
	

	return 0;
}

int CVICALLBACK OnCmdWinDCM (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int iIdx = -1;
	int i;
	
	if(event != EVENT_COMMIT)
		return 0;
	
	for (i=0; i<DEF_COMM_CNT;i++)
	{
		if (m_pCOMM[i].ihWndDCM == panel)
		{	
			iIdx = i;
			break;
		}
	}
	
	if (iIdx == -1)
		return 0;
	

	return 0;
}

int CVICALLBACK OnCmdWinFSM (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int iIdx = -1;
	int i;
	
	if(event != EVENT_COMMIT)
		return 0;
	
	for (i=0; i<DEF_COMM_CNT;i++)
	{
		if (m_pCOMM[i].ihWndFSM == panel)
		{	
			iIdx = i;
			break;
		}
	}
	
	if (iIdx == -1)
		return 0;
	

	return 0;
}


int CVICALLBACK OnCmdWinSCM (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int iIdx = -1;
	int i;
	
	if(event != EVENT_COMMIT)
		return 0;
	
	for (i=0; i<DEF_COMM_CNT;i++)
	{
		if (m_pCOMM[i].ihWndSCM == panel)
		{	
			iIdx = i;
			break;
		}
	}
	
	if (iIdx == -1)
		return 0;
	
	
/*		case PNLSCM_BTN_SETUP :
		{
			EnableTimer(m_hWndMain, m_pCOMM[iIdx].iPollingTimer, FALSE); 
			MainPanelSetData_SCM(iIdx);
			EnableTimer(m_hWndMain, m_pCOMM[iIdx].iWaitTimer, TRUE);  
			break;
		}
*/		
		
/*		case PNLSCM_BTN_TIMESET:
		{
			int iYear, iMonth, iDay, iHour, iMin, iSec;
			UINT8	pRtcTime[7];
			
			GetSystemDate(&iMonth, &iDay, &iYear);
			GetSystemTime(&iHour, &iMin, &iSec);
			
			memset(pRtcTime, NULL, 7);
			
			pRtcTime[0] = DECtoBcd(iYear - 2000);
			pRtcTime[1] = DECtoBcd(iMonth);
			pRtcTime[2] = DECtoBcd(iDay);
			pRtcTime[3] = DECtoBcd(iHour);
			pRtcTime[4] = DECtoBcd(iMin);
			pRtcTime[5] = DECtoBcd(iSec);
			

			SendRequest(iIdx, TIMESET_REQ, pRtcTime, 7);	
			break;
		}
		
		case PNLSCM_BTN_DATASAVE:
		{
			int iYear, iMonth, iDay, iHour, iMin, iSec;
			UINT8	pRtcTime[7];
			
			GetSystemDate(&iMonth, &iDay, &iYear);
			GetSystemTime(&iHour, &iMin, &iSec);
			
			memset(pRtcTime, NULL, 7);
			
			pRtcTime[0] = DECtoBcd(iYear - 2000);
			pRtcTime[1] = DECtoBcd(iMonth);
			pRtcTime[2] = DECtoBcd(iDay);
			pRtcTime[3] = DECtoBcd(iHour);
			pRtcTime[4] = DECtoBcd(iMin);
			pRtcTime[5] = DECtoBcd(iSec);
			

			SendRequest(iIdx, DataSaveSetRQST, pRtcTime, 0);	
			break;
		}
*/		

		return 0;
}


int CVICALLBACK OnCmdWinSPM (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int iIdx = -1;
	int i;
	
	if(event != EVENT_COMMIT)
		return 0;
	
	for (i=0; i<DEF_COMM_CNT;i++)
	{
		if (m_pCOMM[i].ihWndSPM == panel)
		{	
			iIdx = i;
			break;
		}
	}
	
	if (iIdx == -1)
		return 0;
	

	return 0;
}


int CVICALLBACK OnCmdWinFAN (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int iIdx = -1;
	int i;
	
	if(event != EVENT_COMMIT)
		return 0;
	
	for (i=0; i<DEF_COMM_CNT;i++)
	{
		if (m_pCOMM[i].ihWndFAN == panel)
		{	
			iIdx = i;
			break;
		}
	}
	
	if (iIdx == -1)
		return 0;
	

	return 0;
}


int CVICALLBACK OnCmdWinACU (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int iIdx = -1;
	int i;
	
	if(event != EVENT_COMMIT)
		return 0;
	
	for (i=0; i<DEF_COMM_CNT;i++)
	{
		if (m_pCOMM[i].ihWndACU == panel)
		{	
			iIdx = i;
			break;
		}
	}
	
	if (iIdx == -1)
		return 0;
	

	return 0;
}


int CVICALLBACK OnCmdWinAntenna (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int iIdx = -1;
	int i;
	
	if(event != EVENT_COMMIT)
		return 0;
	
	for (i=0; i<DEF_COMM_CNT;i++)
	{
		if (m_pCOMM[i].ihWndAntenna == panel)
		{	
			iIdx = i;
			break;
		}
	}
	
	if (iIdx == -1)
		return 0;

	return 0;
}


////////////////////////////

#define View_Alarm		0
#define View_Str		1






////////////



