#include <ansi_c.h>
#include <formatio.h>
#include "G_MAIN.h"
#include "H_EXTERN.H"


void DebugStatusDisplay(int iIdx, BYTE *pData, int nLength, BOOL bTx)
{   
	int i;
	int Vline;
	int iMode;
	BYTE cDebugBuf[1024*3];
	int hWndDebug;
	
	hWndDebug = m_pCOMM[iIdx].iPanelHandle_Debug;

	if (hWndDebug)
	{													  
		GetCtrlVal(hWndDebug, DEBPNL_FORMSEL_1, &iMode);
		
		memset(cDebugBuf, NULL, 1024*3);
		
		if(bTx)
			strcpy(cDebugBuf, "[ TX ] ");	       
		else
			strcpy(cDebugBuf, "[ RX ] ");	       
		
		if(!iMode)
		{
			for(i=0; i<nLength; i++) 
				Fmt(&cDebugBuf[7 + i], "%c[w1]", *((UINT8 *)(pData)+i));	
			
		}
		else
		{
		
			for(i=0; i<nLength; i++) 
				Fmt(&cDebugBuf[7 + i*3], "%x[w2p0] ", *((UINT8 *)(pData)+i));	
			
		}
	
		InsertTextBoxLine (hWndDebug, DEBPNL_BOX, -1, cDebugBuf);			  
		GetNumTextBoxLines (hWndDebug,DEBPNL_BOX, &i);
		InsertTextBoxLine (hWndDebug, DEBPNL_BOX, -1, "");

		if(i >= 5) 
			SetCtrlAttribute(hWndDebug, DEBPNL_BOX, ATTR_FIRST_VISIBLE_LINE, i-5);
 
        GetCtrlAttribute(hWndDebug, DEBPNL_BOX, ATTR_TOTAL_LINES, &Vline); 
        if(Vline > 1024) 
			ResetTextBox (hWndDebug, DEBPNL_BOX, ""); 
	}			
}	 


int DebugCallBack(int iIdx, BYTE *pData, int nLength)
{	
	DebugStatusDisplay(iIdx, pData, nLength, FALSE);
	return 0;
}

int CVICALLBACK OnCmdDebug (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int i, iIndex;
	int hWndDebug;
	if(event != EVENT_COMMIT)
		return 0;


		
	hWndDebug =  panel;
	switch(control)
	{
		// Go Button
		case DEBPNL_BTN_GO:
		{
			EnableTimer(m_hWndMain, m_pCOMM[0].iPollingTimer, TRUE); 
			break;
		}
		
		// Stop Button
		case DEBPNL_BTN_STOP:
		{
			EnableTimer(m_hWndMain, m_pCOMM[0].iPollingTimer, FALSE); 
			break;
		}
		
		// List Clear Button
		case DEBPNL_BTN_CLEAR:
		{
			ResetTextBox(hWndDebug, DEBPNL_BOX, ""); 	 
			break;
		}
		
		// Close Window Button
		case DEBPNL_BTN_EXIT:
		{
			ClosePanel(&m_pCOMM[0].iPanelHandle_Debug);
			m_pCOMM[0].iPanelHandle_Debug = 0;
			break;
		}
		
		// Window Position (Always Top or Not) Button
		case DEBPNL_TOPSEL :
		{
			int Mode;
			
			GetCtrlVal(hWndDebug, DEBPNL_TOPSEL, 	&Mode);
		
			if(Mode == 1)
				SetPanelAttribute(hWndDebug, ATTR_FLOATING, VAL_FLOAT_APP_ACTIVE);
			else  				
				SetPanelAttribute(hWndDebug, ATTR_FLOATING, VAL_FLOAT_NEVER);
				
			break;
		}
		
		// Hex Display Mode Button 
		case DEBPNL_FORMSEL_1 :
		{
			SetCtrlVal(hWndDebug, DEBPNL_FORMSEL_1, 1);
			SetCtrlVal(hWndDebug, DEBPNL_FORMSEL_2, 0);
			
			break;
		}
		
		// ASCII Display Mode Button
		case DEBPNL_FORMSEL_2 :
		{
			SetCtrlVal(hWndDebug, DEBPNL_FORMSEL_1, 0);
			SetCtrlVal(hWndDebug, DEBPNL_FORMSEL_2, 1);
			
			break;
		}
		
		case DEBPNL_MODESEL :
		{
			int iMode;
			
			GetCtrlVal(hWndDebug, DEBPNL_MODESEL, &iMode);
			RS232SetMode(0, DebugCallBack, !iMode);	
			UDPSetMode_Temp(0, DebugCallBack, !iMode);	
			
			if(iMode)
				SetCtrlVal(hWndDebug, DEBPNL_TMSG_MODE, "�� Release Mode (Mode Change : F3 Key)  ");
			else
				SetCtrlVal(hWndDebug, DEBPNL_TMSG_MODE, "�� Debug Mode (Mode Change : F3 Key)  ");
			
			break;
		}
	}
	return 0;
}  

int CVICALLBACK OnCmdModeChange (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int i, iIndex;
	int hWndDebug;
	int iMode; 	
	
	if(event != EVENT_COMMIT)
		return 0;


		
	hWndDebug =  panel;		
		
	GetCtrlVal(hWndDebug, DEBPNL_MODESEL, &iMode);
	SetCtrlVal(hWndDebug, DEBPNL_MODESEL, !iMode);

	OnCmdDebug(panel, DEBPNL_MODESEL, EVENT_COMMIT, 0, 0, 0);

	return 0;
}
