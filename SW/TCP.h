#ifndef TCP_LIB_HEADER
#define TCP_LIB_HEADER

#include "StdUtil.h"
  
//General Define
#define EVENT_CONNECT_OK	0x01
#define EVENT_CONNECT_FAIL	0x02
#define EVENT_PEERCONNECTED 0x03
#define EVENT_SEND			0x04
#define EVENT_RECV			0x05
#define EVENT_ONCLOSE		0x06


//Error Code 
#define ERR_CONNECT_NOERROR		0x00		//Connect Success
#define ERR_CONNECT_VALIDIP		0x01		//Connect Error - 유효하지 않은 IP
#define ERR_CONNECT_CONEXIST	0x02		//Connect Error - 이미 연결된 Connection ID
#define ERR_CONNECT_MEMORY		0x03		//Connect Error - Memory 부족
#define ERR_CONNECT_FAIL		0x04		//Connect Error - Connect 실패. (Peer is not Listening)
#define ERR_CONNECT_ETC			0x05		//Connect Error - ETC Error



//Callbakc Func Type Define
typedef int (__cdecl *PLENCALLBACK)(BYTE *pLenBuf);
typedef int (__cdecl *PTCPPACKETEVENTCALLBACK)(BYTE dwEventFlag, int iConnectID, BYTE *pData, int nLength);
typedef int (__cdecl *PTCPCONNECTEVENTCALLBACK)(BYTE dwEventFlag, int iConnectID, int iErrCode);


// CallBack 함수 설명

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FuncType 	: int PLENCALLBACK(BYTE *pLenBuf)
// Purpose  	: 패킷 구조에서 패킷 길이를 나타내는 필드를 가지고, 실제 패킷의 데이터를 계산, 돌려주는 함수이다.
//			  	  이 함수는 Receive 시 패킷 길이를 계산하기 위해 Lib 로 부터 호출된다.
// Parameter 	: 
//					pLenBuf - 패킷의 길이를 나타내는 필드 포인터
//
// Return		: pLenBuf 를 가지고 계산된 실제 패킷 길이
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FuncType 	: int PTCPPACKETEVENTCALLBACK(BYTE dwEventFlag, int iConnectID, BYTE *pData, int nLength)
// Purpose  	: 하나의 패킷이 완전히 보내지거나 수신되었을 때 Lib 로부터 호출된다.
// Parameter 	: 
//					dwEventFlag - 0x04 : 하나의 패킷이 전송되었을 경우 
//							    - 0x05 : 하나의 패킷이 수신되었을 경우
//
//					iConnectID  : 어떤 Peer 를 상대로 전송/수신 이 이루어졌는지를 나타내는 Connection ID (Multi Connect 일 경우 사용)
//					pData		: 전송/수신된 데이터 포인터
//					nLength		: 전송/수신된 데이터의 길이
//
// Return		: 의미 없음.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FuncType 	: int PTCPCONNECTEVENTCALLBACK(BYTE dwEventFlag, int iConnectID, int iErrCode)
// Purpose  	: 연결관련하여 각 상황에 따른 이벤트 발생
// Parameter 	: 
//					dwEventFlag - 0x01 : Peer 에게 연결을 시도한 후 해당 연결이 성공적으로 이루어졌을경우
//							    - 0x02 : Peer 에게 연결을 시도, 연결이 이루어 지지 않고 실패한 경우
//								- 0x03 : Peer 로부터 연결 요청이 발생하여 연결이 이루어진 경우
//								- 0x06 : 연결된 Connection 이 Drop 된 경우 
//								
//					iConnectID  : 어떤 Peer 를 상대로 디벤트가 발생했는지를 나타내는 Connection ID (Multi Connect 일 경우 사용)
//					iErrCode    : Error Code
//
// Return		: dwEventFlag 가 0x03 일 경우 즉, Peer 가 연결을 요청하여 연결이 이루어진 경우 앞으로 해당 Peer 와의 연결에 사용할
//				  Connection ID 를 리턴해야 한다. (Connection ID 는 각 연결마다 고유한 값을 가져야 한다.)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// Functions
//////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : TCPInit
// Purpose  : TCP Send, Rcv 를 위한 초기화.
// Prameter :   
//				nLenStartByte - 패킷 구조 중 데이터 길이를 나타내는 필드의 시작 바이트
//				nLenFieldSize - 데이터 길이를 나타내는 필드의 크기
//				pLengthFunc   - 데이터 길이를 계산하기 위한 callback 함수 포인터. //								
//				pDataEventFunc- TCP Event (Send, Rcv) 를 감지하기 위한 callback 함수 포인터.
//				pConEvnetFunc - TCP Event (Conenct, Close 관련) 를 감지하기 위한 callback 함수 포인터. 						
// Return   : 
//				TRUE  [1] - Success
//				FALSE [0] - Fail
// Remark   : 
//				pLenthFunc - 잘라지거나, 혹은 여러개의 패킷이 동시에 밀려들어오는 경우를 대비하여 본 Lib 를 사용한다.
//							 그러기 위해서 패킷구조상 데이터의 길이를 나타내는 필드를 실제 유효한 길이값으로 변환해야 하는데
//							 이 작업을 App 에서 대신한다. 이렇게 함으로써 본 Lib 는 공용성을 유지할 수 있다.
//							 pLenthFunc 함수는 App 에서 정의 해야 하며 다음과 같은 기능을 제공해야 한다.
//								
//								Input  : BYTE *pLenBuf - 길이 필드 포인터 (길이 필드의 byte 수는 APP 에서 이미 알고 있다.)
//								Output : int 		   - pLenBuf 를 사용하여 패킷 전체의 길이를 계산, return 한다.
//
//				pEventFunc 		- PTCPPACKETEVENTCALLBACK 함수 포인터
//				pConEvnetFunc 	- PTCPCONNECTEVENTCALLBACK 함수 포인터
//				
//
//				주의) !!! 본 함수에서 사용되는 세개의 callback 함수들을 반드시 정의해야 TCP 가 제대로 동작할 수 있다. !!!
//				주의) !!! TCP/IP Connect, Close, DoListen 를 수행하기 전에 반드시 TCPInit 함수를 호출해야 한다. !!!
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
BOOL 	TCPInit(UINT8 nLenStartByte, UINT8 nLenFieldSize, PLENCALLBACK pLengthFunc, PTCPPACKETEVENTCALLBACK pDataEventFunc, PTCPCONNECTEVENTCALLBACK pConEvnetFunc);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : TCPEnd
// Purpose  : TCP/IP Lib 사용 종료
// Prameter : 
// Return   : 
// Remart   : TCPInit 호출 후 , 프로그램 종료 전 본 함수를 반드시 호출해야 한다. ( Socket 관련 Dll 종료 )
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void	TCPEnd(void);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : TCPConnect
// Purpose  : TCP/IP 로 PEER 에 연결 시도.
// Prameter : iConnectID - Connection ID
//			  pIPString  - Doted IP String
//			  uPort		 - Port Number
// Return   : 
// Remart   : 연결 성공 여부는 PTCPCONNECTEVENTCALLBACK 함수가 발생될 때 알수 있다.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void	TCPConnect(int iConnectID, char *pIPString, UINT uPort);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : TCPClose
// Purpose  : 연결된 커넥션 해제
// Prameter : iConnectID - Connection ID
// Return   : 
// Remart   : 연결이 해제되면 PTCPCONNECTEVENTCALLBACK 함수에 OnClose Flag 발생.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void	TCPClose(int iConnectID);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : DoListen
// Purpose  : Socket Listen For Accept connection
// Prameter : uPort	  	 - Listen 시 사용할 Port Number
// Return   : TRUE 	[1]  - Success
//			  FALSE [0]	 - Fail
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
BOOL	DoListen(UINT uPort);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : EndListen
// Purpose  : Stop Socket Listen Process
// Prameter : 
// Return   : 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void	EndListen(void);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : TCPSend
// Purpose  : TCP Send
// Prameter : iConnectID - Connection ID
//			  pData - 전송할 데이터
//			  nSize - 전송할 데이터 사이즈
// Return   : 전송된 크기
// Remart   : 전송된 후 PTCPPACKETEVENTCALLBACK 의 OnSend Flag 발생.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int		TCPSend(int iConnectID, BYTE *pData, int nSize);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : TCPSetSync
// Purpose  : 패킷 Sync 코드 사용 유무 결정 (ex 0x16161616 Sync Code 사용)
// Prameter : iConnectID - Connection ID
//			  pSyncData - Sync Code 로 사용할 코드 버퍼 (MAX 32 Byte) 
//			  nSize - Sync Code 길이
// Return   : 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void	TCPSetSync(BYTE *pSyncData, int nSize);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcname : TCPIsConnected
// Purpose  : 해당 Connection ID 가 연결된 상태인지 아닌지를 판단.
// Prameter : iConnectID - Connection ID
// Return   : TRUE  - Connected
//			  FALSE - DisConnected
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
BOOL	TCPIsConnected(int iConnectID);


#endif


