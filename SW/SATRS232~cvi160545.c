#include <windows.h>
#include <mmsystem.h>
//#pragma commit(lib."winmm.lib");
//Test Code for Check Length
///////////////////////////////////////
#include <formatio.h>
/////////////////////////////////////
#include "toolbox.h"
#include <userint.h>
#include <ansi_c.h>
#include <utility.h>
#include <rs232.h>
#include "SATRS232.h"
#include "G_MAIN.h"
#include "H_EXTERN.H" 

static int CVICALLBACK DataRecvFunc(void *pNULL);
static int CVICALLBACK DataSendFunc(void *pNULL); 

void RS232SetRcvMaxSize(int iIdx, int nMaxSize)
{
	if(nMaxSize <= MAX_RCV_BUF)
		m_pCOMM_SetInfo[iIdx].L_nRcvPacketMax = nMaxSize;
}

int CVICALLBACK RcvEventFunc(int panel, int control, int event, void *callbackData, int eventData1, int eventData2)
{	
	int i;
	
	if(event != EVENT_TIMER_TICK)
		return 0;
	
	for (i=0; i<DEF_COMM_CNT;	i++)
	{
		if(control == m_pCOMM[i].iEventTimer)
		{
			ResetTimer(panel, m_pCOMM[i].iEventTimer);
			SetCtrlAttribute (panel, m_pCOMM[i].iEventTimer, ATTR_ENABLED, 0);
	
			(*m_pCOMM_SetInfo[i].L_EventFunc)(i, EVENT_RECV, m_pCOMM_SetInfo[i].L_CopyBuff, m_pCOMM_SetInfo[i].L_CopyDataLen);

			m_pCOMM_SetInfo[i].L_bThreadHold = FALSE;
			return 0;
		}
	}

	
	return 0;
}

BOOL RS232Init(int iIdx,  int hMainPnl, COMATTR *ComAttr, UINT8 nLenStartByte, UINT8 nLenFieldSize, PRSLENCALLBACK pLengthFunc, PRSEVENTCALLBACK pEventFunc)
{
	char	BUF_1[10];
	char	BUF_2[10];
	int 	i, iErrCode;
	
	if(hMainPnl <= 0)
		return FALSE;

	if(!pLengthFunc)
		return FALSE;
		
	if(!pEventFunc)
		return FALSE;
	
//	SetCtrlVal (m_hWndMain,	MAINPNL_PORTTEST,	"Init Step 1");
	
	DisableBreakOnLibraryErrors();
	RS232End(iIdx);
	EnableBreakOnLibraryErrors();
	
//	SetCtrlVal (m_hWndMain,	MAINPNL_PORTTEST,	"Init Step 2");
	m_pCOMM_SetInfo[iIdx].L_hMainPnl = hMainPnl;
	m_pCOMM_SetInfo[iIdx].L_LengthFunc = pLengthFunc;
	m_pCOMM_SetInfo[iIdx].L_EventFunc = pEventFunc;
	
	m_pCOMM_SetInfo[iIdx].L_nLenStartByte = nLenStartByte;
	m_pCOMM_SetInfo[iIdx].L_nLenFieldSize = nLenFieldSize;
	
	if(m_pCOMM_SetInfo[iIdx].L_bOpenPort)
	{
	//	SetCtrlVal (m_hWndMain,	MAINPNL_PORTTEST,	"Init Step 3");
		DisableBreakOnLibraryErrors();
		CloseCom (m_pCOMM_SetInfo[iIdx].L_ComAttr.iComport);    
		EnableBreakOnLibraryErrors();
		m_pCOMM_SetInfo[iIdx].L_bOpenPort = FALSE;
	//	return FALSE;
	}		
	
	memcpy(&m_pCOMM_SetInfo[iIdx].L_ComAttr, ComAttr, sizeof(COMATTR));
	
//	SetCtrlVal (m_hWndMain,	MAINPNL_PORTTEST,	"Init Step 4");
//	DisableBreakOnLibraryErrors();
//	SetBreakOnProtectionErrors (TRUE);
	SetBreakOnLibraryErrors (0);
//	SetCtrlVal (m_hWndMain,	MAINPNL_NUMERIC,	GetComStat (L_ComAttr.iComport)); 
	Sleep(500); 
	iErrCode = OpenComConfig(m_pCOMM_SetInfo[iIdx].L_ComAttr.iComport, m_pCOMM_SetInfo[iIdx].L_ComAttr.pStrDeviceName, 
							 m_pCOMM_SetInfo[iIdx].L_ComAttr.iBaudrate, m_pCOMM_SetInfo[iIdx].L_ComAttr.iParity, 
							 m_pCOMM_SetInfo[iIdx].L_ComAttr.iDatabits, m_pCOMM_SetInfo[iIdx].L_ComAttr.iStopbits, 
							 m_pCOMM_SetInfo[iIdx].L_ComAttr.nInputq, m_pCOMM_SetInfo[iIdx].L_ComAttr.nOutputq);
//	SetBreakOnProtectionErrors (FALSE);
	SetBreakOnLibraryErrors (1);
//	EnableBreakOnLibraryErrors();

	if(iErrCode < 0) 
		return FALSE;
	else
	{
//		SetCtrlVal (m_hWndMain,	MAINPNL_PORTTEST,	"Init Step 5");
		SetXMode (m_pCOMM_SetInfo[iIdx].L_ComAttr.iComport, m_pCOMM_SetInfo[iIdx].L_ComAttr.iXmode);
		SetCTSMode (m_pCOMM_SetInfo[iIdx].L_ComAttr.iComport, m_pCOMM_SetInfo[iIdx].L_ComAttr.iCtsmode);
		SetComTime (m_pCOMM_SetInfo[iIdx].L_ComAttr.iComport, m_pCOMM_SetInfo[iIdx].L_ComAttr.nTimeout);
		
		m_pCOMM[iIdx].iPollingTimer = NewCtrl (m_hWndMain, CTRL_TIMER, "Timer", 	10, 10);
		SetCtrlAttribute (m_hWndMain, 	m_pCOMM[iIdx].iPollingTimer, ATTR_INTERVAL, 	1.0);
		SetCtrlAttribute (m_hWndMain,	m_pCOMM[iIdx].iPollingTimer, ATTR_CALLBACK_FUNCTION_POINTER, PollingTimer);    
		SetCtrlAttribute (m_hWndMain,	m_pCOMM[iIdx].iPollingTimer, ATTR_ENABLED, 	FALSE);
	
		m_pCOMM[iIdx].iPortDropTimer = NewCtrl (m_hWndMain, CTRL_TIMER, "Timer", 	10, 10);
		SetCtrlAttribute (m_hWndMain, 	m_pCOMM[iIdx].iPortDropTimer, ATTR_INTERVAL, 	1.0);
		SetCtrlAttribute (m_hWndMain,	m_pCOMM[iIdx].iPortDropTimer, ATTR_CALLBACK_FUNCTION_POINTER, PortDropTimer);    
		SetCtrlAttribute (m_hWndMain,	m_pCOMM[iIdx].iPortDropTimer, ATTR_ENABLED, 	FALSE);
	
		m_pCOMM[iIdx].iWaitTimer = NewCtrl (m_hWndMain, CTRL_TIMER, "Timer", 	10, 10);
		SetCtrlAttribute (m_hWndMain, 	m_pCOMM[iIdx].iWaitTimer, ATTR_INTERVAL, 	10.0);
		SetCtrlAttribute (m_hWndMain,	m_pCOMM[iIdx].iWaitTimer, ATTR_CALLBACK_FUNCTION_POINTER, ResponseTimer);    
		SetCtrlAttribute (m_hWndMain,	m_pCOMM[iIdx].iWaitTimer, ATTR_ENABLED, 	FALSE);
		
		m_pCOMM[iIdx].iEventTimer = NewCtrl(hMainPnl, CTRL_TIMER, "Rs232 Lib Event Timer", 0, 0);
		SetCtrlAttribute(hMainPnl, m_pCOMM[iIdx].iEventTimer, ATTR_INTERVAL, 0.01);
		SetCtrlAttribute(hMainPnl, m_pCOMM[iIdx].iEventTimer, ATTR_ENABLED, 	0);
		SetCtrlAttribute(hMainPnl, m_pCOMM[iIdx].iEventTimer,	ATTR_CALLBACK_FUNCTION_POINTER, RcvEventFunc);
		
		m_pCOMM_SetInfo[iIdx].L_bOpenPort = TRUE; 

		//Create Receive Lookup Thread
		m_pCOMM_SetInfo[iIdx].L_ThreadEndFlagRCV = FALSE;
		CmtNewThreadPool(1, &m_pCOMM_SER[iIdx].hThreadRecvFuncId);
		CmtScheduleThreadPoolFunction (m_pCOMM_SER[iIdx].hThreadRecvFuncId, DataRecvFunc, (void *)iIdx, NULL);
		
		m_pCOMM_SetInfo[iIdx].L_ThreadEndFlagSND = FALSE;
		CmtNewThreadPool(1, &m_pCOMM_SER[iIdx].hThreadSendFuncId);
		CmtScheduleThreadPoolFunction (m_pCOMM_SER[iIdx].hThreadSendFuncId, DataSendFunc, (void *)iIdx, NULL);
		
		return TRUE;		
	}
//	SetCtrlVal (m_hWndMain,	MAINPNL_PORTTEST,	"Init Step 6");

	return FALSE;
}

BOOL RS232ExtraCheck(int iIdx,  UINT8 nCheckStartByte, UINT8 nCheckFieldSize, PRSFRAMECHECKCALLBACK pExtraCheckFunc)
{
	m_pCOMM_SetInfo[iIdx].L_bUseExtraCheck = TRUE;
	m_pCOMM_SetInfo[iIdx].L_ExtraCheckStartByte = nCheckStartByte;
	m_pCOMM_SetInfo[iIdx].L_ExtraCheckLength = nCheckFieldSize;
	m_pCOMM_SetInfo[iIdx].L_ExtraCheckFunc = pExtraCheckFunc;	

	return TRUE;
}

void RS232End(int iIdx)
{
	int i;		
	while(m_pCOMM_SetInfo[iIdx].L_bThreadRunRCV || m_pCOMM_SetInfo[iIdx].L_bThreadRunSND)
	{   
		m_pCOMM_SetInfo[iIdx].L_ThreadEndFlagRCV = TRUE;
		m_pCOMM_SetInfo[iIdx].L_ThreadEndFlagSND = TRUE;
		
		if(m_pCOMM_SetInfo[iIdx].L_hSendEvent)
			SetEvent(m_pCOMM_SetInfo[iIdx].L_hSendEvent);			
		
		Sleep(10);
//		SetCtrlVal (m_hWndMain,	MAINPNL_PORTTEST,	"End Step 1");
	//	MessagePopup ("Test",	"Step 1");
	}		

	if(m_pCOMM_SetInfo[iIdx].L_bOpenPort)
	{   
//		SetCtrlVal (m_hWndMain,	MAINPNL_PORTTEST,	"End Step 4");
	//	DisableBreakOnLibraryErrors();
//		SetCtrlVal (m_hWndMain,	MAINPNL_PORTTEST,	"End Step 4-1");
//		SetCtrlVal (m_hWndMain,	MAINPNL_NUMERIC_2,	GetComStat (L_ComAttr.iComport)); 
//		SetCtrlVal (m_hWndMain,	MAINPNL_PORTTEST,	"End Step 4-2");
		Sleep(10);
		SetBreakOnLibraryErrors (0);
		if(GetComStat (m_pCOMM_SetInfo[iIdx].L_ComAttr.iComport)==0)
		{
			Sleep(500);
			if(GetComStat (m_pCOMM_SetInfo[iIdx].L_ComAttr.iComport)==0)
				CloseCom (m_pCOMM_SetInfo[iIdx].L_ComAttr.iComport);
		}
		SetBreakOnLibraryErrors (1);
//		SetCtrlVal (m_hWndMain,	MAINPNL_PORTTEST,	"End Step 4-3");
	//	EnableBreakOnLibraryErrors();
//		SetCtrlVal (m_hWndMain,	MAINPNL_PORTTEST,	"End Step 4-4");
		m_pCOMM_SetInfo[iIdx].L_bOpenPort = FALSE;
//		SetCtrlVal (m_hWndMain,	MAINPNL_PORTTEST,	"End Step 5");
	}	
	
	m_pCOMM_SetInfo[iIdx].L_bOpenPort = FALSE;								
	DisableBreakOnLibraryErrors();
	CloseCom (m_pCOMM_SetInfo[iIdx].L_ComAttr.iComport);    
	EnableBreakOnLibraryErrors();
	m_pCOMM_SetInfo[iIdx].L_bOpenPort = FALSE;			
	m_pCOMM_SetInfo[iIdx].L_bThreadRunRCV = FALSE;
	m_pCOMM_SetInfo[iIdx].L_bCriticalEnter = FALSE;
	
	for(i=0; i<m_pCOMM_SetInfo[iIdx].L_nSendItemCount; i++)
	{
		free(m_pCOMM_SetInfo[iIdx].L_SendItemList[i]->pData);
		free(m_pCOMM_SetInfo[iIdx].L_SendItemList[i]);
//		SetCtrlVal (m_hWndMain,	MAINPNL_PORTTEST,	"End Step 2");
	}		   
	
	if(m_pCOMM_SetInfo[iIdx].L_hSendEvent)
	{
		CloseHandle(m_pCOMM_SetInfo[iIdx].L_hSendEvent);
		m_pCOMM_SetInfo[iIdx].L_hSendEvent = NULL;
//		SetCtrlVal (m_hWndMain,	MAINPNL_PORTTEST,	"End Step 3");
	}
	

	m_pCOMM_SetInfo[iIdx].L_nSendItemCount = 0;
	m_pCOMM_SetInfo[iIdx].L_nRcvDataSize = 0;
	m_pCOMM_SetInfo[iIdx].L_nCntPacketSize = MAX_RCV_BUF;

	m_pCOMM_SetInfo[iIdx].L_bUseExtraCheck = FALSE;
	m_pCOMM_SetInfo[iIdx].L_ExtraCheckStartByte = 0;
	m_pCOMM_SetInfo[iIdx].L_ExtraCheckLength = 0;
	m_pCOMM_SetInfo[iIdx].L_ExtraCheckFunc = NULL;	
//	SetCtrlVal (m_hWndMain,	MAINPNL_PORTTEST,	"End Step 6");
	
	m_pCOMM_SER[iIdx].bConnect = FALSE;
	
//	if(m_pCOMM_UDP[iIdx].hSocket >= 0)	 ////////////????
//		closesocket(m_pCOMM_UDP[iIdx].hSocket);
	
	if (m_pCOMM[iIdx].iPortDropTimer > 0)
	{
		EnableTimer(m_hWndMain, m_pCOMM[iIdx].iPortDropTimer, FALSE);
					DiscardCtrl (m_hWndMain, m_pCOMM[iIdx].iPortDropTimer);	
		m_pCOMM[iIdx].iPortDropTimer  = 0;
	}
	if (m_pCOMM[iIdx].iPollingTimer > 0)
	{
		EnableTimer(m_hWndMain, m_pCOMM[iIdx].iPollingTimer, FALSE);
		DiscardCtrl (m_hWndMain, m_pCOMM[iIdx].iPollingTimer);	
		m_pCOMM[iIdx].iPollingTimer  = 0;
	}
	if (m_pCOMM[iIdx].iWaitTimer > 0)
	{
		EnableTimer(m_hWndMain, m_pCOMM[iIdx].iWaitTimer, FALSE);
		DiscardCtrl (m_hWndMain, m_pCOMM[iIdx].iWaitTimer);	
		m_pCOMM[iIdx].iWaitTimer  = 0;
	}
	if (m_pCOMM[iIdx].iEventTimer > 0)
	{
		EnableTimer(m_hWndMain, m_pCOMM[iIdx].iEventTimer, FALSE);
		DiscardCtrl (m_hWndMain, m_pCOMM[iIdx].iEventTimer);	
		m_pCOMM[iIdx].iEventTimer  = 0;
	}
	
	if (m_pCOMM_SER[iIdx].hThreadRecvFuncId > 0)
	{
		CmtDiscardThreadPool (m_pCOMM_SER[iIdx].hThreadRecvFuncId);   
		m_pCOMM_SER[iIdx].hThreadRecvFuncId = 0;
	}
	if (m_pCOMM_SER[iIdx].hThreadRecvFuncId > 0)
	{
		CmtDiscardThreadPool (m_pCOMM_SER[iIdx].hThreadSendFuncId);   
		m_pCOMM_SER[iIdx].hThreadSendFuncId = 0;
	}
	
}

void RS232SetSync(int iIdx, BYTE *pSyncData, int nSize)
{
	if(!pSyncData)
		return;
		
	if(nSize <= 0)
		return;
	
	if(nSize > 32)
		return;

	memcpy(m_pCOMM_SetInfo[iIdx].L_SyncBuf, pSyncData, nSize);
	m_pCOMM_SetInfo[iIdx].L_SyncLen = nSize;
	m_pCOMM_SetInfo[iIdx].L_bUseSyncCode = TRUE;
}

int RS232Send(int iIdx, BYTE *pData, int nSize)
{   
	HANDLE hEvent; 
	SENDITEM *pItem;
	
	if(!RS232IsOpen(iIdx))
		return 0;		 
	
	if(!pData)
		return 0;
		
	if(nSize <= 0)
		return 0;
	
	if(m_pCOMM_SetInfo[iIdx].L_nSendItemCount >= MAX_SEND_PACKET)
		return 0;		//overflow
		
	pItem = malloc(sizeof(SENDITEM));
	if(!pItem)
		return 0;
		
	pItem->pData = malloc(nSize);
	if(!pItem->pData)
	{
		free(pItem);
		return 0;
	}
	
	pItem->nDataCount = nSize;
	memcpy(pItem->pData, pData, nSize);
	
	while(m_pCOMM_SetInfo[iIdx].L_bCriticalEnter)
		Sleep(10);
		
	m_pCOMM_SetInfo[iIdx].L_bCriticalEnter = TRUE;
	m_pCOMM_SetInfo[iIdx].L_SendItemList[m_pCOMM_SetInfo[iIdx].L_nSendItemCount++] = pItem;
	m_pCOMM_SetInfo[iIdx].L_bCriticalEnter = FALSE;
	
	if(m_pCOMM_SetInfo[iIdx].L_hSendEvent)
		SetEvent(m_pCOMM_SetInfo[iIdx].L_hSendEvent);
	
	return nSize;
}

/*
void FindPacketHeader()
{
	int i, j;
	int iHeaderIndex = -1;
	BOOL bSyncMatch;

	for( i=0; i<L_nRcvDataSize-L_SyncLen; i++)
	{
		iHeaderIndex = i;

		bSyncMatch = TRUE;
		for(j=0; j<L_SyncLen; j++)
		{
			if(L_RcvBuff[i+j] != L_SyncBuf[j])
			{
				bSyncMatch = FALSE;
				break;
			}
		}
		
		if(bSyncMatch)
			break;					
	}

	if(iHeaderIndex >= L_nRcvDataSize - (L_SyncLen + 1))
	{
		L_nRcvDataSize = 0;
		return;
	}
	else if(iHeaderIndex >= 0)
	{
		SMemcopy(&L_RcvBuff[0], &L_RcvBuff[iHeaderIndex], L_nRcvDataSize - iHeaderIndex);
		L_nRcvDataSize -= iHeaderIndex;
	}
}
*/
void FindPacketHeader(int iIdx)
{
	int i, j;
	int iHeaderIndex = -1;
	BOOL bSyncMatch;

	for( i=0; i<m_pCOMM_SetInfo[iIdx].L_nRcvDataSize; i++)
	{
		iHeaderIndex = i;

		bSyncMatch = TRUE;
		for(j=0; j<m_pCOMM_SetInfo[iIdx].L_SyncLen; j++)
		{
			if(i+j >= m_pCOMM_SetInfo[iIdx].L_nRcvDataSize)
			{
				bSyncMatch = TRUE;
				break;
			}
			else if(m_pCOMM_SetInfo[iIdx].L_RcvBuff[i+j] != m_pCOMM_SetInfo[iIdx].L_SyncBuf[j])
			{
				bSyncMatch = FALSE;
				break;
			}
		}
		
		if(bSyncMatch)
			break;					
	}

	if(iHeaderIndex >= 0)
	{
		SMemcopy(&m_pCOMM_SetInfo[iIdx].L_RcvBuff[0], &m_pCOMM_SetInfo[iIdx].L_RcvBuff[iHeaderIndex], m_pCOMM_SetInfo[iIdx].L_nRcvDataSize - iHeaderIndex);
		m_pCOMM_SetInfo[iIdx].L_nRcvDataSize -= iHeaderIndex;
	}
}

void WriteDebugString(int hFile, char *pString)
{
	int iMon, iDay, iYear;
	int iMin, iSec, iHour;
	char stDate[255];
	char stBuf[255];

	if(hFile <= 0)
		return;

	if(!pString)
		return;	
			
	GetSystemDate(&iMon, &iDay, &iYear);
	GetSystemTime(&iHour, &iMin, &iSec);

	memset(stDate, 0, 255);
	memset(stBuf, 0, 255);

	sprintf(stDate, "[ %d/%d/%d - %d:%d:%d ] ", iYear, iMon, iDay, iHour, iMin, iSec);
	strcpy(stBuf, pString);	

	strcat(stDate, stBuf);
	FmtFile(hFile, "%s<%s", stDate);
//	WriteFile(hFile, stDate, strlen(stDate));
}

static int CVICALLBACK DataRecvFunc(void *pData)
{   
	int iIdx = (int)pData;
	int nRcvSize;
	int i;
	BOOL bSyncMatch;		

	m_pCOMM_SetInfo[iIdx].L_bThreadRunRCV = TRUE;
	
	while(!m_pCOMM_SetInfo[iIdx].L_ThreadEndFlagRCV)
	{
		if(!m_pCOMM_SetInfo[iIdx].L_bOpenPort)
			break;										 		
		DisableBreakOnLibraryErrors();
		nRcvSize = GetInQLen(m_pCOMM_SetInfo[iIdx].L_ComAttr.iComport);   				
		EnableBreakOnLibraryErrors();
		
		if(!m_pCOMM_SetInfo[iIdx].L_bOpenPort)
			break;										 		
		
		if(nRcvSize <= 0)
		{				
			Sleep(10);
			continue;
		}		
	
		if(!m_pCOMM_SetInfo[iIdx].L_bOpenPort)
			break;										 		

		if(nRcvSize > (MAX_RCV_BUF - m_pCOMM_SetInfo[iIdx].L_nRcvDataSize))
			nRcvSize = (MAX_RCV_BUF - m_pCOMM_SetInfo[iIdx].L_nRcvDataSize);				
		
		DisableBreakOnLibraryErrors();
		nRcvSize = ComRd(m_pCOMM_SetInfo[iIdx].L_ComAttr.iComport, &m_pCOMM_SetInfo[iIdx].L_RcvBuff[m_pCOMM_SetInfo[iIdx].L_nRcvDataSize], nRcvSize);			
		EnableBreakOnLibraryErrors();
  
		/////////////////////////////////////////////////////////////////////////////////		
		if(nRcvSize <= 0)
		{				
			// Port Closed ...
			////////////////////////////////////////////////////////////////////////////			
			m_pCOMM_SetInfo[iIdx].L_bOpenPort = FALSE;								
			
			DisableBreakOnLibraryErrors();
			CloseCom (m_pCOMM_SetInfo[iIdx].L_ComAttr.iComport);    
			EnableBreakOnLibraryErrors();
			m_pCOMM_SetInfo[iIdx].L_bOpenPort = FALSE;			
			m_pCOMM_SetInfo[iIdx].L_bThreadRunRCV = FALSE;
			RS232End(iIdx);						
			(*m_pCOMM_SetInfo[iIdx].L_EventFunc)(iIdx, EVENT_PTDROP, 0, 0);
			
			m_pCOMM_SetInfo[iIdx].L_bCriticalEnter = FALSE;
			return 0;
		}
		/////////////////////////////////////////////////////////////////////////////////
	
		if(m_pCOMM_SetInfo[iIdx].L_ThreadEndFlagRCV)
			break;

		if(nRcvSize == 0)
		{			
			Sleep(10);
			continue;
		}
		
		if(m_pCOMM_SetInfo[iIdx].L_bDebugMode)
			(*m_pCOMM_SetInfo[iIdx].L_DebugFunc)(0, &m_pCOMM_SetInfo[iIdx].L_RcvBuff[m_pCOMM_SetInfo[iIdx].L_nRcvDataSize], nRcvSize);
	
		m_pCOMM_SetInfo[iIdx].L_nRcvDataSize += nRcvSize;				

		while(TRUE)
		{
			if(m_pCOMM_SetInfo[iIdx].L_ThreadEndFlagRCV)
				break;

			if(m_pCOMM_SetInfo[iIdx].L_bUseSyncCode)
			{   
				if(m_pCOMM_SetInfo[iIdx].L_nRcvDataSize <= m_pCOMM_SetInfo[iIdx].L_SyncLen)
					break;
				else
				{
					bSyncMatch = TRUE;
					for(i=0; i<m_pCOMM_SetInfo[iIdx].L_SyncLen; i++)
					{
						if(m_pCOMM_SetInfo[iIdx].L_RcvBuff[i] != m_pCOMM_SetInfo[iIdx].L_SyncBuf[i])
						{
							bSyncMatch = FALSE;
							break;
						}
					}
					
					if(!bSyncMatch)			
						FindPacketHeader(iIdx);					
				}
			}
			
			if(m_pCOMM_SetInfo[iIdx].L_bUseExtraCheck)
			{
				int ExtraCheckResult;			

				if(m_pCOMM_SetInfo[iIdx].L_nRcvDataSize <= (m_pCOMM_SetInfo[iIdx].L_ExtraCheckStartByte + m_pCOMM_SetInfo[iIdx].L_ExtraCheckLength))
					break;						

				ExtraCheckResult = (*m_pCOMM_SetInfo[iIdx].L_ExtraCheckFunc)(iIdx, &m_pCOMM_SetInfo[iIdx].L_RcvBuff[m_pCOMM_SetInfo[iIdx].L_ExtraCheckStartByte], m_pCOMM_SetInfo[iIdx].L_ExtraCheckLength);			

				if(!ExtraCheckResult)
				{				
					SMemcopy(m_pCOMM_SetInfo[iIdx].L_RcvBuff, &m_pCOMM_SetInfo[iIdx].L_RcvBuff[m_pCOMM_SetInfo[iIdx].L_ExtraCheckStartByte + m_pCOMM_SetInfo[iIdx].L_ExtraCheckLength], MAX_RCV_BUF - (m_pCOMM_SetInfo[iIdx].L_ExtraCheckStartByte + m_pCOMM_SetInfo[iIdx].L_ExtraCheckLength));
					m_pCOMM_SetInfo[iIdx].L_nRcvDataSize -= (m_pCOMM_SetInfo[iIdx].L_ExtraCheckStartByte + m_pCOMM_SetInfo[iIdx].L_ExtraCheckLength);				
					continue;
				}		
			}
			
			if(m_pCOMM_SetInfo[iIdx].L_nRcvDataSize >= (m_pCOMM_SetInfo[iIdx].L_nLenStartByte + m_pCOMM_SetInfo[iIdx].L_nLenFieldSize))
			{
				m_pCOMM_SetInfo[iIdx].L_nCntPacketSize = (*m_pCOMM_SetInfo[iIdx].L_LengthFunc)(iIdx, &m_pCOMM_SetInfo[iIdx].L_RcvBuff[m_pCOMM_SetInfo[iIdx].L_nLenStartByte]);				

				if(m_pCOMM_SetInfo[iIdx].L_nCntPacketSize > m_pCOMM_SetInfo[iIdx].L_nRcvPacketMax)
				{						
					SMemcopy(m_pCOMM_SetInfo[iIdx].L_RcvBuff, &m_pCOMM_SetInfo[iIdx].L_RcvBuff[m_pCOMM_SetInfo[iIdx].L_nLenStartByte + m_pCOMM_SetInfo[iIdx].L_nLenFieldSize], MAX_RCV_BUF - (m_pCOMM_SetInfo[iIdx].L_nLenStartByte + m_pCOMM_SetInfo[iIdx].L_nLenFieldSize));
					continue;
				}
			
				if(m_pCOMM_SetInfo[iIdx].L_nRcvDataSize >= m_pCOMM_SetInfo[iIdx].L_nCntPacketSize)
				{						
					//(*L_EventFunc)(EVENT_RECV, L_RcvBuff, L_nCntPacketSize);
					if( m_pCOMM_SetInfo[iIdx].L_nCntPacketSize < 0 )
					{
						memset ( m_pCOMM_SetInfo[iIdx].L_CopyBuff, 0x00,	sizeof(m_pCOMM_SetInfo[iIdx].L_CopyBuff));
						memset ( m_pCOMM_SetInfo[iIdx].L_RcvBuff, 0x00,	sizeof(m_pCOMM_SetInfo[iIdx].L_RcvBuff));
						m_pCOMM_SetInfo[iIdx].L_nSendItemCount = 0;
						m_pCOMM_SetInfo[iIdx].L_nRcvDataSize = 0;
						m_pCOMM_SetInfo[iIdx].L_nCntPacketSize = MAX_RCV_BUF;

						m_pCOMM_SetInfo[iIdx].L_bUseExtraCheck = FALSE;
						m_pCOMM_SetInfo[iIdx].L_ExtraCheckStartByte = 0;
						m_pCOMM_SetInfo[iIdx].L_ExtraCheckLength = 0;
						m_pCOMM_SetInfo[iIdx].L_ExtraCheckFunc = NULL;	
						break;	// 2009.08.10.
					}
					memcpy(m_pCOMM_SetInfo[iIdx].L_CopyBuff, m_pCOMM_SetInfo[iIdx].L_RcvBuff, m_pCOMM_SetInfo[iIdx].L_nCntPacketSize);
					m_pCOMM_SetInfo[iIdx].L_CopyDataLen = m_pCOMM_SetInfo[iIdx].L_nCntPacketSize;

					m_pCOMM_SetInfo[iIdx].L_bThreadHold = TRUE;

					ResetTimer(m_pCOMM_SetInfo[iIdx].L_hMainPnl, m_pCOMM[iIdx].iEventTimer);
					SetCtrlAttribute (m_pCOMM_SetInfo[iIdx].L_hMainPnl, m_pCOMM[iIdx].iEventTimer, ATTR_ENABLED, 1);

					while(m_pCOMM_SetInfo[iIdx].L_bThreadHold)		
					{
						if(m_pCOMM_SetInfo[iIdx].L_ThreadEndFlagRCV)					
							break;				
				
						Sleep(10);				
					}

					SMemcopy(m_pCOMM_SetInfo[iIdx].L_RcvBuff, &m_pCOMM_SetInfo[iIdx].L_RcvBuff[m_pCOMM_SetInfo[iIdx].L_nCntPacketSize], MAX_RCV_BUF - m_pCOMM_SetInfo[iIdx].L_nCntPacketSize);
					m_pCOMM_SetInfo[iIdx].L_nRcvDataSize -= m_pCOMM_SetInfo[iIdx].L_nCntPacketSize;				
				}
				else				
					break;			
			}
			else 
				break;
		}		

		Sleep(20);
	}
	
	m_pCOMM_SetInfo[iIdx].L_bThreadRunRCV = FALSE;
	
	return 0;
}

BOOL RS232IsOpen(int iIdx)
{
	return m_pCOMM_SetInfo[iIdx].L_bOpenPort;
}

static int CVICALLBACK DataSendFunc(void *pData)
{   
	int iIdx = (int)pData;
	HANDLE hReconEvent;	
	int Y, M, D, h, m, s;
	char strEvent[32];
	GetSystemTime(&h, &m, &s);
	GetSystemDate(&M, &D, &Y);
	memset (strEvent, '\0', sizeof(strEvent));
	sprintf(strEvent, "%04d%02d%02d%02d%02d%02d\n",Y, M, D, h, m, s);
	
	if(m_pCOMM_SetInfo[iIdx].L_hSendEvent)
	{
		CloseHandle(m_pCOMM_SetInfo[iIdx].L_hSendEvent);
		m_pCOMM_SetInfo[iIdx].L_hSendEvent = NULL;
	}	
	
	m_pCOMM_SetInfo[iIdx].L_hSendEvent = CreateEvent(NULL, FALSE, FALSE, strEvent);
	
	if(!m_pCOMM_SetInfo[iIdx].L_hSendEvent)
		return 0;
	
	m_pCOMM_SetInfo[iIdx].L_bThreadRunSND = TRUE;
	
	while(!m_pCOMM_SetInfo[iIdx].L_ThreadEndFlagSND)
	{   
		if(WaitForSingleObject(m_pCOMM_SetInfo[iIdx].L_hSendEvent, INFINITE) == WAIT_OBJECT_0)
		{   		
			while(m_pCOMM_SetInfo[iIdx].L_bCriticalEnter)
				Sleep(10);
				
			if(!m_pCOMM_SetInfo[iIdx].L_bOpenPort)
				break;
				
			m_pCOMM_SetInfo[iIdx].L_bCriticalEnter = TRUE;
			
			while(m_pCOMM_SetInfo[iIdx].L_nSendItemCount > 0)
			{
				int i;
				SENDITEM *pItem =  m_pCOMM_SetInfo[iIdx].L_SendItemList[0];
				
				if(pItem)
				{   
					int err;
					int nSendCount = 0;
					
					while(nSendCount < pItem->nDataCount)
					{   
						DisableBreakOnLibraryErrors();
						err = ComWrt(m_pCOMM_SetInfo[iIdx].L_ComAttr.iComport, pItem->pData+nSendCount, pItem->nDataCount - nSendCount);      
						EnableBreakOnLibraryErrors();
						if(err < 0)
						{								
							// Port Closed ...
							////////////////////////////////////////////////////////////////////////////								
							m_pCOMM_SetInfo[iIdx].L_bOpenPort = FALSE;															
							
							DisableBreakOnLibraryErrors();
							CloseCom (m_pCOMM_SetInfo[iIdx].L_ComAttr.iComport);    
							EnableBreakOnLibraryErrors();
							m_pCOMM_SetInfo[iIdx].L_bOpenPort = FALSE;
							m_pCOMM_SetInfo[iIdx].L_bThreadRunSND = FALSE;							
							RS232End(iIdx);											
							(*m_pCOMM_SetInfo[iIdx].L_EventFunc)(iIdx, EVENT_PTDROP, 0, 0);

							m_pCOMM_SetInfo[iIdx].L_bCriticalEnter = FALSE;
							
							return 0;
						}
						else
							nSendCount += err;
					}
						
					(*m_pCOMM_SetInfo[iIdx].L_EventFunc)(iIdx, EVENT_SEND, pItem->pData, pItem->nDataCount); 
					
					free(pItem->pData);
					free(pItem);
							
					m_pCOMM_SetInfo[iIdx].L_nSendItemCount--;
					for(i=0; i<m_pCOMM_SetInfo[iIdx].L_nSendItemCount; i++)
						m_pCOMM_SetInfo[iIdx].L_SendItemList[i] = m_pCOMM_SetInfo[iIdx].L_SendItemList[i+1];
					
				}
				else
				{   
					m_pCOMM_SetInfo[iIdx].L_nSendItemCount--;
					for(i=0; i<m_pCOMM_SetInfo[iIdx].L_nSendItemCount; i++)
						m_pCOMM_SetInfo[iIdx].L_SendItemList[i] = m_pCOMM_SetInfo[iIdx].L_SendItemList[i+1];
				}
				
				Sleep(20);
			} 
		}
		
		m_pCOMM_SetInfo[iIdx].L_bCriticalEnter = FALSE;
	}
	
	if(m_pCOMM_SetInfo[iIdx].L_hSendEvent)
	{
		CloseHandle(m_pCOMM_SetInfo[iIdx].L_hSendEvent);
		m_pCOMM_SetInfo[iIdx].L_hSendEvent = NULL;
	}
		
	m_pCOMM_SetInfo[iIdx].L_bThreadRunSND = FALSE;
	
	return 0;	
}


void RS232SetMode(int iIdx, DEBUGCALLBACK pNotiFunc, BOOL bDebugMode)
{				   
	m_pCOMM_SetInfo[iIdx].L_DebugFunc = *pNotiFunc;
	m_pCOMM_SetInfo[iIdx].L_bDebugMode = bDebugMode;
}


BOOL RS232IsDebugMode(int iIdx)
{
	return m_pCOMM_SetInfo[iIdx].L_bDebugMode;
}


//////
static int SerialThreadInit (int iData)
{
	int i = (int)iData;
	BYTE cSync[4] = {0x16, 0x16, 0x16, 0x16};
	
	RS232SetSync(i, cSync, 4);   
	RS232SetRcvMaxSize(i, DATABUFMAX);

	if(!RS232Init(i, m_hWndMain, &m_pCOMM_SER[i].m_ComAttr, 9, 2, CalculLength_UDP, OnPacket_UDP))
	{
		if (m_bConnectMsg)
			MessagePopup("AutoConnect", "통신 포트를 초기화 할 수 없습니다.\n다른 프로그램에서 해당 포트를 사용중이거나 시스템에 존재하지 않는 포트일 수 있습니다.");
		return 0;
	}	
	else
	{
		m_pCOMM_SER[i].bConnect = TRUE;
		MessagePopup("AutoConnect", "Serial Init Success");
		EnableTimer(m_hWndMain, m_pCOMM[i].iPollingTimer, TRUE);  
		return 1;
	}

	return 0;
}
/////////////////////////
int AutoConnect (int iData) 
{     
	return SerialThreadInit(iData);
}


//////////
void BuzzerOn(void)
{
	sndPlaySound("buzzer.wav", SND_ASYNC | SND_NODEFAULT);
}
