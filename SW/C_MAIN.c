
#include "pwctrl.h"
#include "toolbox.h"
#include <userint.h>
#include <formatio.h>
#include <ansi_c.h>
#include <utility.h>
#include "H_EXTERN.H"
#include "G_MAIN.h"    

void InitValues()
{
	int i;
	
	m_bConnectMsg = FALSE;
	
	m_hWndMain		= 0;
	m_hWndTable		= 0;
	m_hWndDownload	= 0;
	m_hWndProgress	= 0;
	m_hWndDnWait	= 0;
	
	m_hWndHPA		= 0;
	m_hWndLNA		= 0;
	m_hWndAMP		= 0;
	m_hWndCHA		= 0;
	
	
	m_hWndEMERGENCY	= 0;
	m_hWndBROAD		= 0;
	m_hWndSPEAKER	= 0;
	m_hWndIP		= 0;
	m_hWndEMCU		= 0; 
	
	m_hWndENV		= 0;

	STATUS_RPY_Flag = 0;
	
	m_hWndMsg = 0;

		
	m_pSendIO		= (COMMIO *)malloc(sizeof(COMMIO)); 
	m_pRecvIO		= (COMMIO *)malloc(sizeof(COMMIO)); 

	m_pDebugSts		= (DEBUG_STATUS *)malloc(sizeof(DEBUG_STATUS)); 
	m_pDebugSet		= (DEBUG_CONTROL *)malloc(sizeof(DEBUG_CONTROL)); 

	m_DnWaitTime	= DN_WAITTIME;

	m_pCOMM 	= (COMMINFO *)malloc(sizeof(COMMINFO));
	m_pCOMM_UDP = (COMMINFO_UDP *)malloc(sizeof(COMMINFO_UDP));
	m_pCOMM_SER = (COMMINFO_SER *)malloc(sizeof(COMMINFO_SER));
    m_pCOMM_SetInfo = (COMMDATA_SER *)malloc(sizeof(COMMDATA_SER));  

	memset (m_pCOMM,		'\0', sizeof(COMMINFO));
	memset (m_pCOMM_UDP,	'\0', sizeof(COMMINFO_UDP));
	memset (m_pCOMM_SER,	'\0', sizeof(COMMINFO_SER));
	memset (m_pCOMM_SetInfo,		'\0', sizeof(COMMDATA_SER)); 


	m_pDebugSts		= (DEBUG_STATUS *)malloc(sizeof(DEBUG_STATUS)); 
	m_pDebugSet		= (DEBUG_CONTROL *)malloc(sizeof(DEBUG_CONTROL));
	
	
	m_pHPASet		= (HPA_CONTROL *)malloc(sizeof(HPA_CONTROL)); 
	m_pHPASts		= (HPA_STATUS *)malloc(sizeof(HPA_STATUS));
	
	m_pAMPSet		= (AMP_CONTROL *)malloc(sizeof(AMP_CONTROL)); 
	m_pAMPSts		= (AMP_STATUS *)malloc(sizeof(AMP_STATUS));
	
	m_pLNASet		= (LNA_CONTROL *)malloc(sizeof(LNA_CONTROL)); 
	m_pLNASts		= (LNA_STATUS *)malloc(sizeof(LNA_STATUS));
	
	m_pCHASet		= (CHASET *)malloc(sizeof(CHASET)); 
	m_pCHASts		= (CHASET *)malloc(sizeof(CHASET));
	
	m_pIPSet		= (IP_SET *)malloc(sizeof(IP_SET));	
		
	
	Get_Channel_Select = CHA_AM;
	
	m_pCOMM[0].iCommMethod = 0;
		
	//////
	GetDir(m_SysPath);
	//////
	
}

void ReleaseMemory()
{		   
	free(m_pCOMM_SetInfo);
	free(m_pCOMM_SER);
	free(m_pCOMM_UDP);
	free(m_pCOMM);

	free(m_pSendIO);
	free(m_pRecvIO);
	free(m_pDebugSts);
	free(m_pDebugSet);
	
	
	free(m_pHPASet);
	free(m_pHPASts);
	
	free(m_pAMPSet);
	free(m_pAMPSts);
	
	free(m_pLNASet);
	free(m_pLNASts);
	
	free(m_pCHASet);
	free(m_pCHASts);
	
}

//////////

int main (int argc, char *argv[])
{   
  
		
	int thereIsAnother;
	
	InitValues();	 
	
	if(InitCVIRTE (0, argv, 0) == 0)
		return -1;
	
	if (CheckForDuplicateAppInstance (ACTIVATE_OTHER_INSTANCE, &thereIsAnother) < 0)
	{
		MessagePopup ("Start Error", "The server program that you want to run is already roading.");
		return -1; /* out of memory, or not Win 2000/NT/9x */
	}
	
	if (thereIsAnother)
	{
		MessagePopup ("Start Error", "The server program that you want to run is already roading.");
		return 0; /* prevent duplicate instance */

	}		   
		 
	
//	if(!OpenNewPanel_Main(&m_hWndMain, MAINPNL, FALSE, SW_TITLE, UIR_FILE_G, FALSE))
	if(!OpenNewPanel(&m_hWndMain, MAINPNL, FALSE, SW_TITLE, UIR_FILE, FALSE))  
		return -1;
	

	
	RunUserInterface(); 
	ReleaseMemory();
	
	
	  
	
	   
	return 0;
}


int CVICALLBACK OnCmdMain (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	UINT8	Buffer_data[10]; 
	
	if(event != EVENT_COMMIT)
		return 0;

	switch(control)
	{
		case MAINPNL_BTN_EXIT :					
		{

			EnableTimer(m_hWndMain, MAINPNL_TIMER, FALSE);
			
			if (m_pCOMM[0].iCommMethod==COMM_UDP)
			{
				UDPClose (0);
				m_pCOMM[0].iCommMethod = 0;
			}
			
			m_pCOMM_UDP[0].L_UDP_bThreadRunRCV = FALSE;
			
			AllPanelCloseFunc(0);

			QuitUserInterface (0);          
			break;
		}
		
		case MAINPNL_BTN_DUBUGOPEN :
		{

			OpenNewPanel(&m_pCOMM[0].iPanelHandle_Debug, DEBPNL, TRUE, "Debug #1 ", UIR_FILE, FALSE);
			break;
		}
		
		case MAINPNL_BTN_DOWNLOAD :
		{
		//	DownOpen();
			char strDir[1024];
			char strFileName[1024];
			char CurrentDir[MAX_PATHNAME_LEN]="";

			strcpy(strDir, "");    
			
			GetProjectDir(CurrentDir);
			strcpy(strDir,CurrentDir);
			
			strcpy(strFileName, "\\DOWNLOAD\\DTR.exe");         


			strcat(strDir, strFileName);
			if (LaunchExecutable(strDir) != 0)
			{
				MessagePopup("NOTICE!", "DTR Tool started Fail"); 
			}
				
			break;
		}
		case MAINPNL_BTN_TABLE:
		{

			TableOpen(0);
		   	

			break;
		}
	
		case MAINPNL_BTN_HPA:
		{
			AllPanelCloseFunc(0);
			
			OpenNewPanel(&m_hWndHPA, HPAPNL, FALSE, "HPA", UIR_FILE, FALSE);
			
			SendRequest(0, HPA_CON_STS_REQ, NULL, 0);
			
			break;
		}	
		case MAINPNL_BTN_LNA:	
		{
			AllPanelCloseFunc(0);
			
			OpenNewPanel(&m_hWndLNA, LNAPNL, FALSE, "LNA", UIR_FILE, FALSE);
			
			SendRequest(0, LNA_CON_STS_REQ, NULL, 0); 
			
			break;
		}

		case MAINPNL_BTN_AMP:	
		{
			AllPanelCloseFunc(0);
			
			OpenNewPanel(&m_hWndAMP, AMPPNL, FALSE, "LINE AMP", UIR_FILE, FALSE);
			
			SendRequest(0, AMP_CON_STS_REQ, NULL, 0); 
			
			break;
		}
		
		case MAINPNL_BTN_CHA:	
		{
			AllPanelCloseFunc(0);
			
			OpenNewPanel(&m_hWndCHA, CHAPNL, FALSE, "CHANNEL", UIR_FILE, FALSE);
		
			SetCtrlVal (m_hWndCHA,	CHAPNL_RING_TYPE, 		Get_Channel_Select);
			
			Buffer_data[0] = Get_Channel_Select;
				
			SendRequest(0, CHANNEL_CON_STS_REQ, Buffer_data, 1);
			
			CHADisplay_Init();
			
			break;
		}
		
		case MAINPNL_BTN_ENV:	
		{
			AllPanelCloseFunc(0);
			
			OpenNewPanel(&m_hWndENV, ENVPNL, FALSE, "ENV", UIR_FILE, FALSE);
			
			STATUS_RPY_Flag = 0;
			SendRequest(0, STATUS_REQ, NULL, 0); 
			
			break;
		}
		
		
			
		
			 
		case MAINPNL_BTN_EMERGENCY:	
		{
	//		OpenNewPanel(&m_hWndEMERGENCY, AMPPNL, FALSE, "LINE AMP", UIR_FILE, FALSE);
			char strDir[1024];
			char strFileName[1024];
			char CurrentDir[MAX_PATHNAME_LEN]="";

			strcpy(strDir, "");    
			
			GetProjectDir(CurrentDir);
			strcpy(strDir,CurrentDir);
			
			strcpy(strFileName, "\\EBSC\\EBStreamer.exe");         


			strcat(strDir, strFileName);
			if (LaunchExecutable(strDir) != 0)
			{
				MessagePopup("NOTICE!", "EBStreamer Tool started Fail"); 
			}
			
			break;
		}
		
		case MAINPNL_BTN_EMECH:	
		{
			AllPanelCloseFunc(0);
			
//			OpenNewPanel(&m_hWndBROAD, BROPNL, FALSE, "EMERGENCY CHANNEL", UIR_FILE, FALSE);
			
			break;
		}
		
		case MAINPNL_BTN_SPEAKER:	
		{
	//		OpenNewPanel(&m_hWndSPEAKER, AMPPNL, FALSE, "LINE AMP", UIR_FILE, FALSE);
			
			break;
		}
		
		case MAINPNL_BTN_IP:	
		{
			AllPanelCloseFunc(0);
			
			OpenNewPanel(&m_hWndIP, IPPNL, FALSE, "IP SET", UIR_FILE, FALSE);
			
			//LastEnvInfoLoad();
			SendRequest(0, IP_STS_REQ, NULL, 0); 
			
			break;
		}
		
		case MAINPNL_BTN_EMCU_IP:	
		{
			AllPanelCloseFunc(0);
			
			OpenNewPanel(&m_hWndEMCU, EMCUPNL, FALSE, "EMCU IP", UIR_FILE, FALSE);
			
			LastEnvInfoLoad();
			
			break;
		}
		
		
			
		
		
	}
	return 0;
}  

int CVICALLBACK OnCmdMain2 (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	
	if((event != EVENT_COMMIT)&& (event != EVENT_RIGHT_CLICK)) 
		return 0;
	
	AllPanelCloseFunc(0);
			
	OpenNewPanel(&m_hWndAMP, AMPPNL, FALSE, "LINE AMP", UIR_FILE, FALSE);
	
	SendRequest(0, AMP_CON_STS_REQ, NULL, 0); 
	
	return 0;   
	
}

int CVICALLBACK OnCmdHPA (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
		
	if(event != EVENT_COMMIT)
		return 0;	 
		
	if(m_hWndHPA)
	{
		switch(control)
		{
			case HPAPNL_BTN_REQ:
					
					SendRequest(0, HPA_CON_STS_REQ, NULL, 0); 
				break;
				
			case HPAPNL_BTN_STS:
			if(m_hWndHPA)
			{
//				Buffer_data[0] = SYSTEM_PORT_ID;
//				SendRequest(CMD_LINE_AMP_STAT_REQ, Buffer_data, 1);
			}
				break;
				
			case HPAPNL_BTN_SET :		 
			{
				HPASetData();    
			}
				 break;
				 
			case HPAPNL_BTN_CLOSE :		 
			{
				ClosePanel(&m_hWndHPA);
			
				break;
			}
			
		}
	}
	
	return 0;
}



int CVICALLBACK OnCmdLNA (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
		
	if(event != EVENT_COMMIT)
		return 0;	 
		
	if(m_hWndLNA)
	{
		switch(control)
		{
			case LNAPNL_BTN_REQ:

				SendRequest(0, LNA_CON_STS_REQ, NULL, 0); 
				
				break;
				
			case LNAPNL_BTN_STS:
			if(m_hWndHPA)
			{
//				Buffer_data[0] = SYSTEM_PORT_ID;
//				SendRequest(CMD_LINE_AMP_STAT_REQ, Buffer_data, 1);
			}
				break;
				
			case LNAPNL_BTN_SET :		 
			{
				LNASetData();    
			}
				 break;
				 
			case LNAPNL_BTN_CLOSE :		 
			{
				ClosePanel(&m_hWndLNA);
			
				break;
			}
			
		}
	}
	
	return 0;
}

			
int CVICALLBACK OnCmdAMP_Check (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	if(event != EVENT_COMMIT)
		return 0;	 
		
	if(m_hWndAMP)
	{
		if(event == EVENT_COMMIT)
		{
			AMPStatusChange(control);
		}
	}
	
	return 0; 
	
}

int CVICALLBACK OnCmdAMP (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
		
	if(event != EVENT_COMMIT)
		return 0;	 
		
	if(m_hWndAMP)
	{
		switch(control)
		{
			case AMPPNL_BTN_REQ:
					
					SendRequest(0, AMP_CON_STS_REQ, NULL, 0); 
				break;
				
			case AMPPNL_BTN_STS:
			if(m_hWndAMP)
			{
//				Buffer_data[0] = SYSTEM_PORT_ID;
//				SendRequest(CMD_LINE_AMP_STAT_REQ, Buffer_data, 1);
			}
				break;
				
			case AMPPNL_BTN_SET :		 
			{
				AMPSetData();    
			}
				 break;
				 
			case AMPPNL_BTN_CLOSE :		 
			{
				ClosePanel(&m_hWndAMP);
			
				break;
			}
			
		}
	}
	
	return 0;
}



int CVICALLBACK OnCmdCHA (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int		iVal;
	UINT8	Buffer_data[10];  
	
		
	if(event != EVENT_COMMIT)
		return 0;	 
		
	if(m_hWndCHA)
	{
		switch(control)
		{
			case CHAPNL_BTN_REQ:
				GetCtrlVal (m_hWndCHA,	CHAPNL_RING_TYPE, 		&Get_Channel_Select);
				
				Buffer_data[0] = Get_Channel_Select;
				
				SendRequest(0, CHANNEL_CON_STS_REQ, Buffer_data, 1);
				break;
				
			case CHAPNL_BTN_SET :		 
			{
				CHASetData();    
			}
				 break;
				 
			case CHAPNL_BTN_CLOSE :		 
			{
				ClosePanel(&m_hWndCHA);
			
				break;
			}
			
			case CHAPNL_RING_TYPE:
			{
				GetCtrlVal (m_hWndCHA,	CHAPNL_RING_TYPE, 		&Get_Channel_Select);
				
				Buffer_data[0] = Get_Channel_Select;
				
				SendRequest(0, CHANNEL_CON_STS_REQ, Buffer_data, 1);
				
				CHADisplay_Init();
			
			}
				 break;
		
			case CHAPNL_TG_CFR :		 
			{
				GetCtrlVal (m_hWndCHA,	CHAPNL_TG_CFR, 		&iVal);
				
				if(iVal ==1) // ON
				{
					SetCtrlAttribute(m_hWndCHA, CHAPNL_NMR_CFR_THE, ATTR_DIMMED, 0);
				}
				else
				{
					SetCtrlAttribute(m_hWndCHA, CHAPNL_NMR_CFR_THE, ATTR_DIMMED, 1);
				}
			}
				break;
			
			case CHAPNL_TG_AGC:
			{
				 CHADisplay_Change();   
				 
			}
				break;
			
		}
	}
	
	return 0;
}

int CVICALLBACK OnCmdBOARD (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
		
	if(event != EVENT_COMMIT)
		return 0;	 
		
	if(m_hWndBROAD)
	{
		switch(control)
		{
			case BROPNL_BTN_REQ:
//				Buffer_data[0] = SYSTEM_PORT_ID;
//				SendRequest(CMD_LINE_AMP_REQ, Buffer_data, 1);
				break;
				
			case BROPNL_BTN_STS:
			if(m_hWndBROAD)
			{
//				Buffer_data[0] = SYSTEM_PORT_ID;
//				SendRequest(CMD_LINE_AMP_STAT_REQ, Buffer_data, 1);
			}
				break;
				
			case BROPNL_BTN_SET :		 
			{
//				AMPSetData();    
			}
				 break;
				 
			case BROPNL_BTN_CLOSE :		 
			{
				ClosePanel(&m_hWndBROAD);
			
				break;
			}
			
		}
	}
	
	return 0;
}


int CVICALLBACK OnCmdIP (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	
	UINT8	Buffer_data[50];
	UINT8	i, Buffer_cnt;
	
		
	if(event != EVENT_COMMIT)
		return 0;	 
		
	if(m_hWndIP)
	{
		switch(control)
		{
			case IPPNL_BTN_SET:
				
				//	LastEnvInfoSave();
				
				GetCtrlVal(m_hWndIP, IPPNL_NMR_SERVER1, &m_pIPSet->EMCU_IP[0]);
				GetCtrlVal(m_hWndIP, IPPNL_NMR_SERVER2, &m_pIPSet->EMCU_IP[1]);
				GetCtrlVal(m_hWndIP, IPPNL_NMR_SERVER3, &m_pIPSet->EMCU_IP[2]);
				GetCtrlVal(m_hWndIP, IPPNL_NMR_SERVER4, &m_pIPSet->EMCU_IP[3]);
		
			//	GetCtrlVal(m_hWndIP, IPPNL_NMR_PORT, &m_pIPSet->EMCU_PORT);
		
				m_pIPSet->EMCU_PORT = 11121;
		
				GetCtrlVal(m_hWndIP, IPPNL_NMR_GW1, &m_pIPSet->EMCU_GATEWAY[0]);
				GetCtrlVal(m_hWndIP, IPPNL_NMR_GW2, &m_pIPSet->EMCU_GATEWAY[1]);
				GetCtrlVal(m_hWndIP, IPPNL_NMR_GW3, &m_pIPSet->EMCU_GATEWAY[2]);
				GetCtrlVal(m_hWndIP, IPPNL_NMR_GW4, &m_pIPSet->EMCU_GATEWAY[3]);
		
				GetCtrlVal(m_hWndIP, IPPNL_NMR_SM1, &m_pIPSet->EMCU_SUBNET[0]);
				GetCtrlVal(m_hWndIP, IPPNL_NMR_SM2, &m_pIPSet->EMCU_SUBNET[1]);
				GetCtrlVal(m_hWndIP, IPPNL_NMR_SM3, &m_pIPSet->EMCU_SUBNET[2]);
				GetCtrlVal(m_hWndIP, IPPNL_NMR_SM4, &m_pIPSet->EMCU_SUBNET[3]);
		
				GetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_U_1, &m_pIPSet->EBU_U_IP[0]);
				GetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_U_2, &m_pIPSet->EBU_U_IP[1]);
				GetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_U_3, &m_pIPSet->EBU_U_IP[2]);
				GetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_U_4, &m_pIPSet->EBU_U_IP[3]);
		
				GetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_L_1, &m_pIPSet->EBU_L_IP[0]);
				GetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_L_2, &m_pIPSet->EBU_L_IP[1]);
				GetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_L_3, &m_pIPSet->EBU_L_IP[2]);
				GetCtrlVal(m_hWndIP, IPPNL_NMR_EBU_L_4, &m_pIPSet->EBU_L_IP[3]);
				
				Buffer_cnt = 0;
				
				for (i = 0; i<4; i++) 
				{
					Buffer_data[Buffer_cnt++] = m_pIPSet->EMCU_IP[i];
				}
				
				for (i = 0; i<4; i++)   
				{
					Buffer_data[Buffer_cnt++] = m_pIPSet->EMCU_SUBNET[i];
				}
				
				for (i = 0; i<4; i++)   
				{
					Buffer_data[Buffer_cnt++] = m_pIPSet->EMCU_GATEWAY[i];
				}
				
				for (i = 0; i<4; i++)
				{
					Buffer_data[Buffer_cnt++] = m_pIPSet->EBU_L_IP[i];
				}
				
				for (i = 0; i<4; i++)
				{
					Buffer_data[Buffer_cnt++] = m_pIPSet->EMCU_SUBNET[i];
				}
				
				for (i = 0; i<4; i++)
				{
					Buffer_data[Buffer_cnt++] = m_pIPSet->EMCU_GATEWAY[i];
				}
				
				for (i = 0; i<4; i++)
				{
					Buffer_data[Buffer_cnt++] = m_pIPSet->EBU_U_IP[i];
				}
				
				for (i = 0; i<4; i++)
				{
					Buffer_data[Buffer_cnt++] = m_pIPSet->EMCU_SUBNET[i];
				}
				
				for (i = 0; i<4; i++)
				{
					Buffer_data[Buffer_cnt++] = m_pIPSet->EMCU_GATEWAY[i];
				}
			
				
				SendRequest(0, IP_SET_REQ, Buffer_data, Buffer_cnt);     
				break;
				
			case IPPNL_BTN_LOAD:

				SendRequest(0, IP_STS_REQ, NULL, 0);
				break;
				

			case IPPNL_BTN_CLOSE :		 
			{
				ClosePanel(&m_hWndIP);
			
				break;
			}
			
		}
	}
	
	return 0;
}

int CVICALLBACK OnCmdEMCU (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	UINT8	Buffer_data[50];
	UINT8	i, Buffer_cnt;
	
		
	if(event != EVENT_COMMIT)
		return 0;	 
		
	if(m_hWndEMCU)
	{
		switch(control)
		{
			case EMCUPNL_BTN_SET:
				
				 LastEnvInfoSave();
				 
			break;
				

			case EMCUPNL_BTN_CLOSE :		 
			{
				ClosePanel(&m_hWndEMCU);
			
				break;
			}
				
		}
	}
	
	return 0;	
}


int CVICALLBACK IPInfoAutoCheck (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
		
	if(event != EVENT_COMMIT)
		return 0;	 
		
	if(m_hWndIP)
	{
	
	}
	
	return 0;
}



int CVICALLBACK OnCmdENV (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
		
	if(event != EVENT_COMMIT)
		return 0;	 
		
	if(m_hWndENV)
	{
		switch(control)
		{
			case ENVPNL_BTN_REQ:
				STATUS_RPY_Flag = 0;
				
				SendRequest(0, STATUS_REQ, NULL, 0);
				break;
				
			case ENVPNL_BTN_SET:
				STATUS_RPY_Flag = 0;
				
				if(m_hWndENV)
				{
					SYSSet();
					
				}
				break;
				

			case ENVPNL_BTN_CLOSE :		 
			{
				ClosePanel(&m_hWndENV);
			
				break;
			}
			
		}
	}
	
	return 0;
}

/*********************************************************************************/
/* RS232 Lib Functions Define													 */
/*********************************************************************************/

// CRC Check Function
//////////////////////////////////////////////////////////////////////////////////
unsigned short sum_CRC( UINT8 *buf_ptr, int len)
{
	unsigned short dt,i=0;
	unsigned short crc16;
	int temp_crc;   

	len*=8;   // bit수
	for(crc16=0x0000; len>=8; len-=8, buf_ptr++) 
	{
		crc16=Crc_tbl[(crc16>>8) ^ *buf_ptr] ^ (crc16<<8);
	}
	if(len!=0)
	{
		dt=((unsigned short)(*buf_ptr)) << 8 ;
		while(len-- != 0)
		{
			if(((crc16^dt) & ((unsigned short)1 << 15)) != 0) 
			{
			        crc16<<=1;
			        crc16 ^= 0x1021;
			}
			else crc16<<=1;
			
			dt<<=1;
		}
	}
	return(crc16);
}


// Determines whether the Packet is Vaild Packet (By CRC and Sync)
////////////////////////////////////////////////////////////////////////////////////
BOOL IsValidPacket(void)
{
	int i;
	unsigned short iCodeCRC;
	BYTE cCrcBuf[1024*3];
	int nDataLength;

	if(m_pRecvIO->STX != SYNC)
		return FALSE;

	nDataLength = MAKEWORD(m_pRecvIO->DataLen[1], m_pRecvIO->DataLen[0]);
	
	if(nDataLength > DATABUFMAX)
		return FALSE;
	
	memcpy(cCrcBuf, m_pRecvIO, sizeof(COMMIO));
	iCodeCRC = sum_CRC(&cCrcBuf[0], nDataLength  -2);		

	if(m_pRecvIO->DataBuf[nDataLength-7] != ((iCodeCRC >> 8) & 0x00ff) ||
	   m_pRecvIO->DataBuf[nDataLength-6] != (iCodeCRC & 0x00ff))
	   return FALSE;

	return TRUE;
}





//-------------------------------------------------------------------------------//
//	Timer Function                                  
//-------------------------------------------------------------------------------//
int CVICALLBACK PortDropTimer (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int i, iIndex;
	char buf[10];
	int iValue;
	BYTE cSync[4] = {0x16, 0x16, 0x16, 0x16};

	if(event != EVENT_TIMER_TICK)
		return 0;


	AllPanelCloseFunc(0);
	
	EnableTimer(m_hWndMain, m_pCOMM[0].iPortDropTimer, FALSE);
	EnableTimer(m_hWndMain, m_pCOMM[0].iPollingTimer , FALSE);

	DisableBreakOnLibraryErrors ();
	RS232End(0);
	EnableBreakOnLibraryErrors ();
	
	
	/*	
	if(RS232Init(m_hWndMain, &m_ComAttr, 5, 2, CalculLength, OnPacket))
	{
		RS232SetSync(cSync, 4);
		RS232SetRcvMaxSize(DATABUFMAX);
		EnableTimer(m_hWndMain, MAINPNL_PORTTIMER, FALSE);
	}

	*/	

	return 0;
}

int CVICALLBACK PollingTimer (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int i, iIndex, iID1, iID2;

	if(event != EVENT_TIMER_TICK)
		return 0;
	
	

	if(m_pCOMM[0].iPanelHandle_Table) 
	{
		GetCtrlVal(m_pCOMM[0].iPanelHandle_Table, TABPNL_RING_SYSTEM1, &iID1); 
		
		GetCtrlVal(m_pCOMM[0].iPanelHandle_Table, TABPNL_RING_SYSTEM2, &iID2); 
		
		if( iID2 == 0)
		{
			if(iID1 == 0) SendRequest(0, LNA_STATUS_REQ, NULL, 0);	// LNA
			
			if(iID1 == 1) SendRequest(0, HPA_STATUS_REQ, NULL, 0);  // HPA
		}
			
	//	SetCtrlAttribute(m_hWndMain, MAINPNL_TIMER, ATTR_INTERVAL, 1.0);
	}
	else
	{
		if ((!m_hWndAMP) &&  (!m_hWndHPA) && (!m_hWndLNA))
		{
			if( Polling_Cnt == 0)
			{
				SendRequest(0, STATUS_REQ, NULL, 0); 
				
			}
			else if( Polling_Cnt == 1) 	SendRequest(0, HPA_STATUS_REQ, NULL, 0);
			else if( Polling_Cnt == 2) 	SendRequest(0, AMP_STATUS_REQ, NULL, 0);
			else if( Polling_Cnt == 3)	SendRequest(0, LNA_STATUS_REQ, NULL, 0);
			else 						SendRequest(0, HPA_CON_STS_REQ, NULL, 0);    

		}
		else if(m_hWndHPA)	SendRequest(0, HPA_STATUS_REQ, NULL, 0);
		else if(m_hWndAMP) 	SendRequest(0, AMP_STATUS_REQ, NULL, 0);
		else if(m_hWndLNA) 	SendRequest(0, LNA_STATUS_REQ, NULL, 0);

	}
	
	if( Polling_Cnt++ >= 4) Polling_Cnt = 0;   
		
	
	return 0;
}


int CVICALLBACK ResponseTimer (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)		
{
	int i, iIndex;
	
	if(event != EVENT_TIMER_TICK)
		return 0;
	

	EnableTimer(panel, control, FALSE);
	
	
	EnableTimer(m_hWndMain, m_pCOMM[0].iPollingTimer, TRUE);
	return 0;
}

/*********************************************************************************/

//-------------------------------------------------------------------------------//
//	Main Pannel's Command Function
//-------------------------------------------------------------------------------//	



void AllPanelCloseFunc (int iType)
{

//	ClosePanel(&m_hWndTable); 
	
	ClosePanel(&m_pCOMM[0].iPanelHandle_Table); 
	
	ClosePanel(&m_hWndDownload); 
	ClosePanel(&m_hWndProgress); 
	ClosePanel(&m_hWndDnWait); 

	ClosePanel(&m_hWndHPA); 
	ClosePanel(&m_hWndLNA); 
	ClosePanel(&m_hWndAMP); 
	ClosePanel(&m_hWndCHA); 
	
	ClosePanel(&m_hWndEMERGENCY); 
	ClosePanel(&m_hWndBROAD); 
	ClosePanel(&m_hWndSPEAKER); 
	ClosePanel(&m_hWndIP);
	ClosePanel(&m_hWndEMCU);
	

}


//-------------------------------------------------------------------------------//
//	Etc Pannel's Command Function
//-------------------------------------------------------------------------------//

void SetConMode(void)
{
	
	
}

void PreOperation (int hWnd, int iSeld)	// iSeld : 2009.09.04. Add.. Tree에서 선택한 ROU의 Branch ID. 인빌딩 or 통합광 ROU 판단하기 위함.
{
	if(hWnd == m_hWndMain)	
	{
		LastEnvInfoLoad();	
	}
}


//-------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------//
int CVICALLBACK OnUdpConnect (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{			 
	
	int iAdd1;
	int iAdd2;
	int iAdd3;
	int iAdd4;
	char cPort[8];
	BYTE cSync[4] = {0x16, 0x16, 0x16, 0x16};

	if(event != EVENT_COMMIT)
		return 0;
	
		switch(control)
		{
			case MAINPNL_BTN_UDPOPEN :					
			{
				if (m_pCOMM[0].iCommMethod==COMM_UDP)
				{
					EnableTimer(m_hWndMain, MAINPNL_TIMER, FALSE);
					UDPClose (0);
					MessagePopup("UDP", "UDP 연결을 해제하였습니다."); 
					m_pCOMM[0].iCommMethod = 0;
					SetCtrlAttribute(m_hWndMain, control, ATTR_LABEL_TEXT, "Connect");
					
					SetCtrlAttribute(m_hWndMain, MAINPNL_NMR_EMCU_1, ATTR_DIMMED, 0);
					SetCtrlAttribute(m_hWndMain, MAINPNL_NMR_EMCU_2, ATTR_DIMMED, 0);
					SetCtrlAttribute(m_hWndMain, MAINPNL_NMR_EMCU_3, ATTR_DIMMED, 0);
					SetCtrlAttribute(m_hWndMain, MAINPNL_NMR_EMCU_4, ATTR_DIMMED, 0);

					return 0;
				}
	
			//	EnableTimer(m_hWndMain, MAINPNL_TIMER, FALSE);


				SetCtrlAttribute(m_hWndMain, control, ATTR_LABEL_TEXT, "Disconnect");
	
			
				if(RS232IsOpen(0))	RS232End(0); 
				
				UdpThreadInit(0);
			
				m_pCOMM[0].iCommMethod = COMM_UDP;
				
				SetCtrlAttribute(m_hWndMain, MAINPNL_NMR_EMCU_1, ATTR_DIMMED, 1);
				SetCtrlAttribute(m_hWndMain, MAINPNL_NMR_EMCU_2, ATTR_DIMMED, 1);
				SetCtrlAttribute(m_hWndMain, MAINPNL_NMR_EMCU_3, ATTR_DIMMED, 1);
				SetCtrlAttribute(m_hWndMain, MAINPNL_NMR_EMCU_4, ATTR_DIMMED, 1);
			
			
				
				break;
			}
		
		}
		
	return 0;

}



void SerialMode(BYTE iMode)
{
	BOOL bSrl = FALSE;
	BOOL bUdp = FALSE;
	BOOL bSet = FALSE;
/*	
	if(iMode == MODE_SERIAL)
	{
		bSrl = TRUE;
		SetCtrlAttribute(m_hWndMain, MAINPNL_MODE_BACK, ATTR_TEXT_BGCOLOR, COLOR_SERAILMODE);
		SetCtrlAttribute(m_hWndMain, MAINPNL_MODE_BACK_2, ATTR_TEXT_BGCOLOR, COLOR_SERAILMODE);
		
		SetCtrlAttribute(m_hWndMain, MAINPNL_INFO_TEXT1, ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(m_hWndMain, MAINPNL_INFO_TEXT1_2, ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(m_hWndMain, MAINPNL_INFO_TEXT1_3, ATTR_VISIBLE, TRUE);
		SetCtrlVal(m_hWndMain, MAINPNL_INFO_TEXT1_3, "■  SERIAL");
	}
	else if(iMode == MODE_UDP)
	{
		bUdp = TRUE;
		SetCtrlAttribute(m_hWndMain, MAINPNL_MODE_BACK, ATTR_TEXT_BGCOLOR, COLOR_UDPMODE);
		SetCtrlAttribute(m_hWndMain, MAINPNL_MODE_BACK_2, ATTR_TEXT_BGCOLOR, COLOR_UDPMODE);

		SetCtrlAttribute(m_hWndMain, MAINPNL_INFO_TEXT1, ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(m_hWndMain, MAINPNL_INFO_TEXT1_2, ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(m_hWndMain, MAINPNL_INFO_TEXT1_3, ATTR_VISIBLE, FALSE);
		SetCtrlVal(m_hWndMain, MAINPNL_INFO_TEXT1, "■  MASTER SETUP");
		SetCtrlVal(m_hWndMain, MAINPNL_INFO_TEXT1_2, "■  SLAVE SETUP");
	}

	
	// Serial Mode
	SetCtrlAttribute(m_hWndMain, MAINPNL_SMODE_TEXT1, 	ATTR_VISIBLE, bSrl);
	SetCtrlAttribute(m_hWndMain, MAINPNL_SMODE_TEXT2,	ATTR_VISIBLE, bSrl);
	SetCtrlAttribute(m_hWndMain, MAINPNL_COMPORT, 		ATTR_VISIBLE, bSrl);
	SetCtrlAttribute(m_hWndMain, MAINPNL_COMRING_2, 	ATTR_VISIBLE, bSrl);
	SetCtrlAttribute(m_hWndMain, MAINPNL_BTN_PORTOPEN, 	ATTR_VISIBLE, bSrl);

	// MASTER Mode
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_TEXT1, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_TEXT2, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_TEXT3, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_TEXT4, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_TEXT5, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_NMR1, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_NMR2, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_NMR3, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_NMR4, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_NMR5, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_BTN_USET, ATTR_VISIBLE, bUdp);

	// Slave Mode 
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_TEXT1_2, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_TEXT2_2, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_TEXT3_2, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_TEXT4_2, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_TEXT5_2, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_NMR1_2, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_NMR2_2, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_NMR3_2, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_NMR4_2, ATTR_VISIBLE, bUdp);
	SetCtrlAttribute(m_hWndMain, MAINPNL_UMODE_NMR5_2, ATTR_VISIBLE, bUdp);
	
*/	
}



		


////////
////////////////////////////////////////////////////////////////////////////////////
#define	DEF_MAXCNT_TMS			20

int CheckInputKeyData(int eventData1, int eventData2, int iKeyType) ;


#define DEF_KEY_ALNUM			0
#define DEF_KEY_ALPAH			1
#define DEF_KEY_NUM				2
#define DEF_KEY_IPADDR			3
#define DEF_KEY_PASSWORD		4


int CheckInputKeyData(int eventData1, int eventData2, int iKeyType)  //영문자 및 숫자만 입력..
{
    int asciiCode;
    int virtualKey;         /* cursor keys, function keys, esc, enter, etc.. */
                            /* are "virtual" keys.                           */
    int swallowEvent = 0;   /* return non-zero to "swallow" the event */

	asciiCode = eventData1 & VAL_ASCII_KEY_MASK;
	virtualKey = eventData1 & VAL_VKEY_MASK;
  
    if (asciiCode != 0 && (eventData1 & VAL_MENUKEY_MODIFIER) == 0)
    {
		if (iKeyType == DEF_KEY_ALNUM)
		{
	        if (!isalnum(asciiCode))
			{
				if (asciiCode != 8 && asciiCode != 32 && asciiCode != 45 && asciiCode != 95)
	            	swallowEvent = 1;
			}
		}
		else if (iKeyType == DEF_KEY_ALPAH)
		{
	        if (!isalpha(asciiCode))
			{
				if (asciiCode != 8 && asciiCode != 32 && asciiCode != 45 && asciiCode != 95)
		            swallowEvent = 1;
			}
		}
		else if (iKeyType == DEF_KEY_NUM)
		{
	        if (!isdigit(asciiCode))
			{
				if (asciiCode != 8)
	     	       swallowEvent = 1;
			}
		}
  		else if (iKeyType == DEF_KEY_IPADDR)
		{
	        if (!isdigit(asciiCode))
			{
				if (asciiCode != 8  && asciiCode != 46 ) 
	     	       swallowEvent = 1;
			}
		}  
		else if (iKeyType == DEF_KEY_PASSWORD)
		{
			
			if ((!ispunct(asciiCode)) && (!isalnum(asciiCode)))
			{
				if (asciiCode != 8)
	            	swallowEvent = 1;
			}
			/*
	        if (!isalnum(asciiCode))
			{
				if (asciiCode != 8)
	            	swallowEvent = 1;
			}*/
		}
  }
		
    return swallowEvent;
}

int CVICALLBACK OnAlpahNumKeyCheck (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	if (event == EVENT_KEYPRESS)
	{
		return CheckInputKeyData(eventData1, eventData2, DEF_KEY_ALNUM);		 
	}
	return 0;
}

int CVICALLBACK OnNumKeyCheck (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	if (event == EVENT_KEYPRESS)
	{
		return CheckInputKeyData(eventData1, eventData2, DEF_KEY_NUM);		 
	}
	return 0;
}

int CVICALLBACK OnPasswordKeyCheck (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	if (event == EVENT_KEYPRESS)
	{
		return CheckInputKeyData(eventData1, eventData2, DEF_KEY_PASSWORD);
	}
	return 0;
}



//////////////////////

int CVICALLBACK OnTmrBuzzer (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int i, iIndex;

	if(event != EVENT_TIMER_TICK)
		return 0;
	
	
//	EnableTimer(m_hWndMain, MAINPNL_TMR_BUZZER, FALSE);

	BuzzerOn();
	return 0;
}
