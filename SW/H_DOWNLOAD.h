#ifndef DOWNLOADHEADER
#define DOWNLOADHEADER			

// Define
/////////////////////////////////////////////////////////////////////
#define DN_READY			0		// Ready
#define DN_STARTCONFIRM		1		// 다운로드 통보명령	
#define DN_DATASEND			2		// 다운로드 데이터 전송명령 
#define DN_ENDCONFIRM		3		// 다운로드 종료 확인 명령
#define DN_ENDWAIT			4		// 다운로드 종료 대기 명령 


// Variables
/////////////////////////////////////////////////////////////////////
int		iDnStep;
char 	strFileName[MAX_PATHNAME_LEN];

int		nFrameCnt;
int		iFrameIndex;
BOOL 	bLastFrame;
int		nLastDataLength;
BYTE 	cDataBuf[DN_LEN*1024];

int 	nRetryCnt;
int 	hPrgDlg;
int 	iDnTimeM;
int 	iDnTimeS;

int		iWriteFlag;



void DownLoad_Start(UINT8 Command);
void DownLoad_Data_Send(unsigned int Data_Offset);
void DownloadItemDisplayFunc(void);
void DownloadCallBackDefineFunc(void);
	
#endif
