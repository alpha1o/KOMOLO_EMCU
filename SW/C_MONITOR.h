//==============================================================================
//
// Title:       C_MONITOR.h
// Purpose:     A short description of the interface.
//
// Created on:  2015-12-01 at ���� 9:05:16 by tsKim.
// Copyright:   . All Rights Reserved.
//
//==============================================================================

#ifndef __C_MONITOR_H__
#define __C_MONITOR_H__

#ifdef __cplusplus
    extern "C" {
#endif

//==============================================================================
// Include files

#include "cvidef.h"

//==============================================================================
// Constants

//==============================================================================
// Types

//==============================================================================
// External variables

//==============================================================================
// Global functions

int Declare_Your_Functions_Here (int x);

#ifdef __cplusplus
    }
#endif

#endif  /* ndef __C_MONITOR_H__ */
