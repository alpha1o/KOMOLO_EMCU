#include "toolbox.h"
#include <utility.h>
#include <ansi_c.h>
#include <formatio.h>
#include "G_MAIN.h"
#include "H_EXTERN.H"
#include "H_TABLE.H"

#define UIR_FILE	"G_MAIN.uir"

void GraphDataView(int iTarget);

// Set Table Data Maximum Value (Enable Max 140)
//////////////////////////////////////////////////////////////////////////////////////////////
#define TBLMAXVAL	120


// Set Table Count
//////////////////////////////////////////////////////////////////////////////////////////////
#define TBLCOUNT		32

#define	DNRTBCNT			6
#define	DNRATTTBCNT			4
#define	DNRTEMPTBCNT		10

#define	RMTTBCNT			3
#define	RMTATTTBCNT			1
#define	RMTTEMPTBCNT		4


// Set way get voltage (Polling or Button)
//////////////////////////////////////////////////////////////////////////////////////////////
//int m_WayGetVolt = GETVOLT_BTN	/* or GETVOLT_POL */;
int m_WayGetVolt = GETVOLT_POL	/* or GETVOLT_POL */;
//#define BigEndian

// Set Table Attribute
//////////////////////////////////////////////////////////////////////////////////////////////
#define GP_L_PWR		0

#define GP_L_TEMP		1

#define GP_H_PWR		2

#define GP_H_TEMP		3

#define GP_A_PWR		4

#define GP_A_TEMP		5

void UserSetTblAttr(void)
{

//					  Table Index	| Data Length	| Group			| Fix Length	| Table Name	 	
//		Range Unit	| Range Step	| Range Init	| Range Order	| Range Decimal Place	
//		Value Unit	| Value Bit		| Value Step	| Value Form	| Value Max Val	| Value Min Val	| Value Decimal Place	
//		Offset Use	| Offset Step	| Offset Max	| Offset Min	| Offset Deciamal Place
//		Cal Use 	| Exception	Use	| Voltage Text 	| AD Text		

	// Donor Group
	SetTblAttr(		  0				, 101			, GP_L_PWR		, FALSE			, "  TRS Power ", 
		UNIT_DBM	, 0.5			, -80.			, ORDER_ASC		, 1				,	
		UNIT_D	, 12			, 0.001			, 1.			, 3300			, 0.			, 0,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");

	SetTblAttr(		  1				, 101			, GP_L_PWR		, FALSE			, "  AM Power ",
		UNIT_DBM	, 0.5			, -80.			, ORDER_ASC		, 1				,	
		UNIT_D		, 12			, 0.001			, 1.			, 3300			, 0.			, 0,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");

	SetTblAttr(		  2				, 101			, GP_L_PWR		, FALSE			, "  FM Power ",
		UNIT_DBM	, 0.5			, -80.			, ORDER_ASC		, 1				,	
		UNIT_D		, 12			, 0.001			, 1.			, 3300			, 0.			, 0,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");

	SetTblAttr(		  3				, 101			, GP_L_PWR		, FALSE			, "  DMB Power ",
		UNIT_DBM	, 0.5			, -80.			, ORDER_ASC		, 1				,	
		UNIT_D		, 12			, 0.001			, 1.			, 3300			, 0.			, 0,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	SetTblAttr(		  4				, 101			, GP_L_PWR		, FALSE			, "  TRS-UL Power ",
		UNIT_DBM	, 0.5			, -80.			, ORDER_ASC		, 1				,	
		UNIT_D		, 12			, 0.001			, 1.			, 3300			, 0.			, 0,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");

	
	//-----------------------------	
	SetTblAttr(		  0				, 25			, GP_L_TEMP		, FALSE			, "  TRS TEMP ",
		UNIT_C		, 5.			, -30.			, ORDER_ASC		, 0				,	
		UNIT_DB		, 12			, 0.001			, 1.			, 10.			, -10.			, 1,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	SetTblAttr(		  1				, 25			, GP_L_TEMP		, FALSE			, "  AM TEMP ",
		UNIT_C		, 5.			, -30.			, ORDER_ASC		, 0				,	
		UNIT_DB		, 12			, 0.1			, 1.			, 10.			, -10.			, 1,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	SetTblAttr(		  2				, 25			, GP_L_TEMP		, FALSE			, "  FM TEMP ",
		UNIT_C		, 5.			, -30.			, ORDER_ASC		, 0				,	
		UNIT_DB		, 12			, 0.5			, 1.			, 10.			, -10.			, 1,
		TRUE		, 0.01			, 10.			, -10.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	SetTblAttr(		  3				, 25			, GP_L_TEMP		, FALSE			, "  DMB TEMP ",
		UNIT_C		, 5.			, -30.			, ORDER_ASC		, 0				,	
		UNIT_DB		, 12			, 0.001			, 1.			, 10.			, -10.			, 1,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	SetTblAttr(		  4				, 25			, GP_L_TEMP		, FALSE			, "  TRS-UL TEMP ",
		UNIT_C		, 5.			, -30.			, ORDER_ASC		, 0				,	
		UNIT_DB		, 12			, 0.001			, 1.			, 10.			, -10.			, 1,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	//----------------------------
	SetTblAttr(		  0				, 101			, GP_H_PWR		, FALSE			, "  AM1 Power ", 
		UNIT_DBM	, 0.5			, -80.			, ORDER_ASC		, 1				,	
		UNIT_D		, 12			, 0.001			, 1.			, 3300			, 0.			, 0,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");

	SetTblAttr(		  1				, 101			, GP_H_PWR		, FALSE			, "  AM2 Power ",
		UNIT_DBM	, 0.5			, -80.			, ORDER_ASC		, 1				,	
		UNIT_D		, 12			, 0.001			, 1.			, 3300			, 0.			, 0,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");

	SetTblAttr(		  2				, 101			, GP_H_PWR		, FALSE			, "  FM1 Power ",
		UNIT_DBM	, 0.5			, -80.			, ORDER_ASC		, 1				,	
		UNIT_D		, 12			, 0.001			, 1.			, 3300			, 0.			, 0,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");

	SetTblAttr(		  3				, 101			, GP_H_PWR		, FALSE			, "  FM2 Power ",
		UNIT_DBM	, 0.5			, -80.			, ORDER_ASC		, 1				,	
		UNIT_D		, 12			, 0.001			, 1.			, 3300			, 0.			, 0,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	SetTblAttr(		  4				, 101			, GP_H_PWR		, FALSE			, "  DMB Power ",
		UNIT_DBM	, 0.5			, -80.			, ORDER_ASC		, 1				,	
		UNIT_D		, 12			, 0.001			, 1.			, 3300			, 0.			, 0,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	SetTblAttr(		  5				, 101			, GP_H_PWR		, FALSE			, "  TRS_DL Power ",
		UNIT_DBM	, 0.5			, -80.			, ORDER_ASC		, 1				,	
		UNIT_D		, 12			, 0.001			, 1.			, 3300			, 0.			, 0,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	SetTblAttr(		  6				, 101			, GP_H_PWR		, FALSE			, "  TRS-UL Power ",
		UNIT_DBM	, 0.5			, -80.			, ORDER_ASC		, 1				,	
		UNIT_D		, 12			, 0.001			, 1.			, 3300			, 0.			, 0,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");

	
	//-----------------------------	
	SetTblAttr(		  0				, 25			, GP_H_TEMP		, FALSE			, "  AM1 TEMP ",
		UNIT_C		, 5.			, -30.			, ORDER_ASC		, 0				,	
		UNIT_DB		, 12			, 0.001			, 1.			, 10.			, -10.			, 1,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	SetTblAttr(		  1				, 25			, GP_H_TEMP		, FALSE			, "  AM2 TEMP ",
		UNIT_C		, 5.			, -30.			, ORDER_ASC		, 0				,	
		UNIT_DB		, 12			, 0.1			, 1.			, 10.			, -10.			, 1,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	SetTblAttr(		  2				, 25			, GP_H_TEMP		, FALSE			, "  FM1 TEMP ",
		UNIT_C		, 5.			, -30.			, ORDER_ASC		, 0				,	
		UNIT_DB		, 12			, 0.5			, 1.			, 10.			, -10.			, 1,
		TRUE		, 0.01			, 10.			, -10.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	SetTblAttr(		  3				, 25			, GP_H_TEMP		, FALSE			, "  FM2 TEMP ",
		UNIT_C		, 5.			, -30.			, ORDER_ASC		, 0				,	
		UNIT_DB		, 12			, 0.001			, 1.			, 10.			, -10.			, 1,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	SetTblAttr(		  4				, 25			, GP_H_TEMP		, FALSE			, "  DMB TEMP ",
		UNIT_C		, 5.			, -30.			, ORDER_ASC		, 0				,	
		UNIT_DB		, 12			, 0.001			, 1.			, 10.			, -10.			, 1,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	SetTblAttr(		  5				, 25			, GP_H_TEMP		, FALSE			, "  TRS-DL TEMP ",
		UNIT_C		, 5.			, -30.			, ORDER_ASC		, 0				,	
		UNIT_DB		, 12			, 0.001			, 1.			, 10.			, -10.			, 1,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	SetTblAttr(		  6				, 25			, GP_H_TEMP		, FALSE			, "  TRS-UL TEMP ",
		UNIT_C		, 5.			, -30.			, ORDER_ASC		, 0				,	
		UNIT_DB		, 12			, 0.001			, 1.			, 10.			, -10.			, 1,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	//----------------------------
	SetTblAttr(		  0				, 101			, GP_A_PWR		, FALSE			, "  TRS Power ", 
		UNIT_DBM	, 0.5			, -80.			, ORDER_ASC		, 1				,	
		UNIT_D		, 12			, 0.001			, 1.			, 3300			, 0.			, 0,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");

	SetTblAttr(		  1				, 101			, GP_A_PWR		, FALSE			, "  AM Power ",
		UNIT_DBM	, 0.5			, -80.			, ORDER_ASC		, 1				,	
		UNIT_D		, 12			, 0.001			, 1.			, 3300			, 0.			, 0,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");

	SetTblAttr(		  2				, 101			, GP_A_PWR		, FALSE			, "  FM Power ",
		UNIT_DBM	, 0.5			, -80.			, ORDER_ASC		, 1				,	
		UNIT_D		, 12			, 0.001			, 1.			, 3300			, 0.			, 0,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");

	SetTblAttr(		  3				, 101			, GP_A_PWR		, FALSE			, "  DMB Power ",
		UNIT_DBM	, 0.5			, -80.			, ORDER_ASC		, 1				,	
		UNIT_D		, 12			, 0.001			, 1.			, 3300			, 0.			, 0,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	//-----------------------------	
	SetTblAttr(		  0				, 25			, GP_A_TEMP		, FALSE			, "  TRS TEMP ",
		UNIT_C		, 5.			, -30.			, ORDER_ASC		, 0				,	
		UNIT_DB		, 12			, 0.001			, 1.			, 10.			, -10.			, 1,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	SetTblAttr(		  1				, 25			, GP_A_TEMP		, FALSE			, "  AM TEMP ",
		UNIT_C		, 5.			, -30.			, ORDER_ASC		, 0				,	
		UNIT_DB		, 12			, 0.1			, 1.			, 10.			, -10.			, 1,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	SetTblAttr(		  2				, 25			, GP_A_TEMP		, FALSE			, "  FM TEMP ",
		UNIT_C		, 5.			, -30.			, ORDER_ASC		, 0				,	
		UNIT_DB		, 12			, 0.5			, 1.			, 10.			, -10.			, 1,
		TRUE		, 0.01			, 10.			, -10.			, 2,
		FALSE		, FALSE			, ""			, "");
	
	SetTblAttr(		  3				, 25			, GP_A_TEMP		, FALSE			, "  DMB TEMP ",
		UNIT_C		, 5.			, -30.			, ORDER_ASC		, 0				,	
		UNIT_DB		, 12			, 0.001			, 1.			, 10.			, -10.			, 1,
		TRUE		, 0.01			, 5.			, -5.			, 2,
		FALSE		, FALSE			, ""			, "");
	
//-------------------------------------------------------------------------------------------------------------//
 
 //-------------------------------------------------------------------------------------------------------------//
 //					  Table Index	| Data Length	| Group			| Fix Length	| Table Name	 	
//		Range Unit	| Range Step	| Range Init	| Range Order	| Range Decimal Place	
//		Value Unit	| Value Bit		| Value Step	| Value Form	| Value Max Val	| Value Min Val	| Value Decimal Place	
//		Offset Use	| Offset Step	| Offset Max	| Offset Min	| Offset Deciamal Place
//		Cal Use 	| Exception	Use	| Voltage Text 	| AD Text		


}


int CVICALLBACK OnCmdSelSys (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int iTarget;
	int iID;
	int iType;
	
	iTarget = 0;
	
	if (event != EVENT_COMMIT)
		return 0;

/////////////
	if (event != EVENT_COMMIT)
		return 0;
		
	switch (control)
	{
		case TABPNL_RING_SYSTEM1 :
		{
			
			
			GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_SYSTEM1, &iID);
			if(iID==2)	// LINE AMP
			{
				SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_ID3, ATTR_VISIBLE, 1);
				m_iCntGroup = GP_A_PWR;
			}
			if(iID==1)	// HPA
			{
				SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_ID3, ATTR_VISIBLE, 0);				
				m_iCntGroup = GP_H_PWR;
			}
			else
			{
				SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_ID3, ATTR_VISIBLE, 0);				
				m_iCntGroup = GP_L_PWR;
			}
				  
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_SYSTEM2, 0);  
			break;
		}

		case TABPNL_RING_SYSTEM2 :
		{
			
			
				GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_SYSTEM1, &iID);
				
				GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_SYSTEM2, &iType);
				
				if(iID==2)	// LINE AMP
				{
					SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_ID3, ATTR_VISIBLE, 1); 
					if(iType==0)	m_iCntGroup = GP_A_PWR;
					else 			m_iCntGroup = GP_A_TEMP;
				}
				if(iID==1)	// HPA
				{
					SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_ID3, ATTR_VISIBLE, 0);				
					if(iType==0)	m_iCntGroup = GP_H_PWR;
					else 			m_iCntGroup = GP_H_TEMP;
					
				}
				else
				{
					SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_ID3, ATTR_VISIBLE, 0);				
					if(iType==0)	m_iCntGroup = GP_L_PWR;
					else 			m_iCntGroup = GP_L_TEMP;
				}
			
				
				

		
			break;
		}
	}
	
	InitTableRing(0);


	return 0;
}


//////////////////////////////////////////////////////////////////////////////////////////////
typedef struct 
{	
	UINT8	DATA_SET[TBLMAXVAL*2+5];
	
}TABLE;

TABLE * m_pTblSet;
TABLE * m_pTblSts;
TBLATTR * m_TblAttr[TBLCOUNT];  


void InitTableValue(int iSys)
{
	int i;

	m_nTblcnt = 0;
	


	m_pTblSts = NULL;
	m_pTblSet = NULL;

	m_pTblSts = (TABLE *)malloc(sizeof(TABLE));
	m_pTblSet = (TABLE *)malloc(sizeof(TABLE));
	
	for(i=0; i<TBLCOUNT; i++)
		m_TblAttr[i] = (TBLATTR *)malloc(sizeof(TBLATTR));

	UserSetTblAttr();
}

void SetTblAttr(int iIndex, int nDataLen, int iGroup, BOOL bFixLen, char *pTblName, 
				char R_cUnit, double R_fStep, double R_fInit, char R_cOrder, char R_pDecPlace,
				char V_cUnit, char V_cBitNum, double V_fStep, double V_fForm, double V_fMaxVal, double V_fMinVal, char V_cDecPlace,
				BOOL F_bUse, double F_fStep, double F_fMaxVal, double F_fMinVal, double F_cDecPlace,
				BOOL E_bCalUse, BOOL E_bException, char *E_pTextVo, char *E_pTextAd)
{
	strcpy(m_TblAttr[m_nTblcnt]->stTblName, pTblName);
	m_TblAttr[m_nTblcnt]->iIndex = iIndex;
	m_TblAttr[m_nTblcnt]->nDataLen = nDataLen;
	m_TblAttr[m_nTblcnt]->iGroup = iGroup;
	m_TblAttr[m_nTblcnt]->bFixLen = bFixLen;
	
	m_TblAttr[m_nTblcnt]->R_cUnit = R_cUnit;
	m_TblAttr[m_nTblcnt]->R_fStep = R_fStep;
	m_TblAttr[m_nTblcnt]->R_fInit = R_fInit;
	m_TblAttr[m_nTblcnt]->R_cOrder = R_cOrder;
	m_TblAttr[m_nTblcnt]->R_cDecPlace = R_pDecPlace;

	m_TblAttr[m_nTblcnt]->V_cUnit = V_cUnit;
	m_TblAttr[m_nTblcnt]->V_cBitNum = V_cBitNum;
	m_TblAttr[m_nTblcnt]->V_fStep = V_fStep;
	m_TblAttr[m_nTblcnt]->V_fForm = V_fForm;
	m_TblAttr[m_nTblcnt]->V_fMaxVal = V_fMaxVal;
	m_TblAttr[m_nTblcnt]->V_fMinVal = V_fMinVal;
	m_TblAttr[m_nTblcnt]->V_cDecPlace = V_cDecPlace;

	m_TblAttr[m_nTblcnt]->F_bUse = F_bUse;
	m_TblAttr[m_nTblcnt]->F_fStep = F_fStep;
	m_TblAttr[m_nTblcnt]->F_fMaxVal = F_fMaxVal;
	m_TblAttr[m_nTblcnt]->F_fMinVal = F_fMinVal;
	m_TblAttr[m_nTblcnt]->F_cDecPlace = F_cDecPlace;

	m_TblAttr[m_nTblcnt]->E_bCalUse = E_bCalUse;
	m_TblAttr[m_nTblcnt]->E_bException = E_bException;
	strcpy(m_TblAttr[m_nTblcnt]->E_stTextVo, E_pTextVo);
	strcpy(m_TblAttr[m_nTblcnt]->E_stTextAd, E_pTextAd);

	m_nTblcnt++;
}

void TableOpen(int iTarget)
{
	int i;
	int icnt;
	int iIndex;
	char strTemp[64];
	
	int iWndCh = 0;
																				 
	AllPanelCloseFunc(iTarget);
	
	//EnableTimer(m_hWndMain, m_pCOMM[iTarget].iPollingTimer, FALSE);
		
	// Init Table Value
	/////////////////////////////////////////////////////////////////////////////////
	InitTableValue(iWndCh);


	OpenNewPanel(&m_pCOMM[iTarget].iPanelHandle_Table, TABPNL, FALSE, SW_TITLE, UIR_FILE, FALSE);  


	// Set Tab Order
	/////////////////////////////////////////////////////////////////////////////////
	for (icnt = 0; icnt < 140; icnt++)
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[icnt], ATTR_CTRL_TAB_POSITION, icnt);	
	for ( ; icnt < 140*2; icnt++)
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[icnt-140], ATTR_CTRL_TAB_POSITION, icnt);
	for ( ; icnt < 140*3; icnt++)
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlCB[icnt-280], ATTR_CTRL_TAB_POSITION, icnt);

	// Set Range Init & Length Attribute
	/////////////////////////////////////////////////////////////////////////////////
	SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_NMR_ATTR2, ATTR_MAX_VALUE, TBLMAXVAL);
	
	for(i=0; i<TBLMAXVAL; i++)
		SetCtrlAttribute (m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i], ATTR_CALLBACK_FUNCTION_POINTER, VoltageChang);
	
	m_iCntGroup = GP_L_PWR;

	InitTableRing(iTarget);
}

int CVICALLBACK OnChangeIDRing (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
	int i, iID, iID2, iID3;
	int iIndex;
	BOOL bVolt;
	char stLabel[128];
	BOOL bVoltVal = FALSE;
	BOOL bGetVoltFromBtn = FALSE;
	BOOL bOffsetUse = FALSE;
	int iType, iSys;
	int iTarget = -1;

	
	if(event != EVENT_COMMIT)
		return 0;
	
	iTarget = 0;
	
	if (iTarget == -1)
		return 0;
	
	m_iIndex = -1;
	SetTableCtrlColor(iTarget);
	
	// Find Current Index
	////////////////////////////////////////////////////////////////////////////////
	if(control==TABPNL_RING_ID)
	{
		GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_ID, &iIndex);

		for(i=0; i<TBLCOUNT; i++)
		{

			if((m_TblAttr[i]->iGroup == m_iCntGroup) && (m_TblAttr[i]->iIndex == iIndex)) 
			{
				m_iIndex = i;
				break;
			}
		}	
			
	}
	
	
	
	if(m_iIndex < 0)
	{
		return 0;
	}
	
	// Range Init & Length Attribute
	////////////////////////////////////////////////////////////////////////////////
	if(!m_TblAttr[m_iIndex]->bFixLen)	
		bVolt = TRUE;
	else
		bVolt = FALSE;

	SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_NMR_ATTR1, ATTR_VISIBLE, bVolt);
	SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_NMR_ATTR2, ATTR_VISIBLE, bVolt);
	SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_TMSG_ATTR1, ATTR_VISIBLE, bVolt);
	
	
	SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_NMR_ATTR1, m_TblAttr[m_iIndex]->R_fInit); 
	SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_NMR_ATTR2, m_TblAttr[m_iIndex]->nDataLen);
	

	// Show each controls 
	////////////////////////////////////////////////////////////////////////////////
	if(m_TblAttr[m_iIndex]->V_cBitNum == 8)
		m_iAdMaxVal = 255;
	else if(m_TblAttr[m_iIndex]->V_cBitNum == 10)
		m_iAdMaxVal = 1023;
	else if(m_TblAttr[m_iIndex]->V_cBitNum == 12)
		m_iAdMaxVal = 3300;

	// Exception
	if(m_TblAttr[m_iIndex]->E_bException)
	{
		for(i=0; i<TBLMAXVAL; i++)
		{
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i],	ATTR_CTRL_MODE, VAL_HOT);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i],	ATTR_WIDTH, 55);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i],	ATTR_TEXT_COLOR, VAL_BLACK);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i],	ATTR_DATA_TYPE,	VAL_DOUBLE);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i],	ATTR_PRECISION,	m_TblAttr[m_iIndex]->V_cDecPlace);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i],	ATTR_INCR_VALUE, m_TblAttr[m_iIndex]->V_fStep);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i],	ATTR_MAX_VALUE,	m_TblAttr[m_iIndex]->V_fMaxVal);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i],	ATTR_MIN_VALUE,	m_TblAttr[m_iIndex]->V_fMinVal);

			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i],	ATTR_TEXT_COLOR, VAL_BLACK);			 
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i],	ATTR_DATA_TYPE,	VAL_DOUBLE);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i],	ATTR_PRECISION,	m_TblAttr[m_iIndex]->V_cDecPlace);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i],	ATTR_INCR_VALUE, m_TblAttr[m_iIndex]->V_fStep);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i],	ATTR_MAX_VALUE,	m_TblAttr[m_iIndex]->V_fMaxVal);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i],	ATTR_MIN_VALUE,	m_TblAttr[m_iIndex]->V_fMinVal);

			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i],	ATTR_FORMAT, VAL_FLOATING_PT_FORMAT);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i],	ATTR_FORMAT, VAL_FLOATING_PT_FORMAT);

			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i], 0.);
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], 0.);
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlCB[i], 0);
		}

	}
	else
	{	
		for(i=0; i<TBLMAXVAL; i++)
		{
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i],	ATTR_CTRL_MODE, VAL_INDICATOR);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i],	ATTR_WIDTH, 45);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i],	ATTR_TEXT_BGCOLOR, VAL_WHITE);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i],	ATTR_TEXT_COLOR, VAL_BLACK);			 
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i],	ATTR_DATA_TYPE,	VAL_UNSIGNED_INTEGER);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i],	ATTR_MAX_VALUE,	m_iAdMaxVal);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i],	ATTR_MIN_VALUE,	0);

			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i],	ATTR_TEXT_COLOR, VAL_BLACK);			 
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i],	ATTR_DATA_TYPE,	VAL_DOUBLE);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i],	ATTR_PRECISION,	m_TblAttr[m_iIndex]->V_cDecPlace);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i],	ATTR_INCR_VALUE, m_TblAttr[m_iIndex]->V_fStep);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i],	ATTR_MAX_VALUE,	m_TblAttr[m_iIndex]->V_fMaxVal);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i],	ATTR_MIN_VALUE,	m_TblAttr[m_iIndex]->V_fMinVal);

			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i],	ATTR_FORMAT, VAL_FLOATING_PT_FORMAT);

			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i], 0);
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], 0.);
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlCB[i], 0);
		}
	}

	DisplayCtrlsRetry(iTarget);

	for(i=0; i<7; i++)
	{
		if(m_TblAttr[m_iIndex]->R_cUnit == UNIT_DBM)
		{
		//	if(m_TblAttr[m_iIndex]->iIndex == 8)
		//			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlTEXT3[i], "[℃]");
		//	else	SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlTEXT3[i], "[dBm]");
			
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlTEXT3[i], "[dBm]");        
		}
		else if(m_TblAttr[m_iIndex]->R_cUnit == UNIT_DB)
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlTEXT3[i], "[dB]");
		else if(m_TblAttr[m_iIndex]->R_cUnit == UNIT_C)
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlTEXT3[i], "[℃]");

		if(m_TblAttr[m_iIndex]->E_bException)
		{
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlTEXT1[i], m_TblAttr[m_iIndex]->E_stTextVo);
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlTEXT2[i], m_TblAttr[m_iIndex]->E_stTextAd);
		}
		else if(m_TblAttr[m_iIndex]->V_cUnit == UNIT_VOLT)
		{
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlTEXT1[i], "Voltage");
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlTEXT2[i], "AD");
		}
		else if(m_TblAttr[m_iIndex]->V_cUnit == UNIT_DB)
		{
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlTEXT1[i], "dB");
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlTEXT2[i], "");
		}		
		else if(m_TblAttr[m_iIndex]->V_cUnit == UNIT_AD)
		{
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlTEXT1[i], "AD");
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlTEXT2[i], "");
		}		
		else if(m_TblAttr[m_iIndex]->V_cUnit == UNIT_D)
		{
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlTEXT1[i], "mV");
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlTEXT2[i], "");
		}
	}

	// Button hide or visible
	////////////////////////////////////////////////////////////////////////////////////
	if((m_TblAttr[m_iIndex]->R_cUnit == UNIT_DB) &&  (m_TblAttr[m_iIndex]->V_cUnit = UNIT_DB))
	{
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_NMR_ATTR1,		ATTR_PRECISION,		1);
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_NMR_ATTR1,		ATTR_INCR_VALUE,	0.5);
	}
	else
	{
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_NMR_ATTR1,		ATTR_PRECISION,		0);
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_NMR_ATTR1,		ATTR_INCR_VALUE,	1.0);
	}
	
	
	if(m_TblAttr[m_iIndex]->R_cUnit == UNIT_DBM)
	{
		bVoltVal = TRUE;

		if(m_WayGetVolt == GETVOLT_BTN)
			bGetVoltFromBtn = TRUE;	
	}
	SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRAD, ATTR_VISIBLE, bVoltVal);
	SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRVO, ATTR_VISIBLE, bVoltVal);
	SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_TMSGVOLT, ATTR_VISIBLE, bVoltVal);
	SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_BTN_VOLT, ATTR_VISIBLE, bVoltVal & bGetVoltFromBtn);
	
//////	EnableTimer(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_TIMER, bVoltVal & !bGetVoltFromBtn);

	if(m_TblAttr[m_iIndex]->E_bException && !m_TblAttr[m_iIndex]->E_bCalUse)
	{
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_TOG_CAL, ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_BTN_SPREADING, ATTR_VISIBLE, FALSE);
	}
	else if(m_TblAttr[m_iIndex]->E_bCalUse)
	{
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_TOG_CAL, ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_BTN_SPREADING, ATTR_VISIBLE, FALSE);
	}
	else
	{
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_TOG_CAL, ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_BTN_SPREADING, ATTR_VISIBLE, TRUE);
	}
		

	// Offset Attribute
	////////////////////////////////////////////////////////////////////////////////////
	if(m_TblAttr[m_iIndex]->F_bUse)
		bOffsetUse = TRUE;
	
	SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_OFFSET, ATTR_PRECISION, m_TblAttr[m_iIndex]->F_cDecPlace);
	SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_OFFSET, ATTR_INCR_VALUE, m_TblAttr[m_iIndex]->F_fStep);
	SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_OFFSET, ATTR_MAX_VALUE, m_TblAttr[m_iIndex]->F_fMaxVal);
	SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_OFFSET, ATTR_MIN_VALUE, m_TblAttr[m_iIndex]->F_fMinVal);

	SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_OFFSET, ATTR_VISIBLE, bOffsetUse);
	SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_BTN_APPLY, ATTR_VISIBLE, bOffsetUse);

	SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_OFFSET, 0.);

	// Table Data Request
	////////////////////////////////////////////////////////////////////////////////////

	if((control==TABPNL_RING_SYSTEM1)&&(control==TABPNL_RING_SYSTEM2)&&(bTableBKMode==dBackupSet))
		return 0;	
	
	
	
	GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_SYSTEM1, &iID2);
	GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_ID3, &iID3);

	
	memset(m_pTblSet, NULL, sizeof(TABLE));   
	
	m_pTblSet->DATA_SET[0] = iID2;
	m_pTblSet->DATA_SET[1] = m_TblAttr[m_iIndex]->iIndex; 
	
	if( iID2 == 2)	m_pTblSet->DATA_SET[2] = iID3;
	else			m_pTblSet->DATA_SET[2] = 0;
	
	if((m_iCntGroup == GP_L_PWR) || (m_iCntGroup == GP_H_PWR) || (m_iCntGroup == GP_A_PWR))
	{
		
		SendRequest(iTarget, HPA_PWR_STS_REQ, m_pTblSet, 3);
	}
	else
	{
		SendRequest(iTarget, HPA_TEMP_STS_REQ, m_pTblSet, 3);
	}
	
	return 0;
}

int CVICALLBACK VoltageChang (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
	int iTarget = -1;
	int i;
	
	if((event != EVENT_VAL_CHANGED) && (event != EVENT_RIGHT_CLICK))
		return 0;


	for (i=0; i<DEF_COMM_CNT;i++)
	{
		if (m_pCOMM[i].iPanelHandle_Table == panel)
		{	
			iTarget = i;
			break;
		}
	}
	
	if(event == EVENT_VAL_CHANGED)
	{
		int i;
		int iPos;
		int iAdMaxVal;
		double fval;
		double fCntVolt;
		int iAdData;

		if(m_TblAttr[m_iIndex]->R_cUnit == UNIT_DB && m_TblAttr[m_iIndex]->V_cUnit == UNIT_DB)
		{
			BOOL bOn;

			GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_TOG_CAL, &bOn);
			if(!bOn)
				return 0;

			for(iPos=0; iPos<TBLMAXVAL; iPos++)
			{
				if(control == ControlVO[iPos] || control == ControlAD[iPos])
					break;
			}

			m_pTblSet->DATA_SET[0] = 0x13;
			m_pTblSet->DATA_SET[1] = 0x31;

			m_pTblSet->DATA_SET[2] = m_TblAttr[m_iIndex]->iIndex;

			GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[iPos], &fval);
			m_pTblSet->DATA_SET[3] = fval * m_TblAttr[m_iIndex]->V_fForm;

			if(m_TblAttr[m_iIndex]->E_bException)
			{
				GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[iPos], &fval);
				m_pTblSet->DATA_SET[4] = fval * m_TblAttr[m_iIndex]->V_fForm;
			}
			else
				m_pTblSet->DATA_SET[4] = 0xFF;

			SendRequest(0, HPA_PWR_CON_REQ, m_pTblSet, 5);
		}
		else if(m_TblAttr[m_iIndex]->V_cUnit == UNIT_VOLT)
		{	
			for(i=0; i<TBLMAXVAL; i++)
			{
				GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], &fCntVolt);
				if(fCntVolt >= 3.3)
				{
					fCntVolt = 3.3;
					SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], fCntVolt);
				}

				iAdData = (int)((fCntVolt * (double)m_iAdMaxVal) / 3.3);
				SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i], iAdData);
			}

			for(iPos=0; iPos<TBLMAXVAL; iPos++)
			{
				if(control == ControlVO[iPos])
					break;
			}

			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, control, ATTR_TEXT_COLOR, VAL_BLUE);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[iPos], ATTR_TEXT_COLOR, VAL_BLUE);
		}
		// 2006.08.09.
		else if(m_TblAttr[m_iIndex]->V_cUnit == UNIT_DB)
		{	
			for(i=0; i<TBLMAXVAL; i++)
			{
				GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], &fCntVolt);
				if(fCntVolt >= m_TblAttr[m_iIndex]->V_fMaxVal)
				{
					fCntVolt = m_TblAttr[m_iIndex]->V_fMaxVal;
					SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], fCntVolt);
				}
			}

			for(iPos=0; iPos<TBLMAXVAL; iPos++)
			{
				if(control == ControlVO[iPos])
					break;
			}

			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, control, ATTR_TEXT_COLOR, VAL_BLUE);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[iPos], ATTR_TEXT_COLOR, VAL_BLUE);
		}
		GraphDataView(iTarget); 
	}
	

	else if(event == EVENT_RIGHT_CLICK)
	{
		int iPos;
		double fCntVolt;
		int iAdData;
		
		//if(m_TblAttr[m_iIndex]->V_cUnit != UNIT_VOLT)
		if(m_TblAttr[m_iIndex]->V_cUnit != UNIT_D) 
			return 0;
		
		

		for(iPos=0; iPos<TBLMAXVAL; iPos++)
		{
			if(control == ControlVO[iPos])
				break;
		}

	//	GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRVO, &fCntVolt);
		GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRAD, &iAdData);
		fCntVolt = (double)iAdData;
		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[iPos], fCntVolt);
		GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRAD, &iAdData);
		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[iPos], iAdData);
		
   		SetCtrlAttribute (m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[iPos], ATTR_TEXT_COLOR, VAL_BLUE);
		SetCtrlAttribute (m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[iPos], ATTR_TEXT_COLOR, VAL_BLUE); 
		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlCB[iPos], 1);
		
		GraphDataView(iTarget); 
	}

	return 0;
}

int CVICALLBACK TablePolling (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
	int iSys, iType;
 	int iTarget = -1;
	int i;

	if(event != EVENT_TIMER_TICK)
		return 0;

	for (i=0; i<DEF_COMM_CNT;i++)
	{
		if (m_pCOMM[i].iPanelHandle_Table == panel)
		{	
			iTarget = i;
			break;
		}
	}
	
	if (iTarget == -1)
		return 0;
	
	memset(m_pTblSet, NULL, sizeof(TABLE));

	SendRequest(iTarget, STATUS_REQ, NULL, 0);

	return 0;
}

int CVICALLBACK OnCmdTable (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int iTarget = -1;
	int i;
	
	if(event != EVENT_COMMIT)
		return 0;
	
	for (i=0; i<DEF_COMM_CNT;i++)
	{
		if (m_pCOMM[i].iPanelHandle_Table == panel)
		{	
			iTarget = i;
			break;
		}
	}
	
	if (iTarget == -1)
		return 0;
		
	switch (control)
	{
		
		case TABPNL_BTN_DEFAULTSET :
			{
				int iMsg = GenericMessagePopup ("Table Backup",
	                                "전체 테이블 설정을 진행하시겠습니까?", "YES", "NO",
	                                "", NULL, 0, 0, VAL_GENERIC_POPUP_BTN1,
	                                VAL_GENERIC_POPUP_BTN1, VAL_GENERIC_POPUP_BTN2);
			

				break;
			}
		case TABPNL_BTN_DEFAULTMAKE :
			{
				int iMsg = GenericMessagePopup ("Table Backup",
	                                "전체 테이블 데이터 수집을 진행하시겠습니까?", "YES", "NO",
	                                "", NULL, 0, 0, VAL_GENERIC_POPUP_BTN1,
	                                VAL_GENERIC_POPUP_BTN1, VAL_GENERIC_POPUP_BTN2);
			

				break;
			}
		

	case TABPNL_BTN_VOLT :
		{
//			m_pTblSet->DATA_SET[0] = m_TblAttr[m_iIndex]->iIndex;
//			SendRequest(TBLGET_REQ, m_pTblSet, 1);
			break;
		}

	case TABPNL_BTN_CLOSE :
		{
			int i;
		
			if(m_pTblSet)
				free(m_pTblSet);
			
			if(m_pTblSts)
				free(m_pTblSts);
			
			if(m_TblAttr)
			{
				int i;				
				for(i=0; i<TBLCOUNT; i++)
					free(m_TblAttr[i]);
			}
			
			ClosePanel(&m_pCOMM[iTarget].iPanelHandle_Table);
		

			EnableTimer (m_hWndMain,	m_pCOMM[iTarget].iPollingTimer,	TRUE);
//			SetCtrlAttribute (m_hWndMain,	MAINPNL_BTN_TABLE,	ATTR_DIMMED,	TRUE);
//			SetCtrlAttribute (m_hWndMain,	MAINPNL_BTN_RESET,	ATTR_DIMMED,	TRUE);
			break;
		}

	case TABPNL_BTN_SPREADING :
		{
			int i=0, j=0;
			int FirstCheck = 0, SecondCheck = 0, First = 0;
			int LargeIndex, SmallIndex;
			double LargeVal, SmallVal, dBSetVal;
			unsigned int  AD_Data;
			double  CurrentVolt;
			
			for ( i = 0; i < TBLMAXVAL; i++)
			{
				GetCtrlVal (m_pCOMM[iTarget].iPanelHandle_Table, ControlCB[i], &FirstCheck); 
		
				if( (FirstCheck == 1) && (SecondCheck == 0) ) 
				{
					GetCtrlVal (m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], &SmallVal); 
					SmallIndex = i;
					SetCtrlVal (m_pCOMM[iTarget].iPanelHandle_Table, ControlCB[i], 0); 
					SecondCheck++;	
				}
				if( (FirstCheck == 1) && (SecondCheck == 1)) 
				{
					GetCtrlVal (m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], &LargeVal); 
					LargeIndex = i;
					SetCtrlVal (m_pCOMM[iTarget].iPanelHandle_Table, ControlCB[i], 0);
			
					for ( j = SmallIndex; j < LargeIndex; j++) 
					{
						dBSetVal = SmallVal - ((SmallVal-LargeVal)/(LargeIndex-SmallIndex))*(j-SmallIndex);
		 				SetCtrlVal (m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[j], dBSetVal);
		 				SetCtrlAttribute (m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[j], ATTR_TEXT_COLOR, VAL_BLUE);
	 				
		 				GetCtrlVal (m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[j], &CurrentVolt);
		 				if (CurrentVolt >= m_TblAttr[m_iIndex]->V_fMaxVal)
					//	if(CurrentVolt >= 5.0)
						{
							CurrentVolt = m_TblAttr[m_iIndex]->V_fMaxVal;
						//	CurrentVolt = 5;
							SetCtrlVal (m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[j], CurrentVolt);
						}
						AD_Data = (unsigned int)((CurrentVolt * m_iAdMaxVal) / 3.3);
						SetCtrlVal (m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[j],	AD_Data); 
						SetCtrlAttribute (m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[j], ATTR_TEXT_COLOR, VAL_BLUE);
		 			}
		 			SetCtrlAttribute (m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[LargeIndex], ATTR_TEXT_COLOR, VAL_BLUE);
		 			SetCtrlAttribute (m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[LargeIndex], ATTR_TEXT_COLOR, VAL_BLUE);
	 			
					SmallVal = LargeVal;
					SmallIndex = LargeIndex;
				}
			}
			
			GraphDataView(iTarget); 

			
			break;
		}

	case TABPNL_BTN_SAVE :
		{
			int i,hFile, iIdx;
			double fval;
			UINT8 cval;
			double aVoltBuf[500];
			char strPathName[260];
			char strLable[64];
				
			memset (strPathName, '\0', sizeof(strPathName));
			
			memset (strLable, '\0', sizeof(strLable));
			GetCtrlIndex (m_pCOMM[iTarget].iPanelHandle_Table,		TABPNL_RING_ID,		&iIdx);
			
			GetLabelFromIndex (m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_ID, iIdx, strLable);
			strcat (strLable, ".txt");
			
			FileSelectPopup("", strLable, "*.txt", "Table Data Save",	VAL_OK_BUTTON, 0, 0, 1, 1, strPathName);

			if(strPathName[0] == 0)
				break;
			
			for(i=0; i<TBLMAXVAL; i++) 
				GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], &aVoltBuf[i]);
								 
			hFile = OpenFile(strPathName, 2, 0, 1);
			
			// Start Fange / Length Save
			GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_NMR_ATTR1, &fval); 
			FmtFile(hFile, "%s<%f[x]", fval);
			WriteFile(hFile, "\n", 1);
			GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_NMR_ATTR2, &cval);
			FmtFile(hFile, "%s<%f[x]", (double)cval);
			WriteFile(hFile, "\n", 1);
			
			// Exception
			if(m_TblAttr[m_iIndex]->E_bException)
			{
				double aAdBuf[500];

				for(i=0; i<TBLMAXVAL; i++) 
				GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i], &aAdBuf[i]);

				for(i=0; i<m_TblAttr[m_iIndex]->nDataLen; i++) 
				{
					FmtFile(hFile, "%s<%f[x]", aVoltBuf[i]);
					WriteFile(hFile, "\n", 1);
					FmtFile(hFile, "%s<%f[x]", aAdBuf[i]);
					WriteFile(hFile, "\n", 1);
				}
			}
			else
			{
				for(i=0; i<TBLMAXVAL; i++) 
				{
					FmtFile(hFile, "%s<%f[x]", aVoltBuf[i]);
					WriteFile(hFile, "\n", 1);
				}
			}

			CloseFile(hFile);
			break;
		}

	case TABPNL_BTN_LOAD :
		{
			int i,hFile;
			char strPathName[260]={0,};
			int iAdData;
			double aVoltBuf[500];
	
			FileSelectPopup("", "*.txt", "*.txt", "Table Data Load",
							 VAL_LOAD_BUTTON, 0, 0, 1, 0, strPathName);
			
			if(strPathName[0] == 0)	
				break;

			hFile = OpenFile(strPathName, VAL_READ_ONLY, VAL_OPEN_AS_IS,VAL_ASCII);			

			for(i=0; i<TBLMAXVAL+2; i++) 
				ScanFile(hFile, "%s>%f[x]", &aVoltBuf[i]);
				
			CloseFile(hFile);
			
			if(!m_TblAttr[m_iIndex]->bFixLen)
			{
				SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_NMR_ATTR1, aVoltBuf[0]);
				SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_NMR_ATTR2, (unsigned char)aVoltBuf[1]);
				
				m_TblAttr[m_iIndex]->nDataLen = (unsigned char)aVoltBuf[1];
				m_TblAttr[m_iIndex]->R_fInit = aVoltBuf[0];
			}

			// Exception
			if(m_TblAttr[m_iIndex]->E_bException)
			{
				for(i=0; i<m_TblAttr[m_iIndex]->nDataLen; i++)		
				{
					SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], aVoltBuf[2*i+2]);
					SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i], aVoltBuf[2*i+3]);
				}
			}
			else
			{
				for(i=0; i<TBLMAXVAL; i++)		
				{
					SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], aVoltBuf[i+2]);

					if(m_TblAttr[m_iIndex]->V_cUnit == UNIT_VOLT)
					{
						iAdData = (int)((aVoltBuf[i+2] * (double)m_iAdMaxVal) / 3.3);
						SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i], iAdData);
					}
				}
			}
			
			DisplayCtrlsRetry(iTarget);
			
			break;
		}

	case TABPNL_BTN_APPLY :
		{
			int i;
			int iAdData;
			double fval, fOffset;

			GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_OFFSET, &fOffset);

			for(i=0; i<m_TblAttr[m_iIndex]->nDataLen; i++)
			{
				GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], &fval);
				fval = fval + fOffset;
				if(fval > m_TblAttr[m_iIndex]->V_fMaxVal)
					fval = m_TblAttr[m_iIndex]->V_fMaxVal;
				else if(fval < m_TblAttr[m_iIndex]->V_fMinVal)
					fval = m_TblAttr[m_iIndex]->V_fMinVal;
		
				SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], fval);


				if(m_TblAttr[m_iIndex]->V_cUnit == UNIT_VOLT)
				{
					iAdData = (int)(fval * (double)m_iAdMaxVal / 5.);
					SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i], iAdData);
					SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i], ATTR_TEXT_COLOR, VAL_BLUE);
				}

				SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], ATTR_TEXT_COLOR, VAL_BLUE);
			}
			GraphDataView(iTarget); 
			break;
		}

	case TABPNL_BTN_SETUP :
		{
			TableDataSetup(iTarget);
			break;
		}

	case TABPNL_TOG_CAL :
		{
			BOOL bOn;

			GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_TOG_CAL, &bOn);

			m_pTblSet->DATA_SET[0] = 0x13;
			m_pTblSet->DATA_SET[1] = 0x30;
			m_pTblSet->DATA_SET[2] = !bOn;

			SendRequest(iTarget, CONTROL_REQ, m_pTblSet, 3);
			break;
		}
	}
	return 0;
}

void TableDataSetup (int iTarget)
{
	int i, iVlaue;
	int iSys, iType;
	double fval;
	BYTE nLen;
	int iAdData;
	BYTE cCrcBuf[1024*2];
	unsigned short iCodeCRC;
	
	int	iID1, iID2, iID3, iID4;
	
	SetTableCtrlColor(iTarget);
	memset(m_pTblSet, NULL, sizeof(TABLE));

	// Set table index
	/////////////////////////////////////////////////////////////////////////////////////
	
	GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_SYSTEM1, &iID1);
	GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_SYSTEM2, &iID2); 
	GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_ID, &iID3);
	GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_ID3, &iID4);

	
	
	m_pTblSet->DATA_SET[0] = iID1;
	m_pTblSet->DATA_SET[1] = iID3;
	
	if( iID1 == 2)	m_pTblSet->DATA_SET[2] = iID4;
	else			m_pTblSet->DATA_SET[2] = 0;
	
	
	GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_NMR_ATTR1, &fval);
	GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_NMR_ATTR2, &nLen);
	
	iVlaue = fval;

	m_pTblSet->DATA_SET[3] = (UINT8) iVlaue; 
	m_pTblSet->DATA_SET[4] = nLen;

	if( iID2 == 0)
	{
		for(i=0; i<m_TblAttr[m_iIndex]->nDataLen; i++)
		{

				GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], &fval);
		
				if( fval > 0) 	fval = fval + 0.001;
				else 			fval = fval - 0.001;
		
				iAdData = (unsigned short int)(fval);

				m_pTblSet->DATA_SET[i*2+5] = LOBYTE(iAdData);
				m_pTblSet->DATA_SET[i*2+6] = HIBYTE(iAdData);
		}
	
	 	SendRequest(iTarget, HPA_PWR_CON_REQ, m_pTblSet, m_TblAttr[m_iIndex]->nDataLen * 2 + 5);
	}
	else
	{
		for(i=0; i<m_TblAttr[m_iIndex]->nDataLen; i++)
		{

				GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], &fval);
		
				if( fval > 0) 	fval = fval + 0.001;
				else 			fval = fval - 0.001;
		
				iAdData = (unsigned short int)(fval*2);

				m_pTblSet->DATA_SET[i+5] = iAdData;
		}
		
		SendRequest(iTarget, HPA_TEMP_CON_REQ, m_pTblSet, m_TblAttr[m_iIndex]->nDataLen + 5);
	}
		
	
}

void TableDataDisplay(int iTarget)
{
	int i;
	double fVolt;
	int aVal[2], iVal;
	int	iID1, iID2;  

	SetTableCtrlColor(iTarget);
	memcpy(m_pTblSts, m_pRecvIO->DataBuf, sizeof(TABLE));
	

	
	

	// Set table index
	/////////////////////////////////////////////////////////////////////////////////////
	
	GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_SYSTEM1, &iID1);
	GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_SYSTEM2, &iID2); 
	
	
	if(!m_TblAttr[m_iIndex]->bFixLen)
	{	
		double fInit;
		
 		fInit = (char)m_pTblSts->DATA_SET[3];
		
		if(fInit >=0 )
			m_TblAttr[m_iIndex]->R_fInit = fInit + 0.001;
		else
			m_TblAttr[m_iIndex]->R_fInit = fInit - 0.001;
			
		m_TblAttr[m_iIndex]->nDataLen = m_pTblSts->DATA_SET[4];
		
		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_NMR_ATTR1, m_TblAttr[m_iIndex]->R_fInit); 
		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_NMR_ATTR2, m_TblAttr[m_iIndex]->nDataLen);

		DisplayCtrlsRetry(iTarget);
	}	
	
	if( iID2 == 0)
	{
		for(i=0; i<m_TblAttr[m_iIndex]->nDataLen; i++)
		{
			aVal[0] = m_pTblSts->DATA_SET[i*2+5];
			aVal[1] = m_pTblSts->DATA_SET[i*2+6];


			iVal = MAKEWORD(aVal[0], aVal[1]);	

			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], (double)iVal);
		}
	}
	else
	{
		for(i=0; i<m_TblAttr[m_iIndex]->nDataLen; i++)
		{
			SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], (double)((INT8)m_pTblSts->DATA_SET[i+5])/2.);
		}	
	}
	
	GraphDataView(iTarget); 

}


void TableCntVoltDisplay(int iTarget)
{
	double fVolt;
	int aVal[2], iVal;
	int	iID1, iID2, iID3, iID4;
	
	// Set table index
	/////////////////////////////////////////////////////////////////////////////////////
	
	GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_SYSTEM1, &iID1);
	GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_SYSTEM2, &iID2); 
	GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_ID, &iID3);
	GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_ID3, &iID4);
	
	
	
	
	
	if (iID1 == 0)	//LNA	
	{
		if( iID3 == 1)	iVal = MAKEWORD(m_pLNASts->AM_RSSI[0], m_pLNASts->AM_RSSI[1]);
		if( iID3 == 2)	iVal = MAKEWORD(m_pLNASts->FM_RSSI[0], m_pLNASts->FM_RSSI[1]);
		if( iID3 == 3)	iVal = MAKEWORD(m_pLNASts->DMB_RSSI[0], m_pLNASts->DMB_RSSI[1]);
		if( iID3 == 0)	iVal = MAKEWORD(m_pLNASts->TRS_RSSI[0], m_pLNASts->TRS_RSSI[1]);
		
		
		SetCtrlVal(m_pCOMM[0].iPanelHandle_Table, TABPNL_CURRAD, iVal);
		fVolt = 0; //fVolt = (double)iVal * 5. / (double)m_iAdMaxVal;
		SetCtrlVal(m_pCOMM[0].iPanelHandle_Table, TABPNL_CURRVO, fVolt);
		
	}
	else  if (iID1 == 1)	//HPA
	{
		if( iID3 == 0)	iVal = MAKEWORD(m_pHPASts->AM1_RSSI[0], m_pHPASts->AM1_RSSI[1]);
		if( iID3 == 1)	iVal = MAKEWORD(m_pHPASts->AM2_RSSI[0], m_pHPASts->AM2_RSSI[1]);
		if( iID3 == 2)	iVal = MAKEWORD(m_pHPASts->FM1_RSSI[0], m_pHPASts->FM1_RSSI[1]);
		if( iID3 == 3)	iVal = MAKEWORD(m_pHPASts->FM2_RSSI[0], m_pHPASts->FM2_RSSI[1]);
		if( iID3 == 4)	iVal = MAKEWORD(m_pHPASts->DMB_RSSI[0], m_pHPASts->DMB_RSSI[1]);
		if( iID3 == 5)	iVal = MAKEWORD(m_pHPASts->TRS_DL_RSSI[0], m_pHPASts->TRS_DL_RSSI[1]);
		if( iID3 == 6)	iVal = MAKEWORD(m_pHPASts->TRS_UL_RSSI[0], m_pHPASts->TRS_UL_RSSI[1]);
		
		
		SetCtrlVal(m_pCOMM[0].iPanelHandle_Table, TABPNL_CURRAD, iVal);
		fVolt = 0; //fVolt = (double)iVal * 5. / (double)m_iAdMaxVal;
		SetCtrlVal(m_pCOMM[0].iPanelHandle_Table, TABPNL_CURRVO, fVolt);	
		
	}
	

	
	/*

	if (m_TblAttr[m_iIndex]->iIndex == 0)		
	{

		iVal = MAKEWORD(m_pDebugSts->SSPA_ADC[0][0], m_pDebugSts->SSPA_ADC[0][1]);

		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRAD, iVal);
		fVolt = (double)iVal * 3.3 / (double)m_iAdMaxVal;
		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRVO, fVolt);
	}
	if (m_TblAttr[m_iIndex]->iIndex == 1)		
	{
		iVal = MAKEWORD(m_pDebugSts->SSPA_ADC[1][0], m_pDebugSts->SSPA_ADC[1][1]);

		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRAD, iVal);
		fVolt = (double)iVal * 3.3 / (double)m_iAdMaxVal; 
		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRVO, fVolt);
	}
	
	if (m_TblAttr[m_iIndex]->iIndex == 2)		
	{
		iVal = MAKEWORD(m_pDebugSts->SSPA_ADC[2][0], m_pDebugSts->SSPA_ADC[2][1]);

		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRAD, iVal);
		fVolt = (double)iVal * 3.3 / (double)m_iAdMaxVal; 
		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRVO, fVolt);
	}
	
	if (m_TblAttr[m_iIndex]->iIndex == 3)		
	{
		iVal = MAKEWORD(m_pDebugSts->SSPA_ADC[3][0], m_pDebugSts->SSPA_ADC[3][1]);

		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRAD, iVal);
		fVolt = (double)iVal * 3.3 / (double)m_iAdMaxVal; 
		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRVO, fVolt);
	}
	if (m_TblAttr[m_iIndex]->iIndex == 4)		
	{
		iVal = MAKEWORD(m_pDebugSts->UCM_ADC[0], m_pDebugSts->UCM_ADC[1]);

		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRAD, iVal);
		fVolt = (double)iVal * 3.3 / (double)m_iAdMaxVal;
		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRVO, fVolt);
	}
	if (m_TblAttr[m_iIndex]->iIndex == 5)		
	{
		iVal = MAKEWORD(m_pDebugSts->DCM_ADC[0], m_pDebugSts->DCM_ADC[1]); 

		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRAD, iVal);
		fVolt = (double)iVal * 3.3 / (double)m_iAdMaxVal; 
		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRVO, fVolt);
	}
	
	if (m_TblAttr[m_iIndex]->iIndex == 6)		
	{
		iVal = MAKEWORD(m_pDebugSts->FWD_ADC[0], m_pDebugSts->FWD_ADC[1]);  

		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRAD, iVal);
		fVolt = (double)iVal * 3.3 / (double)m_iAdMaxVal; 
		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRVO, fVolt);
	}
	
	if (m_TblAttr[m_iIndex]->iIndex == 7)		
	{
		iVal = MAKEWORD(m_pDebugSts->RVS_ADC[0], m_pDebugSts->RVS_ADC[1]);  

		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRAD, iVal);
		fVolt = (double)iVal * 3.3 / (double)m_iAdMaxVal; 
		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRVO, fVolt);
	}
	
	if (m_TblAttr[m_iIndex]->iIndex == 8)		
	{
		iVal = MAKEWORD(m_pDebugSts->Noise_ADC[0], m_pDebugSts->Noise_ADC[1]);  

		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRAD, iVal);
		fVolt = (double)iVal * 3.3 / (double)m_iAdMaxVal; 
		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRVO, fVolt);
	}
*/	
	
/*	
	memcpy(m_pTblSts, m_pRecvIO->DataBuf, sizeof(TABLE));

	// Display table voltage data case 10 bit or 12 bit
	/////////////////////////////////////////////////////////////////////////////////////
	if(m_TblAttr[m_iIndex]->V_cBitNum == 10 || m_TblAttr[m_iIndex]->V_cBitNum == 12)
	{
		aVal[0] = m_pTblSts->DATA_SET[1];
		aVal[1] = m_pTblSts->DATA_SET[2];
		
		iVal = MAKEWORD(aVal[0], aVal[1]);

		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRAD, iVal);
		fVolt = (double)iVal * 5. / (double)m_iAdMaxVal;
		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRVO, fVolt);
	}

	// Display table voltage data case 8 bit
	/////////////////////////////////////////////////////////////////////////////////////
	else
	{
		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRAD, m_pTblSts->DATA_SET[1]);
		fVolt = (double)m_pTblSts->DATA_SET[1] * 5. / (double)m_iAdMaxVal;
		SetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_CURRVO, fVolt);
	}*/
	
}

void SetTableCtrlColor(int iTarget)
{
	int i;

	for(i=0; i<TBLMAXVAL; i++)
	{
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], ATTR_TEXT_COLOR, VAL_BLACK);
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i], ATTR_TEXT_COLOR, VAL_BLACK);
	}
}

void InitTableRing(int iTarget)
{
	int i, iLen;

	// Set Table Ring
	/////////////////////////////////////////////////////////////////////////////////
	DeleteListItem (m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_ID, 0, -1);   

	for(i=0; i<TBLCOUNT; i++)
	{
		if(m_TblAttr[i]->iGroup == m_iCntGroup)
		{
			InsertListItem(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_ID, -1, m_TblAttr[i]->stTblName, m_TblAttr[i]->iIndex);	
		}
	}	

	OnChangeIDRing(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_ID, EVENT_COMMIT, 0, 0, 0);  
}


int CVICALLBACK OnCmdAttrChange (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int i;
	double fValue;
	BYTE cValue;
	int iTarget = -1;
	
	if (event != EVENT_COMMIT)
		return 0;

	
	for (i=0; i<DEF_COMM_CNT;i++)
	{
		if (m_pCOMM[i].iPanelHandle_Table == panel)
		{	
			iTarget = i;
			break;
		}
	}
	
	switch(control)
	{
	case TABPNL_NMR_ATTR1:
		{
	
			GetCtrlVal(panel, TABPNL_NMR_ATTR1, &fValue);
			m_TblAttr[m_iIndex]->R_fInit = fValue;
			GetCtrlVal(panel, TABPNL_NMR_ATTR2, &cValue);
			m_TblAttr[m_iIndex]->nDataLen = cValue;
			break;
		}

	case TABPNL_NMR_ATTR2:
		{

			GetCtrlVal(panel, TABPNL_NMR_ATTR1, &fValue);
			m_TblAttr[m_iIndex]->R_fInit = fValue;
			GetCtrlVal(panel, TABPNL_NMR_ATTR2, &cValue);
			m_TblAttr[m_iIndex]->nDataLen = cValue;
			
			GraphDataView(iTarget);
			break;
		}
	}
	
	DisplayCtrlsRetry(iTarget);

	return 0;
}

void DisplayCtrlsRetry(int iTarget)
{
	int i;
	
	if(m_TblAttr[m_iIndex]->E_bException) 
	{
		for(i=0; i<m_TblAttr[m_iIndex]->nDataLen; i++)
		{
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], ATTR_VISIBLE, 1);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlCB[i], ATTR_VISIBLE, 0);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i], ATTR_VISIBLE, 1);	
		}
	}
	else if(m_TblAttr[m_iIndex]->E_bCalUse) 
	{
		for(i=0; i<m_TblAttr[m_iIndex]->nDataLen; i++)
		{
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], ATTR_VISIBLE, 1);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlCB[i], ATTR_VISIBLE, 0);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i], ATTR_VISIBLE, 0);	
		}
	}
	else
	{
		if(m_TblAttr[m_iIndex]->nDataLen > TBLMAXVAL)
			m_TblAttr[m_iIndex]->nDataLen = TBLMAXVAL;
		
		for(i=0; i<m_TblAttr[m_iIndex]->nDataLen; i++)
		{
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], ATTR_VISIBLE, 1);
			SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlCB[i], ATTR_VISIBLE, 1);

			if(m_TblAttr[m_iIndex]->V_cUnit == UNIT_VOLT)
					SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i], ATTR_VISIBLE, 1);
			else
				SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i], ATTR_VISIBLE, 0);		
		}
	}
	
	for(; i<140; i++)
	{
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], ATTR_VISIBLE, 0);
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlAD[i], ATTR_VISIBLE, 0);
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlCB[i], ATTR_VISIBLE, 0);
	}

	for(i=0; i<TBLMAXVAL; i++)
	{
		double fval;
		char stval[16];

		if(m_TblAttr[m_iIndex]->R_cOrder == ORDER_DESC)
			fval = m_TblAttr[m_iIndex]->R_fInit - (double)i * m_TblAttr[m_iIndex]->R_fStep;
		else
			fval = m_TblAttr[m_iIndex]->R_fInit + (double)i * m_TblAttr[m_iIndex]->R_fStep;

		if(m_TblAttr[m_iIndex]->R_cDecPlace == 0)
			Fmt(stval, "%s<%f[p0]", fval);
		else if(m_TblAttr[m_iIndex]->R_cDecPlace == 1)
			Fmt(stval, "%s<%f[p1]", fval);
		else if(m_TblAttr[m_iIndex]->R_cDecPlace == 2)
			Fmt(stval, "%s<%f[p2]", fval);

		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], ATTR_LABEL_JUSTIFY, VAL_RIGHT_JUSTIFIED);
		SetCtrlAttribute(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], ATTR_LABEL_TEXT, stval);
	}
}



void TableSetResponse (int iTarget)
{
	if(m_hTblBackUp>0)
	{
		int iTotal;
		
		GetNumListItems (m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_RING_SYSTEM2, &iTotal);
	 	if(m_hTblBackCnt==iTotal-1)
		//if(m_hTblBackCnt==28)
		{
			SetCtrlAttribute (m_pCOMM[iTarget].iPanelHandle_Table,	TABPNL_RING_SYSTEM2,	ATTR_VISIBLE,	TRUE);
			SetCtrlAttribute (m_pCOMM[iTarget].iPanelHandle_Table,	TABPNL_RING_ID,			ATTR_VISIBLE,	TRUE);
//			SetCtrlAttribute (m_pCOMM[iTarget].iPanelHandle_Table,	TABPNL_RING_ID2,		ATTR_VISIBLE,	FALSE);

			ClosePanel(&m_pCOMM[iTarget].iPanelHandle_TableBK);
			CloseFile (m_hTblBackUp);
			m_hTblBackUp = 0;
			MessagePopup ("Data Backup", "Table Backup Data 설정을 완료하였습니다.");
			return;
		}
		else
		{

		}
	}
	else
		MessagePopup ("Success!", " Table Data가 설정되었습니다."); 
}



void GraphDataView(int iTarget)
{
	double fVolt;
	double gVolt[TBLMAXVAL];				  
	UINT8  ucCnt;
	int i;
	
	//emset(gVolt, '\0', sizeof(gVolt));
	GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_NMR_ATTR2, &ucCnt);
	
	if(ucCnt == 0)
		return;

	for (i = 0; i < ucCnt; i++)
	{
		GetCtrlVal(m_pCOMM[iTarget].iPanelHandle_Table, ControlVO[i], &fVolt);
		gVolt[i] = (double)fVolt; 
	}

	DeleteGraphPlot (m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_GRAPH, -1, 1);
	PlotY (m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_GRAPH, gVolt, ucCnt, VAL_DOUBLE, VAL_THIN_LINE, VAL_EMPTY_SQUARE, VAL_SOLID, 1, VAL_RED);
	RefreshGraph (m_pCOMM[iTarget].iPanelHandle_Table, TABPNL_GRAPH); 
}

